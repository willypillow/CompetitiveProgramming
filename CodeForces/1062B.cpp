#include <bits/stdc++.h>
using namespace std;

bool isPow2(int x) {
	return (1 << __lg(x)) == x;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n; cin >> n;
	if (n == 1) {
		cout << "1 0";
		return 0;
	}
	int x = n, p = 1;
	vector<pair<int, int> > v;
	for (int i = 2; i <= n; i++) {
		if (n % i == 0) {
			p *= i;
			int cnt = 0;
			while (n % i == 0) {
				n /= i;
				cnt++;
			}
			v.push_back({ cnt, i });
		}
	}
	sort(v.begin(), v.end());
	if (v.begin()->first == v.rbegin()->first && isPow2(v.begin()->first)) {
		cout << p << ' ' << __lg(v.begin()->first);
	} else {
		cout << p << ' ' << 1 + (int)ceil(log2(v.rbegin()->first));
	}
}
