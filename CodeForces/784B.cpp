#include <bits/stdc++.h>
using namespace std;

int holes[256] = { 0 };

int main() {
	holes['0'] = holes['4'] = holes['6'] = holes['9'] = holes['A'] = holes['D'] = 1;
	holes['8'] = holes['B'] = 2;
	char str[100];
	long long n; cin >> n;
	sprintf(str, "%llx", n);
	int ans = 0;
	for (int i = strlen(str) - 1; i >= 0; i--) ans += holes[toupper(str[i])];
	cout << ans << '\n';
}
