#include <bits/stdc++.h>
using namespace std;

int a[200000];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n, k; cin >> n >> k;
	for (int i = 0; i < n; i++) cin >> a[i];
	vector<tuple<int, int, int> > v;
	v.reserve(n * 10);
	for (int i = 0; i < n; i++) {
		int64_t acc = 10;
		for (int j = 1; j <= 10; j++, acc = acc * 10 % k) v.push_back({a[i] * acc % k, j, i});
	}
	sort(v.begin(), v.end());
	int64_t ans = 0;
	for (int i = 0; i < n; i++) {
		int candid = k - a[i] % k, lg = (int)floor(log10(a[i])) + 1;
		if (candid == k) candid = 0;
		auto itL = lower_bound(v.begin(), v.end(), make_tuple(candid, lg, -1)),
			itR = upper_bound(v.begin(), v.end(), make_tuple(candid, lg + 1, -1));
		ans += itR - itL;
		if (binary_search(v.begin(), v.end(), make_tuple(candid, lg, i))) ans--;
	}
	cout << ans;
}
