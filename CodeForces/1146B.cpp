#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(nullptr), ios_base::sync_with_stdio(false);
	string t; cin >> t;
	int cnt = 0, n = (int)t.size();
	for (int i = 0; i < n; i++) {
		cnt += t[i] == 'a';
		if ((i + 1) + (i + 1 - cnt) == n) {
			string tt;
			for (int j = 0; j < i + 1; j++) {
				if (t[j] != 'a') tt.push_back(t[j]);
			}
			if (tt == t.substr(i + 1)) {
				cout << t.substr(0, i + 1);
				return 0;
			}
		}
	}
	cout << ":(";
}
