#include <iostream>
#include <cmath>
using namespace std;

const double EPS = 5.0E-7;

long long n, m;

bool solve(long long x) {
	long long xx;
	if (x & 1) xx = (x + 1) / 2 * x;
	else xx = x / 2 * (x + 1);
	return xx >= n - m;
}

int main() {
	cin >> n >> m;
	if (n <= m) {
		cout << n << '\n';
	} else {
		long long l = 1, r = 2E9;
		while (l < r) {
			long long mid = (l + r) >> 1;
			if (solve(mid)) r = mid;
			else l = mid + 1;
		}
		cout << l + m << '\n';
	}
}

