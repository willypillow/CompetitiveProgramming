#include <bits/stdc++.h>
using namespace std;

int a[10000];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 1; i <= n; i++) cin >> a[i];
	int ans = 0;
	a[n + 1] = 1001;
	for (int i = 0; i <= n + 1; i++) {
		for (int j = i + 1; j <= n + 1; j++) {
			if (j - i == a[j] - a[i]) ans = max(ans, j - i - 1);
		}
	}
	cout << ans;
}
