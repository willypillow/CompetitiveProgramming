#include <bits/stdc++.h>
using namespace std;

int ans[200001], n;

struct Node {
	static Node pool[40000000], *ptr;
	Node *lc = NULL, *rc = NULL;
	void insert(int p, int l = 1, int r = n) {
		if (l == r) return;
		int m = (l + r) >> 1;
		if (p <= m) {
			if (!lc) lc = new(ptr++) Node();
			lc->insert(p, l, m);
		} else {
			if (!rc) rc = new(ptr++) Node();
			rc->insert(p, m + 1, r);
		}
	}
} *tree[101], Node::pool[40000000], *Node::ptr = Node::pool;

Node *merge(Node *a, Node *b, int l = 1, int r = n) {
	if (!a || !b) return a ? a : b;
	if ((!!a->lc + !!a->rc) > (!!b->lc + !!b->rc)) swap(a, b);
	if (!a->lc && !a->rc) return b;
	int m = (l + r) >> 1;
	a->lc = merge(a->lc, b->lc, l, m);
	a->rc = merge(a->rc, b->rc, m + 1, r);
	return a;
}

void split(int p, Node *nod, Node *&a, Node *&b, int l = 1, int r = n) {
	if (!nod || l == r) {
		if (nod) a = new(Node::ptr++) Node();
		return;
	}
	a = new(Node::ptr++) Node(), b = new(Node::ptr++) Node();
	int m = (l + r) >> 1;
	if (p <= m) {
		b->rc = nod->rc;
		split(p, nod->lc, a->lc, b->lc, l, m);
	} else {
		a->lc = nod->lc;
		split(p, nod->rc, a->rc, b->rc, m + 1, r);
	}
}

void dfsAns(Node *nod, int color, int l = 1, int r = n) {
	if (!nod) return;
	if (l == r) {
		ans[l] = color;
	} else {
		int m = (l + r) >> 1;
		if (nod->lc) dfsAns(nod->lc, color, l, m);
		if (nod->rc) dfsAns(nod->rc, color, m + 1, r);
	}
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	cin >> n;
	for (int i = 1; i <= n; i++) {
		int x; cin >> x;
		if (!tree[x]) tree[x] = new(Node::ptr++) Node();
		tree[x]->insert(i);
	}
	int q; cin >> q;
	while (q--) {
		int l, r, x, y; cin >> l >> r >> x >> y;
		if (x == y) continue;
		Node *tmpA = NULL, *tmpB = NULL;
		if (l == 1) swap(tree[x], tmpA);
		else split(l - 1, tree[x], tree[x], tmpA);
		split(r, tmpA, tmpA, tmpB);
		tree[x] = merge(tree[x], tmpB), tree[y] = merge(tmpA, tree[y]);
	}
	for (int i = 1; i <= 100; i++) dfsAns(tree[i], i);
	for (int i = 1; i <= n; i++) cout << ans[i] << ' ';
}
