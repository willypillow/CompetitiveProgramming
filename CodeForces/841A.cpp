#include <bits/stdc++.h>
using namespace std;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, k;
	string str;
	cin >> n >> k >> str;
	map<char, int> mp;
	for (char c: str) mp[c]++;
	for (auto p: mp) {
		if (p.second > k) {
			cout << "NO";
			return 0;
		}
	}
	cout << "YES";
}
