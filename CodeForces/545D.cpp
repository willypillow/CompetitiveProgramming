#include <bits/stdc++.h>
using namespace std;

int a[100010];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	int64_t wait = 0, ans = 0;
	for (int i = 1; i <= n; i++) cin >> a[i];
	sort(a + 1, a + 1 + n);
	for (int i = 1; i <= n; i++) {
		if (wait <= a[i]) {
			ans++;
			wait += a[i];
		}
	}
	cout << ans;
}
