#include <bits/stdc++.h>
using namespace std;

const int MAXN = 3E5 + 5, INF = 2E9;

int mx[MAXN * 4], rmq[MAXN * 4], res[MAXN];
struct Query { int cmd, l, r, x; } q[MAXN];
map<int, int> cnt;

void modify1(int id, int l, int r, int mL, int mR, int v) {
	if (mR < l || r < mL) return;
	if (mL <= l && r <= mR) {
		mx[id] = min(mx[id], v);
	} else {
		int m = (l + r) >> 1;
		modify1(id << 1, l, m, mL, mR, v);
		modify1(id << 1 | 1, m + 1, r, mL, mR, v);
	}
}

int query1(int id, int l, int r, int pos) {
	if (l == r) return mx[id];
	int m = (l + r) >> 1;
	if (pos <= m) return min(mx[id], query1(id << 1, l, m, pos));
	else return min(mx[id], query1(id << 1 | 1, m + 1, r, pos));
}

void initRmq(int id, int l, int r) {
	if (l == r) {
		rmq[id] = res[l];
	} else {
		int m = (l + r) >> 1;
		initRmq(id << 1, l, m); initRmq(id << 1 | 1, m + 1, r);
		rmq[id] = max(rmq[id << 1], rmq[id << 1 | 1]);
	}
}

void modifyRmq(int id, int l, int r, int pos, int v) {
	if (l == r) {
		rmq[id] = v;
	} else {
		int m = (l + r) >> 1;
		if (pos <= m) modifyRmq(id << 1, l, m, pos, v);
		else modifyRmq(id << 1 | 1, m + 1, r, pos, v);
		rmq[id] = max(rmq[id << 1], rmq[id << 1 | 1]);
	}
}

int queryRmq(int id, int l, int r, int qL, int qR) {
	if (qL <= l && r <= qR) return rmq[id];
	int m = (l + r) >> 1, res = 0;
	if (qL <= m) res = max(res, queryRmq(id << 1, l, m, qL, min(qR, m)));
	if (m < qR) res = max(res, queryRmq(id << 1 | 1, m + 1, r, max(qL, m), qR));
	return res;
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	int n, m; cin >> n >> m;
	for (int i = 1; i <= (n << 2); i++) mx[i] = INF;
	for (int i = 1; i <= n; i++) res[i] = INF + 1;
	for (int i = 0; i < m; i++) {
		cin >> q[i].cmd;
		if (q[i].cmd == 1) {
			cin >> q[i].l >> q[i].r >> q[i].x;
			modify1(1, 1, n, q[i].l, q[i].r, q[i].x);
		} else {
			cin >> q[i].l >> q[i].x;
			if (res[q[i].l] == INF + 1) {
				res[q[i].l] = query1(1, 1, n, q[i].l);
			}
		}
	}
	for (int i = 1; i <= n; i++) {
		if (res[i] == INF + 1) res[i] = query1(1, 1, n, i);
	}
	bool fail = false;
	initRmq(1, 1, n);
	//for (int i = 1; i <= n; i++) modifyRmq(1, 1, n, i, res[i]);
	for (int i = 0; i < m; i++) {
		if (q[i].cmd == 1) {
			if (queryRmq(1, 1, n, q[i].l, q[i].r) != q[i].x) {
				fail = true;
				break;
			}
		} else {
			modifyRmq(1, 1, n, q[i].l, q[i].x);
		}
	}
	if (fail) {
		cout << "NO\n";
	} else {
		cout << "YES\n";
		for (int i = 1; i <= n; i++) cnt[res[i]]++;
		if (cnt[INF] >= 2) {
			for (int i = 1; i <= n; i++) {
				if (res[i] >= INF) res[i] = (--cnt[INF] ? 1E9 : ((1 << 29) - 1));
				cout << res[i] << ' ';
			}
		} else {
			int orRes = 0;
			for (int i = 1; i <= n; i++) {
				if (res[i] == 0 || res[i] == INF) continue;
				if (--cnt[res[i]]) {
					int ln = __builtin_clz(res[i]) ^ 31;
					res[i] = (1 << ln) - 1;
				}
				orRes |= res[i];
			}
			int freeNum = 0;
			for (int i = 29; i >= 0; i--) {
				if ((orRes & (1 << i)) || (freeNum | (1 << i)) > 1E9) continue;
				freeNum |= 1 << i;
			}
			for (int i = 1; i <= n; i++) {
				cout << ((res[i] == INF) ? freeNum : res[i]) << ' ';
			}
		}
	}
}
