#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(nullptr), ios_base::sync_with_stdio(false);
	string s; cin >> s;
	sort(s.begin(), s.end());
	int cnt = 0;
	for (int i = 0; i < s.size(); i++) cnt += s[i] == 'a';
	cout << min(cnt * 2 - 1, (int)s.size());
}
