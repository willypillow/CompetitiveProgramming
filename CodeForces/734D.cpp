#include <bits/stdc++.h>
using namespace std;

char t[500010];
int x[500010], y[500010];
pair<int64_t, int> sight[8];

int64_t dist(int64_t x0, int64_t y0, int64_t x1, int64_t y1) {
	return abs(x1 - x0) + abs(y1 - y0);
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, x0, y0; cin >> n >> x0 >> y0;
	memset(sight, 0x3f, sizeof(sight));
	for (int i = 0; i < n; i++) {
		cin >> t[i] >> x[i] >> y[i];
		int64_t d = dist(x0, y0, x[i], y[i]);
		if (x[i] - y[i] == x0 - y0) {
			if (x[i] < x0) sight[0] = min(sight[0], { d, i });
			else sight[1] = min(sight[1], { d, i });
		}
		if (x[i] + y[i] == x0 + y0) {
			if (x[i] < x0) sight[2] = min(sight[2], { d, i });
			else sight[3] = min(sight[3], { d, i });
		}
		if (x[i] == x0) {
			if (y[i] < y0) sight[4] = min(sight[4], { d, i });
			else sight[5] = min(sight[5], { d, i });
		}
		if (y[i] == y0) {
			if (x[i] < x0) sight[6] = min(sight[6], { d, i });
			else sight[7] = min(sight[7], { d, i });
		}
	}
	bool ans = false;
	for (int i = 0; i < 4; i++) {
		if (sight[i].second >= n) continue;
		ans |= (t[sight[i].second] == 'B' || t[sight[i].second] == 'Q');
	}
	for (int i = 4; i < 8; i++) {
		if (sight[i].second >= n) continue;
		ans |= (t[sight[i].second] == 'R' || t[sight[i].second] == 'Q');
	}
	cout << (ans ? "YES" : "NO");
}
