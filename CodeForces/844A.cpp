#include <bits/stdc++.h>
using namespace std;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	string s;
	int k;
	cin >> s >> k;
	if (s.size() < k) {
		cout << "impossible";
	} else {
		set<char> st;
		for (char c: s) st.insert(c);
		cout << max(0, k - (int)st.size());
	}
}
