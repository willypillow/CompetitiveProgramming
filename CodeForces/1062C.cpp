#include <bits/stdc++.h>
using namespace std;

const int MOD = 1E9 + 7;
int a[200000], pw2[200000];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n, q; cin >> n >> q;
	for (int i = 1; i <= n; i++) {
		char x; cin >> x;
		a[i] = x == '1';
	}
	for (int i = 1; i <= n; i++) a[i] += a[i - 1];
	pw2[0] = 1;
	for (int i = 1; i <= n; i++) pw2[i] = (pw2[i - 1] << 1) % MOD;
	while (q--) {
		int l, r; cin >> l >> r;
		int x = r - l + 1, y = a[r] - a[l - 1];
		cout << (pw2[y] - 1 + MOD + 1LL * (pw2[y] - 1) * (pw2[x - y] - 1)) % MOD << '\n';
	}
}
