#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	string s; cin >> s;
	int a = 0, d = 0;
	for (int i = 0; i < n; i++) {
		if (s[i] == 'A') a++;
		else d++;
	}
	cout << ((a > d) ? "Anton" : (a == d) ? "Friendship" : "Danik");
}
