#include <bits/stdc++.h>
using namespace std;

const int FULL = (1 << 3) - 1;
bitset<2500> vis[51][51][51], dp[51][51][51];
char str[60][60];
int n, m, s[3];

void updateS(char o, char c) {
	if (isalpha(o)) s[0]--;
	else if (isdigit(o)) s[1]--;
	else if (o != '\0') s[2]--;
	if (isalpha(c)) s[0]++;
	else if (isdigit(c)) s[1]++;
	else s[2]++;
}

bool dfs(int cur, int left) {
	if (s[0] && s[1] && s[2]) return true;
	if (cur >= n) return false;
	auto &v = vis[cur][s[0]][s[1]], &ans = dp[cur][s[0]][s[1]];
	if (v[left]) return ans[left];
	v[left] = true;
	for (int i = 0, top = min(m, left + 1); i < top; i++) {
		updateS(str[cur][0], str[cur][i]);
		if (dfs(cur + 1, left - i)) ans[left] = true;
		updateS(str[cur][i], str[cur][0]);
		if (ans[left]) return ans[left];
		if (!i) continue;
		updateS(str[cur][0], str[cur][m - i]);
		if (dfs(cur + 1, left - i)) ans[left] = true;
		updateS(str[cur][m - i], str[cur][0]);
		if (ans[left]) return ans[left];
	}
	return ans[left] = false;
}

int main() {
	cin >> n >> m;
	for (int i = 0; i < n; i++) {
		cin >> str[i];
		updateS('\0', str[i][0]);
	}
	int l = 0, r = n * m;
	while (l != r) {
		int m = (l + r) >> 1;
		if (dfs(0, m)) r = m;
		else l = m + 1;
	}
	cout << l << '\n';
}
