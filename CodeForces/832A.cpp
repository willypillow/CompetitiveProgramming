#include <bits/stdc++.h>
using namespace std;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	uint64_t n, k; cin >> n >> k;
	uint64_t t = n / k;
	cout << ((t & 1) ? "YES" : "NO");
}
