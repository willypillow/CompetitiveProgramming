#include <bits/stdc++.h>
using namespace std;

bool posMat[100010];
int arr[100010];

struct Trie {
	Trie *ch[2];
	vector<int> vec;
	Trie(): ch{ nullptr, nullptr }, vec() { }
	void insert(int pos, int val, int depth = 16) {
		if (depth < 0) {
			vec.push_back(val);
		} else {
			int bit = !!(pos & (1 << depth));
			if (!ch[bit]) ch[bit] = new Trie();
			ch[bit]->insert(pos, val, depth - 1);
		}
	}
	void collect(bool *matrix, int depth = 16) {
		if (depth < 0) {
			for (auto val: vec) matrix[val] = true;
		} else {
			if (ch[0]) ch[0]->collect(matrix, depth - 1);
			if (ch[1]) ch[1]->collect(matrix, depth - 1);
		}
	}
} *positive, *negative;

void split(int pos, Trie *n, Trie *&a, Trie *&b, bool eqLeft, int depth = 16) {
	if (!n) {
		a = b = nullptr;
		return;
	}
	if (depth < 0) {
		if (eqLeft) a = n;
		else b = n;
		return;
	}
	int bit = !!(pos & (1 << depth));
	if (bit) {
		a = new Trie(), b = new Trie();
		a->ch[0] = n->ch[0];
		split(pos, n->ch[1], a->ch[1], b->ch[1], eqLeft, depth - 1);
	} else {
		a = new Trie(), b = new Trie();
		b->ch[1] = n->ch[1];
		split(pos, n->ch[0], a->ch[0], b->ch[0], eqLeft, depth - 1);
	}
}

Trie *join(Trie *a, Trie *b, int depth = 16) {
	if (!a || !b) return a ? a : b;
	Trie *n = new Trie();
	if (depth < 0) {
		for (auto v: a->vec) n->vec.push_back(v);
		for (auto v: b->vec) n->vec.push_back(v);
	} else {
		n->ch[0] = join(a->ch[0], b->ch[0], depth - 1);
		n->ch[1] = join(a->ch[1], b->ch[1], depth - 1);
	}
	return n;
}

int main() {
	cin.tie(nullptr), ios_base::sync_with_stdio(false);
	positive = new Trie(), negative = new Trie();
	int n, q; cin >> n >> q;
	for (int i = 0; i < n; i++) {
		cin >> arr[i];
		if (arr[i] > 0) positive->insert(abs(arr[i]), i);
		else if (arr[i] < 0) negative->insert(abs(arr[i]), i);
	}
	while (q--) {
		char s; cin >> s;
		int x; cin >> x;
		Trie *a, *b;
		if (x >= 0) {
			if (s == '<') {
				split(x, positive, a, b, false);
				positive = join(negative, b);
				negative = a;
			} else {
				split(x, positive, a, b, true);
				positive = a;
				negative = join(negative, b);
			}
		} else {
			if (s == '<') {
				split(-x, negative, a, b, true);
				negative = a;
				positive = join(positive, b);
			} else {
				split(-x, negative, a, b, false);
				negative = join(b, positive);
				positive = a;
			}
		}
	}
	positive->collect(posMat);
	for (int i = 0; i < n; i++) {
		cout << (posMat[i] ? 1 : -1) * abs(arr[i]) << ' ';
	}
}
