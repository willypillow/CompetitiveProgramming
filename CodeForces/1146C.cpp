#include <bits/stdc++.h>
using namespace std;

int group[1 << 10];

int getRes() {
	int x; cin >> x;
	if (x == -1) assert(0);
	return x;
}

int main() {
	cin.tie(nullptr), ios_base::sync_with_stdio(false);
	int t; cin >> t;
	while (t--) {
		int n; cin >> n;
		int answer = 0;
		for (int d = 1; d < n; d <<= 1) {
			int cnt[2] = { 0 };
			for (int i = 0; i < n; i++) {
				group[i] = i & d;
				cnt[!!(i & d)]++;
			}
			cout << cnt[0] << ' ' << cnt[1] << ' ';
			for (int i = 0; i < n; i++) {
				if (!group[i]) cout << i + 1 << ' ';
			}
			for (int i = 0; i < n; i++) {
				if (group[i]) cout << i + 1 << ' ';
			}
			cout << endl;
			answer = max(answer, getRes());
		}
		cout << "-1 " << answer << endl;
	}
}
