#include <bits/stdc++.h>
using namespace std;

const int MAXN = 2E5 + 10;

int st[MAXN * 4 * 4], lazy[MAXN * 4 * 4], lIdx[MAXN], rIdx[MAXN];
vector<int> pos;

void push(int id) {
	st[id << 1] += lazy[id];
	lazy[id << 1] += lazy[id];
	st[id << 1 | 1] += lazy[id];
	lazy[id << 1 | 1] += lazy[id];
	lazy[id] = 0;
}

void pull(int id) {
	st[id] = min(st[id << 1], st[id << 1 | 1]);
}

void modify(int id, int l, int r, int qL, int qR, int v) {
	if (qL <= l && r <= qR) {
		lazy[id] += v;
		st[id] += v;
	} else {
		int m = (l + r) >> 1;
		push(id);
		if (qL <= m) modify(id << 1, l, m, qL, qR, v);
		if (m < qR) modify(id << 1 | 1, m + 1, r, qL, qR, v);
		pull(id);
	}
}

int query(int id, int l, int r, int qL, int qR) {
	if (qL <= l && r <= qR) {
		return st[id];
	} else {
		int m = (l + r) >> 1, t = 1E9;
		push(id);
		if (qL <= m) t = min(t, query(id << 1, l, m, qL, qR));
		if (m < qR) t = min(t, query(id << 1 | 1, m + 1, r, qL, qR));
		return t;
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> lIdx[i] >> rIdx[i];
		rIdx[i] += 1;
		pos.push_back(lIdx[i]); pos.push_back(rIdx[i]);
	}
	sort(pos.begin(), pos.end());
	pos.resize(unique(pos.begin(), pos.end()) - pos.begin());
	int top = (pos.size() - 1) * 2 + 1;
	for (int i = 0; i < n; i++)  {
		lIdx[i] = lower_bound(pos.begin(), pos.end(), lIdx[i]) - pos.begin();
		rIdx[i] = lower_bound(pos.begin(), pos.end(), rIdx[i]) - pos.begin();
		lIdx[i] = lIdx[i] * 2 + 1; rIdx[i] = rIdx[i] * 2 + 1;
		modify(1, 1, top, lIdx[i], rIdx[i], 1);
	}
	for (int i = 0; i < n; i++) {
		if (query(1, 1, top, lIdx[i], rIdx[i]) != 1) {
			cout << i + 1;
			return 0;
		}
	}
	cout << "-1";
	return 0;
}
