#include <bits/stdc++.h>
using namespace std;

const double EPS = 1E-8;

int y[1010], n;

bool solve(double m, double b1, double b2) {
	int c1 = 0, c2 = 0;
	if (fabs(b1 - b2) < EPS) return false;
	for (int i = 1; i <= n; i++) {
		if (fabs(i * m + b1 - y[i]) < EPS) c1++;
		else if (fabs(i * m + b2 - y[i]) < EPS) c2++;
		else return false;
	}
	if (!c1 || !c2) return false;
	else return true;
}

bool solve2(double m, double b1) {
	double b2 = nan("");
	int c1 = 0, c2 = 0;
	for (int i = 1; i <= n; i++) {
		if (fabs(i * m + b1 - y[i]) < EPS) c1++;
		else if (isnan(b2)) b2 = y[i] - i * m, c2++;
		else if (fabs(i * m + b2 - y[i]) < EPS) c2++;
		else return false;
	}
	if (!c1 || !c2) return false;
	else return true;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	cin >> n;
	for (int i = 1; i <= n; i++) cin >> y[i];
	// 1 != 2, 1 == 3
	double m = (y[3]- y[1]) / 2.0, b1 = y[1] - m, b2 = y[2] - m * 2;
	bool ans = solve(m, b1, b2);
	m = (y[3] - y[2]), b1 = y[1] - m, b2 = y[2] - m * 2;
	ans |= solve(m, b1, b2);
	m = y[2] - y[1], b1 = y[1] - m;
	ans |= solve2(m, b1);
	cout << (ans ? "Yes" : "No");
}
