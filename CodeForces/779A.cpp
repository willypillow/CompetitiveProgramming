#include <bits/stdc++.h>
using namespace std;

multiset<int> a, b;

int main() {
	int n; cin >> n;
	for (int i = 0, x; i < n; i++) { cin >> x; a.insert(x); }
	for (int i = 0, x; i < n; i++) { cin >> x; b.insert(x); }
	int cnt = 0, cnt2 = 0;
	for (int i = 1; i <= 5; i++) {
		while (a.count(i) > b.count(i)) {
			a.erase(a.find(i));
			b.insert(i);
			cnt++;
		}
		while (a.count(i) < b.count(i)) {
			b.erase(b.find(i));
			a.insert(i);
			cnt2++;
		}
		if (a.count(i) != b.count(i)) {
			cout << "-1\n";
			return 0;
		}
	}
	if (cnt != cnt2) {
		cout << "-1\n";
	} else {
		cout << cnt << '\n';
	}
}
