#include <bits/stdc++.h>
using namespace std;

void fail() {
	cout << "NO";
	exit(0);
}

int a[1000000];

int main() {
	cin.tie(0), cin.sync_with_stdio(0);
	int n; cin >> n >> ws;
	for (int i = 0; i < n; i++) cin >> a[i];
	sort(a, a + n);
	int q; cin >> q;
	while (q--) {
		int m; cin >> m;
		cout << upper_bound(a, a + n, m) - a << '\n';
	}
}
