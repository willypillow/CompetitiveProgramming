#include <bits/stdc++.h>
using namespace std;

const int MAXN = 2E5 + 2, MAXK = 6;
vector<int> g[MAXN];
long long cnt[MAXN][MAXK], dist[MAXN][MAXK], n, k, ans = 0;

void update(int x, int y, int mult) {
	for (int i = 0; i < k; i++) {
		cnt[x][(i + 1) % k] += mult * cnt[y][i];
		dist[x][(i + 1) % k] += mult * dist[y][i];
	}
	dist[x][1 % k] += mult * cnt[y][0];
}


void dfs(int x, int p) {
	cnt[x][0] = 1;
	for (int y: g[x]) {
		if (y == p) continue;
		dfs(y, x);
		update(x, y, 1);
	}
}

void dfs2(int x, int p) {
	for (int i = 0; i < k; i++) ans += dist[x][i];
	for (int y: g[x]) {
		if (y == p) continue;
		update(x, y, -1);
		update(y, x, 1);
		dfs2(y, x);
		update(y, x, -1);
		update(x, y, 1);
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	cin >> n >> k;
	for (int i = 1; i < n; i++) {
		int a, b; cin >> a >> b;
		g[a].push_back(b); g[b].push_back(a);
	}
	dfs(1, 1);
	dfs2(1, 1);
	cout << ans / 2 << '\n';
}
