#include <bits/stdc++.h>
using namespace std;

void fail() {
	cout << "NO";
	exit(0);
}

//int a[1000000];

int main() {
	cin.tie(0), cin.sync_with_stdio(0);
	int n, k, l, c, d, p, nl, np; cin >> n >> k >> l >> c >> d >> p >> nl >> np;
	cout << min({k * l / nl, p / np, c * d}) / n;
}
