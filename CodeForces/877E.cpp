#include <bits/stdc++.h>
using namespace std;

const int MAXN = 2E5 + 5;

int stCnt[MAXN * 4], a[MAXN], start[MAXN], endd[MAXN];
bool stLazy[MAXN * 4];
vector<int> g[MAXN], tour;

void dfs(int x, int p) {
	tour.push_back(a[x]);
	start[x] = (int)tour.size() - 1;
	for (int y: g[x]) {
		if (y != p) dfs(y, x);
	}
	endd[x] = (int)tour.size() - 1;
}

void build(int id, int l, int r) {
	if (l == r) {
		stCnt[id] = tour[l];
	} else {
		int m = (l + r) >> 1;
		build(id << 1, l, m); build(id << 1 | 1, m + 1, r);
		stCnt[id] = stCnt[id << 1] + stCnt[id << 1 | 1];
	}
}

void push(int id, int l, int m, int r) {
	stCnt[id << 1] = (m - l + 1) - stCnt[id << 1];
	stCnt[id << 1 | 1] = (r - m) - stCnt[id << 1 | 1];
	stLazy[id << 1] = !stLazy[id << 1];
	stLazy[id << 1 | 1] = !stLazy[id << 1 | 1];
	stLazy[id] = false;
}

int query(int id, int l, int r, int qL, int qR) {
	if (qL <= l && r <= qR) {
		return stCnt[id];
	} else {
		int m = (l + r) >> 1, t = 0;
		if (stLazy[id]) push(id, l, m, r);
		if (qL <= m) t += query(id << 1, l, m, qL, qR);
		if (m < qR) t += query(id << 1 | 1, m + 1, r, qL, qR);
		return t;
	}
}

void modify(int id, int l, int r, int qL, int qR) {
	if (qL <= l && r <= qR) {
		stCnt[id] = (r - l + 1) - stCnt[id];
		stLazy[id] = !stLazy[id];
	} else {
		int m = (l + r) >> 1;
		if (stLazy[id]) push(id, l, m, r);
		if (qL <= m) modify(id << 1, l, m, qL, qR);
		if (m < qR) modify(id << 1 | 1, m + 1, r, qL, qR);
		stCnt[id] = stCnt[id << 1] + stCnt[id << 1 | 1];
	}
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 2; i <= n; i++) {
		int t; cin >> t;
		g[t].push_back(i);
	}
	for (int i = 1; i <= n; i++) cin >> a[i];
	tour.push_back(7122);
	dfs(1, 1);
	build(1, 1, n);
	int q; cin >> q;
	while (q--) {
		char cmd[10]; cin >> cmd;
		int v; cin >> v;
		if (cmd[0] == 'g') cout << query(1, 1, n, start[v], endd[v]) << '\n';
		else modify(1, 1, n, start[v], endd[v]);
	}
}
