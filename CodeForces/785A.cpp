#include <iostream>
using namespace std;

char str[50];

int main() {
	int n; cin >> n;
	int ans = 0;
	while (n--) {
		cin >> str;
		switch (str[0]) {
		case 'I':
			ans += 20; break;
		case 'D':
			ans += 12; break;
		case 'O':
			ans += 8; break;
		case 'C':
			ans += 6; break;
		case 'T':
			ans += 4;
		}
	}
	cout << ans << '\n';
}
