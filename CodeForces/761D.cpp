#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E5 + 10, INF = 0x3f3f3f3f;
int a[MAXN], b[MAXN], c[MAXN], invC[MAXN];

int main() {
	int n, l, r; cin >> n >> l >> r;
	for (int i = 1; i <= n; i++) cin >> a[i];
	for (int i = 1; i <= n; i++) {
		cin >> c[i];
		invC[c[i]] = i;
	}
	int lastC = -INF;
	for (int i = 1; i <= n; i++) {
		lastC = max(lastC, l - a[invC[i]] - 1);
		if (lastC < r - a[invC[i]]) {
			b[invC[i]] = lastC + 1 + a[invC[i]];
			lastC = lastC + 1;
		} else {
			cout << "-1\n";
			return 0;
		}
	}
	for (int i = 1; i <= n; i++) cout << b[i] << ' ';
	cout << '\n';
}
