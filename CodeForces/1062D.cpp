#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n; cin >> n;
	int64_t ans = 0;
	for (int i = 2; i <= n; i++) {
		for (int j = i + i; j <= n; j += i) {
			// i <-> j, i <-> -j, -i <-> j, -i <-> -j
			ans += 4 * (j / i);
		}
	}
	cout << ans;
}
