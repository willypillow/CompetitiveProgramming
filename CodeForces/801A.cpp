#include <bits/stdc++.h>
using namespace std;

char str[110];
bool use[110];

int main() {
	cin >> str;
	int ans = 0, len = strlen(str);
	bool change = false;
	for (int i = 1; i < len; i++) {
		if (str[i - 1] == 'V' && str[i] == 'K') {
			use[i - 1] = use[i] = true;
			ans++;
		}
	}
	for (int i = 1; i < len; i++) {
		if (!use[i - 1] && !use[i]) {
			if (str[i - 1] == str[i]) change = true;
		}
	}
	cout << (change ? ans + 1 : ans) << '\n';
}
