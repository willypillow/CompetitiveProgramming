#include <bits/stdc++.h>
using namespace std;

const int MAXN = 55, S = 0, T = MAXN * 3 + 1, INF = 0x3f3f3f3f;

struct Edge { int from, to, cap, flow, cost; };

int mn[MAXN], mx[MAXN], a[MAXN * 4], dist[MAXN * 4], par[MAXN * 4];
bool inQueue[MAXN * 4];
vector<Edge> edges;
vector<int> g[MAXN * 4];

inline int element(int x) { return x; }
inline int value(int x) { return MAXN + x; }
inline int repeat(int x) { return MAXN + MAXN + x; }

inline void addEdge(int from, int to, int cap, int cost) {
	edges.push_back({ from, to, cap, 0, cost });
	g[from].push_back((int)edges.size() - 1);
	edges.push_back({ to, from, 0, 0, -cost });
	g[to].push_back((int)edges.size() - 1);
}

int minCostMaxFlow(int *cost, int flow = 0) {
	memset(a, 0, sizeof(a));
	memset(inQueue, false, sizeof(inQueue));
	memset(dist, INF, sizeof(dist));
	queue<int> q;
	q.push(S);
	dist[S] = 0;
	a[S] = INF;
	while (!q.empty()) {
		int cur = q.front(); q.pop();
		inQueue[cur] = false;
		for (int eNo: g[cur]) {
			auto &e = edges[eNo];
			if (e.cap > e.flow && dist[e.to] > dist[cur] + e.cost) {
				dist[e.to] = dist[cur] + e.cost;
				par[e.to] = eNo;
				a[e.to] = min(a[cur], e.cap - e.flow);
				if (!inQueue[e.to]) {
					q.push(e.to);
					inQueue[e.to] = true;
				}
			}
		}
	}
	if (!a[T]) return flow;
	for (int cur = T; cur != S; cur = edges[par[cur]].from) {
		edges[par[cur]].flow += a[T];
		edges[par[cur] ^ 1].flow -= a[T];
	}
	*cost += dist[T] * a[T];
	return minCostMaxFlow(cost, flow + a[T]);
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, q; cin >> n >> q;
	for (int i = 1; i <= n; i++) mn[i] = 1, mx[i] = n;
	for (int i = 0; i < q; i++) {
		int t, l, r, v; cin >> t >> l >> r >> v;
		if (t == 1) {
			for (int j = l; j <= r; j++) mn[j] = max(mn[j], v);
		} else {
			for (int j = l; j <= r; j++) mx[j] = min(mx[j], v);
		}
	}
	for (int i = 1; i <= n; i++) {
		addEdge(S, element(i), 1, 0);
		for (int j = mn[i]; j <= mx[i]; j++) addEdge(element(i), value(j), 1, 0);
	}
	for (int j = 1; j <= n; j++) {
		for (int k = 1; k <= n; k++) addEdge(value(j), repeat(k), 1, k * 2 - 1);
	}
	for (int k = 1; k <= n; k++) addEdge(repeat(k), T, INF, 0);
	int cost = 0;
	cout << ((minCostMaxFlow(&cost) == n) ? cost : -1);
}
