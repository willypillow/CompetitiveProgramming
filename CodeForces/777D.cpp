#include <bits/stdc++.h>
using namespace std;

const int MAXN = 5E5 + 10;

string str[MAXN];
int dp[2][MAXN], ansL[MAXN];
vector<vector<int> > p;

int cmp(int l, int r) {
	for (int i = 0, top = min(str[l].size(), str[r].size()); i < top; i++) {
		if (str[l][i] > str[r][i]) return i;
		if (str[l][i] < str[r][i]) return -i;
	}
	return (str[l].size() > str[r].size()) ? str[r].size() : -str[l].size();
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	p.resize(n);
	for (int i = 0; i < n; i++) {
		cin >> str[i];
		p[i].resize(str[i].size() + 1);
	}
	for (unsigned j = 1; j <= str[0].size(); j++) dp[0][j] = str[0].size() - j;
	for (int i = 1; i < n; i++) {
		int cc = cmp(i - 1, i);
		for (unsigned j = 1; j <= str[i].size(); j++) {
			int c = cc, &ans = dp[i & 1][j];
			ans = 1 << 30;
			if (c < 0) c = (-c >= j ? j : str[i - 1].size());
			else if (c > (int)j) c = j;
			for (int k = c; k > 0; k--) {
				int t = dp[~i & 1][k];
				if (t < ans) {
					ans = t;
					p[i][j] = k;
					break;
				}
			}
			ans += str[i].size() - j;
		}
	}
	int ans = 1 << 30, pp = 0;
	for (unsigned i = 1; i <= str[n - 1].size(); i++) {
		int t = dp[~n & 1][i];
		if (t < ans) {
			ans = t;
			pp = i;
		}
	}
	for (int i = n - 1; i >= 0; i--) {
		ansL[i] = pp;
		pp = p[i][pp];
	}
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < ansL[i]; j++) cout << str[i][j];
		cout << '\n';
	}
}
