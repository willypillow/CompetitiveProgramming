#include <bits/stdc++.h>
using namespace std;

int a[1000];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 0; i < n; i++) cin >> a[i];
	if (n % 2 == 1 && a[0] % 2 == 1 && a[n - 1] % 2 == 1) cout << "Yes";
	else cout << "No";
}
