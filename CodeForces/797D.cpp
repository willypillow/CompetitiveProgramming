#include <bits/stdc++.h>
using namespace std;

set<int> ok;
int vals[100001];
struct P { int v, l, r, p; } p[100001];

int dfs(int x, int l, int r) {
	if (l > r) return 0;
	if (vals[l] <= p[x].v && p[x].v <= vals[r]) ok.insert(p[x].v);
	int m = lower_bound(vals + l, vals + r + 1, p[x].v) - vals;
	int ret = 0;
	if (p[x].l != -1) ret += dfs(p[x].l, l, m - 1);
	while (vals[m] == p[x].v) m++;
	if (p[x].r != -1) ret += dfs(p[x].r, m, r);
	return ret;
}

int main() {
	int n; cin >> n;
	for (int i = 1; i <= n; i++) {
		cin >> p[i].v >> p[i].l >> p[i].r;
		if (p[i].l != -1) p[p[i].l].p = i;
		if (p[i].r != -1) p[p[i].r].p = i;
		vals[i] = p[i].v;
	}
	int root = 0;
	for (int i = 1; i <= n; i++) {
		if (!p[i].p) {
			root = i;
			break;
		}
	}
	sort(vals + 1, vals + n + 1);
	int vs = unique(vals + 1, vals + n + 1) - vals - 1;
	dfs(root, 1, vs);
	int ans = 0;
	for (int i = 1; i <= n; i++) {
		if (!ok.count(p[i].v)) ans++;
	}
	cout << ans << '\n';
}
