#include <bits/stdc++.h>
using namespace std;

char str[100010];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int k; cin >> k >> str;
	int n = strlen(str);
	sort(str, str + n);
	int sum = 0, cnt = 0;
	for (int i = 0; i < n; i++) sum += str[i] - '0';
	for (int i = 0; sum < k && i < n; i++) {
		sum += 9 - (str[i] - '0');
		cnt++;
	}
	cout << cnt;
}
