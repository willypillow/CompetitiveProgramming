#include <bits/stdc++.h>
using namespace std;

const int P[][6] = { { 0, 1, 2, 2, 1, 0 }, { 1, 0, 0, 1, 2, 2 } };

int main() {
	int n, x; cin >> n >> x;
	if (P[0][n % 6] == x) cout << "0\n";
	else if (P[1][n % 6] == x) cout << "1\n";
	else cout << "2\n";
}
