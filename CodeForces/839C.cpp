#include <bits/stdc++.h>
using namespace std;

vector<int> edges[100010];

double dfs(int x, int p) {
	if (edges[x].size() == 1 && p != x) return 1.0;
	double d = 1.0 / (edges[x].size() - (x != p)), ans = 0;
	for (int y: edges[x]) {
		if (y == p) continue;
		ans += d * dfs(y, x);
	}
	return ans + 1;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 1; i < n; i++) {
		int u, v; cin >> u >> v;
		edges[u].push_back(v);
		edges[v].push_back(u);
	}
	cout << fixed << setprecision(9) << dfs(1, 1) - 1;
}
