#include <bits/stdc++.h>
using namespace std;

void fail() {
	cout << "NO";
	exit(0);
}

int main() {
	int cA = 0, cB = 0, cC = 0;
	int n; cin >> n;
	for (int i = 0; i < n; i++) {
		int x; cin >> x;
		if (x == 25) {
			cA++;
		} else if (x == 50) {
			cB++, cA--;
			if (cA < 0) fail();
		} else if (x == 100) {
			cC++, cA--, cB--;
			if (cA < 0 || cB < 0) fail();
		} else fail();
	}
	cout << "YES";
}
