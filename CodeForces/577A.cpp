#include <bits/stdc++.h>
using namespace std;

int main() {
	int n, x; cin >> n >> x;
	int ans = 0;
	for (int i = 1; i * i <= x; i++) {
		if (x % i == 0 && i <= n && x / i <= n) {
			ans += 1 + (i * i != x);
		}
	}
	cout << ans;
}
