#include <bits/stdc++.h>
using namespace std;

int arr[100010];

int main() {
	int n; cin >> n;
	for (int i = 0; i < n; i++) cin >> arr[i];
	sort(arr, arr + n);
	int ans = 0;
	for (int i = 1; i < n - 1; i++) {
		if (arr[0] != arr[i] && arr[i] != arr[n - 1]) ans++;
	}
	cout << ans << '\n';
}
