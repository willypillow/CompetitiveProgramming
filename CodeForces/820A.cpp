#include <bits/stdc++.h>
using namespace std;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int c, v0, v1, a, l; cin >> c >> v0 >> v1 >> a >> l;
	int d;
	for (d = 0; c > 0; d++) {
		int canRead = min(v1, v0 + d * a);
		if (d) canRead -= l;
		c -= canRead;
	}
	cout << d;
}
