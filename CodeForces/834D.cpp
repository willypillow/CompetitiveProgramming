#include <bits/stdc++.h>
using namespace std;

const int MAXN = 35010;

int lazy[MAXN * 4], st[MAXN * 4], arr[MAXN], dist[MAXN], last[MAXN], dp[MAXN];

void add(int id, int l, int r, int qL, int qR, int v) {
	if (qL <= l && r <= qR) {
		lazy[id] += v;
		st[id] += v;
	} else {
		lazy[id << 1] += lazy[id];
		st[id << 1] += lazy[id];
		lazy[id << 1 | 1] += lazy[id];
		st[id << 1 | 1] += lazy[id];
		lazy[id] = 0;
		int m = (l + r) >> 1;
		if (qL <= m) add(id << 1, l, m, qL, min(m, qR), v);
		if (m < qR) add(id << 1 | 1, m + 1, r, max(qL, m), qR, v);
		st[id] = max(st[id << 1],
			st[id << 1 | 1]);
	}
}

int query(int id, int l, int r, int qL, int qR) {
	if (qL <= l && r <= qR) {
		return st[id];
	} else {
		lazy[id << 1] += lazy[id];
		st[id << 1] += lazy[id];
		lazy[id << 1 | 1] += lazy[id];
		st[id << 1 | 1] += lazy[id];
		lazy[id] = 0;
		int m = (l + r) >> 1, t = -1;
		if (qL <= m) t = max(t, query(id << 1, l, m, qL, min(m, qR)));
		if (m < qR) t = max(t, query(id << 1 | 1, m + 1, r, max(qL, m), qR));
		return t;
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, k; cin >> n >> k;
	for (int i = 1; i <= n; i++) cin >> arr[i];
	{
		set<int> s;
		for (int i = 1; i <= n; i++) {
			s.insert(arr[i]);
			dist[i] = s.size();
		}
	}
	for (int i = 1; i <= k; i++) {
		memset(last, 0, sizeof(last));
		for (int j = 1; j <= n; j++) {
			if (max(1, last[arr[k]]) <= j - 1) {
				add(1, 1, n, max(1, last[arr[j]]), j - 1, 1);
			}
			int ans = 1;
			if (max(1, i - 1) <= j - 1) {
				ans = max(dist[j], query(1, 1, n, max(1, i - 1), j - 1));
			}
			last[arr[j]] = j;
			dp[j] = ans;
		}
		memset(st, 0, sizeof(st));
		memset(lazy, 0, sizeof(lazy));
		for (int j = 1; j <= n; j++) add(1, 1, n, j, j, dp[j]);
	}
	cout << dp[n] << '\n';
}
