#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E3 + 2;

char field[MAXN][MAXN];
bool vis[MAXN][MAXN][4];

struct Node { int x, y, dir, dist; };

int bfs(int x1, int y1, int x2, int y2, int k) {
	const int DIR[][2] = { { -1, 0 }, { 0, -1 }, { 1, 0 }, { 0, 1 } };
	queue<Node> q;
	for (int i = 0; i < 4; i++) q.push({ x1, y1, i, 0 });
	while (q.size()) {
		Node cur = q.front(); q.pop();
		if (cur.x == x2 && cur.y == y2) return cur.dist;
		for (int i = 0; i < 4; i++) {
			for (int nx = cur.x + DIR[i][0], ny = cur.y + DIR[i][1], cnt = 1;
					cnt <= k && field[nx][ny] == '.' && !vis[nx][ny][i];
					nx += DIR[i][0], ny += DIR[i][1], cnt++) {
				vis[nx][ny][i] = true;
				q.push({ nx, ny, i, cur.dist + 1 });
			}
		}
	}
	return -1;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, m, k; cin >> n >> m >> k >> std::ws;
	for (int i = 1; i <= n; i++) cin.getline(&field[i][1], m + 1);
	int x1, y1, x2, y2; cin >> x1 >> y1 >> x2 >> y2;
	cout << bfs(x1, y1, x2, y2, k);
}
