#include <bits/stdc++.h>
using namespace std;

const int MAXN = 80, INF = 0x3f3f3f3f;

char str[MAXN];
vector<int> vs, ks, xs;
int dp[MAXN][MAXN][MAXN][2];

int remain(vector<int> &vec, int start, int lim) {
	int res = 0;
	for (unsigned i = start; i < vec.size() && vec[i] < lim; i++) res++;
	return res;
}

void update(int &dst, int v, int k, int x, int canK, int val) {
	int d = remain(vs, v, val) + remain(ks, k, val) + remain(xs, x, val);
	dst = min(dst, dp[v][k][x][canK] + d);
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n >> str;
	for (int i = 0; i < n; i++) {
		if (str[i] == 'V') vs.push_back(i);
		else if (str[i] == 'K') ks.push_back(i);
		else xs.push_back(i);
	}
	memset(dp, INF, sizeof(dp));
	dp[0][0][0][1] = 0;
	for (unsigned v = 0; v <= vs.size(); v++) {
		for (unsigned k = 0; k <= ks.size(); k++) {
			for (unsigned x = 0; x <= xs.size(); x++) {
				for (unsigned canK = 0; canK <= 1; canK++) {
					if (v < vs.size()) update(dp[v + 1][k][x][0], v, k, x, canK, vs[v]);
					if (canK && k < ks.size()) {
						update(dp[v][k + 1][x][1], v, k, x, canK, ks[k]);
					}
					if (x < xs.size()) update(dp[v][k][x + 1][1], v, k, x, canK, xs[x]);
				}
			}
		}
	}
	int *ans = dp[vs.size()][ks.size()][xs.size()];
	cout << min(ans[0], ans[1]) << '\n';
}
