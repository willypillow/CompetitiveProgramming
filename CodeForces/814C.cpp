#include <bits/stdc++.h>
using namespace std;

char str[1501];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n >> str;
	int q; cin >> q;
	while (q--) {
		int m, ans = 0;
		char c;
		cin >> m >> c;
		for (int l = 0, r = 0, use = 0; r < n; l++) {
			while (r < n && use < m) {
				if (str[r] != c) use++;
				r++;
			}
			while (r < n && str[r] == c) r++;
			ans = max(ans, r - l);
			if (str[l] != c) use--;
		}
		cout << ans << '\n';
	}
}
