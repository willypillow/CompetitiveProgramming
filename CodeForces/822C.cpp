#include <bits/stdc++.h>
using namespace std;

vector<tuple<int, int, int> > byL, byR;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, x; cin >> n >> x;
	for (int i = 0; i < n; i++) {
		int l, r, c; cin >> l >> r >> c;
		byL.push_back(make_tuple(l, r, c));
		byR.push_back(make_tuple(r, l, c));
	}
	sort(byL.begin(), byL.end()); sort(byR.begin(), byR.end());
	int ans = 2E9 + 10;
	map<int, int> m;
	for (int i = 0, j = 0; i < (int)byL.size(); i++) {
		while (j < (int)byR.size() && get<0>(byR[j]) < get<0>(byL[i])) {
			int dur = get<0>(byR[j]) - get<1>(byR[j]) + 1;
			auto it = m.find(dur);
			if (it != m.end()) it->second = min(it->second, get<2>(byR[j]));
			else m[dur] = get<2>(byR[j]);
			j++;
		}
		auto it = m.find(x - (get<1>(byL[i]) - get<0>(byL[i]) + 1));
		if (it != m.end()) ans = min(ans, get<2>(byL[i]) + it->second);
	}
	cout << (ans == 2E9 + 10 ? -1 : ans) << '\n';
}
