#include <iostream>
using namespace std;

bool vis[100001] = { false };

int main() {
	int n; cin >> n;
	int ans = 0, cnt = 0;
	for (int i = 0; i < n * 2; i++) {
		int x; cin >> x;
		if (vis[x]) cnt--;
		else cnt++;
		ans = max(ans, cnt);
		vis[x] = !vis[x];
	}
	cout << ans << '\n';
}
