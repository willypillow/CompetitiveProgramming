#include <bits/stdc++.h>
#include <x86intrin.h>
using namespace std;

map<tuple<int, int, int, int64_t>, int64_t > mp;
int64_t bit[2501][2501];

int n, m;

int64_t query2(int x, int y) {
	int64_t ret = 0;
	for (; y > 0; y -= y & -y) ret ^= bit[x][y];
	return ret;
}

int64_t query(int x, int y) {
	int64_t ret = 0;
	for (; x > 0; x -= x & -x) ret ^= query2(x, y);
	return ret;
}

void modify2(int x, int y, int64_t v) {
	for (; y <= m; y += y & -y) bit[x][y] ^= v;
}

void modify(int x, int y, int64_t v) {
	for (; x <= n; x += x & -x) modify2(x, y, v);
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int q; cin >> n >> m >> q;
	mt19937_64 mt(__rdtsc());
	while (q--) {
		int type, x1, y1, x2, y2; cin >> type >> x1 >> y1 >> x2 >> y2;
		if (type == 3) {
			cout << (query(x1, y1) == query(x2, y2) ? "Yes\n" : "No\n");
		} else {
			int64_t d = (type == 1 ? mt() : mp[{ x1, y1, x2, y2 }]);
			if (type == 1) mp[{ x1, y1, x2, y2 }] = d;
			modify(x1, y1, d); modify(x1, y2 + 1, d); modify(x2 + 1, y1, d); modify(x2 + 1, y2 + 1, d);
		}
	}
}
