#include <bits/stdc++.h>
using namespace std;

typedef pair<int, int> PII;
typedef pair<PII, PII> PIPI;
const int MAXN = 1E5 + 5, INF = 1E9;
int p[MAXN], in[MAXN], out[MAXN], depth[MAXN];
vector<int> order, g[MAXN];

PIPI combine(PIPI a, PIPI b) {
	PII arr[] = { a.first, a.second, b.first, b.second };
	sort(arr, arr + 4);
	return { arr[3], arr[2] };
}

struct SegTree {
	PIPI node[MAXN * 2 * 4];
	template<class Pred> void build(int id, int l, int r, Pred f) {
		if (l == r) {
			node[id] = { f(l), { -INF, -INF } };
		} else {
			int m = (l + r) >> 1;
			build(id << 1, l, m, f), build(id << 1 | 1, m + 1, r, f);
			node[id] = combine(node[id << 1], node[id << 1 | 1]);
		}
	}
	PIPI query(int id, int l, int r, int qL, int qR) {
		if (l > qR || qL > r) return { { -INF, -INF }, { -INF, -INF } };
		if (qL <= l && r <= qR) return node[id];
		int m = (l + r) >> 1;
		return combine(query(id << 1, l, m, qL, qR),
			query(id << 1 | 1, m + 1, r, qL, qR));
	}
} leftist, rightist, height;

void dfs(int x, int d) {
	in[x] = (int)order.size(), depth[x] = d, order.push_back(x);
	for (int y: g[x]) {
		dfs(y, d + 1);
		order.push_back(x);
	}
	out[x] = (int)order.size() - 1;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n, q; cin >> n >> q;
	for (int i = 2; i <= n; i++) {
		cin >> p[i];
		g[p[i]].push_back(i);
	}
	dfs(1, 0);
	leftist.build(1, 1, n, [&](int x) { return make_pair(-in[x], x); });
	rightist.build(1, 1, n, [&](int x) { return make_pair(out[x], x); });
	height.build(1, 0, (int)order.size() - 1,
		[&](int x) { return make_pair(-depth[order[x]], order[x]); });
	while (q--) {
		int l, r; cin >> l >> r;
		auto left = leftist.query(1, 1, n, l, r);
		auto right = rightist.query(1, 1, n, l, r);
		auto lSeg = height.query(1, 0, (int)order.size() - 1,
			-left.second.first, (left.first.second == right.first.second ? right.second.first : right.first.first));
		auto rSeg = height.query(1, 0, (int)order.size() - 1,
			(left.first.second == right.first.second ? -left.second.first : -left.first.first), right.second.first);
		int lDep = -lSeg.first.first, rDep = -rSeg.first.first;
		if (lDep > rDep) cout << left.first.second << ' ' << lDep << '\n';
		else cout << right.first.second << ' ' << rDep << '\n';
	}
}
