#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E5 + 10;
int pre[MAXN], nxt[MAXN], n;
vector<int> pos[MAXN];

struct Node {
	static Node pool[MAXN * 40], *ptr;
	int val;
	Node *lc, *rc;
	Node(): val(0), lc(NULL), rc(NULL) {}
	Node(int l, int r) {
		if (l == r) {
			val = pre[l] == 0;
		} else {
			int m = (l + r) >> 1;
			lc = new(Node::ptr++) Node(l, m); rc = new(Node::ptr++) Node(m + 1, r);
			val = lc->val + rc->val;
		}
	}
	Node *update(int pos, int v, int l = 1, int r = n) {
		Node *nd = new(Node::ptr++) Node(*this);
		nd->val += v;
		if (l == r) return nd;
		int m = (l + r) >> 1;
		if (pos <= m) nd->lc = nd->lc->update(pos, v, l, m);
		else nd->rc = nd->rc->update(pos, v, m + 1, r);
		return nd;
	}
	int query(int k, int l = 1, int r = n) {
		if (k > val) return n + 1;
		if (l == r) return l;
		int m = (l + r) >> 1;
		if (k <= lc->val) return lc->query(k, l, m);
		else return rc->query(k - lc->val, m + 1, r);
	}
} *root[MAXN], Node::pool[MAXN * 40], *Node::ptr = Node::pool;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif
	cin >> n;
	for (int i = 1; i <= n; i++) {
		int t; cin >> t;
		pos[t].push_back(i);
	}
	for (int i = 1; i <= n; i++) {
		for (unsigned j = 1; j < pos[i].size(); j++) {
			pre[pos[i][j]] = pos[i][j - 1];
			nxt[pos[i][j - 1]] = pos[i][j];
		}
	}
	root[0] = new(Node::ptr++) Node(1, n);
	for (int i = 1; i <= n; i++) {
		root[i] = root[i - 1]->update(i, -1);
		if (nxt[i]) root[i] = root[i]->update(nxt[i], 1);
	}
	for (int k = 1; k <= n; k++) {
		int ans = 0;
		for (int cur = 1; cur <= n; ans++) cur = root[cur - 1]->query(k + 1);
		cout << ans << ' ' << flush;
	}
	cout << '\n';
}
