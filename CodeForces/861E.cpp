#include <bits/stdc++.h>
using namespace std;

int toNum[100010];
bool taken[100010], type[100010];
string name[100010];
deque<int> que[2];
vector<int> freeCnt[2];
vector<string> ans;

int toInt(string s) {
	int ret = 0;
	for (char c: s) {
		if ((ret == 0 && c == '0') || !isdigit(c)) return 1E9;
		ret = ret * 10 + c - '0';
	}
	return ret;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	int e = 0;
	for (int i = 1; i <= n; i++) {
		cin >> name[i] >> type[i];
		if (type[i]) e++;
		int k = toInt(name[i]);
		if (k <= n) {
			toNum[i] = k;
			taken[k] = true;
		} else {
			toNum[i] = 1E9;
		}
	}
	for (int i = 1; i <= n; i++) {
		if (!taken[i]) freeCnt[i <= e].push_back(i);
	}
	for (int i = 1; i <= n; i++) {
		if (toNum[i] <= n) {
			if (type[i] != (toNum[i] <= e)) que[type[i]].push_back(i);
		} else {
			que[type[i]].push_front(i);
		}
	}
	while (que[0].size() || que[1].size()) {
		bool ok = false;
		for (int i = 0; i < 2; i++) {
			if (freeCnt[i].size()) {
				int t = que[i].back();
				que[i].pop_back();
				int space = freeCnt[i].back();
				freeCnt[i].pop_back();
				ans.push_back("move " + name[t] + " " + to_string(space));
				if (toNum[t] <= n) freeCnt[!i].push_back(toNum[t]);
				ok = true;
			}
		}
		int idx = 1;
		if (que[idx].empty()) idx = 0;
		if (!ok) {
			int t = que[idx].back();
			ans.push_back("move " + name[t] + " ma7122");
			name[t] = "ma7122";
			que[idx].pop_back();
			que[idx].push_front(t);
			assert(toNum[t] <= n);
			freeCnt[!idx].push_back(toNum[t]);
		}
	}
	cout << ans.size() << '\n';
	for (auto s: ans) cout << s << '\n';
}
