#include <bits/stdc++.h>
using namespace std;

int a[50][50];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	bool fail = false;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) cin >> a[i][j];
	}
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (a[i][j] == 1) continue;
			bool ok = false;
			for (int s = 0; s < n; s++) {
				for (int t = 0; t < n; t++) {
					if (a[i][s] + a[t][j] == a[i][j]) ok = true;
				}
			}
			if (!ok) fail = true;
		}
	}
	cout << (fail ? "No" : "Yes");
}
