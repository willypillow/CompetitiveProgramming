#include <bits/stdc++.h>
using namespace std;

char s1[1010], s2[1010];

int main() {
	int n; cin >> n;
	cin >> s1 >> s2;
	sort(s1, s1 + n); sort(s2, s2 + n);
	int a1 = 0, a2 = 0;
	for (int i = 0, j = 0; i < n; i++) {
		while (j < n && s1[i] > s2[j]) j++;
		if (j == n) {
			a1 += n - i;
			break;
		}
		j++;
	}
	for (int i = 0, j = 0; i < n; i++) {
		while (j < n && s1[i] >= s2[j]) j++;
		if (j == n) {
			break;
		}
		j++;
		a2++;
	}
	cout << a1 << '\n' << a2 << '\n';
}
