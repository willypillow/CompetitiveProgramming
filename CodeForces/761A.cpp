#include <bits/stdc++.h>
using namespace std;

bool can[101][101];

int main() {
	int a, b; cin >> a >> b;
	int cnt[2];
	for (int l = 1; l <= 200; l++) {
		for (int r = l; r <= 200; r++) {
			cnt[0] = cnt[1] = 0;
			for (int i = l; i <= r; i++) {
				cnt[i & 1]++;
			}
			can[cnt[0]][cnt[1]] = true;
		}
	}
	if (can[a][b]) cout << "YES\n";
	else cout << "NO\n";
}
