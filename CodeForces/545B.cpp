#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	string s, t; cin >> s >> t;
	int diff = 0;
	for (int i = 0; i < (int)s.size(); i++) diff += s[i] != t[i];
	if (diff & 1) {
		cout << "impossible";
	} else {
		diff >>= 1;
		for (int i = 0; i < (int)s.size(); i++) {
			if (s[i] != t[i]) {
				s[i] = (s[i] == '0' ? '1' : '0');
				if (!(--diff)) break;
			}
		}
		cout << s;
	}
}
