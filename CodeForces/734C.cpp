#include <bits/stdc++.h>
using namespace std;

pair<int64_t, int64_t> a[200010], b[200010];

bool cmpa(const pair<int64_t, int64_t> &l, const pair<int64_t, int64_t> &r) {
	return (l.first != r.first) ? (l.first > r.first) : (l.second < r.second);
}

bool cmpb(const pair<int64_t, int64_t> &l, const pair<int64_t, int64_t> &r) {
	return (l.first != r.first) ? (l.first > r.first) : (l.second > r.second);
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int64_t n, m, k, x, s; cin >> n >> m >> k >> x >> s;
	for (int64_t i = 0; i < m; i++) cin >> a[i].second;
	for (int64_t i = 0; i < m; i++) cin >> a[i].first;
	for (int64_t i = 0; i < k; i++) cin >> b[i].second;
	for (int64_t i = 0; i < k; i++) cin >> b[i].first;
	sort(a, a + m, cmpa); sort(b, b + k, cmpb);
	int64_t ans = (int64_t)n * x;
	for (int64_t i = 0; i < m; i++) {
		if (a[i].first > s) continue;
		pair<int64_t, int64_t> p = { s - a[i].first, INT64_MAX };
		int j = lower_bound(b, b + k, p, cmpb) - b;
		if (j != k) {
			ans = min(ans, (int64_t)(n - b[j].second) * a[i].second);
		} else {
			ans = min(ans, (int64_t)n * a[i].second);
		}
	}
	for (int64_t i = 0; i < k; i++) {
		if (b[i].first > s) continue;
		pair<int64_t, int64_t> p = { s - b[i].first, 0LL };
		int j = lower_bound(a, a + m, p, cmpa) - a;
		if (j != m) {
			ans = min(ans, (int64_t)(n - b[i].second) * (a[j].second));
		} else {
			ans = min(ans, (int64_t)(n - b[i].second) * x);
		}
	}
	cout << ans;
}
