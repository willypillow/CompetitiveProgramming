#include <iostream>
#include <cstring>
using namespace std;

const int MAXN = 200010;
const long long MOD = 1E9 + 7;
long long f[MAXN], prefix[MAXN], suffix[MAXN];
char str[MAXN];

long long extGcd(long long a, long long b, long long *x, long long *y) {
	if (b == 0) {
		*x = 1, *y = 0;
		return a;
	}
	long long tx, ty;
	long long g = extGcd(b, a % b, &tx, &ty);
	*x = ty;
	*y = tx - (a / b) * ty;
	return g;
}

long long mulMod(long long x, long long y) {
	return x * y % MOD;
}

long long divMod(long long x, long long y) {
	long long inv, nul;
	extGcd(y, MOD, &inv, &nul);
	inv = (inv % MOD + MOD) % MOD;
	return mulMod(x, inv);
}

long long fact(long long a) {
	if (a <= 0) return 1;
	if (f[a]) return f[a];
	return f[a] = mulMod(a, fact(a - 1));
}

long long c(long long a, long long b) {
	return divMod(fact(a), mulMod(fact(a - b), fact(b)));
}

int main() {
	cin >> str;
	int n = strlen(str);
	prefix[0] = str[0] == '(';
	for (int i = 1; i < n; i++) prefix[i] = prefix[i - 1] + (str[i] == '(');
	suffix[n - 1] = str[n - 1] == ')';
	for (int i = n - 2; i >= 0; i--) suffix[i] = suffix[i + 1] + (str[i] == ')');
	long long ans = 0;
	for (int i = 0; i < n; i++) {
		if (str[i] != '(' || suffix[i] == 0) continue;
		ans = (ans + c(prefix[i] + suffix[i] - 1, prefix[i])) % MOD;
	}
	cout << ans << '\n';
}
