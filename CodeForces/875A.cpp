#include <bits/stdc++.h>
using namespace std;

int sum(int x) {
	int ret = 0;
	do { ret += x % 10; } while (x /= 10);
	return ret;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	set<int> ans;
	for (int i = 0; i <= 100; i++) {
		if (sum(n - i) == i) ans.insert(n - i);
	}
	cout << ans.size() << '\n';
	for (int x: ans) cout << x << ' ';
}
