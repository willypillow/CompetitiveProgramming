#include <bits/stdc++.h>
using namespace std;

const int INF = 0x3f3f3f3f;

int dp[2][1001][51][51], scoreA[1001], scoreB[1001], n, p, k;

inline void update(int &dst, int src) {
	dst = max(dst, src);
}

inline void mmset(int i) {
	for (int j = 0; j <= p; j++) {
		for (int a = 0; a < k; a++) {
			for (int b = 0; b < k; b++) dp[i][j][a][b] = ~INF;
		}
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	cin >> n >> p >> k;
	p = min(p, 2 * (n + k - 1) / k);
	int r; cin >> r;
	for (int i = 0; i < r; i++) {
		int x; cin >> x;
		scoreA[x] = 1;
	}
	int s; cin >> s;
	for (int i = 0; i < s; i++) {
		int x; cin >> x;
		scoreB[x] = 1;
	}
	mmset(0); mmset(1);
	dp[0][0][0][0] = 0;
	for (int i = 1; i <= n; i++) {
		int c = i & 1;
		for (int j = 1; j <= p; j++) {
			update(dp[c][j][k - 1][0], dp[!c][j - 1][0][0] + scoreA[i]);
			update(dp[c][j][0][k - 1], dp[!c][j - 1][0][0] + scoreB[i]);
			for (int a = 1; a < k; a++) {
				update(dp[c][j][a - 1][0], dp[!c][j][a][0] + scoreA[i]);
				update(dp[c][j][a - 1][k - 1],
					dp[!c][j - 1][a][0] + (scoreA[i] || scoreB[i]));
			}
			for (int b = 1; b < k; b++) {
				update(dp[c][j][0][b - 1], dp[!c][j][0][b] + scoreB[i]);
				update(dp[c][j][k - 1][b - 1],
					dp[!c][j - 1][0][b] + (scoreA[i] || scoreB[i]));
			}
			for (int a = 1; a < k; a++) {
				for (int b = 1; b < k; b++) {
					update(dp[c][j][a - 1][b - 1],
						dp[!c][j][a][b] + (scoreA[i] || scoreB[i]));
				}
			}
		}
		for (int j = 0; j <= p; j++) update(dp[c][j][0][0], dp[!c][j][0][0]);
		mmset(!c);
	}
	int ans = ~INF, c = n & 1;
	for (int j = 0; j <= p; j++) {
		for (int a = 0; a < k; a++) {
			for (int b = 0; b < k; b++) update(ans, dp[c][j][a][b]);
		}
	}
	cout << ans << '\n';
}
