#include <bits/stdc++.h>
using namespace std;

char s1[2000], s2[2000];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m >> s1 >> s2;
	vector<int> ans;
	ans.resize(2000);
	for (int i = 0; i <= m - n; i++) {
		vector<int> t;
		for (int j = 0; j < n; j++) {
			if (s2[i + j] != s1[j]) t.push_back(j);
		}
		if (ans.size() > t.size()) ans = t;
	}
	cout << ans.size() << '\n';
	for (int x: ans) cout << x + 1 << ' ';
}
