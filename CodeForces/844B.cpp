#include <bits/stdc++.h>
using namespace std;

int a[60][60];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++) cin >> a[i][j];
	}
	int64_t ans = n * m;
	for (int i = 1; i <= n; i++) {
		int t = 0;
		for (int j = 1; j <= m; j++) t += a[i][j];
		ans += (1LL << t) - t - 1;
		t = m - t;
		ans += (1LL << t) - t - 1;
	}
	for (int j = 1; j <= m; j++) {
		int t = 0;
		for (int i = 1; i <= n; i++) t += a[i][j];
		ans += (1LL << t) - t - 1;
		t = n - t;
		ans += (1LL << t) - t - 1;
	}
	cout << ans << '\n';
}
