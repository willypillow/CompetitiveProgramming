#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E5 + 5;

int bit[MAXN], n;
set<int> st[MAXN];

void add(int p, int v) {
	while (p <= n) {
		bit[p] += v;
		p += p & -p;
	}
}

int query(int p) {
	int ret = 0;
	while (p > 0) {
		ret += bit[p];
		p -= p & -p;
	}
	return ret;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	cin >> n;
	for (int i = 1; i <= n; i++) {
		int x; cin >> x;
		st[x].insert(i);
		add(i, 1);
	}
	int idx = 1, item = 1;
	int64_t ans = 0;
	for (int i = 1; i <= n; i++) {
		while (item < MAXN && !st[item].size()) item++;
		auto t = st[item].lower_bound(idx);
		if (t == st[item].end()) t = st[item].begin();
		if (idx <= *t) ans += query(*t) - query(idx - 1);
		else ans += query(n) - query(idx) + query(*t);
		idx = *t;
		add(idx, -1);
		st[item].erase(t);
	}
	cout << ans << '\n';
}
