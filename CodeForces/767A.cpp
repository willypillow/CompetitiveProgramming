#include <bits/stdc++.h>
using namespace std;

set<int, greater<int> > s;

int main() {
	int n; cin >> n;
	int top = n + 1;
	for (int i = 0; i < n; i++) {
		int x; cin >> x;
		s.insert(x);
		while (!s.empty() && *s.begin() == top - 1) {
			cout << *s.begin() << ' ';
			s.erase(s.begin());
			top--;
		}
		cout << '\n';
	}
}
