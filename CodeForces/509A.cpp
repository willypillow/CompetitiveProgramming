#include <bits/stdc++.h>
using namespace std;

int a[101];

int main() {
	int n; cin >> n;
	int ans = 0, mn = 1E9;
	for (int i = 0; i < n; i++) {
		int a, p; cin >> a >> p;
		mn = min(mn, p);
		ans += a * mn;
	}
	cout << ans;
}
