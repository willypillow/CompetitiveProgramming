#include <bits/stdc++.h>
using namespace std;

int main() {
	vector<int> ans;
	int n, k; cin >> n >> k;
	int t = 1;
	for (int i = 2; i <= n; i++) {
		while (n % i == 0 && t < k) {
			ans.push_back(i);
			n /= i;
			t++;
		}
		if (n == 1) {
			cout << "-1\n";
			return 0;
		}
		if (t == k) {
			ans.push_back(n);
			break;
		}
	}
	for (int x: ans) cout << x << ' ';
}
