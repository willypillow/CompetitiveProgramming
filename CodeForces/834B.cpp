#include <bits/stdc++.h>
using namespace std;

bool on[26];
int last[26];
char str[1000010];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, k; cin >> n >> k >> str;
	for (int i = 0; i < n; i++) {
		last[str[i] - 'A'] = i;
	}
	bool ans = false;
	for (int i = 0; i < n; i++) {
		if (!on[str[i] - 'A']) on[str[i] - 'A'] = true;
		int t = 0;
		for (int j = 0; j < 26; j++) {
			if (on[j]) t++;
		}
		if (t > k) ans = true;
		if (last[str[i] - 'A'] == i) on[str[i] - 'A'] = false;
	}
	cout << (ans ? "YES\n" : "NO\n");
}
