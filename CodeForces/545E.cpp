#include <bits/stdc++.h>
using namespace std;

int64_t dist[300010], sum;
vector<int> ans2;
vector<pair<int, int> > g[300010];
vector<tuple<int, int, int> > ans;
map<pair<int, int>, int> edgeMp;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	for (int i = 1; i <= m; i++) {
		int u, v, w; cin >> u >> v >> w;
		g[u].push_back({ w, v }), g[v].push_back({ w, u });
		edgeMp[{ u, v }] = edgeMp[{ v, u }] = i;
	}
	int u; cin >> u;
	priority_queue<
			pair<int64_t, int>,
			vector<pair<int64_t, int> >,
			greater<pair<int64_t, int> > > q;
	memset(dist, 0x3f, sizeof(dist));
	q.push({ dist[u] = 0LL, u });
	while (q.size()) {
		auto p = q.top(); q.pop();
		if (p.first > dist[p.second]) continue;
		for (auto np: g[p.second]) {
			if (np.first + p.first < dist[np.second]) {
				dist[np.second] = np.first + p.first;
				q.push({ dist[np.second], np.second });
			}
		}
	}
	for (int i = 1; i <= n; i++) {
		for (auto p: g[i]) {
			if (dist[i] + p.first == dist[p.second]) {
				ans.push_back({ p.second, p.first, i });
			}
		}
	}
	sort(ans.begin(), ans.end());
	tuple<int, int, int> last;
	for (auto p: ans) {
		if (get<0>(p) == get<0>(last)) continue;
		ans2.push_back(edgeMp[{ get<0>(p), get<2>(p) }]);
		sum += get<1>(p);
		last = p;
	}
	cout << sum << '\n';
	for (int x: ans2) cout << x << ' ';
}
