#include <bits/stdc++.h>
using namespace std;

struct Ring {
	int a, b, h;
	bool operator <(const Ring &rhs) const {
		return b > rhs.b;
	}
} r[100010];
vector<Ring> rings;
long long ans[100010];

int main() {
	int n; cin >> n;
	for (int i = 0; i < n; i++) cin >> r[i].a >> r[i].b >> r[i].h;
	sort(r, r + n);
	for (int i = 0; i < n; ) {
		int j = i + 1;
		while (j < n && r[i].b == r[j].b) j++;
		int newA = 1 << 30, newH = 0;
		for (int k = i; k < j; k++) {
			newA = min(newA, r[k].a);
			newH += r[k].h;
		}
		rings.push_back(Ring { newA, r[i].b, newH });
		i = j;
	}
	long long res = 0;
	stack<int> s;
	for (int i = 0; i < (int)rings.size(); i++) {
		while (!s.empty() && rings[s.top()].a >= rings[i].b) s.pop();
		ans[i] = (s.empty() ? 0 : ans[s.top()]) + rings[i].h;
		res = max(res, ans[i]);
		s.push(i);
	}
	cout << res << '\n';
}
