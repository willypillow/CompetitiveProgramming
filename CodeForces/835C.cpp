#include <bits/stdc++.h>
using namespace std;

vector<pair<int, int> > star[11];
int sum[11][101][101];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, q, c; cin >> n >> q >> c;
	for (int i = 0; i < n; i++) {
		int x, y, s; cin >> x >> y >> s;
		star[s].push_back({ x, y });
	}
	for (int i = 0; i <= c; i++) {
		for (int j = 0; j <= c; j++) {
			for (auto p: star[j]) {
				sum[i][p.first][p.second] += (i + j) % (c + 1);
			}
		}
		for (int j = 1; j <= 100; j++) {
			for (int k = 1; k <= 100; k++) {
				sum[i][j][k] +=
					sum[i][j - 1][k] + sum[i][j][k - 1] - sum[i][j - 1][k - 1];
			}
		}
	}
	while (q--) {
		int t, x1, x2, y1, y2; cin >> t >> x1 >> y1 >> x2 >> y2;
		//t = min(t, c);
		t = t % (c + 1);
		cout << sum[t][x2][y2] - sum[t][x1 - 1][y2] - sum[t][x2][y1 - 1] + sum[t][x1 - 1][y1 - 1] << '\n';
	}
}
