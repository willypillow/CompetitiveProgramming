#include <bits/stdc++.h>
using namespace std;

char str[51][5];
int in[100], ans[100], top = 0;

int main() {
	for (int i = 0; i <= 50; i++) {
		str[i][0] = 'A' + (i % 26);
		str[i][1] = 'a' + ((i / 26) % 26);
		str[i][2] = '\0';
	}
	int n, k; cin >> n >> k;
	for (int i = 0; i < n; i++) ans[i] = i;
	for (int i = 0; i < n - k + 1; i++) {
		char str[5]; cin >> str;
		in[i] = strcmp(str, "YES") == 0;
		if (!in[i]) {
			ans[i + k - 1] = ans[i];
		}
	}
	for (int i = 0; i < n - 1; i++) cout << str[ans[i]] << ' ';
	cout << str[ans[n - 1]] << '\n';
}
