#include <bits/stdc++.h>
using namespace std;

int a[200], b[200];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, k; cin >> n >> k;
	bool ok = false;
	for (int i = 0; i < n; i++) cin >> a[i];
	for (int i = 0; i < k; i++) cin >> b[i];
	for (int i = 0, last = 0; i < n; i++) {
		if (!a[i]) continue;
		if (last > a[i]) ok = true;
		last = a[i];
	}
	sort(b, b + k);
	for (int i = 0, last = 0; i < n; i++) {
		if (!a[i]) {
			if (b[0] < last) ok = true;
		} else {
			last = a[i];
		}
	}
	for (int i = n - 1, last = 1E9; i >= 0; i--) {
		if (!a[i]) {
			if (b[k - 1] > last) ok = true;
		} else {
			last = a[i];
		}
	}
	if (k >= 2) ok = true;
	puts(ok ? "Yes" : "No");
}
