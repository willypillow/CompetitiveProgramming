#include <iostream>
#include <algorithm>
#include <cstring>

const int dirs[][6] = { { 1, -1, 0, 0, 0, 0 }, { 0, 0, 1, -1, 0, 0 }, { 0, 0, 0, 0, 1, -1 } };

bool canvas[102][102][102], vis[102][102][102];
int volume, area, xCnt, yCnt, zCnt, xs[102], ys[102], zs[102];

struct Box {
  int x0, y0, z0, x, y, z;
} boxes[50];

inline int search(int *arr, int len, int element) {
  return std::lower_bound(arr, arr + len, element) - arr;
}

inline void floodfill(int x, int y, int z) {
  volume += (xs[x + 1] - xs[x]) * (ys[y + 1] - ys[y]) * (zs[z + 1] - zs[z]);
  vis[x][y][z] = true;
  for (int i = 0; i < 6; i++) {
    int nx = x + dirs[0][i], ny = y + dirs[1][i], nz = z + dirs[2][i];
    if (nx < 0 || nx + 1 >= xCnt || ny < 0 || ny + 1 >= yCnt || nz < 0 || nz + 1 >= zCnt) continue;
    if (canvas[nx][ny][nz]) {
      if (dirs[0][i]) {
        area += (ys[ny + 1] - ys[ny]) * (zs[nz + 1] - zs[nz]);
      } else if (dirs[1][i]) {
        area += (xs[nx + 1] - xs[nx]) * (zs[nz + 1] - zs[nz]);
      } else {
        area += (xs[nx + 1] - xs[nx]) * (ys[ny + 1] - ys[ny]);
      }
    } else if (!vis[nx][ny][nz]){
      floodfill(nx, ny, nz);
    }
  }
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int t;
  std::cin >> t;
  while (t--) {
    memset(canvas, 0, sizeof(canvas));
    memset(vis, false, sizeof(vis));
    volume = area = 0;
    int n;
    std::cin >> n;
    for (int i = 0; i < n; i++) {
      std::cin >> boxes[i].x0 >> boxes[i].y0 >> boxes[i].z0 >>
        boxes[i].x >> boxes[i].y >> boxes[i].z;
      xs[i * 2] = boxes[i].x0; xs[i * 2 + 1] = boxes[i].x0 + boxes[i].x;
      ys[i * 2] = boxes[i].y0; ys[i * 2 + 1] = boxes[i].y0 + boxes[i].y;
      zs[i * 2] = boxes[i].z0; zs[i * 2 + 1] = boxes[i].z0 + boxes[i].z;
    }
    xs[n * 2] = ys[n * 2] = zs[n * 2] = 0;
    xs[n * 2 + 1] = ys[n * 2 + 1] = zs[n * 2 + 1] = 1001;
    std::sort(xs, xs + n * 2 + 2);
    std::sort(ys, ys + n * 2 + 2);
    std::sort(zs, zs + n * 2 + 2);
    xCnt = std::unique(xs, xs + n * 2 + 2) - xs,
      yCnt = std::unique(ys, ys + n * 2 + 2) - ys;
      zCnt = std::unique(zs, zs + n * 2 + 2) - zs;
    for (int i = 0; i < n; i++) {
      int x0 = search(xs, xCnt, boxes[i].x0),
        x1 = search(xs, xCnt, boxes[i].x0 + boxes[i].x),
        y0 = search(ys, yCnt, boxes[i].y0),
        y1 = search(ys, yCnt, boxes[i].y0 + boxes[i].y),
        z0 = search(zs, zCnt, boxes[i].z0),
        z1 = search(zs, zCnt, boxes[i].z0 + boxes[i].z);
      for (int x = x0; x < x1; x++) {
        for (int y = y0; y < y1; y++) {
          for (int z = z0; z < z1; z++) canvas[x][y][z] = true;
        }
      }
    }
    floodfill(0, 0, 0);
    std::cout << area << ' ' << 1001 * 1001 * 1001 - volume << '\n';
  }
}
