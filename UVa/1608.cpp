#include <iostream>
#include <unordered_map>

struct Element {
  int val, left, right;
} arr[200001];

bool verify(int start, int end) {
  if (end - start <= 1) return true;
  for (int i = start, j = end - 1; i <= j; i++, j--) {
    if (arr[i].left < start && arr[i].right >= end) {
      return verify(start, i) && verify(i + 1, end);
    }
    if (arr[j].left < start && arr[j].right >= end) {
      return verify(start, j) && verify(j + 1, end);
    }
  }
  return false;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  std::unordered_map<int, int> map;
  int t; std::cin >> t;
  while (t--) {
    map.clear();
    int n;
    std::cin >> n;
    for (int i = 0; i < n; i++) {
      std::cin >> arr[i].val;
      arr[i].left = -1, arr[i].right = 1 << 30;
    }
    for (int i = 0; i < n; i++) {
      if (map.count(arr[i].val)) {
        arr[i].left = map[arr[i].val];
        arr[map[arr[i].val]].right = i;
      }
      map[arr[i].val] = i;
    }
    std::cout << (verify(0, n) ? "non-boring\n" : "boring\n");
  }
}
