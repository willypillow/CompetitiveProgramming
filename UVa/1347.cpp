#include <iostream>
#include <cstring>
#include <cmath>
#include <iomanip>
using std::cout; using std::cin; using std::setprecision; using std::fixed;

const int MAX = 1 << 30;
int n;
double dp[1000][1000];

struct Point { int x, y; } a[10000];
double dist(int i, int j) {
  int dx = a[i].x - a[j].x, dy = a[i].y - a[j].y;
  return sqrt(dx * dx + dy * dy);
}

double dfs(int i, int j) {
  if (dp[i][j]) return dp[i][j];
  if (i == n - 2) return dp[i][j] = dist(i, n - 1) + dist(j, n - 1);
  return dp[i][j] =
    std::min(dfs(i + 1, j) + dist(i, i + 1), dfs(i + 1, i) + dist(j, i + 1));
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  while (cin >> n) {
    memset(dp, 0, sizeof(dp));
    for (int i = 0; i < n; i++) cin >> a[i].x >> a[i].y;
    cout << fixed << setprecision(2) << dfs(1, 0) + dist(0, 1) << '\n';
  }
}
