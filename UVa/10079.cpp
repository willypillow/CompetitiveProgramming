#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  while (std::cin >> n && n >= 0) {
  	std::cout << ((unsigned long long)n * n + n + 2) / 2 << '\n';
  }
}
