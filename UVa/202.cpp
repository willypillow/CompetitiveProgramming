#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int a, b;
  while (std::cin >> a >> b) {
    std::cout << a << '/' << b << " = " << a / b << '.';
    int rep[30000] = { 0 }, i;
    char nums[10000] = { 0 };
    for (i = 1;; i++) {
      a = a % b * 10;
      if (a == 0) {
        nums[i] = '0';
        rep[a] = i++;
        break;
      }
      if (rep[a] != 0)
        break;
      rep[a] = i;
      nums[i] = a / b + '0';
    }
    for (int j = 1; j < i; j++) {
      if (j > 50) {
        std::cout << "...";
        break;
      }
      if (j == rep[a]) std::cout << '(';
      std::cout << nums[j];
    }
    std::cout << ")\n   " << i - rep[a] <<
      " = number of digits in repeating cycle\n\n";
  }
}
