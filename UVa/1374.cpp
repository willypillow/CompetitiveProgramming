#include <iostream>
#include <algorithm>
#include <set>

int nums[100];

bool dfs(int steps, int maxDepth, int n) {
  if (nums[steps] == n) {
    std::cout << steps << '\n';
    return true;
  }
  if (steps >= maxDepth) return false;
  int max = *std::max_element(nums, nums + steps + 1);
  if ((max << (maxDepth - steps)) < n) return false; // Too small to reach it
  if (max != nums[steps] && max > n && nums[steps] > n) return false;
  for (int i = steps; i >= 0; i--) {
    nums[steps + 1] = nums[steps] + nums[i];
    if(dfs(steps + 1, maxDepth, n)) return true;
    nums[steps + 1] = nums[steps] - nums[i];
    if (dfs(steps + 1, maxDepth, n)) return true;
  }
  return false;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif 
  int n;
  while (std::cin >> n && n) {
    nums[0] = 1;
    for (int maxDepth = 2; !dfs(0, maxDepth, n); maxDepth++);
  }
}
