#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  std::cin >> n;
  while (n--) {
    unsigned long long s, d;
    std::cin >> s >> d;
    int b = s - d;
    if (b < 0 || b & 1) std::cout << "impossible\n";
    else {
      int a = (s + d) / 2; // If b is even, a is also even
      std::cout << a << " " << b / 2 << "\n";
    }
  }
}
