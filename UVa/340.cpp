#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n, games = 0;
  while (std::cin >> n) {
    int ans[1000], ansNums[10] = { 0 };
    if (!n) break;
    std::cout << "Game " << ++games << ":\n";
    for (int i = 0; i < n; i++) {
      std::cin >> ans[i];
      ansNums[ans[i]]++;
    }
    for (;;) {
      int a = 0, b = 0, in[1000], inNums[10] = { 0 };
      for (int i = 0; i < n; i++) {
        std::cin >> in[i];
        if (in[i] == ans[i]) a++;
        inNums[in[i]]++;
      }
      if (!in[0]) break;
      for (int i = 1; i <= 9; i++) {
        b += (ansNums[i] > inNums[i]) ? inNums[i] : ansNums[i];
      }
      std::cout << "    (" << a << "," << b - a << ")\n";
    }
  }
}
