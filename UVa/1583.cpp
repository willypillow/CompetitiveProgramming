#include <iostream>

const int MAXN = 100000;
int arr[MAXN + 1] = { 0 };

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  for (int i = 1; i <= MAXN; i++) {
    int sum = i;
    for (int num = i; num; num /= 10) {
      sum += num % 10;
    }
    if (sum <= MAXN && !arr[sum]) arr[sum] = i; // i is guranteed the smallest
  }
  int n;
  std::cin >> n;
  while (n--) {
    int in;
    std::cin >> in;
    std::cout << arr[in] << '\n';
  }
}
