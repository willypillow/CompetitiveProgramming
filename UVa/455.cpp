#include <iostream>
#include <cstring>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int t;
  std::cin >> t;
  while (t--) {
    char str[100];
    std::cin >> str;
    int len = strlen(str), i;
    for (i = 1; i <= len; i++) {
      if (len % i == 0) {
        bool isPeriod = true;
        for (int j = 0; j < len; j++) {
          if (str[j] != str[j % i]) {
            isPeriod = false;
            break;
          }
        }
        if (isPeriod) break;
      }
    }
    std::cout << i << '\n';
    if (t) std::cout << '\n';
  }
}
