#include <bits/stdc++.h>
using namespace std;

char str[110];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	for (int g; cin >> g && g; ) {
		cin >> str;
		int len = (int)strlen(str), blkSz = len / g;
		for (int i = 0; i < g; i++) {
			reverse(str + i * blkSz, str + (i + 1) * blkSz);
		}
		cout << str << '\n';
	}
}
