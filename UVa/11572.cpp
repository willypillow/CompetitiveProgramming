#include <iostream>
#include <unordered_set>

int arr[1000000];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  int t;
  std::unordered_set<int> s;
  std::cin >> t;
  while (t--) {
    s.clear();
    int n, l = 0, r = 0, len = 0;
    std::cin >> n;
    for (int i = 0; i < n; i++) std::cin >> arr[i];
    while (r < n) {
      while (!s.count(arr[r]) && r < n) s.insert(arr[r++]);
      len = std::max(len, r - l);
      while (arr[l] != arr[r]) s.erase(arr[l++]);
      s.erase(arr[l++]);
    }
    std::cout << len << '\n';
  }
}
