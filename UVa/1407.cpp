#include <bits/stdc++.h>
using namespace std;

int size[501], dp[501][501][2];
vector<vector<pair<int, int> > > g;

void dfs(int cur, int par) {
	size[cur] = 1;
	dp[cur][1][0] = dp[cur][1][1] = 0;
	for (auto &p: g[cur]) {
		if (p.first == par) continue;
		dfs(p.first, cur);
		size[cur] += size[p.first];
		for (int i = size[cur]; i > 1; i--) {
			for (int j = 0; j <= i && j <= size[p.first]; j++) {
				dp[cur][i][0] = min(dp[cur][i][0], dp[cur][i - j][1] + dp[p.first][j][0] + p.second);
				dp[cur][i][0] = min(dp[cur][i][0], dp[cur][i - j][0] + dp[p.first][j][1] + p.second * 2);
				dp[cur][i][1] = min(dp[cur][i][1], dp[cur][i - j][1] + dp[p.first][j][1] + p.second * 2);
			}
		}
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int cases = 0, n;
	while (cin >> n && n) {
		memset(dp, 0x3f, sizeof(dp)); g.clear(); g.resize(n + 1);
		for (int i = 1; i < n; i++) {
			int a, b, d; cin >> a >> b >> d;
			g[a].push_back(make_pair(b, d)); g[b].push_back(make_pair(a, d));
		}
		dfs(0, 0);
		cout << "Case " << ++cases << ":\n";
		int q; cin >> q;
		while (q--) {
			int x; cin >> x;
			for (int i = n; i >= 0; i--) {
				if (dp[0][i][0] <= x) {
					cout << i << '\n';
					break;
				}
			}
		}
	}
}
