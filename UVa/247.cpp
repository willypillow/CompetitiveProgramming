#include <iostream>
#include <unordered_map>
using std::cin; using std::cout; using std::string; using std::unordered_map;

string toName[30];
unordered_map<string, int> idMap;
int id(string &str) {
  if (idMap.count(str)) return idMap[str];
  int newId = idMap.size();
  toName[newId] = str;
  return idMap[str] = newId;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int n, m, cases = 0;
  bool firstCase = true;
  while (cin >> n >> m && n) {
    idMap.clear();
    bool call[30][30] = { false };
    if (!firstCase) cout << '\n';
    firstCase = false;
    for (int i = 0; i < n; i++) call[i][i] = true;
    for (int i = 0; i < m; i++) {
      string name;
      cin >> name;
      int from = id(name);
      cin >> name;
      int to = id(name);
      call[from][to] = true;
    }
    for (int k = 0; k < n; k++) {
      for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
          call[i][j] |= call[i][k] && call[k][j];
        }
      }
    }
    cout << "Calling circles for data set " << ++cases << ":\n";
    bool vis[30] = { false };
    for (int i = 0; i < n; i++) {
      if (!vis[i]) {
        bool first = true;
        for (int j = 0; j < n; j++) {
          if (call[i][j] && call[j][i]) {
            if (!first) cout << ", ";
            first = false;
            cout << toName[j];
            vis[j] = true;
          }
        }
        cout << '\n';
      }
    }
  }
}

