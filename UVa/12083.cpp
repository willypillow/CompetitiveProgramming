#include <iostream>
#include <cstring>
#include <vector>
#include <unordered_map>
using namespace std;

const int MAXN = 500;

struct Student { int h, sex, music, sport; } stu[MAXN];

unordered_map<string, int> dict;
vector<vector<int> > g;
bool visX[MAXN], visY[MAXN];
int matchY[MAXN], n;

int toId(string str) {
	if (dict.count(str)) return dict[str];
	int id = dict.size();
	return dict[str] = id;
}

bool dfs(int cur) {
	visX[cur] = true;
	for (int y: g[cur]) {
		if (visY[y]) continue;
		visY[y] = true;
		if (matchY[y] == -1 || dfs(matchY[y])) {
			matchY[y] = cur;
			return true;
		}
	}
	return false;
}

bool isEdge(int i, int j) {
	if (stu[i].sex == stu[j].sex) return false;
	if (abs(stu[i].h - stu[j].h) > 40) return false;
	if (stu[i].music != stu[j].music) return false;
	if (stu[i].sport == stu[j].sport) return false;
	return true;
}

int main() {
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif
	int t; cin >> t;
	while (t--) {
		dict.clear();
		g.clear();
		cin >> n;
		g.resize(n);
		for (int i = 0; i < n; i++) {
			string sex, music, sport;
			cin >> stu[i].h >> sex >> music >> sport;
			stu[i].sex = sex[0] == 'M';
			stu[i].music = toId(music);
			stu[i].sport = toId(sport);
		}
		for (int i = 0; i < n; i++) {
			if (stu[i].sex) continue;
			for (int j = 0; j < n; j++) {
				if (i != j && isEdge(i, j)) g[i].push_back(j);
			}
		}
		int ans = 0;
		memset(matchY, -1, sizeof(matchY));
		for (int i = 0; i < n; i++) {
			if (stu[i].sex) continue;
			memset(visX, false, sizeof(visX)); memset(visY, false, sizeof(visY));
			if (dfs(i)) ans++;
		}
		cout << n - ans << '\n';
	}
}
