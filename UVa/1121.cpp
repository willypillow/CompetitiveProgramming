#include <iostream>
#include <algorithm>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n, s, arr[100001], sum[100002];
  while (std::cin >> n >> s) {
    sum[0] = 0;
    for (int i = 0; i < n; i++) {
      std::cin >> arr[i];
      sum[i + 1] = sum[i] + arr[i];
    }
    int min = 1 << 30;
    for (int i = 0; i < n; i++) {
      int len = std::lower_bound(sum + i, sum + n, sum[i] + s) - sum - i;
      if (sum[len + i] - sum[i] < s) break;
      min = std::min(min, len);
    }
    if (min == 1 << 30) min = 0;
    std::cout << min << '\n';
  }
}

