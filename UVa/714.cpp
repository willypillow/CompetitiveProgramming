#include <iostream>

int k, m, arr[510];

bool solve(unsigned long long per) {
  unsigned long long sum = 0;
  int cnt = 1;
  for (int i = 0; i < m; i++) {
    if (sum + arr[i] > per) {
      cnt++;
      sum = 0;
    }
    sum += arr[i];
  }
  return cnt <= k;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int n;
  std::cin >> n;
  while (n--) {
    int max = 0;
    unsigned long long sum = 0;
    bool cut[510] = { false };
    std::cin >> m >> k;
    for (int i = 0; i < m; i++) {
      std::cin >> arr[i];
      sum += arr[i];
      max = std::max(max, arr[i]);
    }
    unsigned long long l = max, r = sum;
    while (l < r) {
      unsigned long long m = (l + r) >> 1;
      if (solve(m)) r = m;
      else l = m + 1;
    }
    int cnt = 0;
    unsigned long long curSum = 0;
    for (int i = m - 1; i >= 0; i--) {
      if (curSum + arr[i] > l) {
        curSum = 0;
        cut[i] = true;
        cnt++;
      }
      curSum += arr[i];
    }
    for (int i = 0, p = 0; i < (k - 1 - cnt); i++) {
      while (cut[p]) p++;
      cut[p] = true;
    }
    for (int i = 0; i < m - 1; i++) {
      std::cout << arr[i] << (cut[i] ? " / " : " ");
    }
    std::cout << arr[m - 1] << '\n';
  }
}


