#include <iostream>

struct P { int x, y; };

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  int k;
  while (std::cin >> k) {
    P ans[10000];
    int aTop = 0;
    for (int y = k + 1; y <= k * 2; y++) {
      int numer = y - k, denom = y * k;
      if (denom % numer == 0) {
        ans[aTop++] = P { denom / numer, y };
      }
    }
    std::cout << aTop << '\n';
    for (int i = 0; i < aTop; i++) {
      std::cout << "1/" << k << " = 1/" << ans[i].x << " + 1/" << ans[i].y << '\n';
    }
  }
}
