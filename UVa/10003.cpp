#include <iostream>
#include <cstring>
using std::cin; using std::cout; using std::min;

int c[52], d[52][52];

int dp(int i, int j) {
  if (i >= j) return 0;
  if (d[i][j]) return d[i][j];
  int ans = 1 << 30;
  for (int k = i; k < j; k++) {
    ans = min(ans, dp(i, k) + dp(k + 1, j));
  }
  return d[i][j] = ans + c[j + 1] - c[i];
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int l;
  while (cin >> l && l) {
    memset(d, 0, sizeof(d));
    int n;
    cin >> n;
    for (int i = 1; i <= n; i++) cin >> c[i];
    c[0] = 0; c[n + 1] = l;
    cout << "The minimum cutting is " << dp(0, n) << ".\n";
  }
}
