#include <iostream>
#include <cstring>
using std::cin; using std::cout; using std::min;

const int MAXN = 5002, INF = 0x6f6f6f6f;

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int t; cin >> t >> std::ws;
  while (t--) {
    char s[2][MAXN];
    int start[2][26], end[2][26] = { 0 }, dp[MAXN] = { 0 }, c[MAXN] = { 0 };
    memset(start, 0x6f, sizeof(start));
    cin.getline(s[0] + 1, MAXN - 1); cin.getline(s[1] + 1, MAXN - 1);
    int len[] =  { strlen(s[0] + 1), strlen(s[1] + 1) };
    for (int i = 0; i < 2; i++) {
      for (int j = 1; j <= len[i]; j++) {
        s[i][j] -= 'A';
        if (start[i][s[i][j]] == INF) start[i][s[i][j]] = j;
        end[i][s[i][j]] = j;
      }
    }
    for (int i = 0; i <= len[0]; i++) {
      for (int j = 0; j <= len[1]; j++) {
        if (!i && !j) continue;
        int v1 = (i ? dp[j] + c[j] : INF), v2 = (j ? dp[j - 1] + c[j - 1] : INF);
        if (v1 < v2) {
          dp[j] = v1;
          if (start[0][s[0][i]] == i && start[1][s[0][i]] > j) c[j]++;
          if (end[0][s[0][i]] == i && end[1][s[0][i]] <= j) c[j]--;
        } else {
          dp[j] = v2;
          c[j] = c[j - 1];
          if (start[1][s[1][j]] == j && start[0][s[1][j]] > i) c[j]++;
          if (end[1][s[1][j]] == j && end[0][s[1][j]] <= i) c[j]--;
        }
      }
    }
    cout << dp[len[1]] << '\n';
  }
}
