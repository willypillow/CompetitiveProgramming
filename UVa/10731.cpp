#include <bits/stdc++.h>
using namespace std;

vector<vector<int> > g1, g2, scc;
vector<int> fin;
bool use[26], vis[26];

void dfs(int x) {
	vis[x] = true;
	for (int y: g1[x]) {
		if (!vis[y]) dfs(y);
	}
	fin.push_back(x);
}

void dfsScc(int x, int curCol) {
	vis[x] = true;
	scc[curCol].push_back(x);
	for (int y: g2[x]) {
		if (!vis[y]) {
			dfsScc(y, curCol);
		}
	}
}

int main() {
	bool first = true;
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	for (int n; cin >> n >> ws && n; ) {
		if (!first) cout << '\n';
		first = false;
		scc.clear(); fin.clear();
		g1.clear(); g2.clear(); g1.resize(26); g2.resize(26);
		memset(use, false, sizeof(use));
		for (int i = 0; i < n; i++) {
			char chr[6];
			for (int j = 0; j < 6; j++) cin >> chr[j];
			for (int j = 0; j < 5; j++) {
				g1[chr[j] - 'A'].push_back(chr[5] - 'A');
				g2[chr[5] - 'A'].push_back(chr[j] - 'A');
				use[chr[j] - 'A'] = use[chr[5] - 'A'] = true;
			}
		}
		memset(vis, false, sizeof(vis));
		for (int i = 0; i < 26; i++) {
			if (use[i] && !vis[i]) dfs(i);
		}
		int col = 0;
		memset(vis, false, sizeof(vis));
		for (int i = fin.size() - 1; i >= 0; i--) {
			if (!vis[fin[i]]) { scc.resize(col + 1); dfsScc(fin[i], col++); }
		}
		for (int i = 0; i < col; i++) sort(scc[i].begin(), scc[i].end());
		sort(scc.begin(), scc.end());
		for (int i = 0; i < col; i++) {
			cout << (char)(scc[i][0] + 'A');
			for (int j = 1; j < scc[i].size(); j++) cout << ' ' << (char)(scc[i][j] + 'A');
			cout << '\n';
		}
	}
}

