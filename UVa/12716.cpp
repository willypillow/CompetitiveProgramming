#include <iostream>
#include <cmath>
using std::cin; using std::cout;

const int MAXN = 30000000;
int arr[MAXN + 1];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
  for (int c = 1; c <= MAXN; c++) {
    for (int a = c + c; a <= MAXN; a += c) {
      if ((a ^ (a - c)) == c) arr[a]++;
    }
  }
  for (int i = 1; i <= MAXN; i++) arr[i] += arr[i - 1];
	int t; cin >> t;
  for (int cases = 1; cases <= t; cases++) {
		int n; cin >> n;
    cout << "Case " << cases << ": " << arr[n] << '\n';
  }
}
