#include <bits/stdc++.h>
using namespace std;

long long mat[110][110];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	int cases = 0;
	int t; cin >> t;
	while (t--) {
		cin.ignore(100, '=');
		int n; cin >> n;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) cin >> mat[i][j];
		}
		bool fail = false;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (mat[i][j] != mat[n - i - 1][n - j - 1] || mat[i][j] < 0) {
					fail = true;
					goto End;
				}
			}
		}
End:
		cout << "Test #" << ++cases << ": " << (fail ? "Non-symmetric.\n" : "Symmetric.\n");
	}
}
