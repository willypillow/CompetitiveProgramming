#include <iostream>
#include <cstring>
#include <cstdio>

int getChr() {
  static char buf[1 << 20], *p = buf, *end = buf;
  if (p == end) {
    if ((end = buf + fread(buf, 1, 1 << 20, stdin)) == buf) return -1;
    p = buf;
  }
  return *p++;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int wc = 0;
  bool getAlpha = false;
  char c;
  while ((c = getChr()) != -1) {
    if (c == '\n') {
      std::cout << (getAlpha ? wc + 1 : wc) << '\n';
      wc = 0;
      getAlpha = false;
    } else if (c == ' ' || !isalpha(c)) {
      getAlpha ? wc++ : wc;
      getAlpha = false;
    } else {
      getAlpha = true;
    }
  }
}

