#include <iostream>
#include <cmath>
using std::cin; using std::cout;

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int t; cin >> t;
  while (t--) {
    int n, m;
    cin >> n >> m;
    cout << (int)(ceil((double)(n - 2) / 3) * ceil((double)(m - 2) / 3)) << '\n';
  }
}
