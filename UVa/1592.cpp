#include <iostream>
#include <map>

std::map<std::string, int> cache;
int top;

inline int toId(const std::string &str) {
  if (cache.count(str)) return cache[str];
  else return cache[str] = top++;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n, m;
  std::map<long long, int> strSet[10][10];
  while (std::cin >> n >> m) {
    std::cin.get(); // Ignore '\n'
    cache.clear();
    top = 0;
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < i; j++) strSet[i][j].clear();
    }
    int ansR1, ansR2 = 0, ansC1, ansC2;
    for (int i = 0; i < n; i++) {
      std::string str;
      int ids[10];
      for (int j = 0; j < m; j++) {
        if (j == m - 1) std::getline(std::cin, str);
        else std::getline(std::cin, str, ',');
        if (!ansR2) {
          ids[j] = toId(str);
          for (int k = 0; k < j; k++) {
            long long d = (ids[k] * 100000) + ids[j];
            if (strSet[j][k].count(d)) {
              ansR1 = strSet[j][k][d] + 1;
              ansR2 = i + 1;
              ansC1 = k + 1;
              ansC2 = j + 1;
              break;
            }
            else strSet[j][k][d] = i;
          }
        }
      }
    }
    if (ansR2) {
      std::cout << "NO\n" <<
        ansR1 << ' ' << ansR2 << '\n' << ansC1 << ' ' << ansC2 << '\n';
    } else {
      std::cout << "YES\n";
    }
  }
}
