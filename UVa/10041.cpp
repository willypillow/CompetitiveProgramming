#include <iostream>
#include <algorithm>
#include <cmath>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  std::cin >> n;
  while (n--) {
  	int r, s[512];
  	std::cin >> r;
  	for (int i = 0; i < r; i++) std::cin >> s[i];
  	std::sort(s, s + r);
  	double m = 0, ans = 0;
  	if (r & 1) m = s[r / 2];
  	else m = (s[r / 2 - 1] + s[r / 2]) / 2;
  	for (int i = 0; i < r; i++) ans += abs(m - s[i]);
  	std::cout << (int)ans << '\n';
  }
}
