#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n, b, h, w;
  while (std::cin >> n >> b >> h >> w) {
    int ans = b + 1;
    for (int i = 0; i < h; i++) {
      int p;
      std::cin >> p;
      if (p * n < ans) {
        for (int j = 0; j < w; j++) {
          int a;
          std::cin >> a;
          if (a >= n) {
            ans = p * n;
            std::cin.ignore(1000, '\n');
            break;
          }
        }
      } else {
        std::cin.ignore(1000, '\n'); std::cin.ignore(1000, '\n');
      }
    }
    if (ans == b + 1) std::cout << "stay home\n";
    else std::cout << ans << '\n';
  }
}
