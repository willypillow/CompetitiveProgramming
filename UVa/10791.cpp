#include <iostream>
#include <cmath>
using std::cin; using std::cout;

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int n, cases = 1;
  while (cin >> n && n) {
    long long int ans = 0;
    if (n == 1) {
      ans = 2;
    } else {
      int dup = 0, max = ceil(sqrt(n));
      for (int i = 2; i <= max; i++) {
        if (n % i == 0) {
          dup++;
          int tmp = 1;
          while (n % i == 0) {
            n /= i;
            tmp *= i;
          }
          ans += tmp;
        }
      }
      if (n > 1) {
        dup++;
        ans += n;
      }
      if (dup <= 1) ans++;
    }
    cout << "Case " << cases++ << ": " << ans << '\n';
  }
}
