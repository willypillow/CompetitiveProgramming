#include <iostream>
#include <algorithm>
using std::cin; using std::cout; using std::lower_bound; using std::upper_bound;

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int squares[400], sTop = 0;
  for (int i = 1; i * i <= 100000; i++) squares[sTop++] = i * i;
  for (int a, b; cin >> a >> b && a; ) {
    cout << (upper_bound(squares, squares + sTop, b) -
      lower_bound(squares, squares + sTop, a)) << '\n';
  }
}
