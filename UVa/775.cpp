#include <bits/stdc++.h>
using namespace std;

int a[260];
bool g[260][260];

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	for (int n; cin >> n; ) {
		memset(g, false, sizeof(g));
		string str;
		while (cin >> str && str[0] != '%') {
			stringstream ss(str);
			int u, v; ss >> u >> v;
			g[u][v] = g[v][u] = true;
		}
		for (int i = 1; i <= n; i++) a[i] = i;
		for (bool continu = false; continu; ) {
			for (int i = 2; i <= n; i++) {
				if (!g[a[i - 1]][a[i]]) {
					for (int j = 2; j <= n; j++) {
						if (i == j || i - 1 == j || i == j - 1) continue;
						if (g[a[i]][a[j]] && g[a[i - 1]][a[j - 1]]) {
							swap(a[i], a[j - 1]);
							continu = true;
							break;
						}
					}
				}
			}
		}
		for (int i = 1; i <= n; i++) cout << a[i] << ' ';
		cout << a[1] << '\n';
	}
}
