#include <iostream>
#include <cstring>
#include <unordered_map>
using std::cin; using std::cout; using std::unordered_map; using std::hash;

struct Node {
  unsigned int left, right, strHash;
  char name[10];
  bool operator ==(const Node &rhs) const {
    return (left == rhs.left) && (right == rhs.right) && (strHash == rhs.strHash);
  }
} nodes[50001];
namespace std {
  template <> struct hash<Node> {
    size_t operator()(const Node &n) const {
      int hash = 17;
      hash = hash * 31 + n.strHash;
      hash = hash * 31 + n.left;
      hash = hash * 31 + n.right;
      return hash;
    }
  };
}

unordered_map<Node, unsigned int> idMap;
unsigned int strPos, cnt, t, vis[50001];
char str[500000];

unsigned int build() {
  unsigned int id = ++cnt;
  Node &cur = nodes[id];
  cur.left = cur.right = cur.strHash = 0;
  unsigned int i;
  for (i = 0; isalpha(str[strPos]); i++, strPos++) {
    cur.name[i] = str[strPos];
    cur.strHash = (cur.strHash << 5) | (cur.name[i] - 'a' + 1);
  }
  cur.name[i] = '\0';
  if (str[strPos] == '(') {
    strPos++;
    cur.left = build();
    strPos++;
    cur.right = build();
    strPos++;
  }
  if (idMap.count(cur)) {
    cnt--;
    return idMap[cur];
  }
  return idMap[cur] = id;
}

void print(unsigned int root) {
  if (vis[root] == t) {
    cout << root;
    return;
  }
  vis[root] = t;
  cout << nodes[root].name;
  if (nodes[root].left) {
    cout << '(';
    print(nodes[root].left);
    cout << ',';
    print(nodes[root].right);
    cout << ')';
  }
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  cin >> t; t++;
  cin.ignore(1000, '\n');
  while (--t) {
    cin.getline(str, 500000);
    idMap.clear();
    cnt = strPos = 0;
    print(build());
    cout << '\n';
  }
}
