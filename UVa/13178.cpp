#include <iostream>
using namespace std;

int main()  {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		long long n; cin >> n;
		long long x = n * (n + 1) / 2;
		if (x % 3) cout << "NO\n";
		else cout << "YES\n";
	}
}
