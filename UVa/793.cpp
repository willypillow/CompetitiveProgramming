#include <bits/stdc++.h>
using namespace std;

struct SplayNode {
	static SplayNode HOLE;
	SplayNode *ch[2], *par;
	bool rev;
	SplayNode(): par(&HOLE), rev(false) { ch[0] = ch[1] = &HOLE; }
	bool isRoot() {
		return (par->ch[0] != this && par->ch[1] != this);
	}
	void push() {
		if (rev) {
			if (ch[0]) ch[0]->rev ^= 1;
			if (ch[1]) ch[1]->rev ^= 1;
			swap(ch[0], ch[1]);
			rev ^= 1;
		}
	}
	void pushFromRoot() {
		if (!isRoot()) par->pushFromRoot();
		push();
	}
	void pull() { }
	void rotate() {
		SplayNode *p = par, *gp = p->par;
		bool dir = (p->ch[1] == this);
		par = gp;
		if (!p->isRoot()) gp->ch[gp->ch[1] == p] = this;
		p->ch[dir] = ch[dir ^ 1];
		p->ch[dir]->par = p;
		p->par = this;
		ch[dir ^ 1] = p;
		p->pull(), pull();
	}
	void splay() {
		pushFromRoot();
		while (!isRoot()) {
			if (!par->isRoot()) {
				SplayNode *gp = par->par;
				if ((gp->ch[0] == par) == (par->ch[0] == this)) rotate();
				else par->rotate();
			}
			rotate();
		}
	}
} SplayNode::HOLE;

namespace LCT {
	SplayNode *access(SplayNode *x) {
		SplayNode *last = &SplayNode::HOLE;
		while (x != &SplayNode::HOLE) {
			x->splay();
			x->ch[1] = last;
			x->pull();
			last = x;
			x = x->par;
		}
		return last;
	}
	void makeRoot(SplayNode *x) {
		access(x);
		x->splay();
		x->rev ^= 1;
	}
	void link(SplayNode *x, SplayNode *y) {
		makeRoot(x);
		x->par = y;
	}
	void cut(SplayNode *x, SplayNode *y) {
		makeRoot(x);
		access(y);
		y->splay();
		y->ch[0] = &SplayNode::HOLE;
		x->par = &SplayNode::HOLE;
	}
	void cutParent(SplayNode *x) {
		access(x);
		x->splay();
		x->ch[0]->par = &SplayNode::HOLE;
		x->ch[0] = &SplayNode::HOLE;
	}
	SplayNode *findRoot(SplayNode *x) {
		x = access(x);
		while (x->ch[0] != &SplayNode::HOLE) x = x->ch[0];
		x->splay();
		return x;
	}
}

SplayNode *node[1000000], pool[1000000];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int t; cin >> t;
	while (t--) {
		int n; cin >> n >> std::ws;
		SplayNode *ptr = pool;
		string in;
		for (int i = 1; i <= n; i++) node[i] = new(ptr++) SplayNode();
		int success = 0, fail = 0;
		while (getline(cin, in) && in.size()) {
			stringstream ss(in);
			char c; ss >> c;
			int x, y; ss >> x >> y;
			auto rX = LCT::findRoot(node[x]), rY = LCT::findRoot(node[y]);
			if (c == 'c') {
				if (rX != rY) LCT::link(node[x], node[y]);
			} else {
				if (rX == rY) success++;
				else fail++;
			}
		}
		cout << success << ',' << fail << '\n';
		if (t) cout << endl;
	}
}
