#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  int a, l, cases = 0;
  while (std::cin >> a >> l && a >= 0) {
    long long n = a, cnt = 1;
    while (n != 1) {
      if (n & 1) n = 3 * n + 1;
      else n >>= 1;
      if (n > l) break;
      cnt++;
    }
    std::cout << "Case " << ++cases << ": A = " << a << ", limit = " << l << ", number of terms = " << cnt << '\n';
  }
}
