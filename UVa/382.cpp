#include <iostream>
#include <iomanip>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  std::cout << "PERFECTION OUTPUT\n";
  int x;
  while (std::cin >> x && x) {
    int sum = 0;
    for (int i = 1; i < x; i++) {
      if (x % i == 0) sum += i;
    }
    std::cout << std::setw(5) << x;
    if (sum > x) std::cout << "  ABUNDANT\n";
    else if (sum < x) std::cout << "  DEFICIENT\n";
    else std::cout << "  PERFECT\n";
  }
  std::cout << "END OF OUTPUT\n";
}
