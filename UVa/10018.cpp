#include <iostream>
#include <cstdio>
#include <cstring>

static inline bool isPalindrome(unsigned int n) {
  char num[11];
  sprintf(num, "%u", n);
  int len = strlen(num);
  int loop = len / 2;
  for (int i = 0; i < loop; i++) {
    if (num[i] != num[len - i - 1]) return false;
  }
  return true;
}

static inline int rev(unsigned int n) {
  int newN = 0;
  while (n) {
    newN = newN * 10 + n % 10;
    n /= 10;
  }
  return newN;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  unsigned int t;
  std::cin >> t;
  while (t--) {
    unsigned int n, i = 0;
    std::cin >> n;
    do {
      n = n + rev(n);
      i++;
    } while (!isPalindrome(n));
    std::cout << i << ' ' << n << '\n';
  }
}
