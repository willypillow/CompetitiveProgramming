#include <iostream>
#include <algorithm>
#include <cstdio>

struct Building {
  int x, y, w, d, h, id;
  bool operator <(const Building &rhs) {
    return (x != rhs.x) ? (x < rhs.x) : (y < rhs.y);
  }
} bs[200], ans[200];

inline bool isVisible(int building, int x, int n) {
  if (bs[building].x > x || bs[building].x + bs[building].w < x) return false;
  for (int i = 0; i < n; i++) {
    if (bs[i].y < bs[building].y && bs[i].h >= bs[building].h &&
      bs[i].x <= x/*bs[building].x*/ &&
      bs[i].x + bs[i].w >= x /*bs[building].x + bs[building].w*/) {
      return false;
    }
  }
  return true;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int cases = 0, n, xs[200];
  while (std::cin >> n && n) {
    if (cases) std::cout << '\n';
    for (int i = 0; i < n; i++) {
      std::cin >> bs[i].x >> bs[i].y >> bs[i].w >> bs[i].d >> bs[i].h;
      xs[i + i] = bs[i].x;
      xs[i + i + 1] = bs[i].x + bs[i].w;
      bs[i].id = i;
    }
    std::sort(xs, xs + n + n);
    int xCnt = std::unique(xs, xs + n + n) - xs, ansTop = 0;
    for (int i = 0; i < n; i++) {
      bool visible = false;
      for (int j = 1; j < xCnt; j++) {
        if (isVisible(i, (xs[j] + xs[j - 1]) / 2, n)) {
          visible = true;
          break;
        }
      }
      if (visible) ans[ansTop++] = bs[i];
    }
    std::sort(ans, ans + ansTop);
    std::cout << "For map #" << ++cases << ", the visible buildings are numbered as follows:\n";
    for (int i = 0; i < ansTop - 1; i++) std::cout << ans[i].id + 1 << ' ';
    std::cout << ans[ansTop - 1].id + 1 << '\n';
  }
}
