#include <bits/stdc++.h>
using namespace std;

int dp[3000], par[30][3000], w[30];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n;
	while (cin >> n) {
		memset(dp, 0, sizeof(dp));
		memset(par, 0, sizeof(par));
		int tracks; cin >> tracks;
		for (int i = 1; i <= tracks; i++) cin >> w[i];
		for (int i = 1; i <= tracks; i++) {
			for (int j = n; j >= w[i]; j--) {
				if (dp[j] < dp[j - w[i]] + w[i]) {
					dp[j] = dp[j - w[i]] + w[i];
					par[i][j] = i;
				}
			}
		}
		int ans = 0;
		for (int i = 0; i <= n; i++) ans = max(ans, dp[i]);
		for (int i = tracks, j = ans; i > 0; i--) {
			if (par[i][j] == i) {
				cout << w[i] << ' ';
				j -= w[i];
			}
		}
		cout << "sum:" << ans << '\n';
	}
}
