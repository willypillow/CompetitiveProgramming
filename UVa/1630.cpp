#include <bits/stdc++.h>
using namespace std;

const int INF = 0x3f3f3f3f;

char str[100];
int dp[100][100], par[100][100], match[100][100];
vector<int> divs[101];

int toStrLen(int x) {
	return x < 10 ? 1 : x < 100 ? 2 : 3;
}
void print(int l, int r) {
	if (l == r) {
		cout << str[l];
	} else {
		if (par[l][r] < 1000) {
			print(l, par[l][r]); print(par[l][r] + 1, r);
		} else {
			int i = par[l][r] - 1000;
			cout << (r - l + 1) / i << '(';
			print(l, l + i - 1);
			cout << ')';
		}
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	for (int i = 1; i <= 100; i++) {
		for (int j = 1; j < i; j++) {
			if (i % j == 0) divs[i].push_back(j);
		}
	}
	while (cin >> str) {
		int n = strlen(str);
		for (int i = 0; i < n; i++) {
			for (int j = 1; i + j - 1 < n; j++) {
				int k;
				for (k = i + j; k < n; k++) {
					if (str[k] != str[k - j]) break;
				}
				match[i][j] = k;
			}
		}
		for (int i = 0; i < n; i++) dp[i][i] = 1;
		for (int len = 2; len <= n; len++) {
			for (int l = 0, r = l + len - 1; r < n; l++, r++) {
				int ans = INF;
				for (int i = l; i < r; i++) {
					int t = dp[l][i] + dp[i + 1][r];
					if (t < ans) {
						ans = t;
						par[l][r] = i;
					}
				}
				for (int i: divs[len]) {
					if (match[l][i] > r) {
						int t = toStrLen(len / i) + 2 + dp[l][l + i - 1];
						if (t < ans) {
							ans = t;
							par[l][r] = i + 1000;
						}
					}
				}
				dp[l][r] = ans;
			}
		}
		print(0, n - 1);
		cout << '\n';
	}
}
