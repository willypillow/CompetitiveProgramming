#include <iostream>
#include <cstring>

bool canvas[32][32];

int fill(int x, int y, int w) {
  char c;
  std::cin >> c;
  if (c == 'p') {
    w /= 2;
    fill(x, y, w);
    fill(x + w, y, w);
    fill(x, y + w, w);
    fill(x + w, y + w, w);
  } else if (c == 'f') {
    for (int i = 0; i < w; i++) {
      for (int j = 0; j < w; j++) canvas[x + i][y + j] = true;
    }
  }
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  std::cin >> n;
  while (n--) {
    memset(canvas, 0, sizeof(canvas));
    fill(0, 0, 32);
    fill(0, 0, 32);
    int ans = 0;
    for (int i = 0; i < 32; i++) {
      for (int j = 0; j < 32; j++) {
        if (canvas[i][j]) ans++;
      }
    }
    std::cout << "There are " << ans << " black pixels.\n";
  }
}
