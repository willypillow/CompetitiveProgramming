#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  std::cin >> n;
  for (int i = 1; i <= n; i++) {
    int a, b;
    std::cin >> a >> b;
    if (!(a & 1)) a++;
    if (!(b & 1)) b--;
    std::cout << "Case " << i << ": " << (b + a) * (b - a + 2) / 4 << '\n';
  }
}

