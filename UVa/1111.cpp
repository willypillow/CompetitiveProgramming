#include <iostream>
#include <cmath>
#include <iomanip>
#include <algorithm>
using std::cin; using std::cout; using std::sort; using std::setprecision; using std::max; using std::min;

struct P {
	int x, y;
	bool operator <(const P &rhs) const {
		return (x != rhs.x) ? (x < rhs.x) : (y < rhs.y);
	}
} p[100], hull[100];
int n;

int cross(P o, P a, P b) {
	return (a.x - o.x) * (b.y - o.y) - (a.y - o.y) * (b.x - o.x);
}

int convexHull() {
	sort(p, p + n);
	int cnt = 0;
	for (int i = 0; i < n; i++) {
		while (cnt >= 2 && cross(hull[cnt - 2], hull[cnt - 1], p[i]) <= 0) cnt--;
		hull[cnt++] = p[i];
	}
	for (int i = n - 2, t = cnt + 1; i >= 0; i--) {
		while (cnt >= t && cross(hull[cnt - 2], hull[cnt - 1], p[i]) <= 0) cnt--;
		hull[cnt++] = p[i];
	}
	return --cnt;
}

int main() {
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif
	int cases = 0;
	while (cin >> n && n) {
		for (int i = 0; i < n; i++) cin >> p[i].x >> p[i].y;
		int cn = convexHull();
		double ans = 1e30;
		for (int i = 0, j = cn - 1; i < cn; i++, j = (j + 1) % cn) {
			int a = hull[i].y - hull[j].y, b = hull[j].x - hull[i].x;
			int c = -(a * hull[i].x + b * hull[i].y);
			double div = hypot(a, b), maxDist = 0;
			for (int k = 0; k < n; k++) {
				double dist = abs(a * p[k].x + b * p[k].y + c) / div;
				maxDist = max(maxDist, dist);
			}
			ans = min(ans, maxDist);
		}
		cout << "Case " << ++cases << ": " <<
			std::fixed << setprecision(2) << ans << '\n';
	}
}
