import java.util.Scanner;
import java.util.Vector;
import java.util.List;

class Main
{
  static Vector<Vector<Integer>> v;
  // If dst = -1, return blocks above src. Otherwise, pile src over dst
  static void process (int src, int pos, int index, int dst, int dPos, int dIndex) {
    int size = v.get(pos).size();
    if (dst == -1) {
      for (int i = index + 1; i < size; i++) {
        int e = v.get(pos).get(i);
        v.get(e).add(e);
      }
      v.get(pos).subList(index + 1, size).clear();
    } else {
      for (int i = index; i < size; i++) v.get(dPos).add(v.get(pos).get(i));
      v.get(pos).subList(index, size).clear();
    }
  }
  public static void main (String[] args) throws java.lang.Exception
  {
    Scanner cin = new Scanner(System.in);
    int n = Integer.parseInt(cin.nextLine());
    v = new Vector<Vector<Integer>>(n);
    for (int i = 0; i < n; i++) {
    	v.add(new Vector<Integer>());
      v.get(i).add(i);
    }
    while (true) {
      String cmd = cin.nextLine();
      if (cmd.equals("quit")) break;
      String[] cmds = cmd.split(" ");
      int src = Integer.parseInt(cmds[1]), dst = Integer.parseInt(cmds[3]);
      if (src == dst) continue;
      int pos, index = -1, dPos, dIndex = -1;
      for (pos = 0; index == -1; pos++) index= v.get(pos).indexOf(src);
      for (dPos = 0; dIndex == -1; dPos++) dIndex = v.get(dPos).indexOf(dst);
      if (--pos == --dPos) continue;
      if (cmds[0].equals("move")) process(src, pos, index, -1, 0, 0);
      if (cmds[2].equals("onto")) process(dst, dPos, dIndex, -1, 0, 0);
      process(src, pos, index, dst, dPos, dIndex);
    }
    for (int i = 0; i < n; i++) {
      System.out.print(i + ":");
      for (int e: v.get(i)) System.out.print(" " + e);
      System.out.print("\n");
    }
  }
}
