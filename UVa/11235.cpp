#include <iostream>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;

const int MAXN = 100010, MAXA = 200010;

struct Query {
	int l, r, block, id;
	bool operator <(const Query &rhs) {
		return (block != rhs.block) ? (block < rhs.block) : (r < rhs.r);
	}
} queries[MAXN];
int cnt[MAXA], freq[MAXN], arr[MAXN], ans[MAXN], maxFreq;

void add(int x) {
	if (cnt[arr[x]] != 0) freq[cnt[arr[x]]]--;
	cnt[arr[x]]++;
	freq[cnt[arr[x]]]++;
	if (cnt[arr[x]] > maxFreq) maxFreq = cnt[arr[x]];
}

void del(int x) {
	if (cnt[arr[x]] == 0) return;
	freq[cnt[arr[x]]]--;
	if (freq[maxFreq] == 0) maxFreq--;
	cnt[arr[x]]--;
	if (cnt[arr[x]] != 0) freq[cnt[arr[x]]]++;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, q;
	while (cin >> n && n) {
		memset(cnt, 0, sizeof(cnt)); memset(freq, 0, sizeof(freq)); maxFreq = 0;
		cin >> q;
		for (int i = 0; i < n; i++) {
			cin >> arr[i];
			arr[i] += 1E5;
		}
		int bSize = sqrt(n);
		for (int i = 0; i < q; i++) {
			cin >> queries[i].l >> queries[i].r;
			queries[i].l--; queries[i].r--;
			queries[i].block = queries[i].l / bSize;
			queries[i].id = i;
		}
		sort(queries, queries + q);
		int l = 0, r = -1;
		for (int i = 0; i < q; i++) {
			while (r < queries[i].r) add(++r);
			while (r > queries[i].r) del(r--);
			while (l < queries[i].l) del(l++);
			while (l > queries[i].l) add(--l);
			ans[queries[i].id] = maxFreq;
		}
		for (int i = 0; i < q; i++) cout << ans[i] << '\n';
	}
}
