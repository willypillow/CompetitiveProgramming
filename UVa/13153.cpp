#include <iostream>
#include <vector>
#include <set>
using namespace std;

const int MAXN = 1E6;

int divs[MAXN + 10], par[MAXN + 10];
vector<int> primes;

static inline int_fast32_t fastAtoi(const char *p, uint_fast32_t len) {
	uint_fast32_t res = 0;
	uint_fast8_t neg = *p == '-';
	if (neg) p++, len--;
	switch (len) {
		case 10: res += (*p++ & 15) * 1000000000;
		case 9: res += (*p++ & 15) * 100000000;
		case 8: res += (*p++ & 15) * 10000000;
		case 7: res += (*p++ & 15) * 1000000;
		case 6: res += (*p++ & 15) * 100000;
		case 5: res += (*p++ & 15) * 10000;
		case 4: res += (*p++ & 15) * 1000;
		case 3: res += (*p++ & 15) * 100;
		case 2: res += (*p++ & 15) * 10;
		case 1: res += (*p & 15);
	}
	return res * (neg ? -1 : 1);
}

static inline int_fast64_t atoll(const char *p, uint_fast32_t len) {
	uint_fast8_t neg = *p == '-';
	int_fast64_t ret = 0;
	if (neg) p++, len--;
	for (uint_fast8_t i = 0; i < len; i++) {
		ret = ret * 10 + (*p++ & 15);
	}
	return ret * (neg ? -1 : 1);
}

static inline bool getRawChar(char *c) {
	static char buf[1 << 20], *p = buf, *end = buf;
	if (p == end) {
		if ((end = buf + fread(buf, 1, 1 << 20, stdin)) == buf) return false;
		p = buf;
	}
	*c = *p++;
	return true;
}

static inline bool getChar(char *c) {
	while (getRawChar(c) && (*c == ' ' || *c == '\n' || *c == '\r'));
	return !(*c == ' ' || *c == '\n' || *c == '\r');
}

static inline bool getStr(char *c) {
	char *p = c;
	while (getRawChar(p) && *p != '\n' && *p != '\r' && *p != ' ') p++;
	*p = '\0';
	return (p != c);
}

static inline bool getLine(char *c) {
	char *p = c;
	while (getRawChar(p) && *p != '\n' && *p != '\r') p++;
	*p = '\0';
	return (p != c);
}

static inline bool getInt(int32_t *x) {
	static char buf[12];
	uint_fast32_t i = 0;
	while (getRawChar(buf + i)) {
		if ((unsigned)(buf[i] - '0') > 10U && buf[i] != '-') {
			if (i) break;
			else continue;
		}
		i++;
	}
	if (!i) return false;
	*x = fastAtoi(buf, i);
	return true;
}

static inline bool getLL(int64_t *x) {
	static char buf[20];
	uint_fast32_t i = 0;
	while (getRawChar(buf + i)) {
		if ((unsigned)(buf[i] - '0') > 10U && buf[i] != '-') {
			if (i) break;
			else continue;
		}
		i++;
	}
	if (!i) return false;
	*x = atoll(buf, i);
	return true;
}
int find(int x) {
	return par[x] == x ? x : par[x] = find(par[x]);
}

void join(int a, int b) {
	int pa = find(a), pb = find(b);
	if (pa != pb) par[pa] = pb;
}

void genPrime() {
	for (int i = 2; i <= MAXN; i++) {
		if (divs[i]) continue;
		primes.push_back(i);
		divs[i] = (int)primes.size();
		for (int j = i + i; j <= MAXN; j += i) divs[j] = divs[i];
	}
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	genPrime();
	int t; getInt(&t);
	for (int cases = 1; cases <= t; cases++) {
		int n; getInt(&n);
		for (int i = 0; i < n + (int)primes.size(); i++) par[i] = i;
		for (int i = 0; i < n; i++) {
			int x; getInt(&x);
			while (x != 1) {
				join(i, n + divs[x] - 1);
				x /= primes[divs[x] - 1];
			}
		}
		set<int> totalPars;
		for (int i = 0; i < n; i++) totalPars.insert(find(i));
		cout << "Case " << cases << ": " << totalPars.size() << '\n';
	}
}
