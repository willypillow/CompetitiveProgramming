#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int a, b, n;
  std::cin >> n;
  for (int i = 0; i < n; i++) {
    std::cin >> a >> b;
    std::cout << ((a > b) ? ">" : ((a == b) ? "=" : "<")) << "\n";
  }
}

