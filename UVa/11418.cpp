#include <bits/stdc++.h>
using namespace std;

const int INF = 0x3f3f3f3f;

struct Edge { int to, cap, rev; };
vector<Edge> g[1010];
int dist[1010], flowS, flowT;
bool vis[1010];
string str[1010][26];

void addEdge(int from, int to, int cap) {
	g[from].push_back({to, cap, (int)g[to].size()});
	g[to].push_back({from, 0, (int)g[from].size() - 1});
}

int bfs() {
	memset(dist, 0x3f, sizeof(dist));
	dist[flowS] = 0;
	queue<int> q;
	q.push(flowS);
	while (q.size()) {
		int cur = q.front(); q.pop();
		for (auto &e: g[cur]) {
			if (e.cap > 0 && dist[e.to] == INF) {
				dist[e.to] = dist[cur] + 1;
				if (e.to == flowT) return dist[e.to];
				q.push(e.to);
			}
		}
	}
	return INF;
}

int dfs(int cur, int f) {
	if (cur == flowT) return f;
	vis[cur] = true;
	for (auto &e: g[cur]) {
		if (e.cap > 0 && dist[cur] + 1 == dist[e.to] && !vis[e.to]) {
			int newF = dfs(e.to, min(e.cap, f));
			if (newF) {
				e.cap -= newF;
				g[e.to][e.rev].cap += newF;
				return newF;
			}
		}
	}
	return 0;
}

int dinic(int flow = 0) {
	while (bfs() < INF) {
		for (int f = INF; f;) {
			memset(vis, false, sizeof(vis));
			flow += (f = dfs(flowS, INF));
		}
	}
	return flow;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int t; cin >> t;
	for (int cases = 1; cases <= t; cases++) {
		int n; cin >> n;
		flowT = 1000;
		for (int i = flowS; i <= flowT; i++) g[i].clear();
		for (int i = 1; i <= 26; i++) addEdge(flowS, i, 1);
		for (int i = 1; i <= n; i++) {
			int cnt; cin >> cnt;
			while (cnt--) {
				string s; cin >> s;
				for (int j = 0; j < (int)s.size(); j++) {
					s[j] = char(j ? tolower(s[j]) : toupper(s[j]));
				}
				str[i][s[0] - 'A'] = s;
				addEdge(s[0] - 'A' + 1, 26 + i, 1);
			}
			addEdge(26 + i, flowT, 1);
		}
		dinic();
		cout << "Case #" << cases << ":\n";
		for (int i = 1; i <= 26; i++) {
			for (auto &e: g[i]) {
				if (e.to > 26 && !e.cap) {
					cout << str[e.to - 26][i - 1] << '\n';
					break;
				}
			}
		}
	}
}
