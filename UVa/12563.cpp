#include <iostream>
using std::cin; using std::cout; using std::min; using std::max;

int n, len[50], dp[180 * 50 + 678 + 1];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int t; cin >> t;
  for (int cases = 1; cases <= t; cases++) {
    int time;
    cin >> n >> time;
    for (int j = 1; j <= time; j++) dp[j] = -(1 << 30);
    dp[0] = 0;
    for (int i = 0; i < n; i++) cin >> len[i];
    for (int i = 0; i < n; i++) {
      for (int j = time - 1; j >= len[i]; j--) {
        dp[j] = max(dp[j], dp[j - len[i]] + 1);
      }
    }
    int ans = time - 1;
    for (int j = ans - 1; j >= 0; j--) if (dp[j] > dp[ans]) ans = j;
    cout << "Case " << cases << ": " << dp[ans] + 1 << ' ' << 678 + ans << '\n';
  }
}

