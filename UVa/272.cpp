#include <iostream>
#include <cstdio>

int getChr() {
  static char buf[1 << 20], *p = buf, *end = buf;
  if (p == end) {
    if ((end = buf + fread(buf, 1, 1 << 20, stdin)) == buf) return -1;
    p = buf;
  }
  return *p++;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  char c;
  bool isRight = false;
  while ((c = getChr()) != -1) {
    if (c == '"') std::cout << ((isRight = !isRight) ? "``" : "''");
    else std::cout << c;
  }
}

