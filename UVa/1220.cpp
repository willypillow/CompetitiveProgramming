#include <iostream>
#include <string>
#include <unordered_map>
using std::cin; using std::cout; using std::string; using std::unordered_map; using std::max;

struct P {
  int v;
  bool uniq;
  bool operator <(const P &rhs) const { return v < rhs.v; }
} const MIN = { -(1 << 30), false };

int empl[200][200], top[200];
unordered_map<string, int> idMap;
int getId(const string &s) {
  if (idMap.count(s)) return idMap[s];
  int newId = idMap.size();
  return idMap[s] = newId;
}

P dfs(int x, bool use) {
  int ans = 0;
  bool uniq = true;
  for (int i = 0; i < top[x]; i++) {
    P v1 = dfs(empl[x][i], false), v2 = (!use ? dfs(empl[x][i], true) : MIN),
      m = max(v1, v2);
    uniq &= (v1.v != v2.v) && m.uniq;
    ans += m.v;
  }
  return P { use ? ans + 1 : ans, uniq };
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  for (int n; cin >> n && n; ) {
    idMap.clear();
    for (int i = 0; i < n; i++) top[i] = 0;
    string boss, name;
    cin >> boss;
    getId(boss); // Set Big Boss as 0
    for (int i = 1; i < n; i++) {
      cin >> name >> boss;
      int bId = getId(boss);
      empl[bId][top[bId]++] = getId(name);
    }
    P v1 = dfs(0, false), v2 = dfs(0, true), m = max(v1, v2);
    cout << m.v << ' ' << (m.uniq && v1.v != v2.v ? "Yes\n" : "No\n");
  }
}
