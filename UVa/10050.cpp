#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);

  int t;
  std::cin >> t;
  while (t--) {
    int n, p, h[100], cnt = 0, calendar[3660] = { false };
    std::cin >> n >> p;
    for (int i = 0; i < p; i++) std::cin >> h[i];
    for (int i = 0; i < p; i++) {
      for (int j = 0; j <= n; j += h[i]) {
        if (j % 7 != 6 && j % 7 != 0) calendar[j] = true;
      }
    }
    for (int i = 1; i <= n; i++) {
      if (calendar[i]) cnt++;
    }
    std::cout << cnt << '\n';
  }
}
