#include <iostream>
#include <cstring>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  char str[100];
  while (std::cin.getline(str, 100)) {
    if (str[0] == '0' && str[1] == '\0') break;
    int ans = 0, len = strlen(str);
    for (int i = len; i; i--) {
      ans += (str[len - i] - '0') * ((1 << i) - 1);
    }
    std::cout << ans << '\n';
  }
}
