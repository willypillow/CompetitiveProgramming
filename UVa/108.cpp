#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n, arr[100][100], sumArr[100][101] = { 0 };
  while (std::cin >> n) {
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        int in;
        std::cin >> in;
        arr[i][j] = in;
        sumArr[i][j + 1] = sumArr[i][j] + in;
      }
    }
    int max = -100000;
    for (int i = 0; i < n; i++) {
      for (int j = i + 1; j <= n; j++) {
        int sum = 0;
        for (int k = 0; k < n; k++) {
          sum += sumArr[k][j] - sumArr[k][i];
          if (sum > max) max = sum;
          if (sum < 0) sum = 0;
        }
      }
    }
    std::cout << max << '\n';
  }
}
