#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t >> ws;
	while (t--) {
		string s; getline(cin, s);
		for (auto it = s.begin(); it != s.end(); ) {
			if (*it == ' ') it = s.erase(it);
			else it++;
		}
		int sum = 0;
		for (int i = 0; i < 16; i += 2) {
			int x = 2 * (s[i] - '0');
			if (x >= 10) sum += x / 10 + x % 10;
			else sum += x;
		}
		for (int i = 1; i < 16; i += 2) sum += s[i] - '0';
		cout << (sum % 10 ? "Invalid\n" : "Valid\n");
	}
}
