#include <iostream>
#include <cstring>

int train[210][60][2], dp[210][60], n, tEnd, t[60];

void initTrain(int type) {
  int m;
  std::cin >> m;
  for (int i = 0; i < m; i++) {
    int time;
    std::cin >> time;
    if (type == 0) {
      for (int i = 0; i < n && time < 210; i++) {
        train[time][i][type] = true;
        if (i != n - 1) time += t[i];
      }
    } else {
      for (int i = n - 1; i >= 0 && time < 210; i--) {
        train[time][i][type] = true;
        if (i != 0) time += t[i - 1];
      }
    }
  }
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif 
  int cases = 0;
  while (std::cin >> n && n) {
    memset(dp, 0x6f, sizeof(dp));
    memset(train, false, sizeof(train));
    std::cin >> tEnd;
    for (int i = 0; i < n - 1; i++) std::cin >> t[i];
    initTrain(0); initTrain(1);
    dp[tEnd][n - 1] = 0;
    for (int time = tEnd - 1; time >= 0; time--) {
      for (int stat = 0; stat < n; stat++) {
        dp[time][stat] = dp[time + 1][stat] + 1;
        if (train[time][stat][0] && stat != n - 1 && time + t[stat] <= tEnd) {
          dp[time][stat] = std::min(dp[time][stat],
            dp[time + t[stat]][stat + 1]);
        }
        if (train[time][stat][1] && stat != 0 && time + t[stat - 1] <= tEnd) {
          dp[time][stat] = std::min(dp[time][stat],
            dp[time + t[stat - 1]][stat - 1]);
        }
      }
    }
    std::cout << "Case Number " << ++cases << ": ";
    if (dp[0][0] >= 1 << 30) std::cout << "impossible\n";
    else std::cout << dp[0][0] << '\n';
  }
}
