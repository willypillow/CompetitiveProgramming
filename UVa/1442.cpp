#include <iostream>

int p[1000000], s[1000000], lHeight[1000000];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int z; std::cin >> z;
  while (z--) {
    int n, ans = 0;
    std::cin >> n;
    for (int i = 0; i < n; i++) std::cin >> p[i];
    for (int i = 0; i < n; i++) std::cin >> s[i];
    int h = s[0];
    for (int i = 0; i < n; i++) {
      if (h < p[i]) h = p[i];
      else if (h > s[i]) h = s[i];
      lHeight[i] = h;
    }
    for (int i = n - 1; i >= 0; i--) {
      if (h < p[i]) h = p[i];
      else if (h > s[i]) h = s[i];
      ans += std::min(h, lHeight[i]) - p[i];
    }
    std::cout << ans << '\n';
  }
}

