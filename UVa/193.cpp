#include <bits/stdc++.h>
using namespace std;

int ans = 0, n, m;
bool use[101], ansArr[101];
vector<int> g[101];

void dfs(int x, int res = 0) {
	if (x == n + 1) {
		if (res > ans) ans = res, memcpy(ansArr, use, sizeof(use));
		return;
	}
	bool can = true;
	for (int y: g[x]) can &= !use[y];
	if (can) {
		use[x] = true;
		dfs(x + 1, res + 1);
	}
	use[x] = false;
	dfs(x + 1, res);
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		cin >> n >> m;
		for (int i = 0; i <= n; i++) g[i].clear(), use[i] = false;
		ans = 0;
		for (int i = 0; i < m; i++) {
			int u, v; cin >> u >> v;
			g[u].push_back(v), g[v].push_back(u);
		}
		dfs(1);
		cout << ans << '\n';
		for (int i = 0, k = 0; i <= n; i++) {
			if (ansArr[i]) cout << (++k == 1 ? "" : " ") << i;
		}
		cout << '\n';
	}
}
