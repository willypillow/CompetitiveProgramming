#include <iostream>
#include <algorithm>
using std::cin; using std::cout; using std::sort;

struct P {
  char alpha;
  int count;
  bool operator <(const P &rhs) const {
    return (count != rhs.count) ? (count > rhs.count) : (alpha < rhs.alpha);
  }
} freq[52];
char str[10000000];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  while (cin.getline(str, 10000000)) {
    for (int i = 0; i < 26; i++) freq[i] = P { (char)(i + 'a'), 0 };
    for (int i = 0; i < 26; i++) freq[i + 26] = P { (char)(i + 'A'), 0 };
    for (char *p = str; *p != '\0'; p++) {
      if (!isalpha(*p)) continue;
      int id = isupper(*p) ? (*p - 'A' + 26) : (*p - 'a');
      freq[id].count++;
    }
    std::sort(freq, freq + 52);
    for (int i = 0; freq[i].count == freq[0].count && i < 52; i++) {
      cout << freq[i].alpha;
    }
    cout << ' ' << freq[0].count << '\n';
  }
}
