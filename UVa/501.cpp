#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace std;
using namespace __gnu_pbds;

typedef pair<int, int> PII;
typedef tree<PII, null_type, less<PII>, rb_tree_tag,
	tree_order_statistics_node_update> Set;

int arr[30010], u[30010];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		int m, n; cin >> m >> n;
		Set s;
		for (int i = 0; i < m; i++) cin >> arr[i];
		for (int i = 0; i < n; i++) cin >> u[i];
		for (int ui = 0, ai = 0, ctr = 0;; ) {
			if (ui < n && s.size() == u[ui]) {
				ui++;
				cout << s.find_by_order(ctr++)->first << '\n';
			} else {
				if (ai >= m) break;
				s.insert({ arr[ai], ai });
				ai++;
			}
		}
		if (t) cout << '\n';
	}
}
