#include <iostream>
using std::cout; using std::cin;

bool notPrime[1000001] = { false };
int primes[10000], pTop = 0;

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  for (int i = 2; i <= 1000; i++) {
    if (notPrime[i]) continue;
    for (int j = i + i; j <= 1000000; j += i) {
      notPrime[j] = true;
    }
  }
  for (int i = 2; i <= 1000000; i++) {
    if (!notPrime[i]) primes[pTop++] = i;
  }
  int n;
  while (cin >> n && n) {
    int ans = 0;
    cout << n << " : ";
    for (int i = 0; n != 1 && i < pTop; i++) {
      if (n % primes[i] == 0) {
        ans++;
        do {
          n /= primes[i];
        } while (n % primes[i] == 0);
      }
    }
    cout << ans << '\n';
  }
}
