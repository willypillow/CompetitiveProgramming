#include <iostream>

char str[10][200] = { "", "1\n", "1\n22\n1\n", "1\n22\n333\n22\n1\n", "1\n22\n333\n4444\n333\n22\n1\n", "1\n22\n333\n4444\n55555\n4444\n333\n22\n1\n", "1\n22\n333\n4444\n55555\n666666\n55555\n4444\n333\n22\n1\n", "1\n22\n333\n4444\n55555\n666666\n7777777\n666666\n55555\n4444\n333\n22\n1\n", "1\n22\n333\n4444\n55555\n666666\n7777777\n88888888\n7777777\n666666\n55555\n4444\n333\n22\n1\n", "1\n22\n333\n4444\n55555\n666666\n7777777\n88888888\n999999999\n88888888\n7777777\n666666\n55555\n4444\n333\n22\n1\n" };

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  std::cin >> n;
  while (n--) {
    int a, f;
    std::cin >> a >> f;
    if (f) {
      while (f--) {
        std::cout << str[a];
        if (f) std::cout << '\n';
      }
      if (n) std::cout << '\n';
    }
  }
}
