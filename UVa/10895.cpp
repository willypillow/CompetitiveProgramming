#include <bits/stdc++.h>
using namespace std;

struct E {
	int r, c, v;
	bool operator <(const E &rhs) const {
		return r < rhs.r;
	}
};
vector<E> elements;

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int m, n;
	while (cin >> m >> n) {
		elements.clear();
		for (int i = 1, r; i <= m; i++) {
			int t[100];
			cin >> r;
			for (int j = 0; j < r; j++) cin >> t[j];
			for (int j = 0, v; j < r; j++) {
				cin >> v;
				elements.push_back(E { t[j], i, v });
			}
		}
		sort(elements.begin(), elements.end());
		cout << n << ' ' << m << '\n';
		for (unsigned int i = 1, front = 0, rear = 0; i <= n; i++) {
			while (rear < elements.size() && elements[rear].r == i) rear++;
			cout << rear - front;
			for (unsigned int j = front; j < rear; j++) cout << ' ' << elements[j].c;
			cout << '\n';
			if (front < rear) cout << elements[front].v;
			for (unsigned int j = front + 1; j < rear; j++) cout << ' ' << elements[j].v;
			cout << '\n';
			front = rear;
		}
	}
}
