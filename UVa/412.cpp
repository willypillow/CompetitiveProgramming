#include <iostream>
#include <iomanip>
#include <cmath>

int gcd(int m, int n) {
  if (n == 0) return m;
  return gcd(n, m % n);
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  int n;
  while (std::cin >> n && n) {
    int arr[50], cnt = 0;
    for (int i = 0; i < n; i++) std::cin >> arr[i];
    for (int i = 0; i < n; i++) {
      for (int j = i + 1; j < n; j++) {
        if (gcd(arr[i], arr[j]) == 1) cnt++;
      }
    }
    if (!cnt) std::cout << "No estimate for this data set.\n";
    else {
      std::cout << std::fixed << std::setprecision(6) <<
        sqrt(6.0 * (n * (n - 1) / 2) / cnt) << '\n';
    }
  }
}
