#include <bits/stdc++.h>
using namespace std;

const int MAXN = 110, INF = 0x3f3f3f3f;

vector<int> big, small;
int dist[MAXN], dp[MAXN][MAXN];

int solve(int bL, int bR, int sL, int sR) {
	int a = big[bL], b = a, ret = 0;
	for (int i = sL; i <= sR; i++) {
		if (a < b) {
			ret = max(ret, small[i] - a);
			a = small[i];
		} else {
			ret = max(ret, small[i] - b);
			b = small[i];
		}
	}
	ret = max({ ret, big[bR] - a, big[bR] - b });
	return ret;
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t; cin >> t;
	for (int cases = 1; cases <= t; cases++) {
		int n, d; cin >> n >> d;
		big.clear(); small.clear();
		big.push_back(0);
		for (int i = 0; i < n; i++) {
			char s[20]; cin >> s;
			if (s[0] == 'B') big.push_back(atoi(s + 2));
			else small.push_back(atoi(s + 2));
		}
		big.push_back(d);
		int ans = 0;
		for (int i = 1, j = 0; i < (int)big.size(); i++) {
			int k = j;
			while (k < (int)small.size() && small[k] < big[i]) k++;
			ans = max(ans, solve(i - 1, i, j, k - 1));
			j = k;
		}
		cout << "Case " << cases << ": " << ans << '\n';
	}
}
