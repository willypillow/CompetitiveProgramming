#include <iostream>
#include <cstring>

bool edge[52][52];

enum State { UNVISITED, VISITING, VISITED };
State state[52];

inline int nodeToId(char *str, int i) {
  int x = i * 2;
  if (str[x] == '0') return 52;
  return (str[x] - 'A') * 2 + (str[x + 1] == '+');
}

inline void buildEdge(char *str) {
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      if (i == j) continue;
      int idI = nodeToId(str, i), idJ = nodeToId(str, j);
      if (idI == 52 || idJ == 52) continue;
      edge[idI][idJ ^ 1] = true;
    }
  }
}

bool isLoop(int i) {
  state[i] = VISITING;
  for (int j = 0; j < 52; j++) {
    if (!edge[i][j]) continue;
    if (state[j] == VISITING || state[j] == UNVISITED && isLoop(j)) return true;
  }
  state[i] = VISITED;
  return false;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  while (std::cin >> n) {
    memset(edge, false, sizeof(edge));
    memset(state, UNVISITED, sizeof(state));
    for (int i = 0; i < n; i++) {
      char tmp[10];
      std::cin >> tmp;
      buildEdge(tmp);
    }
    bool loop = false;
    for (int i = 0; i < 52; i++) {
      if (state[i] == UNVISITED && isLoop(i)) { loop = true; break; }
    }
    if (loop) puts("unbounded");
    else puts("bounded");
  }
}
