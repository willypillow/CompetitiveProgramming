#include <iostream>
#include <cstring>
#include <algorithm>
using std::cin; using std::cout; using std::sort; using std::max;

int n;

struct Block { int h[3]; } blks[30];

int dp[30][3];
int dfs(int id, int vert) {
  if (dp[id][vert]) return dp[id][vert];
  int h[2];
  for (int i = 0, k = 0; i < 3; i++) {
    if (i == vert) continue;
    h[k++] = blks[id].h[i];
  }
  int ans = 0;
  for (int i = 0; i < n; i++) {
    if (blks[i].h[0] < h[0] && blks[i].h[1] < h[1]) {
      ans = std::max(ans, dfs(i, 2));
    }
    if (blks[i].h[0] < h[0] && blks[i].h[2] < h[1]) {
      ans = std::max(ans, dfs(i, 1));
    }
    if (blks[i].h[1] < h[0] && blks[i].h[2] < h[1]) {
      ans = std::max(ans, dfs(i, 0));
    }
  }
  return dp[id][vert] = ans + blks[id].h[vert];
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int cases = 0;
  while (cin >> n && n) {
    memset(dp, 0, sizeof(dp));
    for (int i = 0; i < n; i++) {
      cin >> blks[i].h[0] >> blks[i].h[1] >> blks[i].h[2];
      sort(blks[i].h, blks[i].h + 3);
    }
    int ans = 0;
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < 3; j++) {
        ans = max(ans, dfs(i, j));
      }
    }
    cout << "Case " << ++cases << ": maximum height = " << ans << '\n';
  }
}
