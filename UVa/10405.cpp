#include <iostream>
#include <cstring>
using std::cin; using std::cout; using std::max;

int dp[1001];
char str1[1002], str2[1002];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  while (cin.getline(str1 + 1, 1000) && cin.getline(str2 + 1, 1000)) {
    int len1 = strlen(str1 + 1), len2 = strlen(str2 + 1);
    for (int j = 0; j <= len2; j++) dp[j] = 0;
    for (int i = 1; i <= len1; i++) {
      int last = 0, val;
      for (int j = 1; j <= len2; j++) {
        if (str1[i] == str2[j]) val = last + 1;
        else val = max(dp[j - 1], dp[j]);
        last = dp[j];
        dp[j] = val;
      }
    }
    cout << dp[len2] << '\n';
  }
}
