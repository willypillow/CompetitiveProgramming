#include <iostream>

static const int coin[] = { 1, 5, 10, 25, 50 };
int dp[5][7500];

int solve(int type, int n) {
  if (type == 0) return 1;
  if (dp[type][n]) return dp[type][n];
  int ans = 0;
  for (int i = 0; i <= n; i += coin[type]) {
    ans += solve(type - 1, n - i);
  }
  return dp[type][n] = ans;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  int n;
  while (std::cin >> n) {
    std::cout << solve(4, n) << '\n';
  }
}
