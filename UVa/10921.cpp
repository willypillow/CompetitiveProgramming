#include <iostream>
using std::cin; using std::cout;

int main() {
  char dict[26];
  for (int c = 0; c <= 26; c++) {
    if (c + 'A' < 'S') {
      dict[c] = '2' + (c / 3);
    } else if (c + 'A' < 'Z') {
      dict[c] = '2' + (c - 1) / 3;
    } else {
      dict[c] = '9';
    }
  }
  char str[40];
  while (cin.getline(str, 40)) {
    for (char *c = str; *c != 0; c++) {
      if (!isalpha(*c)) {
        putchar(*c);
      } else {
        putchar(dict[*c - 'A']);
      }
    }
    putchar('\n');
  }
}
