#include <iostream>
#include <algorithm>

struct Pair {
  char c;
  int count = 0;
  bool operator <(const Pair& rhs) {
    if (count < rhs.count) return true;
    if (count > rhs.count) return false;
    if (c > rhs.c) return true;
    return false;
  }
} arr[26];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  std::cin >> n;
  std::cin.ignore(100, '\n');
  for (int i = 0; i < 26; i++) arr[i].c = 'A' + i;
  while (n--) {
    char str[500];
    std::cin.getline(str, 500);
    for (char *p = str; *p != '\0'; p++) {
      if (isalpha(*p)) {
        if (islower(*p)) *p -= 'a' - 'A';
        arr[*p - 'A'].count++;
      }
    }
  }
  std::sort(arr, arr + 26);
  for (int i = 25; i >= 0; i--) {
    if (!arr[i].count) break;
    std::cout << arr[i].c << ' ' << arr[i].count << '\n';
  }
}
