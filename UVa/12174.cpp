#include <iostream>
#include <cstring>

int arr[100001], playCnt[100001];
bool ok[200001];

static inline int fast_atoi(const char *p, unsigned int len) {
  int res = 0;
  switch (len) {
    case 6: res += (*p++ & 15) * 100000;
    case 5: res += (*p++ & 15) * 10000;
    case 4: res += (*p++ & 15) * 1000;
    case 3: res += (*p++ & 15) * 100;
    case 2: res += (*p++ & 15) * 10;
    case 1: res += (*p & 15);
  }
  return res;
}

bool getInt(int *res) {
  char numBuf[11], tmp;
  unsigned int i = 0;
  while (true) {
    static char buf[1 << 20], *p = buf, *end = buf;
    if (p == end) {
      if ((end = buf + fread(buf, 1, 1 << 20, stdin)) == buf) break;
      p = buf;
    }
    tmp = *p++;
    if ((unsigned)(tmp - '0') > 10u && i) break;
    numBuf[i++] = tmp;
  }
  if (!i) return false;
  *res = fast_atoi(numBuf, i);
  return true;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int t; getInt(&t);
  while (t--) {
    memset(playCnt, 0, sizeof(playCnt));
    int s, n, ans = 0;
    getInt(&s); getInt(&n);
    for (int i = 0; i < n; i++) getInt(&arr[i]);
    for (int end = 0, cnt = 0; end <= n + s; end++) { // end is non-inclusive
      ok[end] = cnt == s || (end <= s && cnt == end) ||
        (end > n && cnt == std::min(n, n + s - end));
      if (end < n && ++playCnt[arr[end]] == 1) cnt++;
      if (end - s >= 0 && --playCnt[arr[end - s]] == 0) cnt--;
    }
    for (int i = 1; i <= s; i++) {
      bool suc = true;
      for (int end = i; suc && end <= n + s; end += s) suc &= ok[end];
      if (suc) ans++;
    }
    std::cout << ans << '\n';
  }
}
