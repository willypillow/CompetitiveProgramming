#include <iostream>
#include <cstring>
#include <sstream>

int inOrder[10001], postOrder[10001];
int left[10001], right[10001], minAns, ansNode;

const int INF = 1 << 30;

inline int read(int *arr) {
  std::string buf;
  if (!std::getline(std::cin, buf)) return 0;
  std::stringstream ss(buf);
  int i = 0;
  while (ss >> arr[i]) i++;
  return i;
}

int solve(int inL, int inR, int postL, int postR) {
  if (inR < inL) return 0;
  int root = postOrder[postR], lCnt;
  for (lCnt = inL; inOrder[lCnt] != root; lCnt++);
  left[root] = solve(inL, lCnt - 1, postL, postL + (lCnt - 1 - inL));
  right[root] = solve(lCnt + 1, inR, postR - 1 - (inR - lCnt - 1), postR - 1);
  return root;
}

void dfs(int root, int sum) {
  sum += root;
  if (!left[root] && !right[root] && sum <= minAns) {
    if (sum == minAns && root > ansNode) return;
    ansNode = root;
    minAns = sum;
    return;
  }
  if (left[root]) dfs(left[root], sum);
  if (right[root]) dfs(right[root], sum);
}

inline void init() {
  memset(left, 0, sizeof(left));
  memset(right, 0, sizeof(right));
  minAns = INF;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  while (read(inOrder)) {
    init();
    int n = read(postOrder);
    int root = solve(0, n - 1, 0, n - 1);
    dfs(root, 0);
    std::cout << ansNode << '\n';
  }
}
