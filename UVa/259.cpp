#include <bits/stdc++.h>
using namespace std;

const int INF = 0x3f3f3f3f;

struct Edge { int to, cap, rev; };
vector<Edge> g[101];
int dist[101], flowS, flowT;
bool vis[101];

void addEdge(int from, int to, int cap) {
	g[from].push_back({to, cap, (int)g[to].size()});
	g[to].push_back({from, 0, (int)g[from].size() - 1});
}

int bfs() {
	memset(dist, 0x3f, sizeof(dist));
	dist[flowS] = 0;
	queue<int> q;
	q.push(flowS);
	while (q.size()) {
		int cur = q.front(); q.pop();
		for (auto &e: g[cur]) {
			if (e.cap > 0 && dist[e.to] == INF) {
				dist[e.to] = dist[cur] + 1;
				if (e.to == flowT) return dist[e.to];
				q.push(e.to);
			}
		}
	}
	return INF;
}

int dfs(int cur, int f) {
	if (cur == flowT) return f;
	vis[cur] = true;
	for (auto &e: g[cur]) {
		if (e.cap > 0 && dist[cur] + 1 == dist[e.to] && !vis[e.to]) {
			int newF = dfs(e.to, min(e.cap, f));
			if (newF) {
				e.cap -= newF;
				g[e.to][e.rev].cap += newF;
				return newF;
			}
		}
	}
	return 0;
}

int dinic(int flow = 0) {
	while (bfs() < INF) {
		for (int f = INF; f;) {
			memset(vis, false, sizeof(vis));
			flow += (f = dfs(flowS, INF));
		}
	}
	return flow;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	for (int n, cases = 1; cin >> n && n; cases++) {
		for (int i = 1; i <= n; i++) g[i].clear();
		int m; cin >> flowS >> flowT >> m;
		while (m--) {
			int a, b, c; cin >> a >> b >> c;
			addEdge(a, b, c), addEdge(b, a, c);
		}
		cout << "Network " << cases << '\n';
		cout << "The bandwidth is " << dinic() << ".\n\n";
	}
}
