#include <iostream>

int cache[301][301] = { 0 };

int solve(int m, int n) {
  if (m < n) { int tmp = m; m = n; n = tmp; }
  if (n == 1) return m - 1;
  if (cache[m][n]) return cache[m][n];
  return cache[m][n] = 1 + solve(n, m / 2) + solve(n, m - (m / 2));
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int m, n;
  while (std::cin >> m >> n) {
    std::cout << solve(m, n) << '\n';
  }
}
