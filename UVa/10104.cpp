#include <bits/stdc++.h>
using namespace std;

int extGcd(int a, int b, int *x, int *y) {
	if (!b) {
		*x = 1, *y = 0;
		return a;
	}
	int g = extGcd(b, a % b, x, y);
	int t = *x;
	*x = *y;
	*y = t - (a / b) * *y;
	return g;
}

int main() {
	int a, b;
	while (cin >> a >> b) {
		int x, y;
		int g = extGcd(a, b, &x, &y);
		cout << x << ' ' << y << ' ' << g << '\n';
	}
}
