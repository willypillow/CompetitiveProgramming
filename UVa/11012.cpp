#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E5 + 1,
	MULT[][3] = { { 1, 1, 1 }, { -1, 1, 1 }, { 1, -1, 1}, { 1, 1, -1 } };

int x[MAXN], y[MAXN], z[MAXN];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int t, cases = 0;
	cin >> t;
	while (t--) {
		int n; cin >> n;
		for (int i = 0; i < n; i++) cin >> x[i] >> y[i] >> z[i];
		int ans = 0;
		for (int i = 0, minD, maxD; i < 4; i++) {
			minD = maxD = MULT[i][0] * x[0] + MULT[i][1] * y[0] + MULT[i][2] * z[0];
			for (int j = 1; j < n; j++) {
				int t = MULT[i][0] * x[j] + MULT[i][1] * y[j] + MULT[i][2] * z[j];
				minD = min(minD, t); maxD = max(maxD, t);
			}
			ans = max(ans, maxD - minD);
		}
		cout << "Case #" << ++cases << ": " << ans << '\n';
	}
}
