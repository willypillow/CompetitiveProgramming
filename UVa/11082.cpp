#include <iostream> 
#include <cstring>
#include <queue>
#include <vector>
using namespace std;

struct Edge { int from, to, cap, flow; };

const int S = 0, T = 1, INF = 1 << 30;
int rowSum[20], colSum[20], arr[20][20], row[20], col[20], a[50], par[50];
vector<Edge> edge;
vector<vector<int> > g;

void addEdge(int u, int v, int cap) {
	edge.push_back(Edge { u, v, cap, 0 });
	g[u].push_back(edge.size() - 1);
	edge.push_back(Edge { v, u, 0, 0 });
	g[v].push_back(edge.size() - 1);
}

int maxflow() {
	int flow = 0;
	for (;;) {
		memset(a, 0, sizeof(a));
		queue<int> q;
		a[S] = INF;
		q.push(S);
		while (!q.empty()) {
			int cur = q.front(); q.pop();
			for (int i: g[cur]) {
				Edge &e = edge[i];
				if (!a[e.to] && e.cap > e.flow) {
					a[e.to] = min(a[cur], e.cap - e.flow);
					par[e.to] = i;
					q.push(e.to);
				}
			}
			if (a[T]) break;
		}
		if (!a[T]) break;
		for (int i = T; i != S; i = edge[par[i]].from) {
			edge[par[i]].flow += a[T];
			edge[par[i] ^ 1].flow -= a[T];
		}
		flow += a[T];
	}
	return flow;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int cases = 0;
	int t; cin >> t;
	while (t--) {
		edge.clear(); g.clear();
		int r, c; cin >> r >> c;
		g.resize(r + c + 2);
		for (int i = 0; i < r; i++) {
			cin >> rowSum[i];
			if (i) row[i] = rowSum[i] - rowSum[i - 1];
			else row[i] = rowSum[i];
		}
		for (int i = 0; i < c; i++) {
			cin >> colSum[i];
			if (i) col[i] = colSum[i] - colSum[i - 1];
			else col[i] = colSum[i];
		}
		for (int i = 0; i < r; i++) addEdge(S, i + 2, row[i] - c);
		for (int j = 0; j < c; j++) addEdge(j + r + 2, T, col[j] - r);
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) addEdge(i + 2, j + r + 2, 19);
		}
		maxflow();
		for (Edge e: edge) {
			if (e.from < 2 || e.to < 2 || e.to < e.from) continue;
			int i = e.from - 2, j = e.to - r - 2;
			arr[i][j] = e.flow + 1;
		}
		cout << "Matrix " << ++cases << '\n';
		for (int i = 0; i < r; i++) {
			cout << arr[i][0];
			for (int j = 1; j < c; j++) cout << ' ' << arr[i][j];
			cout << '\n';
		}
		cout << '\n';
	}
}
