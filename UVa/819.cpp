#include <bits/stdc++.h>
using namespace std;

const int MAXN = 105;
const double EPS = 1E-9, PI = acos(-1);

struct P {
	double x, y;
	P operator +(const P &rhs) const {
		return P { x + rhs.x, y + rhs.y };
	}
	P operator -(const P &rhs) const {
		return P { x - rhs.x, y - rhs.y };
	}
	bool operator <(const P &rhs) const {
		return (x != rhs.x) ? x < rhs.x : y < rhs.y;
	}
} p[MAXN], hull[MAXN];
typedef P Vect;
const P ZERO = P { 0, 0 };

int cmp(double x, double y) {
	int d = x - y;
	if (fabs(d) < EPS) return 0;
	return d > 0 ? 1 : -1;
}

int cmp(double x) {
	return (fabs(x) < EPS) ? 0 : x > 0 ? 1 : -1;
}

double cross(const P &o, const P &a, const P &b) {
	return (a.x - o.x) * (b.y - o.y) - (b.x - o.x) * (a.y - o.y);
}

double cross(const P &a, const P &b) {
	return a.x * b.y - a.y * b.x;
}

double dot(const Vect &a, const Vect &b) {
	return a.x * b.x + a.y * b.y;
}

double angle(const Vect &a, const Vect &b) {
	return atan2(cross(a, b), dot(a, b));
}

double turnAngle(const Vect &a, const Vect &b) {
	return (cmp(dot(a, b), 0) > 0) ? angle(a, b) : (PI + angle(a, b));
}

Vect rotate(const Vect &a, double rad) {
	return Vect { a.x * cos(rad) - a.y * sin(rad),
		a.y * cos(rad) + a.x * sin(rad) };
}

double dist(const P &a, const P &b) {
	return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
}

double distToLine(const P &o, const P &a, const P &b) {
	return fabs(cross(o, a, b) / dist(a, b));
}

double area(const P &p1, const P &p2, const P &p3, const P &p4,
		const Vect &v1, const Vect &v2) {
	return distToLine(p1, p2, p2 + v1) * distToLine(p3, p4, p4 + v2);
}


int convex(int n) {
	sort(p, p + n);
	int m = 0;
	for (int i = 0; i < n; i++) {
		while (m >= 2 && cross(hull[m - 2], hull[m - 1], p[i]) < 0) m--;
		hull[m++] = p[i];
	}
	for (int i = n - 2, t = m; i >= 0; i--) {
		while (m > t && cross(hull[m - 2], hull[m - 1], p[i]) < 0) m--;
		hull[m++] = p[i];
	}
	return m - 1;
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int cases = 0, n;
	while (cin >> n && n) {
		for (int i = n - 1; i >= 0; i--) cin >> p[i].x >> p[i].y;
		int m = convex(n);
		int p1 = 0, p2 = 0, p3 = 0, p4 = 0;
		for (int i = 1; i < m; i++) {
			if (cmp(hull[p1].y, hull[i].y) > 0) p1 = i;
			if (cmp(hull[p2].y, hull[i].y) < 0) p2 = i;
			if (cmp(hull[p3].x, hull[i].x) > 0) p3 = i;
			if (cmp(hull[p4].x, hull[i].x) < 0) p4 = i;
		}
		Vect orig = Vect { 1, 0 }, v1 = Vect { 1, 0 }, v2 = Vect { 0, 1 };
		double ansMin = 1E30, ansMax = 0;
		while (cmp(cross(orig, v1)) >= 0) {
			ansMin = min(ansMin, area(hull[p1], hull[p2], hull[p3], hull[p4], v1, v2));
			double minAng = turnAngle(v1, hull[p1 + 1] - hull[p1]);
			minAng = min(minAng, turnAngle(ZERO - v1, hull[p2 + 1] - hull[p2]));
			minAng = min(minAng, turnAngle(ZERO - v2, hull[p3 + 1] - hull[p3]));
			minAng = min(minAng, turnAngle(v2, hull[p4 + 1] - hull[p4]));
			double l = 0, r = minAng;
			while (cmp(l - r)) {
				double m1 = l + (r - l) / 3, m2 = r - (r - l) / 3;
				if (cmp(area(hull[p1], hull[p2], hull[p3], hull[p4], rotate(v1, m1), rotate(v2, m1)) -
						area(hull[p1], hull[p2], hull[p3], hull[p4], rotate(v1, m2), rotate(v2, m2))) > 0) {
					r = m2;
				} else {
					l = m1;
				}
			}
			ansMax = max(ansMax, area(hull[p1], hull[p2], hull[p3], hull[p4], rotate(v1, l), rotate(v2, l)));
			v1 = rotate(v1, minAng); v2 = rotate(v2, minAng);
			if (cmp(angle(v1, hull[p1 + 1] - hull[p1])) == 0) p1 = (p1 + 1) % m;
			if (cmp(angle(ZERO - v1, hull[p2 + 1] - hull[p2])) == 0) p2 = (p2 + 1) % m;
			if (cmp(angle(ZERO - v2, hull[p3 + 1] - hull[p3])) == 0) p3 = (p3 + 1) % m;
			if (cmp(angle(v2, hull[p4 + 1] - hull[p4])) == 0) p4 = (p4 + 1) % m;
		}
		cout << fixed << setprecision(3) <<
			"Gift " << ++cases << "\nMinimum area = " << ansMin <<
			"\nMaximum area = " << ansMax << "\n\n";
	}
}
