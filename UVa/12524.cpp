#include <iostream>
using namespace std;

typedef int_fast32_t Int;
typedef int_fast64_t LL;
const Int MAXN = 1001;
const LL INF = 1LL << 60;

LL dp[MAXN][MAXN], sumW[MAXN], sumWX[MAXN], weight[MAXN], posX[MAXN];

struct Dp {
	LL xs[MAXN], ys[MAXN];
	Int front, rear;
	void init() {
		front = 0, rear = -1;
	}
	LL cross(LL x0, LL y0, LL x1, LL y1, LL x2, LL y2) {
		return (x1 - x0) * (y2 - y0) - (y1 - y0) * (x2 - x0);
	}
	void add(LL x, LL y) {
		while (front < rear &&
				cross(xs[rear - 1], ys[rear - 1], xs[rear], ys[rear], x, y) < 0) {
			rear--;
		}
		rear++;
		xs[rear] = x; ys[rear] = y;
	}
	LL query(LL m) {
		while (front < rear &&
				(ys[front + 1] - ys[front]) < (xs[front + 1] - xs[front]) * m) {
			front++;
		}
		return ys[front] - xs[front] * m;
	}
} slope;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	for (Int n, k; cin >> n >> k; ) {
		for (Int i = 1; i <= n; i++) cin >> posX[i] >> weight[i];
		for (Int i = 1; i <= n; i++) {
			sumW[i] = sumW[i - 1] + weight[i];
			sumWX[i] = sumWX[i - 1] + posX[i] * weight[i];
		}
		for (Int i = 0; i <= n; i++) {
			for (Int j = 0; j <= k; j++) dp[i][j] = INF;
		}
		dp[0][0] = 0;
		for (Int j = 1; j <= k; j++) {
			slope.init();
			for (Int i = j; i <= n; i++) {
				slope.add(sumW[i - 1], dp[i - 1][j - 1] + sumWX[i - 1]);
				dp[i][j] = slope.query(posX[i]) - (sumWX[i] - sumW[i] * posX[i]);
			}
		}
		cout << dp[n][k] << '\n';
	}
}
