#include <bits/stdc++.h>
using namespace std;

struct Point { int x, y, p; } point[1000];
struct Edge {
	int u, v;
	double dist;
	bool operator <(const Edge &rhs) const {
		return dist < rhs.dist;
	}
};
bool vis[1000];
int n, par[1000];
double maxCost[1000][1000];
vector<Edge> edges;
vector<vector<pair<int, double> > > g;

int find(int x) {
	return (par[x] == x) ? x : (par[x] = find(par[x]));
}

double dist(const Point &x, const Point &y) {
	return sqrt((x.x - y.x) * (x.x - y.x) + (x.y - y.y) * (x.y - y.y));
}

void dfsMaxCost(int u) {
	vis[u] = true;
	for (auto &p: g[u]) {
		if (vis[p.first]) continue;
		for (int i = 0; i < n; i++) {
			if (vis[i]) {
				maxCost[p.first][i] = maxCost[i][p.first] =
					max(maxCost[i][p.first], max(maxCost[i][u], p.second));
			}
		}
		dfsMaxCost(p.first);
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		cin >> n;
		memset(vis, false, sizeof(vis)); memset(maxCost, 0, sizeof(maxCost));
		edges.clear(); g.clear(); g.resize(n);
		for (int i = 0; i < n; i++) par[i] = i;
		for (int i = 0; i < n; i++) cin >> point[i].x >> point[i].y >> point[i].p;
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				edges.push_back(Edge { i, j, dist(point[i], point[j]) });
			}
		}
		sort(edges.begin(), edges.end());
		double minCost = 0;
		int cnt = 0;
		for (auto &e: edges) {
			int pu = find(e.u), pv = find(e.v);
			if (pu == pv) continue;
			par[pu] = pv;
			minCost += e.dist;
			g[e.u].push_back(make_pair(e.v, e.dist));
			g[e.v].push_back(make_pair(e.u, e.dist));
			if (++cnt == n - 1) break;
		}
		dfsMaxCost(0);
		double ans = 0;
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				ans = max(ans, (point[i].p + point[j].p) / (minCost - maxCost[i][j]));
			}
		}
		cout << fixed << setprecision(2) << ans << '\n';
	}
}

