#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  double down, loan;
  int months, n;
  while (std::cin >> months >> down >> loan >> n && months >= 0) {
    double rates[101];
    int i, lastI = 0;
    while (n--) {
      std::cin >> i;
      std::cin >> rates[i];
      rates[i] = 1 - rates[i];
      if (i > lastI + 1) {
        double rate = rates[lastI];
        while (++lastI < i) rates[lastI] = rate;
      }
      lastI = i;
    }
    double rate = rates[lastI];
    while (++lastI <= months) rates[lastI] = rate;
    double payPerMonth = loan / months, value = loan + down;
    int m;
    for (m = 0; m < months; m++) {
      if (loan - payPerMonth * m < (value *= rates[m])) break;
    }
    std::cout << m << ((m == 1) ? " month" : " months") << '\n';
  }
}
