#include <iostream>
#include <queue>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  while (std::cin >> n && n) {
    std::priority_queue<int, std::vector<int>, std::greater<int> > q;
    for (int i = 0, x = 0; i < n; i++) { std::cin >> x; q.push(x); }
    int ans = 0;
    while (true) {
      int cur = q.top(); q.pop();
      if (q.empty()) break;
      cur += q.top(); q.pop();
      ans += cur;
      q.push(cur);
    }
    std::cout << ans << '\n';
  }
}

