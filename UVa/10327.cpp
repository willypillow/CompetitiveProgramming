#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  int arr[1000];
  int n;
  while (std::cin >> n) {
    for (int i = 0; i < n; i++) std::cin >> arr[i];
    int cnt = 0;
    for (int i = 0; i < n; i++) {
      for (int j = 1; j < n; j++) {
        if (arr[j - 1] > arr[j]) {
          int tmp = arr[j - 1];
          arr[j - 1] = arr[j];
          arr[j] = tmp;
          cnt++;
        }
      }
    }
    std::cout << "Minimum exchange operations : " << cnt << '\n';
  }
}
