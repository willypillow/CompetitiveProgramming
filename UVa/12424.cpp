#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E5 + 10;

struct Node {
	int p, pPos, depth;
	void init() {
		p = -1;
		pPos = depth = 0;
		arr.clear();
	}
	vector<int> arr, seg[10];
} node[300000];
vector<int> g[MAXN];
int ptr, size[MAXN], maxChild[MAXN], color[MAXN], nodeId[MAXN], pos[MAXN],
	ansColors[10];
bool vis[MAXN];

void dfs(int x, int p) {
	size[x] = 1;
	maxChild[x] = -1;
	for (int y: g[x]) {
		if (y == p) continue;
		dfs(y, x);
		size[x] += size[y];
		if (maxChild[x] == -1 || size[y] > size[maxChild[x]]) maxChild[x] = y;
	}
}

void buildSt(Node &n, int id, int l, int r) {
	if (l == r) {
		assert(color[n.arr[l]] < 10);
		assert(id < n.seg[color[n.arr[l]]].size());
		n.seg[color[n.arr[l]]][id] = 1;
	} else {
		int m = (l + r) >> 1;
		buildSt(n, id << 1, l, m); buildSt(n, id << 1 | 1, m + 1, r);
		for (int i = 0; i < 10; i++) {
			n.seg[i][id] = n.seg[i][id << 1] + n.seg[i][id << 1 | 1];
		}
	}
}

void modifySt(Node &n, int id, int l, int r, int pos, int val) {
	if (l == r) {
		for (int i = 0; i < 10; i++) n.seg[i][id] = 0;
		n.seg[val][id] = 1;
	} else {
		int m = (l + r) >> 1;
		if (pos <= m) modifySt(n, id << 1, l, m, pos, val);
		else modifySt(n, id << 1 | 1, m + 1, r, pos, val);
		for (int i = 0; i < 10; i++) {
			n.seg[i][id] = n.seg[i][id << 1] + n.seg[i][id << 1 | 1];
		}
	}
}

void querySt(Node &n, int id, int l, int r, int qL, int qR) {
	assert(r != -1);
	if (qL <= l && r <= qR) {
		for (int i = 0; i < 10; i++) ansColors[i] += n.seg[i][id];
	} else {
		int m = (l + r) >> 1;
		if (qL <= m) querySt(n, id << 1, l, m, qL, qR);
		if (m < qR) querySt(n, id << 1 | 1, m + 1, r, qL, qR);
	}
}

void build(int x, int p) {
	if (!vis[x]) {
		Node &n = node[ptr++];
		n.init();
		for (int i = x; i != -1; i = maxChild[i]) {
			vis[i] = true;
			nodeId[i] = ptr - 1;
			pos[i] = n.arr.size();
			n.arr.push_back(i);
		}
		unsigned int sz = 1;
		while (sz < n.arr.size()) sz <<= 1;
		for (int i = 0; i < 10; i++) {
			n.seg[i].clear();
			n.seg[i].resize(sz << 1);
		}
		buildSt(n, 1, 0, n.arr.size() - 1);
		if (p != -1) {
			n.p = nodeId[p];
			n.pPos = pos[p];
			n.depth = node[n.p].depth + 1;
		}
	}
	for (int y: g[x]) {
		if (y != p) build(y, x);
	}
}

int query(int u, int v) {
	for (int i = 0; i < 10; i++) ansColors[i] = 0;
	int idU = nodeId[u], idV = nodeId[v], pU = pos[u], pV = pos[v];
	while (idU != idV) {
		if (node[idU].depth < node[idV].depth) swap(idU, idV), swap(pU, pV);
		querySt(node[idU], 1, 0, node[idU].arr.size() - 1, 0, pU);
		pU = node[idU].pPos;
		idU = node[idU].p;
	}
	if (pU > pV) swap(pU, pV);
	querySt(node[idU], 1, 0, node[idU].arr.size() - 1, pU, pV);
	int ans = 0;
	for (int i = 0; i < 10; i++) ans = max(ans, ansColors[i]);
	return ans;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int t; cin >> t;
	while (t--) {
		int n, m; cin >> n >> m;
		for (int i = 0; i < n; i++) g[i].clear();
		memset(vis, false, sizeof(vis));
		ptr = 0;
		for (int i = 0; i < n; i++) {
			cin >> color[i];
			color[i]--;
		}
		for (int i = 1, a, b; i < n; i++) {
			cin >> a >> b;
			a--; b--;
			g[a].push_back(b); g[b].push_back(a);
		}
		dfs(0, -1);
		build(0, -1);
		while (m--) {
			int c, u, v; cin >> c >> u >> v;
			if (c == 0) {
				int x = nodeId[u - 1];
				modifySt(node[x], 1, 0, node[x].arr.size() - 1, pos[u - 1], v - 1);
			} else {
				cout << query(u - 1, v - 1) << '\n';
			}
		}
	}
}
