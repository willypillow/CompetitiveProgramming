#include <iostream>
#include <cstring>

bool edges[26][26], vis[26];
int froms[26], tos[26];

void dfs(int node) {
  if (vis[node]) return;
  vis[node] = true;
  for (int i = 0; i < 26; i++) {
    if (edges[node][i] || edges[i][node]) dfs(i);
  }
}

bool solve() {
  bool first = true, fail = false;
  for (int i = 0; i < 26; i++) {
    if ((froms[i] || tos[i]) && !vis[i]) {
      if (first) {
        dfs(i);
        first = false;
      }
      else return false;
    }
  }
  int sum = 0, count = 0;
  for (int i = 0; i < 26; i++) {
    if (froms[i] != tos[i]) {
      if (count++ >= 2 || abs(froms[i] - tos[i]) != 1) return false;
      sum += froms[i] - tos[i];
    }
  }
  if (sum != 0) return false;
  return true;
}

inline void init() {
  memset(edges, false, sizeof(edges));
  memset(vis, false, sizeof(vis));
  memset(froms, 0, sizeof(froms));
  memset(tos, 0, sizeof(tos));
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int t;
  std::cin >> t;
  while (t--) {
    init();
    int n;
    std::cin >> n;
    while (n--) {
      char tmp[1024];
      std::cin >> tmp;
      int head = tmp[0] - 'a', tail = tmp[strlen(tmp) - 1] - 'a';
      edges[head][tail] = true;
      froms[head]++; tos[tail]++;
    }
    if (solve()) puts("Ordering is possible.");
    else puts("The door cannot be opened.");
  }
}
