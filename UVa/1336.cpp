#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E3 + 5;
const double INF = 1E30;

struct P {
	double x, c, d;
	bool operator <(const P &rhs) const {
		return x < rhs.x;
	}
} p[MAXN];
double dp[MAXN][MAXN][2];
double sumD[MAXN], v, x;
int n;

double dfs(int l, int r, int k) {
	if (l == 0 && r == n - 1) return 0;
	double &ans = dp[l][r][k];
	if (ans != INF) return ans;
	int pos = k ? r : l;
	double dd = sumD[l] + (sumD[n] - sumD[r + 1]);
	if (l != 0) {
		int newPos = l - 1;
		double dt = (p[pos].x - p[newPos].x) / v, dc = dd * dt + p[newPos].c;
		ans = min(ans, dfs(l - 1, r, 0) + dc);
	}
	if (r + 1 < n) {
		int newPos = r + 1;
		double dt = (p[newPos].x - p[pos].x) / v, dc = dd * dt + p[newPos].c;
		ans = min(ans, dfs(l, r + 1, 1) + dc);
	}
	return ans;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	while (cin >> n >> v >> x && n) {
		for (int i = 0; i < n; i++) cin >> p[i].x >> p[i].c >> p[i].d;
		sort(p, p + n);
		for (int i = 1; i <= n; i++) sumD[i] = sumD[i - 1] + p[i - 1].d;
		fill(&dp[0][0][0], &dp[n - 1][n - 1][1], INF);
		double ans = INF;
		int adj = lower_bound(p, p + n, P { x, 0, 0 }) - p;
		for (int i = adj - 1; i <= adj; i++) {
			if (i < 0 || i >= n) continue;
			double dt = fabs(x - p[i].x) / v, dc = sumD[n] * dt + p[i].c;
			ans = min(ans, dfs(i, i, 0) + dc);
		}
		cout << fixed << setprecision(0) << floor(ans) << '\n';
	}
}
