#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  int n;
  while (std::cin >> n && n) {
    long long ans = 0, req = 0;
    int tmp = 0;
    for (int i = 0; i < n; i++) {
      std::cin >> tmp;
      ans += std::abs(req);
      req += tmp;
    }
    std::cout << ans << '\n';
  }
}
