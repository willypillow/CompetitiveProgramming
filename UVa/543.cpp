#include <iostream>

int primes[100000], pTop = 0;
bool notPrime[1000001] = { false };

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  for (int i = 2; i <= 1000; i++) {
    if (!notPrime[i]) {
      for (int j = i << 1; j <= 1000000; j += i) notPrime[j] = true;
    }
  }
  for (int i = 2; i < 1000001; i++) {
    if (!notPrime[i]) primes[pTop++] = i;
  }
  int n;
  while (std::cin >> n && n) {
    bool fail = true;
    for (int i = 0; primes[i] < n; i++) {
      int search = *std::lower_bound(primes, primes + pTop, n - primes[i]);
      if (search == n - primes[i]) {
        std::cout << n << " = " << primes[i] << " + " << n - primes[i] << '\n';
        fail = false;
        break;
      } 
    }
    if (fail) std::cout << "Goldbach's conjecture is wrong.\n";
  }
}
