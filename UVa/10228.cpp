#include <iostream>
#include <cmath>
using std::cin; using std::cout;

struct P { int x, y; } c[100];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  srand(time(0));
  int t; cin >> t;
  while (t--) {
    int n; cin >> n;
    for (int i = 0; i < n; i++) cin >> c[i].x >> c[i].y;
    double x = 5000, y = 5000, temp = 5000, ans = 1.0E15;
    while (temp > 1.0E-3) {
      int rnd = rand();
      double nx = x + (temp * cos(rnd)), ny = y + (temp * sin(rnd)), tot = 0;
      for (int i = 0; i < n; i++) {
        double dx = nx - c[i].x, dy = ny - c[i].y;
        tot += sqrt(dx * dx + dy * dy);
      }
      if (tot < ans) ans = tot, x = nx, y = ny;
      else temp *= 0.99;
    }
    cout << round(ans) << '\n';
    if (t) cout << '\n';
  }
}
