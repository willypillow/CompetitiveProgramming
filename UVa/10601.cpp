#include <bits/stdc++.h>
using namespace std;

typedef long long LL;

int cnt[12];
LL c[13][13];

LL solve(int k, int *cnt) {
	int n = 0;
	LL ans = 1;
	for (int i = 0; i < 12; i++) {
		if (cnt[i] % k != 0) return 0;
		cnt[i] /= k;
		n += cnt[i];
	}
	for (int i = 0; i < 12; i++) {
		ans *= c[n][cnt[i]];
		n -= cnt[i];
	}
	return ans;
}

LL iden() {
	int tmp[12];
	memcpy(tmp, cnt, sizeof(tmp));
	return solve(1, tmp);
}

LL rotFace() {
	int tmp[12];
	memcpy(tmp, cnt, sizeof(tmp));
	LL t = 6 * solve(4, tmp);
	memcpy(tmp, cnt, sizeof(tmp));
	return t + 3 * solve(2, tmp);
}

LL rotEdge() {
	int tmp[12];
	LL ans = 0;
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 6; j++) {
			if (!cnt[i] || !cnt[j]) continue;
			memcpy(tmp, cnt, sizeof(tmp));
			tmp[i]--; tmp[j]--;
			ans += 6 * solve(2, tmp);
		}
	}
	return ans;
}

LL rotVertice() {
	int tmp[12];
	memcpy(tmp, cnt, sizeof(tmp));
	return 8 * solve(3, tmp);
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	for (int i = 0; i <= 12; i++) {
		c[i][0] = c[i][i] = 1;
		for (int j = 1; j < i; j++) c[i][j] = c[i - 1][j] + c[i - 1][j - 1];
	}
	int t; cin >> t;
	while (t--) {
		memset(cnt, 0, sizeof(cnt));
		for (int i = 0, col; i < 12; i++) {
			cin >> col;
			cnt[col - 1]++;
		}
		cout << (iden() + rotFace() + rotEdge() + rotVertice()) / 24 << '\n';
	}
}
