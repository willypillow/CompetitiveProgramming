#include <iostream>
#include <cstring>

int toDir[256];

struct Node {
  bool dirs[4][4];
} nodes[12][12];

struct Status {
  int x, y, dir;
};

inline Status move(Status s, int dir) {
  const int dx[4] = { -1, 0, 1, 0 }, dy[4] = { 0, 1, 0, -1 };
  int newDir = (s.dir + dir) % 4, x = s.x + dx[newDir], y = s.y + dy[newDir];
  return Status { x, y, newDir };
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  toDir['N'] = 0, toDir['E'] = 1, toDir['S'] = 2, toDir['W'] = 3;
  toDir['F'] = 0, toDir['L'] = 3, toDir['R'] = 1;
  char name[32];
  while (std::cin.getline(name, 32) && strcmp(name, "END") != 0) {
    memset(nodes, 0, sizeof(nodes));
    int startX, startY, endX, endY;
    char startDir;
    std::cin >> startX >> startY >> startDir >> endX >> endY;
    int x, y;
    while (std::cin >> x && x) {
      std::cin >> y;
      char tmp[16];
      while (std::cin >> tmp && tmp[0] != '*') {
        int dir = toDir[tmp[0]], len = strlen(tmp);
        for (int i = 1; i < len; i++) {
          nodes[x][y].dirs[dir][toDir[tmp[i]]] = true;
        }
      }
    }
    std::cin.ignore(1000, '\n');
    Status queue[10000], parent[12][12][4] = { 0 }, ans[256],
      s = Status { startX, startY, toDir[startDir] };
    int qFront = 0, qBack = 1, aTop = 0;
    queue[0] = move(s, 0);
    parent[queue[0].x][queue[0].y][queue[0].dir] = Status { -1, -1, -1 };
    while (qFront != qBack) {
      s = queue[qFront++];
      if (s.x == endX && s.y == endY) break;
      for (int i = 0; i < 4; i++) {
        if (!nodes[s.x][s.y].dirs[s.dir][i]) continue;
        Status next = move(s, i);
        if (parent[next.x][next.y][next.dir].x == 0) {
          parent[next.x][next.y][next.dir] = s;
          queue[qBack++] = next;
        }
      }
    }
    if (s.x != endX || s.y != endY) {
      std::cout << name << "\n  No Solution Possible\n";
    } else {
      while (s.x != -1) {
        ans[aTop++] = s;
        s = parent[s.x][s.y][s.dir];
      }
      ans[aTop++] = Status { startX, startY, 0 };
      std::cout << name;
      for (int i = aTop - 1; i >= 0; i--) {
        if ((aTop - 1 - i) % 10 == 0) std::cout << "\n ";
        std::cout << " (" << ans[i].x << ',' << ans[i].y << ")";
        if (i == 0) std::cout << '\n';
      }
    }
  }
}
