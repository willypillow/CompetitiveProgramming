#include <bits/stdc++.h>
using namespace std;

string str[3000];
int dur[3000];

template<class T> using max_heap = priority_queue<T, vector<T>, greater<T> >;

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		int n, k; cin >> n >> k;
		max_heap<pair<int, int> > pq;
		for (int i = 0; i < n; i++) {
			cin >> str[i] >> dur[i];
			pq.push({dur[i], i});
		}
		while (pq.size()) {
			auto cur = pq.top(); pq.pop();
			cout << cur.first << ' ' << str[cur.second] << '\n';
			if (!--k) break;
			pq.push({cur.first + dur[cur.second], cur.second});
		}
	}
}
