#include <iostream>

static inline int fast_atoi(const char *p, unsigned int len) {
  int res = 0;
  switch (len) {
    case 10: res += (*p++ & 15) * 1000000000;
    case 9: res += (*p++ & 15) * 100000000;
    case 8: res += (*p++ & 15) * 10000000;
    case 7: res += (*p++ & 15) * 1000000;
    case 6: res += (*p++ & 15) * 100000;
    case 5: res += (*p++ & 15) * 10000;
    case 4: res += (*p++ & 15) * 1000;
    case 3: res += (*p++ & 15) * 100;
    case 2: res += (*p++ & 15) * 10;
    case 1: res += (*p & 15);
  }
  return res;
}

bool getInt(int *res) {
  char numBuf[15], tmp;
  unsigned int i = 0;
  int neg = 1;
  while (true) {
    static char buf[1 << 20], *p = buf, *end = buf;
    if (p == end) {
      if ((end = buf + fread(buf, 1, 1 << 20, stdin)) == buf) break;
      p = buf;
    }
    tmp = *p++;
    if (tmp == '-') { neg = -1; continue; }
    if ((unsigned)(tmp - '0') >= 10u) {
      if (i) break;
      else continue;
    }
    numBuf[i++] = tmp;
  }
  if (!i) return false;
  *res = fast_atoi(numBuf, i) * neg;
  return true;
}

long long sum[500011];

long long range(int l, int r) {
  return sum[r + 1] - sum[l];
}

struct Node {
  struct Max {
    int l, r;
    bool operator <(const Max &rhs) const {
      return range(l, r) < range(rhs.l, rhs.r);
    }
  } max;
  int lmax, rmax;
  bool isFail() {
    return lmax == -1;
  }
} tree[1500000], FAIL = { { -1, -1 }, -1, -1 };
int arr[500010], n, m;

Node merge(Node &lChild, Node &rChild, int l, int r) {
  if (lChild.isFail()) return rChild;
  if (rChild.isFail()) return lChild;
  Node cur;
  cur.max = std::max(lChild.max, { lChild.rmax, rChild.lmax });
  cur.max = std::max(cur.max, rChild.max);
  //if (range(l, lChild.lmax) < range(l, rChild.lmax)) cur.lmax = rChild.lmax;
  if (sum[lChild.lmax + 1] < sum[rChild.lmax + 1]) cur.lmax = rChild.lmax;
  else cur.lmax = lChild.lmax;
  //if (range(rChild.rmax, r) <= range(lChild.rmax, r)) cur.rmax = lChild.rmax;
  if (sum[rChild.rmax] >= sum[lChild.rmax]) cur.rmax = lChild.rmax;
  else cur.rmax = rChild.rmax;
  return cur;
}

void build(int pos, int l, int r) {
  if (l == r) {
    tree[pos].max = { l, r };
    tree[pos].lmax = tree[pos].rmax = l;
    return;
  }
  int m = (l + r) >> 1;
  build(pos << 1, l, m); build(pos << 1 | 1, m + 1, r);
  tree[pos] = merge(tree[pos << 1], tree[pos << 1 | 1], l, r);
}

Node query(int pos, int a, int b, int l, int r) {
  if (a == l && b == r) return tree[pos];
  if (l == r) return FAIL;
  int m = (l + r) >> 1;
  if (a <= m && m <= b) {
    Node aNode = query(pos << 1, a, m, l, m), bNode = query(pos << 1 | 1, m + 1, b, m + 1, r);
    return merge(aNode, bNode, a, b);
  } else if (b < m) {
    return query(pos << 1, a, b, l, m);
  } else {
    return query(pos << 1 | 1, a, b, m + 1, r);
  }
}


int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif 
  int cases = 0;
  //while (std::cin >> n >> m) {
  while (getInt(&n) && getInt(&m)) {
    for (int i = 0; i < n; i++) {
      getInt(&arr[i]);
      sum[i + 1] = arr[i] + sum[i];
    }
    build(1, 0, n - 1);
    std::cout << "Case " << ++cases << ":\n";
    for (int i = 0; i < m; i++) {
      int a, b;
      getInt(&a), getInt(&b);
      Node ans = query(1, a - 1, b - 1, 0, n - 1);
      std::cout << ans.max.l + 1 << ' ' << ans.max.r + 1 << '\n';
    }
  }
}
