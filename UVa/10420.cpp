#include <iostream>
#include <map>
#include <string>
using std::cin; using std::cout; using std::map; using std::string;

int main() {
  for (int n; cin >> n; ) {
    map<string, int> count;
    while (n--) {
      string str;
      cin >> str; cin.ignore(1000, '\n');
      count[str]++;
    }
    for (auto &p: count) {
      cout << p.first << ' ' << p.second << '\n';
    }
  }
}
