#include <bits/stdc++.h>
using namespace std;

int col[110][110], n, m;
bool g[110][110];

bool color(int x, int y, int c) {
	col[x][y] = col[y][x] = c;
	for (int i = 0; i < n; i++) {
		if (i == x || i == y) continue;
		if (g[i][x] && g[y][i]) {
			if (col[y][i] == 3 - c) return false;
			else if (!col[y][i] && !color(y, i, c)) return false;
		} else if (g[i][x] ^ g[y][i]) {
			int t = g[i][x] ? x : y;
			if (col[t][i] == c) return false;
			else if (!col[t][i] && !color(t, i, 3 - c)) return false;
		}
	}
	return true;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	while (cin >> n >> m && n) {
		memset(g, false, sizeof(g)); memset(col, 0, sizeof(col));
		while (m--) {
			int x, y; cin >> x >> y;
			g[x][y] = g[y][x] = true;
		}
		bool fail = false;
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if (!col[i][j] && g[i][j] && !color(i, j, 1)) {
					fail = true;
					break;
				}
			}
		}
		cout << (fail ? "NO\n" : "YES\n");
	}
}
