#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int a, b;
  while (std::cin >> a >> b) {
    if (!a && !b) break;
    int carryCnt = 0, carry = 0;
    while (a || b) {
      if (a % 10 + b % 10 + carry >= 10) {
        carryCnt++;
        carry = 1;
      } else {
        carry = 0;
      }
      a /= 10;
      b /= 10;
    }
    if (!carryCnt) std::cout << "No carry operation.\n";
    else if (carryCnt == 1) std::cout << "1 carry operation.\n";
    else std::cout << carryCnt << " carry operations.\n";
  }
}

