#include <iostream>
#include <vector>
#include <string>
#include <map>

std::map<std::string, char> getMorseMap();

int main() {
  std::map<std::string, char> morse = getMorseMap();
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  std::cin >> n;
  std::cin.ignore(100, '\n');
  for (int i = 1; i <= n; i++) {
    std::string str, cur;
    getline(std::cin, str);
    int len = str.length();
    std::cout << "Message #" << i << "\n";
    for (int j = 0; j < len; j++) {
      if (str[j] != ' ') cur += str[j];
      else {
        if (cur.length()) std::cout << morse[cur];
        cur.clear();
        if (str[j+1] == ' ') {
          std::cout << ' ';
          j++;
        }
      }
    }
    if (cur.length()) std::cout << morse[cur];
    std::cout << "\n";
    if (i < n) std::cout << "\n";
  }
  return 0;
}

std::map<std::string, char> getMorseMap() {
  std::map<std::string, char> morse;
  morse[".-"]='A';morse["-..."]='B';morse["-.-."]='C';morse["-.."]='D';
  morse["."]='E';morse["..-."]='F';
  morse["--."]='G';morse["...."]='H';
  morse[".."]='I';morse[".---"]='J';morse["-.-"]='K';morse[".-.."]='L';
  morse["--"]='M';morse["-."]='N';morse["---"]='O';
  morse[".--."]='P';morse["--.-"]='Q';morse[".-."]='R';
  morse["..."]='S';morse["-"]='T';morse["..-"]='U';morse["...-"]='V';
  morse[".--"]='W';morse["-..-"]='X';morse["-.--"]='Y';morse["--.."]='Z';
  morse["-----"]='0';morse[".----"]='1';
  morse["..---"]='2';morse["...--"]='3';
  morse["....-"]='4';morse["....."]='5';
  morse["-...."]='6';morse["--..."]='7';
  morse["---.."]='8';morse["----."]='9';
  morse[".-.-.-"]='.';morse["--..--"]=',';
  morse["..--.."]='?';morse[".----."]='\'';
  morse["-.-.--"]='!';morse["-..-."]='/';
  morse["-.--."]='(';morse["-.--.-"]=')';
  morse[".-..."]='&';morse["---..."]=':';morse["-.-.-."]=';';
  morse["-...-"]='=';morse[".-.-."]='+';
  morse["-....-"]='-';morse["..--.-"]='_';
  morse[".-..-."]='"';morse[".--.-."]='@';
  return morse;
}
