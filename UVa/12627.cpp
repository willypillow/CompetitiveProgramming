#include <iostream>

unsigned long long pow3[31];

unsigned long long sum(unsigned long long row, int k) {
  if (row == 0) return 0;
  if (k == 0) return 1;
  if (row >= (1ULL << (k - 1))) {
    return (pow3[k - 1] << 1) + sum(row - (1ULL << (k - 1)), k - 1);
  } else {
    return sum(row, k - 1) << 1;
  }
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  pow3[0] = 1;
  for (int i = 1; i <= 30; i++) {
    pow3[i] = pow3[i - 1] * 3;
  }
  int t;
  std::cin >> t;
  for (int i = 1; i <= t; i++) {
    unsigned long long ans = 0, a, b;
    unsigned int k;
    std::cin >> k >> a >> b;
    ans = sum(b, k) - sum(a - 1, k);
    std::cout << "Case " << i << ": " << ans << '\n';
  }
}

