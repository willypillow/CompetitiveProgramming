#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  while (std::cin >> n && n) {
    while (n >= 10) {
      int newN = 0;
      do {
        newN += n % 10;
      } while (n /= 10);
      n = newN;
    }
    putchar(n + '0'); putchar('\n');
  }
}
