#include "bits/stdc++.h"
using namespace std;

const int MAXN = 1E6 + 2;

vector<int> vals;
int arr[MAXN], n, q;

struct Node {
	static Node pool[MAXN * 40], *ptr;
	static const Node HOLE;
	int sum;
	const Node *lc, *rc;
	Node(): sum(0), lc(&HOLE), rc(&HOLE) {}
	Node(int _s, const Node *l, const Node *r): sum(_s), lc(l), rc(r) {}
	Node *insert(int p, int l = 1, int r = n) const {
		if (l == r) return new(ptr++) Node { sum + 1, lc, rc };
		int m = (l + r) >> 1;
		Node *n = new(ptr++) Node { sum + 1, lc, rc };
		if (p <= m) {
			if (!n->lc) n->lc = Node().insert(p, l, m);
			else n->lc = n->lc->insert(p, l, m);
		} else {
			if (!n->rc) n->rc = Node().insert(p, m + 1, r);
			else n->rc = n->rc->insert(p, m + 1, r);
		}
		return n;
	}
	int query(const Node *old, int k, int l = 1, int r = n) const {
		if (l == r) return l;
		int lSum = lc->sum - old->lc->sum, m = (l + r) >> 1;
		if (k <= lSum) return lc->query(old->lc, k, l, m);
		else return rc->query(old->rc, k - lSum, m + 1, r);
	}
} *root[MAXN], Node::pool[MAXN * 40], *Node::ptr;
const Node Node::HOLE = Node(0, &Node::HOLE, &Node::HOLE);

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	Node init = Node();
	root[0] = &init;
	while (cin >> n >> q) {
		vals.clear();
		Node::ptr = Node::pool;
		for (int i = 1; i <= n; i++) {
			cin >> arr[i];
			vals.push_back(arr[i]);
		}
		sort(vals.begin(), vals.end());
		vals.resize(unique(vals.begin(), vals.end()) - vals.begin());
		for (int i = 1; i <= n; i++) {
			arr[i] = lower_bound(vals.begin(), vals.end(), arr[i]) - vals.begin() + 1;
			root[i] = root[i - 1]->insert(arr[i]);
		}
		while (q--) {
			int l, r, k; cin >> l >> r >> k;
			cout << vals[root[r]->query(root[l - 1], k) - 1] << '\n';
		}
	}
}
