#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int t;
  std::cin >> t;
  for (int i = 1; i <= t; i++) {
    int n, max = 0;
    std::cin >> n;
    while (n--) {
      int c;
      std::cin >> c;
      if (c > max) max = c;
    }
    std::cout << "Case " << i << ": " << max << '\n';
  }
}
