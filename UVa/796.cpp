#include <iostream>
#include <algorithm>
#include <vector>
#include <cstring>
using namespace std;

int timer, low[11000], dfn[11000];
vector<vector<int> > g;
vector<pair<int, int> > ans;

void dfs(int u, int p) {
	low[u] = dfn[u] = ++timer;
	for (int v: g[u]) {
		if (v == p) continue;
		if (dfn[v] == 0) {
			dfs(v, u);
			low[u] = min(low[u], low[v]);
			if (low[v] > dfn[u]) {
				ans.push_back(make_pair(min(u, v), max(u, v)));
			}
		} else {
			low[u] = min(low[u], low[v]);
		}
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int n;
	while (cin >> n) {
		memset(dfn, 0, sizeof(dfn)); memset(low, 0, sizeof(low));
		ans.clear(); g.clear(); g.resize(n + 1);
		timer = 0;
		for (int i = 0, id, k, v; i < n; i++) {
			cin >> id;
			cin.ignore(100, '(');
			cin >> k;
			cin.ignore(100, ')');
			while (k--) {
				cin >> v;
				g[id].push_back(v);
			}
		}
		for (int i = 0; i < n; i++) {
			if (dfn[i] == 0) dfs(i, i);
		}
		sort(ans.begin(), ans.end());
		cout << ans.size() << " critical links\n";
		for (auto &p: ans) {
			cout << p.first << " - " << p.second << '\n';
		}
		cout << '\n';
	}
}

