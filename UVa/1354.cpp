#include <vector>
#include <iomanip>
#include <cstring>
#include <iostream>

double width;
int s, ws[6], sum[1 << 6];
bool vis[1 << 6];

struct P { double l, r; };

std::vector<P> nodes[1 << 6];

void dfs(int subset) {
  if (vis[subset]) return;
  vis[subset] = true;
  int leftSub = (subset - 1) & subset;
  if (leftSub) {
    for (; leftSub != 0; leftSub = (leftSub - 1) & subset) {
      int rightSub = leftSub ^ subset;
      dfs(leftSub); dfs(rightSub);
      double leftArm = (double)sum[rightSub] / sum[subset],
        rightArm = (double)sum[leftSub] / sum[subset];
      for (P &l: nodes[leftSub]) {
        for (P &r: nodes[rightSub]) {
          P p = P { std::max(l.l + leftArm, r.l - rightArm),
            std::max(r.r + rightArm, l.r - leftArm) };
          if (p.l + p.r < width) nodes[subset].push_back(p);
        }
      }
    }
  } else {
    nodes[subset].push_back(P { 0, 0 });
  }
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif 
  int t;
  std::cin >> t;
  while (t--) {
    memset(vis, false, sizeof(vis));
    memset(sum, 0, sizeof(sum));
    std::cin >> width >> s;
    for (int i = 0; i < s; i++) std::cin >> ws[i];
    for (int i = 0; i < (1 << s); i++) {
      nodes[i].clear();
      for (int j = 0; j < s; j++) {
        if (i & (1 << j)) sum[i] += ws[j];
      }
    }
    int root = (1 << s) - 1;
    double ans = -1;
    dfs(root);
    for (P &p: nodes[root]) ans = std::max(ans, p.l + p.r);
    std::cout << std::setprecision(12) << ans << '\n';
  }
}
