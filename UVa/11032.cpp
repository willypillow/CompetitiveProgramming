#include <iostream>
#include <cstring>
using std::cin; using std::cout;

const int MAXN = 10000000;
int ans[MAXN + 1], sum[MAXN + 1];
int sod(int n) {
  int ans = 0;
  while (n) {
    ans += n % 10;
    n /= 10;
  }
  return ans;
}

int f(int a) {
  return ans[a] ? ans[a] : -1;
}
int f(int a, int b) {
  return sum[b] - sum[a - 1];
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  for (int i = 1; i <= MAXN; i++) {
    int idx = i + sod(i);
    if (idx > MAXN) continue;
    if (ans[idx] == 0) ans[idx] = i;
  }
  for (int i = 1; i <= MAXN; i++) {
    sum[i] = sum[i - 1] + (ans[i] == 0);
  }
  int t, cases = 0; cin >> t >> std::ws;
  while (t--) {
    char str[50];
    cin.getline(str, 50);
    char *sec = strchr(str, ' ');
    cout << "Case " << ++cases << ": ";
    if (sec) {
      while (*sec == ' ') sec++;
      *(sec - 1) = '\0';
      if (*sec == '\0') {
        sec = NULL;
      } else {
        int n1 = atoi(str), n2 = atoi(sec);
        cout << f(n1, n2) << '\n';
      }
    }
    if (!sec) {
      int n1 = atoi(str);
      cout << f(n1) << '\n';
    }
  }
}
