#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n, count = 0;
  while (std::cin >> n && n) {
    std::cout << "Set #" << ++count << '\n';
    int arr[50], avg = 0, res = 0;
    for (int i = 0; i < n; i++) {
      std::cin >> arr[i];
      avg += arr[i];
    }
    avg /= n;
    for (int i = 0; i < n; i++) {
      res += (avg > arr[i]) ? (avg - arr[i]) : (arr[i] - avg);
    }
    std::cout << "The minimum number of moves is " << res / 2 << ".\n\n";
  }
}
