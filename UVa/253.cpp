#include <iostream>
#include <algorithm>
#include <cmath>

struct P {
  char a, b;
  bool operator <(const P &rhs) {
    if (a + b != rhs.a + rhs.b) return a + b < rhs.a + rhs.b;
    return abs(a - b) < abs(rhs.a - rhs.b);
  }
  bool operator ==(const P &rhs) {
    return a == rhs.a && b == rhs.b || a == rhs.b && b == rhs.a;
  }
};

static inline void str2P(char *s, P *p) {
  p[0] = P { s[0], s[5] }; p[1] = P { s[1], s[4] }; p[2] = P { s[2], s[3] };
}

static inline bool transform(P *d1, P *d2) {
  int diffCnt = 0;
  for (int i = 0; i < 3; i++) {
    if (!(d1[i] == d2[i])) diffCnt++;
  }
  int swapCnt = (diffCnt == 2) ? 1 : 0;
  std::sort(d1, d1 + 3); std::sort(d2, d2 + 3);
  bool dup = false;
  for (int i = 0; i < 3; i++) {
    if (i && d1[i - 1] == d1[i] || d1[i].a == d1[i].b) dup = true;
    if (d1[i].a == d2[i].b && d1[i].b == d2[i].a) {
      if (!dup) {
        d1[i].a ^= d1[i].b ^= d1[i].a ^= d1[i].b;
        swapCnt++;
      }
    } else if (d1[i].a != d2[i].a || d1[i].b != d2[i].b) return false;
  }
  return dup || !(swapCnt & 1);
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  char str[16];
  while (std::cin.getline(str, 16)) {
    P d1[3], d2[3];
    str2P(str, d1); str2P(str + 6, d2);
    if (transform(d1, d2)) puts("TRUE");
    else puts("FALSE");
  }
}
