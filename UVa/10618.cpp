#include <iostream>
#include <cstring>
using std::cin; using std::cout;

const int LEFT = 0, UP = 1, DOWN = 2, RIGHT = 3, NONE = 4,
	LEFTF = 0, RIGHTF = 1, NOF = 2, INF = 0x5f5f5f5f;
char pos[256], moves[100];
int dp[100][4][4][3], action[100][4][4][3];

void update(int i, int lPos, int rPos, int lastFoot, int foot, int pos) {
	if ((foot == RIGHTF && pos == lPos) || (foot == LEFTF && pos == rPos)) return;
	if (pos == LEFT && foot == RIGHTF && lPos != UP && lPos != DOWN) return;
	if (pos == RIGHT && foot == LEFTF && rPos != UP && rPos != DOWN) return;
	if ((lPos == RIGHT && foot == RIGHTF && pos != rPos)) return;
	if ((rPos == LEFT && foot == LEFTF && pos != lPos)) return;
	int energy = 0;
	if (foot == NOF) {
		energy = 0;
	} else if (lastFoot != foot) {
		energy = 1;
	} else if (foot == LEFTF) {
		if (pos == lPos) energy = 3;
		else if (pos + lPos == 3) energy = 7;
		else energy = 5;
	} else if (foot == RIGHTF) {
		if (pos == rPos) energy = 3;
		else if (pos + rPos == 3) energy = 7;
		else energy = 5;
	}
	if (foot == LEFTF) energy += dp[i + 1][pos][rPos][LEFTF];
	else if (foot == RIGHTF) energy += dp[i + 1][lPos][pos][RIGHTF];
	else energy += dp[i + 1][lPos][rPos][NOF];
	int &ans = dp[i][lPos][rPos][lastFoot],
		&act = action[i][lPos][rPos][lastFoot];
	if (energy < ans) {
		ans = energy;
		act = (foot << 8) | pos;
	}
}

int main() {
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	pos['U'] = UP, pos['L'] = LEFT,
		pos['D'] = DOWN, pos['R'] = RIGHT, pos['.'] = NONE;
	while (cin >> moves && strcmp(moves, "#") != 0) {
		memset(dp, 0, sizeof(dp));
		int len = strlen(moves);
		for (int i = len - 1; i >= 0; i--) {
			for (int lPos = 0; lPos < 4; lPos++) {
				for (int rPos = 0; rPos < 4; rPos++) {
					if (lPos == rPos) continue;
					for (int lastFoot = 0; lastFoot < 3; lastFoot++) {
						dp[i][lPos][rPos][lastFoot] = INF;
						if (pos[(int)moves[i]] == NONE) {
							update(i, lPos, rPos, lastFoot, NOF, NONE);
							for (int m = 0; m < 4; m++) {
								update(i, lPos, rPos, lastFoot, LEFTF, m);
								update(i, lPos, rPos, lastFoot, RIGHTF, m);
							}
						} else {
							update(i, lPos, rPos, lastFoot, LEFTF, pos[(int)moves[i]]);
							update(i, lPos, rPos, lastFoot, RIGHTF, pos[(int)moves[i]]);
						}
					}
				}
			}
		}
		for (int i = 0, lPos = LEFT, rPos = RIGHT, lastFoot = NOF; i < len; i++) {
			int &t = action[i][lPos][rPos][lastFoot];
			int f = t >> 8, p = t & ((1 << 8) - 1);
			if (f == LEFTF) {
				cout << 'L';
				lPos = p;
			} else if (f == RIGHTF) {
				cout << 'R';
				rPos = p;
			} else {
				cout << '.';
			}
			lastFoot = f;
		}
		cout << '\n';
	}
}
