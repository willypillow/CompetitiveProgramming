#include <iostream>
#include <cstring>
#include <queue>
#include <unordered_map>
#include <vector>
#include <string>
using std::cin; using std::cout; using std::vector; using std::unordered_map; using std::string; using std::queue; using std::min;

const int INF = 1 << 30, MAXN = 500, S = 0, T = 1;
struct Edge { int from, to, cap, flow; };
struct Conv { int from, to; } conv[MAXN];
int n, m, k, device[MAXN];
vector<Edge> edges;
vector<int> recept;
vector<vector<int> >g;
unordered_map<string, int> plugs;

int toId(string name) {
	if (plugs.count(name)) return plugs[name];
	int nId = plugs.size();
	return plugs[name] = nId;
}

void addEdge(int from, int to, int cap) {
	edges.push_back(Edge { from, to, cap, 0 });
	edges.push_back(Edge { to, from, 0, 0 });
	g[from].push_back(edges.size() - 2);
	g[to].push_back(edges.size() - 1);
}

void build() {
	for (int i = 0; i < m; i++) {
		addEdge(S, i + 2, 1);
		addEdge(i + 2, m + 2 + device[i], INF);
	}
	for (int i = 0; i < k; i++) {
		addEdge(m + 2 + conv[i].from, m + 2 + conv[i].to, INF);
	}
	for (int i: recept) {
		addEdge(m + 2 + i, T, 1);
	}
}

int maxFlow() {
	int flow = 0, a[MAXN], p[MAXN];
	for (;;) {
		memset(a, 0, sizeof(a));
		queue<int> q;
		q.push(S); a[S] = INF;
		while (!q.empty()) {
			int cur = q.front(); q.pop();
			for (unsigned int i = 0; i < g[cur].size(); i++) {
				Edge &e = edges[g[cur][i]];
				if (!a[e.to] && e.cap > e.flow) {
					a[e.to] = min(a[cur], e.cap - e.flow);
					p[e.to] = g[cur][i];
					q.push(e.to);
				}
			}
			if (a[T]) break;
		}
		if (!a[T]) break;
		for (int x = T; x != S; x = edges[p[x]].from) {
			edges[p[x]].flow += a[T];
			edges[p[x] ^ 1].flow -= a[T];
		}
		flow += a[T];
	}
	return flow;
}

int main() {
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif
	int t; cin >> t;
	while (t--) {
		edges.clear(); g.clear(); plugs.clear(); recept.clear();
		cin >> n;
		for (int i = 0; i < n; i++) {
			string plug;
			cin >> plug;
			recept.push_back(toId(plug));
		}
		cin >> m;
		for (int i = 0; i < m; i++) {
			string name, plug;
			cin >> name >> plug;
			device[i] = toId(plug);
		}
		cin >> k;
		for (int i = 0; i < k; i++) {
			string to, from;
			cin >> from >> to;
			conv[i].to = toId(to), conv[i].from = toId(from);
		}
		g.resize(m + 2 + plugs.size());
		build();
		cout << m - maxFlow() << '\n';
		if (t) cout << '\n';
	}
}
