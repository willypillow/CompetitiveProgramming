#include <iostream>

struct Gate {
  int in[2];
} gate[200001];
int n, m;
bool res[200001];

// Get result when input [1, bound] is false and (bound,) is true
bool run(int bound) {
  for (int i = 1; i <= m; i++) {
    Gate &cur = gate[i];
    bool in1, in2;
    if (cur.in[0] < 0) in1 = (-cur.in[0] > bound) ? true : false;
    else in1 = res[cur.in[0]];
    if (cur.in[1] < 0) in2 = (-cur.in[1] > bound) ? true : false;
    else in2 = res[cur.in[1]];
    res[i] = !(in1 && in2);
  }
  return res[m];
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int d;
  std::cin >> d;
  while (d--) {
    std::cin >> n >> m;
    for (int i = 1; i <= m; i++) std::cin >> gate[i].in[0] >> gate[i].in[1];
    bool initial = run(0); // run(0) -> x = true, run(n) -> x = false
    if (run(n) == initial) { // The output is a constant anyway
      for (int i = 0; i < n; i++) putchar('0');
    } else { // Answer guranteed to be 0...0x1...1
      int l = 0, r = n, ans = -1;
      while (l < r) {
        int m = (l + r) >> 1;
        bool res = run(m);
        if (res == run(m + 1)) { // If input m changes, will the result change?
          if (initial ^ res) r = m; // Go left if result differs from run(0)
          else l = m + 1;
        } else {
          ans = m;
          break;
        }
      }
      for (int i = 0; i < ans; i++) putchar('0');
      putchar('x');
      for (int i = ans + 1; i < n; i++) putchar('1');
    }
    putchar('\n');
  }
}

