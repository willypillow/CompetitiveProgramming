#include <iostream>
using std::cin; using std::cout;

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int n;
  while (cin >> n && n) {
    int row = 0, col = 0;
    bool arr[100][100], fail = false;
    for (int i = 0; i < n; i++) {
      bool odd = 0;
      for (int j = 0; j < n; j++) {
        cin >> arr[i][j];
        odd ^= arr[i][j];
      }
      if (odd) {
        if (!row) row = i + 1;
        else fail = true;
      }
    }
    for (int j = 0; !fail && j < n; j++) {
      bool odd = 0;
      for (int i = 0; i < n; i++) {
        odd ^= arr[i][j];
      }
      if (odd) {
        if (!col) col = j + 1;
        else fail = true;
      }
    }
    if (fail) cout << "Corrupt\n";
    else if (!row && !col) cout << "OK\n";
    else cout << "Change bit (" << row << ',' << col << ")\n";
  }
}
