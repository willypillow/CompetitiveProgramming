#include <bits/stdc++.h>
using namespace std;

int dp[1 << 13];
char str[20];

int dfs(int x) {
	if (dp[x]) return dp[x];
	const int mask = 0b111;
	int ans = __builtin_popcount(x);
	for (int i = 0; i <= 12 - 3; i++) {
		int t = (x >> i) & mask;
		if (t == 0b110) ans = min(ans, dfs(x ^ (0b111 << i)));
		if (t == 0b011) ans = min(ans, dfs(x ^ (0b111 << i)));
	}
	return dp[x] = ans;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		cin >> str;
		int state = 0;
		for (int i = 0; i < 12; i++) state = state << 1 | (str[i] == 'o');
		cout << dfs(state) << '\n';
	}
}
