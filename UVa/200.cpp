#include <iostream>
#include <cstring>
using namespace std;

int tot, rTop;
char str[30], oldStr[30];
bool vis[30], use[30], notFirst[30], g[30][30];
int route[30];

void dfs(int cur) {
	vis[cur] = true;
	for (int i = 0; i < 26; i++) {
		if (g[cur][i] && !vis[i]) dfs(i);
	}
	route[--rTop] = cur;
}

void addEdge(char a, char b) {
	//cout << a << "->" << b << '\n';
	g[a - 'A'][b - 'A'] = true;
	notFirst[b - 'A'] = true;
	use[a - 'A'] = use[b - 'A'] = true;
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	tot = 0;
	memset(vis, false, sizeof(vis)); memset(use, false, sizeof(use));
	memset(notFirst, false, sizeof(notFirst)); memset(g, false, sizeof(g));
	int oldLen = 0, len = 0;
	while (cin >> str && str[0] != '#') {
		len = strlen(str);
		for (int i = 0; i < min(len, oldLen); i++) {
			if (oldStr[i] != str[i]) {
				addEdge(oldStr[i], str[i]);
				break;
			}
		}
		oldLen = len;
		strcpy(oldStr, str);
	}
	for (int i = 0; i < 26; i++) {
		if (use[i]) tot++;
	}
	rTop = tot;
	for (int i = 0; i < 26; i++) {
		if (use[i] && !notFirst[i] && !vis[i]) dfs(i);
	}
	for (int i = 0; i < tot; i++) {
		cout << (char)(route[i] + 'A');
	}
	cout << '\n';
}

