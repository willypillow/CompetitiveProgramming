#include <iostream>
#include <cstring>
using std::cin; using std::cout; using std::min;

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n; cin >> n >> std::ws;
  while (n--) {
    bool isPali[1000][1000] = { false };
    int d[1001];
    char str[1001];
    cin.getline(str, 1001);
    int len = strlen(str);
    for (int i = 0; i < len; i++) {
      // Use i as mid
      for (int j = 0; i - j >= 0 && i + j < len; j++) {
        if (str[i - j] == str[i + j]) isPali[i - j][i + j] = true;
        else break;
      }
      // Use i and i + 1 as mid
      if (str[i] == str[i + 1]) { // str[len] == '\0' anyway
        for (int j = 0; i - j >= 0 && i + 1 + j < len; j++) {
          if (str[i - j] == str[i + 1 + j]) isPali[i - j][i + 1 + j] = true;
          else break;
        }
      }
    }
    d[0] = 0;
    for (int i = 1; i <= len; i++) {
      d[i] = 1 << 30;
      for (int j = 0; j < i; j++) {
        if (isPali[j][i - 1]) {
          d[i] = min(d[i], d[j] + 1);
        }
      }
    }
    cout << d[len] << '\n';
  }
}
