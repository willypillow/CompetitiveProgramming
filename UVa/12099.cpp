#include <bits/stdc++.h>
using namespace std;

typedef int Int;
const Int INF = 0x2f2f2f2f;
Int dp[2131][2131], sumT[71];

struct Book {
	Int h, t;
	bool operator <(const Book &rhs) const {
		return h > rhs.h;
	}
} b[71];

static inline Int min(Int x, Int y) { return (x < y) ? x : y; }
static inline Int max(Int x, Int y) { return (x > y) ? x : y; }

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	Int t; cin >> t;
	while (t--) {
		memset(dp, INF, sizeof(dp));
		Int n; cin >> n;
		for (Int i = 0; i < n; i++) cin >> b[i].h >> b[i].t;
		sort(b, b + n);
		for (Int i = 0; i < n; i++) sumT[i + 1] = sumT[i] + b[i].t;
		Int tot = sumT[n];
		dp[0][0] = b[0].h;
		for (Int i = 0; i < n; i++) {
			for (int j = sumT[i]; j >= 0; j--) {
				for (int k = min(j + 30, sumT[i] - j), flor = max(0, 2 * j + 30 - tot); k >= flor; k--) {
					if (dp[j][k] == INF) continue;
					dp[j][k] = min(dp[j][k], dp[j][k]);
					dp[j + b[i].t][k] = min(dp[j + b[i].t][k], dp[j][k] + (j ? 0 : b[i].h));
					dp[j][k + b[i].t] = min(dp[j][k + b[i].t], dp[j][k] + (k ? 0 : b[i].h));
				}
			}
		}
		Int ans = 1 << 30;
		for (Int j = 1; j <= tot; j++) {
			for (Int k = max(1, 2 * j + 30 - tot), top = min(j + 30, tot - j); k <= top; k++) {
				Int w = max(tot - j - k, max(j, k)), h = dp[j][k];
				if (h < INF) ans = min(ans, w * h);
			}
		}
		cout << ans << '\n';
	}
}
