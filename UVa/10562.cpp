#include <iostream>
#include <cstring>

char str[200][256];
int n;

void solve(int i, int j) {
  std::cout << str[i][j] << '(';
  if (i + 1 < n && str[i + 1][j] == '|') {
    while (str[i + 2][j] == '-') j--;
    j++;
    while (str[i + 3][j] != '\0' && str[i + 2][j] == '-') {
      if (!isspace(str[i + 3][j])) solve(i + 3, j);
      j++;
    }
  }
  std::cout << ')';
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int t;
  std::cin >> t;
  std::cin.ignore(1000, '\n');
  while (t--) {
    n = 0;
    while (std::cin.getline(str[n], 256) && str[n][0] != '#') n++;
    std::cout << '(';
    if (n) {
      int len = strlen(str[0]);
      for (int i = 0; i < len; i++) {
        if (!isspace(str[0][i])) solve(0, i);
      }
    }
    std::cout << ")\n";
  }
}
