#include <iostream>
#include <queue>

int queue[50000], qBack, qFront;
unsigned char toTeam[1000000];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int t, cases = 1;
  while (std::cin >> t && t) {
    std::cout << "Scenario #" << cases++ << '\n';
    qBack = qFront = 0;
    std::queue<int> teamQueues[1001];
    for (int i = 0; i < t; i++) {
      int n;
      std::cin >> n;
      for (int j = 0; j < n; j++) {
        int id;
        std::cin >> id;
        toTeam[id] = i;
      }
    }
    char cmd[10];
    while (std::cin >> cmd && cmd[0] != 'S') {
      if (cmd[0] == 'E') {
        int id;
        std::cin >> id;
        int team = toTeam[id];
        if (teamQueues[team].empty()) queue[qBack++] = team;
        teamQueues[team].push(id);
      } else {
        auto &sub = teamQueues[queue[qFront]];
        std::cout << sub.front() << '\n'; sub.pop();
        if (sub.empty()) qFront++;
      }
    }
    std::cout << '\n';
  }
}
