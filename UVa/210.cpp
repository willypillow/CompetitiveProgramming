#include <iostream>

struct Cmd {
  char action;
  int val;
  char var;
} cmds[2000][50];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int t;
  std::cin >> t;
  while (t--) {
    int n, unitT[5], quant, runQueue[100000], rFront = 0, rBack = 0,
      bQueue[100000], bFront = 0, bBack = 0, cur[2000] = { 0 }, vars[26] = { 0 };
    std::cin >> n >> unitT[0] >> unitT[1] >>
      unitT[2] >> unitT[3] >> unitT[4] >> quant;
    for (int i = 0; i < n; i++) {
      for (int j = 0;; j++) {
        char tmp[32];
        std::cin >> tmp;
        if (tmp[1] == '\0') {
          cmds[i][j].action = 'a';
          cmds[i][j].var = tmp[0];
          std::cin.ignore(1000, '=');
          std::cin >> cmds[i][j].val;
        } else if (tmp[0] == 'p') {
          cmds[i][j].action = 'p';
          std::cin >> cmds[i][j].var;
        } else if (tmp[0] == 'e') {
          cmds[i][j].action = 'e';
          break;
        } else {
          cmds[i][j].action = tmp[0];
        }
      }
      runQueue[rBack++] = i;
    }
    bool lock = false;
    while (rFront != rBack) {
      int p = runQueue[rFront++], time = quant;
      while (time > 0) {
        switch (cmds[p][cur[p]].action) {
        case 'a':
          time -= unitT[0];
          vars[cmds[p][cur[p]].var - 'a'] = cmds[p][cur[p]].val;
          break;
        case 'p':
          time -= unitT[1];
          std::cout << p + 1 << ": " << vars[cmds[p][cur[p]].var - 'a'] << '\n';
          break;
        case 'l':
          time -= unitT[2];
          if (!lock) lock = true;
          else {
            bQueue[bBack++] = p;
            goto Next;
          }
          break;
        case 'u':
          time -= unitT[3];
          lock = false;
          if (bFront != bBack) runQueue[--rFront] = bQueue[bFront++];
          break;
        case 'e':
          goto Next;
        }
        cur[p]++;
      }
      runQueue[rBack++] = p;
Next:;
    }
    if (t) std::cout << '\n';
  }
}
