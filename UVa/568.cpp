#include <iostream>
#include <iomanip>
using std::cin; using std::cout; using std::setw;

// 1 * 2 -> 2 / 8 * 2 * 2 -> 2 / 4 * 2 * 2 * 2 -> 2 / 2 * 2 * 2 * 2 * 2 -> 2
static const int START[] = { 1, 8, 4, 2 };

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  while (cin >> n) {
    int fiveCnt = 0, nn = n, ans = 0;
    while (nn) fiveCnt += (nn /= 5); // Count the factor of fives
    ans = START[fiveCnt % 4]; // 5 cancels out 2,which repeats every 4 times
    nn = n;
    while (nn) {
      for (int i = 2; i <= 9; i++) { // Skip 1 for speed
        if (i == 5) continue;
        int cnt = (nn / 10) + ((nn % 10 >= i) ? 1 : 0); // i in last digit
        cnt %= 4; // All digits repeat within a period of 4
        while (cnt--) ans = (ans * i) % 10;
      }
      nn /= 5; // Process stuff like 2 * 35 = (2 * 5) * 7
    }
    cout << setw(5) << n << " -> " << ans << '\n';
  }
}
