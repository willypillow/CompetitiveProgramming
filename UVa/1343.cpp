#include <iostream>

struct State {
  int arr[24], steps = 0;
  char moves[100] = { 0 };
};

static const int DIRECTIONS[][7] = { { 0, 2, 6, 11, 15, 20, 22 },
  { 1, 3, 8, 12, 17, 21, 23 }, { 10, 9, 8, 7, 6, 5, 4 },
  { 19, 18, 17, 16, 15, 14, 13 }, { 23, 21, 17, 12, 8, 3, 1 },
  { 22, 20, 15, 11, 6, 2, 0 }, { 13, 14, 15, 16, 17, 18, 19 },
  { 4, 5, 6, 7, 8, 9, 10 } };
static const int CENTER[] = { 6, 7, 8, 11, 12, 15, 16, 17 };

static inline State move(State s, int dir) {
  int tmp = s.arr[DIRECTIONS[dir][0]];
  for (int i = 0; i < 6; i++) {
    s.arr[DIRECTIONS[dir][i]] = s.arr[DIRECTIONS[dir][i + 1]];
  }
  s.arr[DIRECTIONS[dir][6]] = tmp;
  s.moves[s.steps++] = dir + 'A';
  return s;
}

static inline int h(State &cur) {
  int cnt[3] = { 0 };
  for (int x: CENTER) cnt[cur.arr[x] - 1]++;
  return 8 - std::max(cnt[0], std::max(cnt[1], cnt[2]));
}

int dfs(State &cur, int maxDepth) {
  int hVal = h(cur);
  if (hVal == 0) {
    if (cur.steps == 0) std::cout << "No moves needed";
    else std::cout << cur.moves;
    std::cout << '\n' << cur.arr[6] << '\n';
    return -1;
  }
  if (cur.steps + hVal > maxDepth) return cur.steps + hVal;
  int nextMaxD = 1 << 30;
  for (int i = 0; i < 8; i++) {
    State s = move(cur, i);
    int res = dfs(s, maxDepth);
    if (res == -1) return -1;
    nextMaxD = std::min(nextMaxD, res);
  }
  return nextMaxD;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif 
  int tmp;
  while (std::cin >> tmp && tmp) {
    State root;
    root.arr[0] = tmp;
    for (int i = 1; i < 24; i++) {
      std::cin >> root.arr[i];
    }
    //for (int maxDepth = 1; maxDepth != -1; maxDepth = dfs(root, maxDepth));
    for (int maxDepth = 0; dfs(root, maxDepth) != -1; maxDepth++);
  }
}
