#include <bits/stdc++.h>
using namespace std;

vector<string> ans;
vector<int> g[110];
int low[110], vis[110], timer;
string strs[110];
unordered_map<string, int> mp;

void dfs(int x, int p) {
	low[x] = vis[x] = ++timer;
	bool ap = false;
	int child = 0;
	for (int y: g[x]) {
		if (y == p) continue;
		if (!vis[y]) {
			child++;
			dfs(y, x);
			low[x] = min(low[x], low[y]);
			if (low[y] >= vis[x]) ap = true;
		} else {
			low[x] = min(low[x], vis[y]);
		}
	}
	if ((x == p && child > 1) || (x != p && ap)) ans.push_back(strs[x]);
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	cin.tie(0), ios_base::sync_with_stdio(0);
	for (int n, cases = 1; cin >> n && n; cases++) {
		memset(low, 0, sizeof(low)), memset(vis, 0, sizeof(vis));
		ans.clear(), mp.clear();
		for (int i = 0; i < n; i++) {
			cin >> strs[i];
			mp[strs[i]] = i;
			g[i].clear();
		}
		int m; cin >> m;
		while (m--) {
			string u, v; cin >> u >> v;
			int iu = mp[u], iv = mp[v];
			g[iu].push_back(iv), g[iv].push_back(iu);
		}
		for (int i = 0; i < n; i++) {
			if (!vis[i]) dfs(i, i);
		}
		sort(ans.begin(), ans.end());
		if (cases > 1) cout << '\n';
		cout << "City map #" << cases << ": " << ans.size() << " camera(s) found\n";
		for (auto &x: ans) cout << x << '\n';
	}
}
