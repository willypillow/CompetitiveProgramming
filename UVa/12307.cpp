#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E5 + 10;
const double EPS = 1E-9, PI = acos(-1);

struct P {
	double x, y;
	P operator +(const P &rhs) const {
		return P { x + rhs.x, y + rhs.y };
	}
	P operator -(const P &rhs) const {
		return P { x - rhs.x, y - rhs.y };
	}
	bool operator <(const P &rhs) const {
		return (x != rhs.x) ? x < rhs.x : y < rhs.y;
	}
} p[MAXN], hull[MAXN];
typedef P Vect;
const P ZERO = P { 0, 0 };

int cmp(double x) {
	return (fabs(x) < EPS) ? 0 : x > 0 ? 1 : -1;
}

double cross(const P &o, const P &a, const P &b) {
	return (a.x - o.x) * (b.y - o.y) - (b.x - o.x) * (a.y - o.y);
}

double cross(const P &a, const P &b) {
	return a.x * b.y - a.y * b.x;
}

double dot(const Vect &a, const Vect &b) {
	return a.x * b.x + a.y * b.y;
}

double angle(const Vect &a, const Vect &b) {
	return atan2(cross(a, b), dot(a, b));
}

double turnAngle(const Vect &a, const Vect &b) {
	return (cmp(dot(a, b)) > 0) ? angle(a, b) : (PI + angle(a, b));
}

Vect rotate(const Vect &a, double rad) {
	return Vect { a.x * cos(rad) - a.y * sin(rad),
		a.y * cos(rad) + a.x * sin(rad) };
}

double dist(const P &a, const P &b) {
	return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
}

double distToLine(const P &o, const P &a, const P &b) {
	return fabs(cross(o, a, b) / dist(a, b));
}

int convex(int n) {
	sort(p, p + n);
	int m = 0;
	for (int i = 0; i < n; i++) {
		while (m >= 2 && cross(hull[m - 2], hull[m - 1], p[i]) < 0) m--;
		hull[m++] = p[i];
	}
	for (int i = n - 2, t = m; i >= 0; i--) {
		while (m > t && cross(hull[m - 2], hull[m - 1], p[i]) < 0) m--;
		hull[m++] = p[i];
	}
	return m - 1;
}

int main() {
	int n;
	while (cin >> n && n) {
		for (int i = 0; i < n; i++) cin >> p[i].x >> p[i].y;
		n = convex(n);
		int p1 = 0, p2 = 0, p3 = 0, p4 = 0;
		for (int i = 1; i < n; i++) {
			if (cmp(hull[i].y - hull[p1].y) < 0) p1 = i;
			if (cmp(hull[i].y - hull[p2].y) > 0) p2 = i;
			if (cmp(hull[i].x - hull[p3].x) < 0) p3 = i;
			if (cmp(hull[i].x - hull[p4].x) > 0) p4 = i;
		}
		Vect orig = { 1, 0 }, v1 = { 1, 0 }, v2 = { 0, 1 };
		double ansArea = 1E30, ansLen = 1E30;
		while (cmp(cross(orig, v1)) >= 0) {
			double d1 = distToLine(hull[p1], hull[p2], hull[p2] + v1),
				d2 = distToLine(hull[p3], hull[p4], hull[p4] + v2);
			ansArea = min(ansArea, d1 * d2);
			ansLen = min(ansLen, 2 * (d1 + d2));
			double minAng = turnAngle(v1, hull[p1 + 1] - hull[p1]);
			minAng = min(minAng, turnAngle(ZERO - v1, hull[p2 + 1] - hull[p2]));
			minAng = min(minAng, turnAngle(ZERO - v2, hull[p3 + 1] - hull[p3]));
			minAng = min(minAng, turnAngle(v2, hull[p4 + 1] - hull[p4]));
			v1 = rotate(v1, minAng); v2 = rotate(v2, minAng);
			if (cmp(angle(v1, hull[p1 + 1] - hull[p1])) == 0) p1 = (p1 + 1) % n;
			if (cmp(angle(ZERO - v1, hull[p2 + 1] - hull[p2])) == 0) p2 = (p2 + 1) % n;
			if (cmp(angle(ZERO - v2, hull[p3 + 1] - hull[p3])) == 0) p3 = (p3 + 1) % n;
			if (cmp(angle(v2, hull[p4 + 1] - hull[p4])) == 0) p4 = (p4 + 1) % n;
		}
		cout << fixed << setprecision(2) << ansArea << ' ' << ansLen << '\n';
	}
}
