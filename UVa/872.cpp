#include <iostream>
#include <cstring>
#include <sstream>
#include <string>
using namespace std;

bool vis[30], g[30][30], use[30];
char ans[30];
int n;
bool success;

void dfs(int cur) {
	if (cur == n) {
		success = true;
		for (int i = 0; i < n - 1; i++) cout << ans[i] << ' ';
		cout << ans[n - 1] << '\n';
		return;
	}
	for (int i = 0; i < 26; i++) {
		bool fail = false;
		if (vis[i] || !use[i]) continue;
		for (int j = 0; j < 26; j++) {
			if (g[i][j] && vis[j]) {
				fail = true;
				break;
			}
		}
		if (fail) continue;
		vis[i] = true;
		ans[cur] = i + 'A';
		dfs(cur + 1);
		vis[i] = false;
	}
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	string str, sub;
	int t; cin >> t >> ws;
	while (t--) {
		getline(cin, str);
		n = 0;
		success = false;
		memset(vis, 0, sizeof(vis)); memset(use, 0, sizeof(use));
		memset(g, false, sizeof(g));
		stringstream ss(str);
		char c;
		while (ss >> c) {
			use[c - 'A'] = true;
		}
		for (int i = 0; i < 26; i++) {
			if (use[i]) n++;
		}
		getline(cin, str);
		ss = stringstream(str);
		while (ss >> sub) {
			g[sub[0] - 'A'][sub[2] - 'A'] = true;
		}
		dfs(0);
		if (!success) cout << "NO\n";
		if (t) cout << '\n';
		cin.ignore(100, '\n');
	}
}
