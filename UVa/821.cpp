#include <iostream>
#include <cstring>
#include <iomanip>
using std::cin; using std::cout; using std::setprecision;

const int INF = 0x3f3f3f3f;
int dist[101][101];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	for (int cases = 1; ; cases++) {
		memset(dist, INF, sizeof(dist));
		int i, a, b;
		for (i = 0; cin >> a >> b && a != 0; i++) dist[a][b] = 1;
		if (!i) break;
		for (int k = 1; k <= 100; k++) {
			for (int i = 1; i <= 100; i++) {
				for (int j = 1; j <= 100; j++) {
					dist[i][j] = std::min(dist[i][j], dist[i][k] + dist[k][j]);
				}
			}
		}
		int cnt = 0, sum = 0;
		for (int i = 1; i <= 100; i++) {
			for (int j = 1; j <= 100; j++) {
				if (i != j && dist[i][j] != INF) {
					cnt++;
					sum += dist[i][j];
				}
			}
		}
		cout << "Case " << cases << ": average length between pages = " <<
			std::fixed << setprecision(3) << (double)sum / cnt << " clicks\n";
	}
}
