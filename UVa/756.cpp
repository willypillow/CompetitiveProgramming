#include <iostream>
using std::cin; using std::cout;

int extGcd(int a, int b, int *x, int *y) {
	if (!b) {
		*x = 1, *y = 0;
		return a;
	} else {
		int g = extGcd(b, a % b, x, y);
		int t = *x;
		*x = *y;
		*y = t - (a / b) * *y;
		return g;
	}
}

int inv(int n, int m) {
	int x, y;
	extGcd(n, m, &x, &y);
	return (x % m + m) % m;
}

int main() {
	int inv1 = inv(28 * 33, 23), inv2 = inv(23 * 33, 28), inv3 = inv(23 * 28, 33);
	int p, e, i, d, cases = 0;
	while (cin >> p >> e >> i >> d && p >= 0) {
		int t = (p * inv1 * 28 * 33) + (e * inv2 * 23 * 33) + (i * inv3 * 23 * 28);
		t = ((t - d) + 21251) % 21252;
		cout << "Case " << ++cases << ": the next triple peak occurs in " << t + 1 << " days.\n";
	}
}
