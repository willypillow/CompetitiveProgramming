#include <iostream>

#define RELINK(x, y) { left[y] = x; right[x] = y; }

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n, m, cases = 1;
  while (std::cin >> n >> m) {
    int left[100001], right[100001];
    bool reverse = false;
    for (int i = 1; i <= n; i++) {
      left[i] = i - 1;
      right[i] = i + 1;
    }
    right[0] = 1;
    right[n] = -1;
    while (m--) {
      int cmd, x, y, lx, rx, ly, ry;
      std::cin >> cmd;
      if (cmd != 4) {
        std::cin >> x >> y;
        lx = left[x];
        rx = right[x];
        ly = left[y];
        ry = right[y];
      }
      if (reverse && (cmd == 1 || cmd == 2)) cmd ^= (1 ^ 2);
      switch (cmd) {
      case 1:
        if (x == ly) break;
        RELINK(lx, rx);
        RELINK(ly, x);
        RELINK(x, y);
        break;
      case 2:
        if (x == ry) break;
        RELINK(lx, rx);
        RELINK(x, ry);
        RELINK(y, x)
        break;
      case 3:
        if (x == ly) {
          RELINK(lx, y);
          RELINK(y, x);
          RELINK(x, ry);
        } else if (y == lx) {
          RELINK(ly, x);
          RELINK(x, y);
          RELINK(y, rx);
        } else {
          RELINK(lx, y);
          RELINK(y, rx);
          RELINK(ly, x);
          RELINK(x, ry);
        }
        break;
      case 4:
        reverse = !reverse;
      }
    }
    long long ans = 0;
    for (int p = right[0]; p != -1; p = right[right[p]]) {
      ans += p;
      if (right[p] == -1) break;
    }
    if (reverse && (n & 1) == 0) ans = (long long)n * (n + 1) / 2 - ans;
    std::cout << "Case " << cases++ << ": " << ans << '\n';
  }
}
