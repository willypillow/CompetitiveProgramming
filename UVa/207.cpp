#include <iostream>
#include <algorithm>
#include <cstring>
#include <iomanip>

struct Player {
  char name[24];
  bool isPro;
  int scores[4] = { 0 }, dq = 4, sum = 0;
  bool isInit = false;
};

bool cmp1(const Player &lhs, const Player &rhs) {
  if (lhs.dq < 2) return false;
  if (rhs.dq < 2) return true;
  return lhs.scores[0] + lhs.scores[1] < rhs.scores[0] + rhs.scores[1];
}

bool cmp2(const Player &lhs, const Player &rhs) {
  if (lhs.dq != rhs.dq) return lhs.dq > rhs.dq;
  if (lhs.sum != rhs.sum) return lhs.sum < rhs.sum;
  return strcmp(lhs.name, rhs.name) < 0;
}

inline void getChars(char *buf, int n) {
  for (int i = 0; i < n; i++) std::cin.get(buf[i]);
  buf[n] = '\0';
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int t;
  std::cin >> t;
  while (t--) {
    Player players[160];
    double total, percent, prizes[70];
    std::cin >> total;
    for (int i = 0; i < 70; i++) {
      std::cin >> percent;
      prizes[i] = total * percent / 100;
    }
    int playerN;
    std::cin >> playerN;
    for (int i = 0; i < playerN; i++) {
      std::cin.ignore(1000, '\n');
      char *name = players[i].name;
      getChars(name, 20);
      players[i].isPro = strchr(name, '*') == NULL;
      for (int j = 0; j < 4; j++) {
        char tmp[8];
        std::cin >> tmp;
        if (tmp[0] == 'D') {
          players[i].dq = j;
          break;
        }
        players[i].sum += players[i].scores[j] = atoi(tmp);
      }
      players[i].isInit = true;
    }
    std::sort(players, players + playerN, cmp1);
    int cut = (playerN < 70) ? playerN : 70,
      lastScore = players[cut - 1].scores[0] + players[cut - 1].scores[1];
    while (players[cut - 1].dq < 2) cut--;
    while (players[cut].isInit && players[cut].dq >= 2 &&
      players[cut].scores[0] + players[cut].scores[1] == lastScore) cut++;
    std::sort(players, players + cut, cmp2);
    std::cout << "Player Name          Place     RD1  RD2  RD3  RD4  TOTAL     Money Won\n"
      "-----------------------------------------------------------------------\n";
    for (int i = 0, prizeCnt = 0; i < cut; i++) {
      int dup = 1, amateur = 0;
      while (players[i + dup].sum == players[i].sum && players[i + dup].isInit) dup++;
      double prize = 0;
      for (int j = 0; j < dup; j++) {
        if (!players[i + j].isPro) amateur++;
        else prize += prizes[prizeCnt + j - amateur];
      }
      prize /= dup - amateur;
      bool isPrize = prizeCnt < 70;
      for (int j = 0, priceI; j < dup; j++) {
        std::cout << players[i + j].name << ' ';
        if (players[i + j].dq != 4) {
          std::cout << "          ";
        } else if (dup - amateur == 1 || !players[i + j].isPro || !isPrize) {
          std::cout << std::setw(10) << std::left << i + 1;
        } else {
          std::cout << i + 1 << std::setw(i + 1 >= 10 ? 8 : 9) << std::left << 'T';
        }
        for (int k = 0; k < 4; k++) {
          if (k < players[i + j].dq) {
            std::cout << std::setw(5) << std::left << players[i + j].scores[k];
          } else {
            std::cout << std::setw(5) << ' ';
          }
        }
        if (players[i + j].dq != 4) std::cout << "DQ";
        else if (players[i + j].isPro && isPrize) std::cout << std::setw(10) << std::left << players[i + j].sum;
        else std::cout << players[i + j].sum;
        if (players[i + j].dq == 4 && players[i + j].isPro && isPrize) {
          std::cout << '$' << std::setw(9) << std::right <<
            std::setprecision(2) << std::fixed << prize;
        }
        std::cout << '\n';
      }
      i += dup - 1;
      prizeCnt += dup - amateur;
    }
    if (t) std::cout << '\n';
  }
}
