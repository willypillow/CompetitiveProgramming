#include <iostream>
#include <sstream>

int subTree() {
  int wl, dl, wr, dr;
  std::cin >> wl >> dl >> wr >> dr;
  if (!wl) wl = subTree();
  if (!wr) wr = subTree();
  return (wl && wr && wl * dl == wr * dr) ? (wl + wr) : 0;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  std::cin >> n;
  while (n--) {
    if (subTree()) puts("YES");
    else puts("NO");
    if (n) puts("");
  }
}
