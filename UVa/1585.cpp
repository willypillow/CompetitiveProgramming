#include <iostream>

int getChr() {
  static char buf[1 << 20], *p = buf, *end = buf;
  if (p == end) {
    if ((end = buf + fread(buf, 1, 1 << 20, stdin)) == buf) return -1;
    p = buf;
  }
  return *p++;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  char c;
  int score = 0, add = 1;
  bool isBlank;
  while ((c = getChr()) != '\n');
  while ((c = getChr()) != -1) {
    if (c == 'O') {
      isBlank = false;
      score += add++;
    } else if (c == 'X') {
      isBlank = false;
      add = 1;
    } else if (!isBlank) {
      std::cout << score << '\n';
      score = 0;
      add = 1;
      isBlank = true;
    }
  }
}
