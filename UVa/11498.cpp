#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int t;
  while (std::cin >> t && t) {
    int x0, y0;
    std::cin >> x0 >> y0;
    while (t--) {
      int x, y;
      std::cin >> x >> y;
      if (x == x0 || y == y0) puts("divisa");
      else if (x > x0 && y > y0) puts("NE");
      else if (x > x0 && y < y0) puts("SE");
      else if (x < x0 && y > y0) puts("NO");
      else puts("SO");
    }
  }
}
