#include <bits/stdc++.h>
using namespace std;

char str[20];

vector<string> dfs(int d) {
	if (d == 0) return vector<string>(1, string(1, str[0]));
	auto v = dfs(d - 1);
	vector<string> ans;
	for (auto &s: v) {
		for (int i = 0; i <= d; i++) {
			string res;
			for (int j = 0; j < i; j++) res.push_back(s[j]);
			res.push_back(str[d]);
			for (int j = i; j < d; j++) res.push_back(s[j]);
			ans.push_back(res);
		}
	}
	return ans;
}

int main() {
	bool first = true;
	while (cin >> str) {
		int n = strlen(str);
		auto v = dfs(n - 1);
		if (!first) cout << '\n';
		first = false;
		for (auto &s: v) cout << s << '\n';
	}
}
