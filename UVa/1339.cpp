#include <iostream>
#include <algorithm>
#include <cstring>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  char str1[100], str2[100];
  while (std::cin >> str1 >> str2) {
    int count1[26] = { 0 }, count2[26] = { 0 };
    int len1 = strlen(str1), len2 = strlen(str2);
    for (int i = 0; i < len1; i++) count1[str1[i] - 'A']++;
    for (int i = 0; i < len2; i++) count2[str2[i] - 'A']++;
    std::sort(count1, count1 + 26);
    std::sort(count2, count2 + 26);
    bool fail = false;
    for (int i = 0; i < 26; i++) {
      if (count1[i] != count2[i]) {
        fail = true;
        break;
      }
    }
    if (fail) puts("NO");
    else puts("YES");
  }
}
