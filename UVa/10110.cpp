#include <iostream>
#include <cmath>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  unsigned int n;
  while (std::cin >> n && n) {
  	int x = round(sqrt(n));
  	if (x * x == n) puts("yes");
  	else puts("no");
  }
}
