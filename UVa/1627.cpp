#include <iostream>
#include <cstring>
#include <vector>
using std::cin; using std::cout; using std::vector;

const int INF = 1 << 30;
int sack[110], sTop, vis[110], n;
bool g[110][110], canMake[110][220];
vector<int> teams[110][2];

bool dfs(int cur, int curType) {
	vis[cur] = curType;
	teams[sTop][curType - 1].push_back(cur);
	for (int i = 1; i <= n; i++) {
		if (i != cur && (!g[cur][i] || !g[i][cur])) {
			if (vis[i] == curType) return false;
			if (!vis[i] && !dfs(i, curType ^ (1 ^ 2))) return false;
		}
	}
	return true;
}

void print() {
	int minSum = 0;
	for (int i = 0; i <= n; i++) {
		if (canMake[sTop][i + n]) { minSum = i; break; }
		if (canMake[sTop][-i + n]) { minSum = -i; break; }
	}
	vector<int> team1, team2;
	for (int i = sTop - 1; i >= 0; i--) {
		if (canMake[i][n + minSum + sack[i]]) {
			minSum += sack[i];
			for (int pers: teams[i][0]) team1.push_back(pers);
			for (int pers: teams[i][1]) team2.push_back(pers);
		} else {
			minSum -= sack[i];
			for (int pers: teams[i][0]) team2.push_back(pers);
			for (int pers: teams[i][1]) team1.push_back(pers);
		}
	}
	cout << team1.size();
	for (int x: team1) cout << ' ' << x;
	cout << '\n';
	cout << team2.size();
	for (int x: team2) cout << ' ' << x;
	cout << '\n';
}

void dp() {
	canMake[0][0 + n] = true;
	for (int i = 0; i < sTop; i++) {
		for (int j = -n; j <= n; j++) {
			if (canMake[i][j + n]) {
				canMake[i + 1][j + n + sack[i]] = canMake[i + 1][j + n - sack[i]] = true;
			}
		}
	}
}

void init() {
	memset(vis, 0, sizeof(vis)); memset(g, false, sizeof(g));
	memset(canMake, false, sizeof(canMake));
	sTop = 0;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif
	int t; cin >> t;
	while (t--) {
		init();
		bool fail = false;
		cin >> n;
		for (int i = 1; i <= n; i++) {
			for (int x = 0; cin >> x && x; ) g[i][x] = true;
		}
		for (int i = 1; i <= n; i++) {
			if (!vis[i]) {
				teams[sTop][0].clear(); teams[sTop][1].clear();
				if (!dfs(i, 1)) {
					fail = true;
					break;
				}
				sack[sTop] = teams[sTop][0].size() - teams[sTop][1].size();
				sTop++;
			}
		}
		if (n > 1 && !fail) { dp(); print(); }
		else cout << "No solution\n";
		if (t) cout << '\n';
	}
}
