#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n, m, cases = 1;
  while (std::cin >> n >> m) {
    if (cases != 1) std::cout << "\n**********************************\n\n";
    std::cout << "Problem #" << cases++ << "\n\n";
    bool v[10][10] = { false }, h[10][10] = { false };
    for (int i = 0; i < m; i++) {
      char type;
      int x, y;
      std::cin >> type >> x >> y;
      if (type == 'V') v[x][y] = true;
      else h[x][y] = true;
    }
    int ans[10] = { 0 };
    for (int w = 1; w < n; w++) {
      for (int i = 1; i + w <= n; i++) {
        for (int j = 1; j + w <= n; j++) {
          bool fail = false;
          for (int k = 0; k < w; k++) {
            if (!v[j + w][i + k] || !v[j][i + k] ||
                !h[i + w][j + k] || !h[i][j + k]) {
              fail = true;
              break;
            }
          }
          if (!fail) ans[w]++;
        }
      }
    }
    bool found = false;
    for (int i = 1; i < n; i++) {
      if (ans[i]) {
        std::cout << ans[i] << " square (s) of size "<< i << '\n';
        found = true;
      }
    }
    if (!found) std::cout << "No completed squares can be found.\n";
  }
}
