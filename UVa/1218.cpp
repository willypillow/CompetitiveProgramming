#include <iostream>
#include <cstring>
#include <vector>
using std::cin; using std::cout; using std::vector; using std::min;

enum State { UNSERVED, SERVED, SERVER };
vector<int> edges[10001];
const int INF = 1 << 15;

int dfs(int x, int parent, State s) {
  int ans = (s == UNSERVED) ? INF : (s == SERVER) ? 1 : 0;
  int served = (s == UNSERVED) ? dfs(x, parent, SERVED) : 0;
  for (unsigned int i = 0; i < edges[x].size(); i++) {
    if (edges[x][i] == parent) continue;
    if (s == SERVED) {
      ans += dfs(edges[x][i], x, UNSERVED);
    } else if (s == SERVER) {
      ans += min(dfs(edges[x][i], x, SERVED), dfs(edges[x][i], x, SERVER));
    } else {
      int v1 = dfs(edges[x][i], x, SERVER), v2 = dfs(edges[x][i], x, UNSERVED);
      ans = min(v1 + served - v2, ans);
    }
  }
  return ans;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  for (int n; cin >> n && n > 0; ) {
    for (int i = 1; i <= n; i++) edges[i].clear();
    for (int a, b, i = 1; i < n; i++) {
      cin >> a >> b;
      edges[a].push_back(b); edges[b].push_back(a);
    }
    int end; cin >> end;
    cout << min(dfs(1, -1, SERVER), dfs(1, -1, UNSERVED)) << '\n';
    if (end) break;
  }
}
