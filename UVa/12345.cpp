#include <bits/stdc++.h>
using namespace std;

struct Query {
	int l, r, lBlk, rBlk, t, id;
	bool operator <(const Query &rhs) const {
		return (lBlk != rhs.lBlk) ? (lBlk < rhs.lBlk) :
				(rBlk != rhs.rBlk) ? (rBlk < rhs.rBlk) : (t > rhs.t);
	}
} query[50000];
struct Modify { int p, v, old; } modif[50000];

int a[50001], ans[50000], cnt[1000001], curAns;

inline void add(int p) {
	if (++cnt[a[p]] == 1) curAns++;
}

inline void rem(int p) {
	if (--cnt[a[p]] == 0) curAns--;
}

inline void addTime(int t, int l, int r) {
	if (l <= modif[t].p && modif[t].p <= r) rem(modif[t].p);
	a[modif[t].p] = modif[t].v;
	if (l <= modif[t].p && modif[t].p <= r) add(modif[t].p);
}

inline void remTime(int t, int l, int r) {
	if (l <= modif[t].p && modif[t].p <= r) rem(modif[t].p);
	a[modif[t].p] = modif[t].old;
	if (l <= modif[t].p && modif[t].p <= r) add(modif[t].p);
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	int blkSz = (int)round(pow(max(n, m), 2.0 / 3)), qTop = 0, tTop = 0;
	for (int i = 0; i < n; i++) cin >> a[i];
	for (int i = 0; i < m; i++) {
		char op; cin >> op;
		int x, y; cin >> x >> y;
		if (op == 'Q') {
			query[qTop] = { x, y - 1, x / blkSz, y / blkSz, tTop, qTop };
			qTop++;
		} else {
			modif[++tTop] = { x, y, a[x] };
			a[x] = y;
		}
	}
	sort(query, query + qTop);
	int l = 0, r = -1, t = tTop;
	for (int i = 0; i < qTop; i++) {
		while (l < query[i].l) rem(l++);
		while (l > query[i].l) add(--l);
		while (r < query[i].r) add(++r);
		while (r > query[i].r) rem(r--);
		while (t < query[i].t) addTime(++t, l, r);
		while (t > query[i].t) remTime(t--, l, r);
		ans[query[i].id] = curAns;
	}
	for (int i = 0; i < qTop; i++) cout << ans[i] << '\n';
}
