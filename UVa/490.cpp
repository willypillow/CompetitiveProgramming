#include <iostream>
#include <cstring>
using std::cin; using std::cout; using std::max;

char str[105][105];
int len[105];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int n = 0, maxL = 0;
  while (cin.getline(str[n++], 105)) {
    len[n - 1] = strlen(str[n - 1]);
    maxL = max(maxL, len[n - 1]);
  }
  n--;
  for (int j = 0; j < maxL; j++) {
    char tmp[105];
    int top = 0;
    for (int i = n - 1; i >= 0; i--) {
      tmp[top++] = ((j < len[i]) ? str[i][j] : ' ');
    }
    tmp[top] = '\0';
    cout << tmp << '\n';
  }
}
