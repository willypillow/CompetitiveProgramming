#include <bits/stdc++.h>
using namespace std;

int par[500], xs[500], ys[500], s, p;
vector<pair<double, int> > edges[500];

int find(int x) {
	return (par[x] == x) ? x : (par[x] = find(par[x]));
}

bool merge(int x, int y) {
	int px = find(x), py = find(y);
	if (px == py) return false;
	par[px] = py;
	return true;
}

bool solve(double m) {
	for (int j = 0; j < p; j++) par[j] = j;
	int cnt = p;
	for (int j = 0; j < p; j++) {
		auto x = make_pair(m, 0);
		int t = lower_bound(edges[j].begin(), edges[j].end(), x) - edges[j].begin();
		for (int k = 0; k < t; k++) {
			if (merge(j, edges[j][k].second)) cnt--;
			if (cnt <= s) return true;
		}
	}
	return false;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		cin >> s >> p;
		for (int i = 0; i < p; i++) {
			cin >> xs[i] >> ys[i];
			edges[i].clear();
		}
		for (int i = 0; i < p; i++) {
			for (int j = 0; j < p; j++) {
				if (i == j) continue;
				double dist = sqrt(pow(xs[i] - xs[j], 2) + pow(ys[i] - ys[j], 2));
				edges[i].push_back({ dist, j });
			}
			sort(edges[i].begin(), edges[i].end());
		}
		double l = 0, r = 15000;
		for (int i = 0; i < 24; i++) {
			double m = (l + r) / 2;
			if (solve(m)) r = m;
			else l = m;
		}
		cout << fixed << setprecision(2) << (l + r) / 2 << '\n';
	}
}
