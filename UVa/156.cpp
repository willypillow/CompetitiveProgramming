#include <iostream>
#include <algorithm>
#include <string>
#include <map>
#include <set>

static inline std::string reg(std::string str) {
  int len = str.length();
  for (int i = 0; i < len; i++) str[i] = tolower(str[i]);
  std::sort(str.begin(), str.end());
  return str;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  std::string str;
  std::map<std::string, std::string> toReg;
  std::map<std::string, int> count;
  while (std::cin >> str && str[0] != '#') {
    std::string r = reg(str);
    if (!count.count(r)) {
      count[r] = 1;
      toReg[str] = r;
    } else count[r] = 2;
  }
  for (auto &p : toReg) {
    if (count[p.second] == 1) std::cout << p.first << '\n';
  }
}
