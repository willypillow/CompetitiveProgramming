#include <iostream>
#include <cstring>
#include <algorithm>

const int thres = '1' + '2';

int main() {
  char str1[105], str2[105];
  while (std::cin >> str1 >> str2) {
    int len1 = strlen(str1), len2 = strlen(str2), i, i2;
    for (i = 0; i < len2; i++) {
      bool fail = false;
      for (int j = i; j < len2; j++) {
        if (j - i >= len1) break;
        if ((int)str1[j - i] + str2[j] > thres) {
          fail = true;
          break;
        }
      }
      if (!fail) break;
    }
    for (i2 = 0; i2 < len1; i2++) {
      bool fail = false;
      for (int j = i2; j < len1; j++) {
        if (j - i2 >= len2) break;
        if ((int)str1[j] + str2[j - i2] > thres) {
          fail = true;
          break;
        }
      }
      if (!fail) break;
    }
    std::cout << std::min(std::max(len1, i2 + len2),
        std::max(len2, i + len1)) << '\n';
  }
}
