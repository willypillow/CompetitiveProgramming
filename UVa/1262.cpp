#include <iostream>
#include <algorithm>
#include <vector>
using std::cin; using std::cout; using std::sort; using std::vector; using std::unique;

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int t; cin >> t;
  while (t--) {
    char mat[2][6][6];
    vector<char> choice[5];
    int k; cin >> k;
    for (int i = 0; i < 2; i++) {
      for (int j = 0; j < 6; j++) cin >> mat[i][j];
    }
    for (int i = 0; i < 5; i++) {
      for (int j = 0; j < 6; j++) {
        for (int k = 0; k < 6; k++) {
          if (mat[0][j][i] == mat[1][k][i]) choice[i].push_back(mat[0][j][i]);
        }
      }
    }
    for (int i = 0; i < 5; i++) {
      sort(choice[i].begin(), choice[i].end());
      auto it = unique(choice[i].begin(), choice[i].end());
      choice[i].resize(std::distance(choice[i].begin(), it));
    }
    int product[6]; product[5] = 1;
    for (int i = 4; i >= 0; i--) {
      product[i] = product[i + 1] * choice[i].size();
    }
    if (k > product[0]) {
      cout << "NO\n";
    } else {
      k--;
      for (int i = 0; i < 5; i++) {
        int p = product[i + 1];
        cout << choice[i][k / p];
        k %= p;
      }
      cout << '\n';
    }
  }
}


