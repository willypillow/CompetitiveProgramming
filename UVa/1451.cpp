#include <iostream>

int sum[100010], s[100010];
char seq[100010];
int cmp(int x1, int y1, int x2, int y2) { // x and y 1-based
  return (sum[y2] - sum[x2 - 1]) * (y1 - x1 + 1) -
    (sum[y1] - sum[x1 - 1]) * (y2 - x2 + 1);
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif 
  int t;
  std::cin >> t;
  while (t--) {
    int n, l, left = 0, right = 0;
    std::cin >> n >> l >> seq;
    int ansStart = 1, ansEnd = l;
    for (int i = 0; i < n; i++) sum[i + 1] = sum[i] + (seq[i] - '0');
    for (int i = l; i <= n; i++) {
      while (right - left > 1 &&
          cmp(s[right - 1], i - l, s[right - 2], i - l) >= 0) {
        right--;
      }
      s[right++] = i - l + 1;
      while (right - left > 1 && cmp(s[left + 1], i, s[left], i) <= 0) {
        left++;
      }
      int cmpRes = cmp(ansStart, ansEnd, s[left], i);
      if (cmpRes > 0 || (cmpRes == 0 && i - s[left] < ansEnd - ansStart)) {
        ansStart = s[left], ansEnd = i;
      }
    }
    std::cout << ansStart << ' ' << ansEnd << '\n';
  }
}
