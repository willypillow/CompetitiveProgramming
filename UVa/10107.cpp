#include <iostream>
#include <vector>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  std::vector<int> v;
  int x, n = 0;
  while (std::cin >> x) {
    ++n;
    auto pos = std::lower_bound(v.begin(), v.end(), x);
    v.insert(pos, x);
    if (n & 1) std::cout << v[n / 2] << '\n';
    else std::cout << (v[n / 2 - 1] + v[n / 2]) / 2 << '\n';
  }
}
