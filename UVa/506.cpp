#include <cstring>
#include <iostream>
#include <list>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

static const int MAXN = 10000;

enum State { UNINSTALLED = 0, INSTALLED, DEPENDENCY };
struct Status {
  State state = UNINSTALLED;
  int dependCnt = 0;
} status[MAXN];

std::unordered_map<std::string, int> ids;
std::string idToStr[MAXN];
std::vector<int> dependencies[MAXN];
std::list<int> installed;

static inline int toId(char *cstr) {
  std::string str(cstr);
  if (ids.count(str)) return ids[str];
  int newId = ids.size();
  idToStr[newId] = str;
  return (ids[str] = newId);
}

static inline void addDep(char *cmd) {
  char *p = strtok(cmd, " "); // Item 1
  int id = toId(p);
  while ((p = strtok(NULL, " "))) 
    dependencies[id].push_back(toId(p));
}

static inline void install(int id) {
  switch (status[id].state) {
  case UNINSTALLED:
    for (int i: dependencies[id]) {
      ++status[i].dependCnt;
      if (status[i].state == UNINSTALLED) {
        install(i);
        status[i].state = DEPENDENCY; // Overwrite
      }
    }
    status[id].state = INSTALLED;
    installed.push_back(id);
    std::cout << "   Installing " << idToStr[id] << '\n';
    break;
  default:
    std::cout << "   " << idToStr[id] << " is already installed.\n";
  }
}

static inline void remove(int id) {
  if (status[id].dependCnt) {
    std::cout << "   " << idToStr[id] << " is still needed.\n";
    return;
  }
  switch(status[id].state) {
  case UNINSTALLED:
    std::cout << "   " << idToStr[id] << " is not installed.\n";
    break;
  default:
    std::cout << "   Removing " << idToStr[id] << '\n';
    for (int i: dependencies[id]) {
      if (--status[i].dependCnt == 0 && status[i].state == DEPENDENCY) {
        remove(i);
      }
    }
    status[id].state = UNINSTALLED;
    installed.remove(id);
  }
}

static inline void list() {
  for (int i: installed) std::cout << "   " << idToStr[i] << '\n';
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  char cmd[100];
  while (std::cin.getline(cmd, 100)) {
    std::cout << cmd << '\n';
    switch (cmd[0]) {
    case 'D': addDep(strchr(cmd, ' ') + 1); break;
    case 'I': strtok(cmd, " "); install(toId(strtok(NULL, " "))); break;
    case 'R': strtok(cmd, " "); remove(toId(strtok(NULL, " "))); break;
    case 'L': list(); break;
    case 'E': goto Exit;
    }
  }
Exit: return 0;
}
