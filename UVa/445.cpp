#include <iostream>
using std::cin; using std::cout;

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  char str[200];
  while (cin.getline(str, 200)) {
    int count = 0;
    for (char *p = str; *p != '\0'; p++) {
      if (*p == '!') {
        cout << '\n';
      } else if (isdigit(*p)) {
        count = count + (*p - '0');
      } else {
        if (*p == 'b') *p = ' ';
        for (int i = 0; i < count; i++) cout << *p;
        count = 0;
      }
    }
    cout << '\n';
  }
}
