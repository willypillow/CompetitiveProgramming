#include <iostream>
#include <cstring>
using std::cin; using std::cout; using std::min; using std::max;

const int INF = 0x5f5f5f5f;
int dp[1 << 11][1 << 11], cnt[1 << 11][1 << 11], m, n;

int dfs(int ask, int has) {
  if (cnt[ask][has] <= 1) return 0;
  int &ans = dp[ask][has];
  if (ans < INF) return ans;
  for (int i = 0; i < m; i++) {
    if (ask & (1 << i)) continue;
    int nA = ask | (1 << i), nH = has | (1 << i);
    if (cnt[nA][nH] > 0 && cnt[nA][has] > 0) {
      ans = min(ans, max(dfs(nA, nH), dfs(nA, has)) + 1);
    }
  }
  return ans;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  while (cin >> m >> n >> std::ws && m) {
    memset(dp, 0x5f, sizeof(dp)); memset(cnt, 0, sizeof(cnt));
    for (int i = 0; i < n; i++) {
      int feat = 0;
      char str[20]; cin.getline(str, 20);
      for (int j = 0; j < m; j++) feat = (feat << 1) | (str[j] - '0');
      for (int j = 0; j < (1 << m); j++) cnt[j][j & feat]++;
    }
    cout << dfs(0, 0) << '\n';
  }
}

