#include <iostream>
#include <cstring>

int arr[200010], start[200010], end[200010], mins[200010];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  int t;
  std::cin >> t;
  while (t--) {
    int n, ans = 0;
    memset(mins, 0x6f, sizeof(mins));
    std::cin >> n;
    for (int i = 0; i < n; i++) std::cin >> arr[i];
    start[0] = end[n - 1] = 1;
    for (int i = 1; i < n; i++) {
      start[i] = (arr[i - 1] < arr[i]) ? (start[i - 1] + 1) : 1;
    }
    for (int i = n - 2; i >= 0; i--) {
      end[i] = (arr[i] < arr[i + 1]) ? (end[i + 1] + 1) : 1;
    }
    for (int i = 0; i < n; i++) {
      int len = std::lower_bound(mins, mins + n, arr[i]) - mins;
      ans = std::max(ans, len + end[i]);
      mins[start[i] - 1] = std::min(mins[start[i] - 1], arr[i]);
    }
    std::cout << ans << '\n';
  }
}

