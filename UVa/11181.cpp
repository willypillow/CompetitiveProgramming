#include <iostream>
#include <cstring>
#include <iomanip>
using std::cin; using std::cout; using std::setprecision;

int n, r;
double p[25], res[25];

double dfs(int depth, int ones, double prob, uint32_t set) {
  if (ones > r || (depth - ones) > (n - r)) return 0.0;
  if (depth == n) {
    if (ones == r) {
      for (int i = 0; i < n; i++) {
        if (set & (1 << i)) res[i] += prob;
      }
    }
    return prob;
  } else {
    return dfs(depth + 1, ones, prob * (1 - p[depth]), set) +
      dfs(depth + 1, ones + 1, prob * p[depth], set | (1 << depth));
  }
}

int main() {
  int cases = 0;
  while (cin >> n >> r && n) {
    memset(res, 0, sizeof(res));
    for (int i = 0; i < n; i++) cin >> p[i];
    double totProb = dfs(0, 0, 1.0, 0);
    cout << "Case " << ++cases << ":\n";
    for (int i = 0; i < n; i++) {
      cout << std::fixed << setprecision(6) << res[i] / totProb << '\n';
    }
  }
}
