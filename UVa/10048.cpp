#include <iostream>
#include <cstring>
using std::cin; using std::cout; using std::min; using std::max;

const int INF = 0x6f6f6f6f;
int edge[100][100];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  for (int c, s, q, cases = 0; cin >> c >> s >> q && c; ) {
    memset(edge, 0x6f, sizeof(edge));
    for (int i = 0; i < s; i++) {
      int c1, c2, d; cin >> c1 >> c2 >> d;
      edge[c1 - 1][c2 - 1] = edge[c2 - 1][c1 - 1] = d;
    }
    for (int k = 0; k < c; k++) {
      for (int i = 0; i < c; i++) {
        for (int j = 0; j < c; j++) {
          edge[i][j] = min(edge[i][j], max(edge[i][k], edge[k][j]));
        }
      }
    }
    if (cases) cout << '\n';
    cout << "Case #" << ++cases << '\n';
    for (int i = 0; i < q; i++) {
      int c1, c2; cin >> c1 >> c2;
      if (edge[c1 - 1][c2 - 1] == INF) cout << "no path\n";
      else cout << edge[c1 - 1][c2 - 1] << '\n';
    }
  }
}

