#include <bits/stdc++.h>
using namespace std;

char str[1100];
int len, sum[3], sum7[2];

bool solve(int d) {
	switch (d) {
	case 2:
		return (str[len - 1] - '0') % 2 == 0;
	case 3:
		return sum[2] % 3 == 0;
	case 4:
		if (len < 2) return (str[len - 1] - '0') % 4 == 0;
		return ((str[len - 2] - '0') * 10 + (str[len - 1] - '0')) % 4 == 0;
	case 5:
		return (str[len - 1] - '0') % 5 == 0;
	case 6:
		return solve(2) && solve(3);
	case 7:
		return (sum7[0] - sum7[1]) % 7 == 0;
	case 8:
		if (len < 2) return (str[len - 1] - '0') % 8 == 0;
		if (len < 3) return ((str[len - 2] - '0') * 10 + (str[len - 1] - '0')) % 8 == 0;
		return ((str[len - 3] - '0') * 100 + (str[len - 2] - '0') * 10 + (str[len - 1] - '0')) % 8 == 0;
	case 9:
		return sum[2] % 9 == 0;
	case 10:
		return solve(2) && solve(5);
	case 11:
		return (sum[0] - sum[1]) % 11 == 0;
	case 12:
		return solve(3) && solve(4);
	}
	return true;
}


int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int t, m, x; cin >> t;
	while (t--) {
		cin >> str;
		len = strlen(str);
		sum[0] = sum[1] = sum[2] = sum7[0] = sum7[1] = 0;
		for (int i = 0; i < len; i++) {
			sum[2] += str[i] - '0';
			sum[i & 1] += str[i] - '0';
		}
		int flag = 0, i;
		for (i = len - 1; i >= 2; i -= 3) {
			sum7[flag] += (str[i - 2] - '0') * 100 + (str[i - 1] - '0') * 10 + (str[i] - '0');
			flag = !flag;
		}
		if (i >= 0) sum7[flag] += str[i] - '0';
		if (i >= 1) sum7[flag] += (str[i - 1] - '0') * 10;
		cin >> m;
		bool fail = false;
		for (int i = 0; i < m; i++) {
			cin >> x;
			if (!solve(x)) fail = true;
		}
		cout << str << " - " << (fail ? "Simple.\n" : "Wonderful.\n");
	}
}
