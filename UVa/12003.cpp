#include <iostream>
#include <cassert>
#include <cmath>
#include <algorithm>
using std::cin; using std::cout; using std::sort; using std::lower_bound; using std::swap;

int n, m, u, blk;
long long arr[300010], orig[300010];

int query(int l, int r, int v) {
	int sum = 0;
	if (r / blk == l / blk) {
		for (; l <= r; l++) {
			if (orig[l] < v) sum++;
		}
		return sum;
	}
	for (; l % blk != 0; l++) {
		if (orig[l] < v) sum++;
	}
	for (; r % blk != 0; r--) {
		if (orig[r] < v) sum++;
	}
	if (orig[r] < v) sum++;
	for (int i = l; i < r; i += blk) {
		sum += lower_bound(arr + i, arr + i + blk, v) - (arr + i);
	}
	return sum;
}

void modify(int p, long long v) {
	int b = p / blk, top = std::min((b + 1) * blk, n);
	int idx = lower_bound(arr + b * blk, arr + top, orig[p]) - arr;
	arr[idx] = v;
	while (idx % blk != 0 && arr[idx - 1] > arr[idx]) {
		swap(arr[idx - 1], arr[idx]);
		idx--;
	}
	while ((idx + 1) % blk != 0 && idx + 1 < n && arr[idx + 1] < arr[idx]) {
		swap(arr[idx], arr[idx + 1]);
		idx++;
	}
	orig[p] = v;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	while (cin >> n >> m >> u) {
		blk = round(sqrt(n));
		for (int i = 0; i < n; i++) {
			cin >> arr[i];
			orig[i] = arr[i];
		}
		for (int i = 0, top = n / blk; i < top; i++) {
			sort(arr + i * blk, arr + i * blk + blk);
		}
		sort(arr + n - (n % blk), arr + n);
		for (int i = 0, l, r, v, p; i < m; i++) {
			cin >> l >> r >> v >> p;
			modify(p - 1, (long long)u * query(l - 1, r - 1, v) / (r - l + 1));
		}
		for (int i = 0; i < n; i++) cout << orig[i] << '\n';
	}
}
