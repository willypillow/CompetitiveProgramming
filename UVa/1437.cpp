#include <iostream>
#include <cstring>
#include <algorithm>
using std::min;

static const char itoa[][4] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "100" };

char src[105], dst[105];
int len;
unsigned int f[105], g[105][105];
static const unsigned int MAX = 0x5f5f5f5f;

bool getStr(char *str) {
  bool res = fgets(str, sizeof(src), stdin);
  len = strlen(str);
  if (len > 0 && str[len - 1] == '\n') { str[len - 1] = '\0'; len--; }
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  while (getStr(src + 1) && getStr(dst + 1)) {
    memset(f, 0x5f, sizeof(f)); memset(g, 0x5f, sizeof(g));
    f[0] = 0;
    for (int i = 1; i < len; i++) {
      g[i][i] = 1;
      g[i][i + 1] = (dst[i] == dst[i + 1]) ? 1 : 2;
    }
    g[len][len] = 1;
    for (int l = 3; l <= len; l++) {
      for (int s = 1, e = l; e <= len; s++, e++) {
        g[s][e] = MAX;
        for (int i = s; i <= e; i++) {
          g[s][e] = min(g[s][e], g[s][i] + g[i + 1][e]);
        }
        if (dst[s] == dst[e]) {
          g[s][e] = min(g[s][e], min(g[s + 1][e], g[s][e - 1]));
        }
      }
    }
    for (int i = 1; i <= len; i++) {
      f[i] = (src[i] == dst[i]) ? f[i - 1] : (f[i - 1] + 1);
      for (int j = 0; j < i; j++) {
        f[i] = min(f[i], f[j] + g[j + 1][i]);
      }
    }
    puts(itoa[f[len]]);
  }
}
