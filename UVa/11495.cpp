#include <bits/stdc++.h>
using namespace std;

pair<int, int> val[500001];
int arr[500001], bit[500001], n;

long long sum(int p) {
	long long ans = 0;
	while (p > 0) {
		ans += bit[p];
		p -= p & -p;
	}
	return ans;
}

void add(int p, int v) {
	while (p <= n) {
		bit[p] += v;
		p += p & -p;
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	while (cin >> n && n) {
		memset(bit, 0, sizeof(bit));
		for (int i = 0; i < n; i++) {
			cin >> arr[i];
		}
		long long ans = 0;
		for (int i = 0; i < n; i++) {
			ans += i - sum(arr[i]);
			add(arr[i], 1);
		}
		cout << (ans % 2 ? "Marcelo" : "Carlos") << '\n';
	}
}
