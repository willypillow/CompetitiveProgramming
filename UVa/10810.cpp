#include <bits/stdc++.h>
#include <ext/pb_ds/tree_policy.hpp>
#include <ext/pb_ds/assoc_container.hpp>
using namespace std;
using namespace __gnu_pbds;

typedef tree<long long, null_type, greater<int>, rb_tree_tag, tree_order_statistics_node_update> tre;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int n;
	while (cin >> n && n) {
		tre t;
		long long ans = 0;
		for (int i = 0, x; i < n; i++) {
			cin >> x;
			t.insert(x);
			ans += t.order_of_key(x);
		}
		cout << ans << '\n';
	}
}
