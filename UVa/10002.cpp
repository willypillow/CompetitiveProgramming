#include <iostream>
#include <iomanip>
#include <algorithm>
using std::cin; using std::cout; using std::setprecision; using std::sort;

struct P {
	double x, y;
} p[110];

double cross(P o, P a, P b) {
	return (a.x - o.x) * (b.y - o.y) - (a.y - o.y) * (b.x - o.x);
}

bool cmp(const P &l, const P &r) {
	return cross(p[0], l, r) > 0;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n;
	while (cin >> n && n >= 3) {
		for (int i = 0; i < n; i++) cin >> p[i].x >> p[i].y;
		int min = 0;
		for (int i = 0; i < n; i++) {
			if (p[i].x < p[min].x || (p[i].x == p[min].x && p[i].y < p[min].y)) {
				min = i;
			}
		}
		std::swap(p[0], p[min]);
		sort(p + 1, p + n, cmp);
		double areaSum = 0, xSum = 0, ySum = 0;
		for (int i = 2; i < n; i++) {
			double area = cross(p[0], p[i - 1], p[i]) / 2.0;
			areaSum += area;
			xSum += (p[0].x + p[i - 1].x + p[i].x) * area;
			ySum += (p[0].y + p[i - 1].y + p[i].y) * area;
		}
		cout << std::fixed << setprecision(3) <<
			(double)xSum / (areaSum * 3) << ' ' << (double)ySum / (areaSum * 3) << '\n';
	}
}
