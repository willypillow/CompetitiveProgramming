#include <iostream>
#include <algorithm>
#include <cstring>
#include <vector>
using namespace std;

const int MAXN = 5E4 + 10;

struct Edge { int u, v, d; };
vector<Edge> edges;
vector<vector<Edge> > g;
int par[MAXN], tIn[MAXN], tOut[MAXN], anc[MAXN][17], dang[MAXN][17],
	timer, n, m;

int find(int x) {
	return (par[x] == x) ? x : (par[x] = find(par[x]));
}

void dfsLca(int u, int p, int d) {
	tIn[u] = ++timer;
	anc[u][0] = p;
	dang[u][0] = d;
	for (int i = 1; (1 << i) < (n / 2); i++) {
		anc[u][i] = anc[anc[u][i - 1]][i - 1];
		dang[u][i] = max(dang[u][i - 1], dang[anc[u][i - 1]][i - 1]);
	}
	for (Edge &e: g[u]) {
		if (e.v != p) dfsLca(e.v, u, e.d);
	}
	tOut[u] = ++timer;
}

bool isAnc(int x, int y) {
	return tIn[x] <= tIn[y] && tOut[x] >= tOut[y];
}

int maxDang(int x, int y) {
	if (isAnc(x, y)) return 0;
	int ans = 0;
	for (int i = 16; i >= 0; i--) {
		if (anc[x][i] != 0 && !isAnc(anc[x][i], y)) {
			ans = max(ans, dang[x][i]);
			x = anc[x][i];
		}
	}
	return max(ans, dang[x][0]);
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	bool first = true;
	while (cin >> n >> m) {
		if (!first) cout << '\n';
		first = false;
		timer = 0;
		memset(anc, 0, sizeof(anc)); memset(dang, 0, sizeof(dang));
		memset(tIn, 0, sizeof(tIn)); memset(tOut, 0, sizeof(tOut));
		edges.clear(); g.clear(); g.resize(n + 1);
		for (int i = 0, a, b, d; i < m; i++) {
			cin >> a >> b >> d;
			edges.push_back(Edge { a, b, d });
		}
		sort(edges.begin(), edges.end(), [](const Edge &l, const Edge &r) {
			return l.d < r.d;
		});
		for (int i = 1; i <= n; i++) par[i] = i;
		for (int i = 0, cnt = 0; cnt < n && i < m; i++) {
			int pu = find(edges[i].u), pv = find(edges[i].v);
			if (pu == pv) continue;
			par[pu] = pv;
			g[edges[i].u].push_back(Edge { edges[i].u, edges[i].v, edges[i].d });
			g[edges[i].v].push_back(Edge { edges[i].v, edges[i].u, edges[i].d });
			cnt++;
		}
		dfsLca(1, 0, 0);
		int q, u, v;
		cin >> q;
		while (q--) {
			cin >> u >> v;
			cout << max(maxDang(u, v), maxDang(v, u)) << '\n';
		}
	}
}
