#include <iostream>
using std::cin; using std::cout;

const int MOD = 10001;

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n, arr[20000]; cin >> n;
  for (int i = 0; i < n * 2; i += 2) cin >> arr[i];
  for (int a = 0; a < MOD; a++) {
    for (int b = 0; b < MOD; b++) {
      bool fail = false;
      for (int i = 1; i < n * 2; i += 2) {
        arr[i] = (arr[i - 1] * a + b) % MOD;
        if (i + 1 < n * 2 && arr[i + 1] != (arr[i] * a + b) % MOD) {
          fail = true;
          break;
        }
      }
      if (!fail) goto Success;
    }
  }
Success:
  for (int i = 1; i < n * 2; i += 2) cout << arr[i] << '\n';
}
