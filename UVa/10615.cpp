#include <bits/stdc++.h>
using namespace std;

int ansMat[110][110], g[110][110], match[110], row[110], col[110], n;
bool realG[110][110], visU[110], visV[110];

bool dfs(int u) {
	visU[u] = true;
	for (int v = 0; v < n; v++) {
		if (g[u][v] && !visV[v]) {
			visV[v] = true;
			if (match[v] == -1 || dfs(match[v])) {
				match[v] = u;
				return true;
			}
		}
	}
	return false;
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int t; cin >> t;
	while (t--) {
		memset(row, 0, sizeof(row)); memset(col, 0, sizeof(col));
		memset(realG, false, sizeof(realG)); memset(g, 0, sizeof(g));
		memset(ansMat, 0, sizeof(ansMat));
		cin >> n;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				char c; cin >> c;
				if (c == '*') {
					row[i]++;
					col[j]++;
					g[i][j]++;
					realG[i][j] = true;
				}
			}
		}
		int ans = 0;
		for (int i = 0; i < n; i++) ans = max(ans, max(row[i], col[i]));
		for (int i = 0; i < n; i++) {
			if (row[i] < ans) {
				for (int j = 0; j < n; j++) {
					while (row[i] < ans && col[j] < ans) {
						row[i]++;
						col[j]++;
						g[i][j]++;
					}
				}
			}
		}
		for (int i = 1; i <= ans; i++) {
			memset(match, -1, sizeof(match));
			for (int j = 0; j < n; j++) {
				memset(visU, false, sizeof(visU)); memset(visV, false, sizeof(visV));
				dfs(j);
			}
			for (int j = 0; j < n; j++) {
				if (match[j] == -1) continue;
				if (realG[match[j]][j]) ansMat[match[j]][j] = i;
				g[match[j]][j]--;
			}
		}
		cout << ans << '\n';
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n - 1; j++) cout << ansMat[i][j] << ' ';
			cout << ansMat[i][n - 1] << '\n';
		}
	}
}
