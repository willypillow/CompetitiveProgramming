#include <iostream>
#include <cstring>

const int MAXN = 1001;

struct P {
  int n, s;
};

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  bool notP[MAXN] = { 0 };
  int primes[MAXN], pTop = 0, fact[MAXN][20] = { 0 };
  for (int i = 2; i <= 32; i++) {
    for (int j = i + i; j < MAXN; j += i) {
      if (!notP[j]) notP[j] = true;
    }
  }
  for (int i = 2; i < MAXN; i++) {
    if (!notP[i]) primes[pTop++] = i;
  }
  for (int i = 3; i < MAXN; i++) {
    for (int j = 0, top = 0; primes[j] < i && j < pTop; j++) {
      if (i % primes[j] == 0) fact[i][top++] = primes[j];
    }
  }

  int a, b, cases = 1;
  while (std::cin >> a >> b && a) {
    P queue[100000];
    int qBack = 0, qFront = 0, ans[MAXN];
    memset(ans, -1, sizeof(ans));
    queue[qFront++] = P {a, 0};
    ans[a] = 0;
    while (qBack < qFront) {
      P cur = queue[qBack++];
      if (cur.n == b) break;
      for (int i = 0; fact[cur.n][i]; i++) {
        int newN = cur.n + fact[cur.n][i];
        if (newN > b) break;
        if (ans[newN] != -1 && cur.s + 1 >= ans[newN]) continue;
        ans[newN] = cur.s + 1;
        queue[qFront++] = P {newN, cur.s + 1};
      }
    }
    std::cout << "Case " << cases++ << ": " << ans[b] << '\n';
  }
}
