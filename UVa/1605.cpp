#include <iostream>

static const char letter[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  int n;
  bool first = true;
  while (std::cin >> n) {
    if (!first) std::cout << '\n';
    first = false;
    std::cout << "2 " << n << ' ' << n << '\n';
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) std::cout << letter[j];
      std::cout << '\n';
    }
    std::cout << '\n';
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) std::cout << letter[i];
      std::cout << '\n';
    }
  }
}
