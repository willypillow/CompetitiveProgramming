#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E5 + 1;
bool vis[MAXN];
int col[MAXN];
vector<int> g[MAXN], g2[MAXN];
set<int> g3[MAXN], g4[MAXN];
vector<int> fin;

void dfs(int x) {
	vis[x] = true;
	for (int y: g[x]) {
		if (!vis[y]) dfs(y);
	}
	fin.push_back(x);
}

void dfsScc(int x, int curCol) {
	vis[x] = true;
	col[x] = curCol;
	for (int y: g2[x]) {
		if (!vis[y]) {
			dfsScc(y, curCol);
		}
	}
}

void dfsMerge(int x) {
	vis[x] = true;
	for (int y: g[x]) {
		if (col[y] != col[x] && !g3[col[x]].count(col[y])) {
			g3[col[x]].insert(col[y]);
			g4[col[y]].insert(col[x]);
		}
		if (!vis[y]) {
			dfsMerge(y);
		}
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	int t; cin >> t;
	while (t--) {
		int n, m; cin >> n >> m;
		for (int i = 0; i <= n; i++) g[i].clear(), g2[i].clear(), g3[i].clear(), g4[i].clear();
		fin.clear();
		while (m--) {
			int x, y; cin >> x >> y;
			g[x].push_back(y);
			g2[y].push_back(x);
		}
		memset(vis, false, sizeof(vis));
		for (int i = 1; i <= n; i++) {
			if (!vis[i]) dfs(i);
		}
		memset(vis, false, sizeof(vis));
		int curCol = 0;
		for (int i = fin.size() - 1; i >= 0; i--) {
			if (!vis[fin[i]]) dfsScc(fin[i], ++curCol);
		}
		//cout << curCol << '\n';
		memset(vis, false, sizeof(vis));
		for (int i = 1; i <= n; i++) {
			if (!vis[i]) dfsMerge(i);
		}
		int ans = 0;
		for (int i = 1; i <= curCol; i++) {
			if (g4[i].size() == 0) ans++;
		}
		cout << ans << '\n';
	}
}
