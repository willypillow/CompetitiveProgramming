#include <bits/stdc++.h>
using namespace std;

const int MAXN = 501, INF = 0x3f3f3f3f;

int slack[MAXN], lx[MAXN], ly[MAXN], w[MAXN][MAXN], mx[MAXN], my[MAXN],
		timer, n, vx[MAXN], vy[MAXN];

bool dfs(int x) {
	vx[x] = timer;
	for (int y = 1; y <= n; y++) {
		if (vy[y] == timer) continue;
		int t = lx[x] + ly[y] - w[x][y];
		if (!t) {
			vy[y] = timer;
			if (!my[y] || dfs(my[y])) {
				mx[x] = y;
				my[y] = x;
				return true;
			}
		} else {
			slack[y] = min(slack[y], t);
		}
	}
	return false;
}

void reweight() {
	int t = INF;
	for (int y = 1; y <= n; y++) {
		if (vy[y] != timer) t = min(t, slack[y]);
	}
	for (int i = 1; i <= n; i++) {
		if (vx[i] == timer) lx[i] -= t;
		if (vy[i] == timer) ly[i] += t;
	}
}

void km() {
	memset(lx, 0, sizeof(lx)), memset(ly, 0, sizeof(ly));
	memset(mx, 0, sizeof(mx)), memset(my, 0, sizeof(my));
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= n; j++) lx[i] = max(lx[i], w[i][j]);
	}
	for (int i = 1; i <= n; i++) {
		for (;;) {
			timer++;
			memset(slack, INF, sizeof(slack));
			if (dfs(i)) break;
			else reweight();
		}
	}
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	while (cin >> n) {
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) cin >> w[i][j];
		}
		km();
		for (int i = 1; i <= n; i++) cout << lx[i] << " \n"[i == n];
		for (int i = 1; i <= n; i++) cout << ly[i] << " \n"[i == n];
		int ans = 0;
		for (int i = 1; i <= n; i++) ans += lx[i] + ly[i];
		cout << ans << '\n';
	}
}
