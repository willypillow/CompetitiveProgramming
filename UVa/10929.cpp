#include <iostream>
#include <cstring>
using std::cin; using std::cout;

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  char str[1100];
  while (cin >> str && strcmp(str, "0") != 0) {
    int cnt[2] = { 0 }, i = 0;
    for (char *c = str; *c != '\0'; c++, i = 1 - i) {
      cnt[i] += *c - '0';
    }
    cout << str << (((cnt[0] - cnt[1]) % 11) ? " is not a multiple of 11.\n" :
        " is a multiple of 11.\n");
  }
}
