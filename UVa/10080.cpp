#include <bits/stdc++.h>
using namespace std;

const int INF = 0x3f3f3f3f;

struct Edge { int to, cap, rev; };
vector<Edge> g[1010];
int dist[1010], flowS, flowT;
bool vis[1010];
pair<double, double> goph[1010];

void addEdge(int from, int to, int cap) {
	g[from].push_back({to, cap, (int)g[to].size()});
	g[to].push_back({from, 0, (int)g[from].size() - 1});
}

int bfs() {
	memset(dist, 0x3f, sizeof(dist));
	dist[flowS] = 0;
	queue<int> q;
	q.push(flowS);
	while (q.size()) {
		int cur = q.front(); q.pop();
		for (auto &e: g[cur]) {
			if (e.cap > 0 && dist[e.to] == INF) {
				dist[e.to] = dist[cur] + 1;
				if (e.to == flowT) return dist[e.to];
				q.push(e.to);
			}
		}
	}
	return INF;
}

int dfs(int cur, int f) {
	if (cur == flowT) return f;
	vis[cur] = true;
	for (auto &e: g[cur]) {
		if (e.cap > 0 && dist[cur] + 1 == dist[e.to] && !vis[e.to]) {
			int newF = dfs(e.to, min(e.cap, f));
			if (newF) {
				e.cap -= newF;
				g[e.to][e.rev].cap += newF;
				return newF;
			}
		}
	}
	return 0;
}

int dinic(int flow = 0) {
	while (bfs() < INF) {
		for (int f = INF; f;) {
			memset(vis, false, sizeof(vis));
			flow += (f = dfs(flowS, INF));
		}
	}
	return flow;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	for (int n, m, s, v; cin >> n >> m >> s >> v; ) {
		double mxDist = s * v * s * v;
		flowT = m + n + 1;
		for (int i = flowS; i <= flowT; i++) g[i].clear();
		for (int i = 1; i <= n; i++) {
			cin >> goph[i].first >> goph[i].second;
			addEdge(flowS, i, 1);
		}
		for (int i = 1; i <= m; i++) {
			double x, y; cin >> x >> y;
			addEdge(i + n, flowT, 1);
			for (int j = 1; j <= n; j++) {
				if (pow(x - goph[j].first, 2) + pow(y - goph[j].second, 2) <= mxDist) {
					addEdge(j, i + n, 1);
				}
			}
		}
		cout << n - dinic() << '\n';
	}
}
