#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  long long a, b;
  while (std::cin >> a >> b) {
    std::cout << ((a > b) ? (a - b) : (b - a)) << "\n";
  }
}
