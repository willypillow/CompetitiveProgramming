#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cmath>
using std::cin; using std::cout; using std::sort; using std::setprecision;

struct P {
	int x, y, dx, dy;
	bool operator <(const P &rhs) const {
		return x > rhs.x;
	}
} p[100];

int main() {
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif
	int t; cin >> t;
	while (t--) {
		int n; cin >> n;
		for (int i = 0; i < n; i++) cin >> p[i].x >> p[i].y;
		sort(p, p + n);
		int thres = -1, use[100], uTop = 0;
		for (int i = 0; i < n; i++) {
			if (i) {
				p[i].dx = p[i].x - p[i - 1].x;
				p[i].dy = p[i].y - p[i - 1].y;
			}
			if (p[i].y <= thres) continue;
			thres = p[i].y;
			use[uTop++] = i;
		}
		double ans = 0;
		for (int i = 1; i < uTop; i++) {
			int ii = use[i];
			ans += hypot(p[ii].dx, p[ii].dy) * (p[ii].y - p[use[i - 1]].y) / p[ii].dy;
		}
		cout << std::fixed << setprecision(2) << ans << '\n';
	}
}
