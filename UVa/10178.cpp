#include <bits/stdc++.h>
using namespace std;

int vis[52], low[52], g[52][52], timer;

void dfs(int x, int p, vector<int> &pts) {
	vis[x] = low[x] = ++timer;
	pts.push_back(x);
	for (int y = 0; y < 52; y++) {
		if (g[x][y]) {
			if (y != p || g[x][y] > 1) {
				if (!vis[y]) dfs(y, x, pts);
				low[x] = min(low[x], low[y]);
				if (low[y] > vis[x]) g[x][y] = g[y][x] = 0;
			}
		}
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, e;
	while (cin >> n >> e) {
		timer = 0;
		memset(vis, 0, sizeof(vis)); memset(low, 0, sizeof(low));
		memset(g, 0, sizeof(g));
		for (int i = 0; i < e; i++) {
			char a, b; cin >> a >> b;
			int aa = isupper(a) ? a - 'A': a - 'a' + 26;
			int bb = isupper(b) ? b - 'A': b - 'a' + 26;
			g[aa][bb]++, g[bb][aa]++;
		}
		for (int i = 0; i < 52; i++) {
			if (!vis[i]) {
				vector<int> tmp;
				dfs(i, i, tmp);
			}
		}
		memset(vis, 0, sizeof(vis)); memset(low, 0, sizeof(low));
		int ans = 0;
		for (int i = 0; i < 52; i++) {
			if (!vis[i]) {
				vector<int> pts;
				dfs(i, i, pts);
				if (pts.size()) {
					int es = 0;
					for (int x: pts) {
						for (int y: pts) {
							if (x < y) es += g[x][y];
							if (x == y) es += g[x][y] >> 1;
						}
					}
					ans += 1 + es - (int)pts.size();
				}
			}
		}
		cout << ans + 1 << '\n';
	}
}
