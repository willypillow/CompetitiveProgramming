#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <cstring>
using namespace std;

vector<vector<int> > g;
int timer, low[110], dfn[110], ans;

void dfs(int u, int p) {
	low[u] = dfn[u] = ++timer;
	int child = 0, ap = 0;
	for (int v: g[u]) {
		if (v == p) continue;
		if (dfn[v] == 0) {
			child++;
			dfs(v, u);
			low[u] = min(low[u], low[v]);
			if (low[v] >= dfn[u]) ap = true;
		} else {
			low[u] = min(low[u], dfn[v]);
		}
	}
	if ((u == 1 && child > 1) || (u != 1 && ap)) ans++;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int n;
	while (cin >> n >> ws && n) {
		ans = timer = 0;
		g.clear(); g.resize(n + 1);
		memset(low, 0, sizeof(low)); memset(dfn, 0, sizeof(dfn));
		string str;
		while (getline(cin, str)) {
			int u, v;
			stringstream ss(str);
			ss >> u;
			if (!u) break;
			while (ss >> v) {
				g[u].push_back(v);
				g[v].push_back(u);
			}
		}
		dfs(1, 0);
		cout << ans << '\n';
	}
}
