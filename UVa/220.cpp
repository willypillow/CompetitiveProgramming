#include <iostream>
#include <cstring>
#include <iomanip>

char board[8][9];
bool isLegal[8][8];
int pieces[256];

static inline bool inBound(int i, int j) {
  if (i < 0 || i >= 8 || j < 0 || j >= 8) return false;
  return true;
}

static inline void searchLegal(char player) {
  char opponent = player ^ ('B' ^ 'W');
  memset(isLegal, false, sizeof(isLegal));
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      if (board[i][j] == player) {
        for (int dx = -1; dx <= 1; dx++) {
          for (int dy = -1; dy <= 1; dy++) {
            if (!dx && !dy || !inBound(i + dx, j + dy) ||
              board[i + dx][j + dy] != opponent) continue;
            int newI = i, newJ = j;
            while (board[newI += dx][newJ += dy] == opponent && inBound(newI, newJ));
            if (board[newI][newJ] == '-' && inBound(newI, newJ)) isLegal[newI][newJ] = true;
          }
        }
      }
    }
  }
  int first = true;
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      if (isLegal[i][j]) {
        if (!first) std::cout << ' ';
        std::cout << '(' << i + 1 << ',' << j + 1 << ')';
        first = false;
      }
    }
  }
  if (first) std::cout << "No legal move.\n";
  else std::cout << '\n';
}

static inline char move(char player) {
  char ci, cj;
  std::cin >> ci >> cj;
  int i = ci - '0' - 1, j = cj - '0' - 1;
  bool kill = false;
  while (!kill) {
    char opponent = player ^ ('B' ^ 'W');
    for (int dx = -1; dx <= 1; dx++) {
      for (int dy = -1; dy <= 1; dy++) {
        if (!dx && !dy || !inBound(i + dx, j + dy) ||
          board[i + dx][j + dy] != opponent) continue;
        int cnt = 0, newI = i, newJ = j;
        while (board[newI += dx][newJ += dy] == opponent) cnt++;
        if (board[newI][newJ] == player) {
          kill = true;
          for (int k = 1; k <= cnt; k++) {
            board[newI - k * dx][newJ - k * dy] = player;
            pieces[player]++; pieces[opponent]--;
          }
        }
      }
    }
    if (kill) {
      board[i][j] = player;
      pieces[player]++;
    }
    player = opponent;
  }
  std::cout << "Black - " << std::setw(2) << pieces['B'] <<
    " White - " << std::setw(2) << pieces['W'] << '\n';
  return player;
}

int main() {
  int t;
  std::cin >> t;
  while (t--) {
    for (int i = 0; i < 8; i++) std::cin >> board[i];
    pieces['B'] = 0; pieces['W'] = 0;
    for (int i = 0; i < 8; i++) {
      for (int j = 0; j < 8; j++) {
        pieces[board[i][j]]++;
      }
    }
    char player, cmd;
    std::cin >> player;
    while (std::cin >> cmd && cmd != 'Q') {
      if (cmd == 'L') searchLegal(player);
      else player = move(player);
    }
    for (int i = 0; i < 8; i++) std::cout << board[i] << '\n';
    if (t) std::cout << '\n';
  }
}
