#include <bits/stdc++.h>
using namespace std;

char arr[210][210];
string ans;

void solve1(int x0, int x1, int y0, int y1) {
	if (x1 < x0 || y1 < y0) return;
	bool split = false;
	for (int i = x0; i <= x1; i++) {
		for (int j = y0; j <= y1; j++) {
			if (arr[i][j] != arr[x0][y0]) {
				split = true;
				break;
			}
		}
	}
	if (!split) {
		ans.push_back(arr[x0][y0]);
	} else {
		ans.push_back('D');
		int mx = (x0 + x1) / 2, my = (y0 + y1) / 2;
		solve1(x0, mx, y0, my); solve1(x0, mx, my + 1, y1);
		solve1(mx + 1, x1, y0, my); solve1(mx + 1, x1, my + 1, y1);
	}
}

void build2(int x0, int x1, int y0, int y1) {
	if (x1 < x0 || y1 < y0) return;
	char c; cin >> c;
	if (c != 'D') {
		for (int i = x0; i <= x1; i++) {
			for (int j = y0; j <= y1; j++) arr[i][j] = c;
		}
	} else {
		int mx = (x0 + x1) / 2, my = (y0 + y1) / 2;
		build2(x0, mx, y0, my); build2(x0, mx, my + 1, y1);
		build2(mx + 1, x1, y0, my); build2(mx + 1, x1, my + 1, y1);
	}
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	char type;
	while (cin >> type && type != '#') {
		ans.clear();
		int n, m; cin >> n >> m;
		if (type == 'B') {
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) cin >> arr[i][j];
			}
			solve1(0, n - 1, 0, m - 1);
		} else {
			build2(0, n - 1, 0, m - 1);
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) ans.push_back(arr[i][j]);
			}
		}
		cout << (type == 'B' ? 'D' : 'B') << setw(4) << n << setw(4) << m << '\n';
		for (int i = 0; i < (int)ans.size(); ) {
			for (int k = 0; k < 50 && i < (int)ans.size(); k++, i++) cout << ans[i];
			cout << '\n';
		}
	}
}
