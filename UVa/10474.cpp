#include <iostream>
#include <algorithm>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int m, n, cases = 1;
  while (std::cin >> m >> n && m) {
    std::cout << "CASE# " << cases++ << ":\n";
    int arr[10000];
    for (int i = 0; i < m; i++) std::cin >> arr[i];
    std::sort(arr, arr + m);
    for (int i = 0; i < n; i++) {
      int q;
      std::cin >> q;
      int p = std::lower_bound(arr, arr + m, q) - arr;
      if (arr[p] == q) std::cout << q << " found at " << p + 1 << '\n';
      else std::cout << q << " not found\n";
    }
  }
}
