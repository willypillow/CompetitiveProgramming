#include <iostream>
#include <cstring>

struct Node {
  int data, left, right;
} ns[256];
int nTop, root;

bool fail;

inline int newNode() {
  ns[nTop] = Node { 0, 0, 0 };
  return nTop++;
}

inline void insNode(int data, char *loc, int len) {
  int cur = root;
  for (int i = 0; i < len; i++) {
    if (loc[i] == 'L') {
      if (!ns[cur].left) ns[cur].left = newNode();
      cur = ns[cur].left;
    } else {
      if (!ns[cur].right) ns[cur].right = newNode();
      cur = ns[cur].right;
    }
  }
  if (ns[cur].data) fail = true;
  else ns[cur].data = data;
}

inline void init() {
  nTop = 0;
  fail = false;
  root = newNode();
}

inline void printNodes() {
  int queue[100000], qFront = 0, qBack = 0, ans[256], aTop = 0;
  queue[qBack++] = root;
  while (qFront != qBack) {
    if (!ns[qFront].data) {
      fail = true;
      break;
    }
    int cur = queue[qFront++];
    ans[aTop++] = cur;
    if (ns[cur].left) queue[qBack++] = ns[cur].left;
    if (ns[cur].right) queue[qBack++] = ns[cur].right;
  }
  if (fail) std::cout << "not complete\n";
  else {
    for (int i = 0; i < aTop - 1; i++) std::cout << ns[ans[i]].data << ' ';
    std::cout << ns[ans[aTop - 1]].data << '\n';
  }
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  char str[100];
  while (std::cin >> str) {
    init();
    do {
      int data = atoi(str + 1);
      char *loc = strchr(str, ',') + 1;
      int len = strlen(loc) - 1;
      insNode(data, loc, len);
    } while (std::cin >> str && str[1] != ')');
    printNodes();
  }
}
