#include <iostream>
#include <algorithm>
using std::cin; using std::cout; using std::sort; using std::min; using std::max;

struct Edge {
  int a, b, w;
  bool operator <(const Edge &rhs) const {
    return w < rhs.w;
  }
} edges[100 * 99 / 2];
int p[101], n, m;

void init() {
  for (int i = 1; i <= n; i++) p[i] = i;
}

int find(int x) {
  return (x == p[x]) ? x : find(p[x]);
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  while (cin >> n >> m && n) {
    for (int i = 0; i < m; i++) cin >> edges[i].a >> edges[i].b >> edges[i].w;
    sort(edges, edges + m);
    int ans = 1 << 30;
    for (int begin = 0; begin < m; begin++) {
      init();
      int count = 0, maxW = -1, minW = 1 << 30;
      for (int i = begin; count != n - 1 && i < m; i++) {
        int pa = find(edges[i].a), pb = find(edges[i].b);
        if (pa != pb) {
          maxW = max(maxW, edges[i].w), minW = min(minW, edges[i].w);
          count++;
          p[pa] = pb;
        }
      }
      if (count == n - 1) ans = min(ans, maxW - minW);
    }
    if (ans == 1 << 30) ans = -1;
    cout << ans << '\n';
  }
}
