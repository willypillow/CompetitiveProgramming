#include <bits/stdc++.h>
using namespace std;

bool notPrime[1001];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	vector<int> primes;
	for (int i = 2; i <= 1000; i++) {
		if (!notPrime[i]) primes.push_back(i);
		for (int p: primes) {
			if (p * i > 1000) break;
			notPrime[p * i] = true;
			if (i % p == 0) break;
		}
	}
	primes.insert(primes.begin(), 1);
	for (int n, c; cin >> n >> c; ) {
		int cnt = int(upper_bound(primes.begin(), primes.end(), n) - primes.begin());
		int l = max(0, ((cnt + 1) >> 1) - c), r = min(cnt, (cnt >> 1) + c);
		cout << n << ' ' << c << ":";
		for (int i = l; i < r; i++) cout << ' ' << primes[i];
		cout << "\n\n";
	}
}
