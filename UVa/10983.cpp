#include <iostream>
#include <cstring>
#include <queue>
#include <vector>
#include <algorithm>
using std::cin; using std::cout; using std::vector; using std::sort; using std::queue;

const int INF = 1 << 30;

struct Edge { int from, to, cap, flow; };
struct Flight {
	int u, v, c, p, e;
	bool operator <(const Flight &rhs) const {
		return p < rhs.p;
	}
} flight[1010];

int a[350], p[350], n, d, m, z[35], total;
vector<Edge> edges;
vector<vector<int> > g;

void addEdge(int from, int to, int cap) {
	edges.push_back(Edge { from, to, cap, 0 });
	edges.push_back(Edge { to, from, 0, 0 });
	int t = edges.size();
	g[from].push_back(t - 2);
	g[to].push_back(t - 1);
}

bool solve(int x) {
	edges.clear(); g.clear(); g.resize(d * n + n + 1);
	int s = d * n + n, t = d * n + (n - 1);
	for (int i = 0; i < n; i++) {
		addEdge(s, i, z[i]);
	}
	for (int i = 0; i <= x; i++) {
		addEdge(flight[i].e * n + (flight[i].u - 1),
			(flight[i].e + 1) * n + (flight[i].v - 1), flight[i].c);
	}
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < d; j++) {
			addEdge(j * n + i, (j + 1) * n + i, INF);
		}
	}
	int flow = 0;
	for (;;) {
		memset(a, 0, sizeof(a));
		queue<int> q;
		q.push(s); a[s] = INF;
		while (!q.empty()) {
			int cur = q.front(); q.pop();
			for (int ei: g[cur]) {
				Edge &e = edges[ei];
				if (!a[e.to] && e.cap > e.flow) {
					p[e.to] = ei;
					a[e.to] = std::min(a[cur], e.cap - e.flow);
					q.push(e.to);
				}
			}
			if (a[t]) break;
		}
		if (!a[t]) break;
		for (int i = t; i != s; i = edges[p[i]].from) {
			edges[p[i]].flow += a[t];
			edges[p[i] ^ 1].flow -= a[t];
		}
		flow += a[t];
	}
	return flow >= total;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t; cin >> t;
	for (int cases = 1; cases <= t; cases++, total = 0) {
		cin >> n >> d >> m;
		for (int i = 0; i < m; i++) {
			cin >> flight[i].u >> flight[i].v >> flight[i].c >>
				flight[i].p >> flight[i].e;
		}
		sort(flight, flight + m);
		flight[m] = Flight { 0, 0, 0, 1 << 30, 0 };
		for (int i = 0; i < n; i++) {
			cin >> z[i];
			total += z[i];
		}
		int l = 0, r = m;
		while (l < r) {
			int mid = (l + r) / 2;
			if (solve(mid)) r = mid;
			else l = mid + 1;
		}
		cout << "Case #" << cases << ": ";
		if (l != m) cout << flight[l].p << '\n';
		else cout << "Impossible\n";
	}
}

