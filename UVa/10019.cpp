#include <iostream>
#include <cstdlib>
using std::cin; using std::cout;

int main() {
  int n; cin >> n;
  while (n--) {
    char num[100];
    cin >> num;
    int x1 = strtol(num, NULL, 10), x2 = strtol(num, NULL, 16);
    cout << __builtin_popcount(x1) << ' ' << __builtin_popcount(x2) << '\n';
  }
}
