#include <iostream>
#include <iomanip>

int main() {
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  std::cout << std::fixed << std::setprecision(3);
  for (;;) {
    int h, m;
    std::cin >> h;
    std::cin.get();
    std::cin >> m;
    if (!h && !m) break;
    float hAng = h * 30 + m * 0.5, mAng = m * 6;
    float diff = (hAng > mAng) ? (hAng - mAng) : (mAng - hAng);
    if (diff > 180.0) diff = 360 - diff;
    std::cout << diff << '\n';
  }
}
