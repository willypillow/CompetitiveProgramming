#include <iostream>
#include <cstring>

bool notPrime[100001] = { false };

inline int subAtoi(char *str, int start, int len) {
  char bkup = str[start + len];
  str[start + len] = '\0';
  int num = atoi(str + start);
  str[start + len] = bkup;
  return num;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  for (int i = 2; i <= 317; i++) {
    for (int j = i + i; j <= 100000; j += i) notPrime[j] = true;
  }
  char str[256];
  while (std::cin >> str) {
    int len = strlen(str), ans = 0;
    if (len == 1 && str[0] == '0') break;
    for (int i = 0; i < len; i++) {
      for (int j = 1; j < 6 && i + j <= len; j++) {
        int num = subAtoi(str, i, j);
        if (num > ans && !notPrime[num]) ans = num;
      }
    }
    std::cout << ans << '\n';
  }
}
