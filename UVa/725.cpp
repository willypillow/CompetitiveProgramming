#include <iostream>
#include <iomanip>

static inline bool isPerm(int n, bool *used) {
  if (n < 10000) {
    if (used[0]) return false;
    used[0] = true;
  }
  do {
    int tmp = n % 10;
    if (used[tmp]) return false;
    used[tmp] = true;
  } while (n /= 10);
  return true;
}


void print(int n, int perm) {
  std::cout << std::setw(5) << std::setfill('0') << n * perm << " / ";
  std::cout << std::setw(5) << std::setfill('0') << perm;
  std::cout << " = " << n << '\n';
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif 
  int n, perms[100000], pTop = 0;
  bool first = true;
  for (int i = 1000; i < 100000; i++) {
    bool used[10] = { false };
    if (isPerm(i, used)) perms[pTop++] = i;
  }
  while (std::cin >> n && n) {
    if (!first) std::cout << '\n';
    first = false;
    bool success = false;
    for (int i = 0; i < pTop; i++) {
      bool used[10] = { false };
      if (isPerm(perms[i], used) && isPerm(n * perms[i], used)) {
        success = true;
        print(n, perms[i]);
      }
    }
    if (!success) std::cout << "There are no solutions for " << n << ".\n";
  }
}
