#include <iomanip>
#include <cmath>
#include <iostream>
#include <cassert>
using std::cin; using std::cout; using std::ostream; using std::setfill; using std::setw;

struct BigNum {
  static const unsigned long long CAP = pow(10, 15);
  static const int SIZE = 100;
  unsigned long long v[SIZE];
  BigNum operator +(const BigNum &rhs) const {
    BigNum res;
    int c = 0;
    for (int i = 0; i < SIZE; i++) {
      res.v[i] = v[i] + rhs.v[i] + c;
      c = res.v[i] / CAP;
      res.v[i] %= CAP;
    }
    assert(c == 0);
    return res;
  }
  friend ostream& operator <<(ostream &s, BigNum b) {
    int i;
    for (i = SIZE - 1; i >= 0 && b.v[i] == 0; i--);
    if (i == -1) {
      s << '0';
    } else {
      s << b.v[i--];
      for (; i >= 0; i--) {
        s << setfill('0') << setw(15) << b.v[i];
      }
    }
    return s;
  }
} fibs[5001] = { 0 };

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  fibs[1].v[0] = 1;
  for (int i = 2; i <= 5000; i++) fibs[i] = fibs[i - 1] + fibs[i - 2];
  for (int n; cin >> n; ) {
    cout << "The Fibonacci number for " << n << " is " << fibs[n] << '\n';
  }
}
