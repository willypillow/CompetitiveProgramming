#include <iostream>
#include <iomanip>
using std::cin; using std::cout; using std::setw; using std::max;

const int MAXN = 100 + 1;
struct Div {
  int divs[50], maxD = 0;
  Div incr(int n) {
    Div tmp = *this;
    tmp.divs[n]++;
    tmp.maxD = max(maxD, n);
    return tmp;
  }
  Div operator +(const Div &rhs) const {
    Div tmp = *this;
    for (int i = 0; i < 50; i++) tmp.divs[i] += rhs.divs[i];
    tmp.maxD = max(tmp.maxD, rhs.maxD);
    return tmp;
  }
} divisors[MAXN], sums[MAXN];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int primes[MAXN], pTop = 0, n;
  bool notPrime[MAXN] = { false };
  for (int i = 2; i < MAXN; i++) {
    if (notPrime[i]) continue;
    for (int j = i + i; j < MAXN; j += i) notPrime[j] = true;
  }
  for (int i = 2; i < MAXN; i++) {
    if (!notPrime[i]) primes[pTop++] = i;
  }
  for (int i = 1; i < MAXN; i++) {
    for (int j = 0; j < pTop; j++) {
      if (i * primes[j] >= MAXN) break;
      divisors[i * primes[j]] = divisors[i].incr(j);
    }
  }
  for (int i = 1; i < MAXN; i++) sums[i] = sums[i - 1] + divisors[i];
  while (cin >> n && n) {
    cout << setw(3) << n << "! =";
    Div &cur = sums[n];
    for (int i = 0; i <= cur.maxD; i++) {
      if (i && i % 15 == 0) cout << "\n      ";
      cout << setw(3) << cur.divs[i];
    }
    cout << '\n';
  }
}

