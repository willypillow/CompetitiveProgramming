#include <iostream>

struct Matrix {
  int m, n;
} m[255], tmp[1000];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  std::cin >> n;
  for (int i = 0; i < n; i++) {
    char c;
    std::cin >> c;
    std::cin >> m[c].m >> m[c].n;
  }
  std::cin.get();
  char c;
  int tTop = 0, ans = 0;
  bool fail = false;
  while (std::cin.get(c)) {
    if (c == ')') {
      Matrix b = tmp[--tTop];
      Matrix a = tmp[--tTop];
      if (a.n != b.m) fail = true;
      else {
        tmp[tTop++] = Matrix { a.m, b.n };
        ans += a.m * a.n * b.n;
      }
    }
    else if (isalpha(c)) tmp[tTop++] = m[c];
    else if (c == '\n') {
      if (fail) std::cout << "error\n";
      else std::cout << ans << '\n';
      ans = tTop = fail = 0;
    }
  }
}
