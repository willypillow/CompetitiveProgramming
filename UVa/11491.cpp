#include <iostream>
#include <set>
#include <vector>
#include <queue>
#include <string>
#include <cstring>
#include <cstdlib>
using namespace std;

int main()  {
	int n, d;
	while (cin >> n >> d && n) {
		string s; cin >> s;
		queue<int> pos[10];
		for (int i = 0; i < n; i++) {
			pos[s[i] - '0'].push(i);
		}
		for (int i = 0; i < n - d; ) {
			for (int j = 9; j >= 0; j--) {
				while (pos[j].size() && pos[j].front() < i) pos[j].pop();
				if (pos[j].size() && pos[j].front() - i <= d) {
					d -= pos[j].front() - i;
					i = pos[j].front() + 1;
					pos[j].pop();
					cout << j;
					break;
				}
			}
		}
		cout << '\n';
	}
}
