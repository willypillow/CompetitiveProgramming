#include <iostream>
#include <stack>
using namespace std;

int pri(char c) {
	switch (c) {
	case '(':
		return 0;
	case ')':
		return 100;
	case '*':
	case '/':
		return 10;
	case '+':
	case '-':
		return 1;
	}
	return 0;
}

char str[20];

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	int t; cin >> t >> ws;
	while (t--) {
		stack<char> s;
		while (cin.getline(str, 10) && str[0] != '\0') {
			if (isdigit(str[0])) {
				cout << str;
			} else {
				if (str[0] == '(') {
					s.push(str[0]);
				} else if (str[0] == ')') {
					while (s.top() != '(') {
						if (s.top() != '(' && s.top() != ')') cout << s.top();
						s.pop();
					}
					s.pop();
				} else {
					while (!s.empty() && pri(s.top()) >= pri(str[0])) {
						if (s.top() != '(' && s.top() != ')') cout << s.top();
						s.pop();
					}
					s.push(str[0]);
				}
			}
		}
		while (!s.empty()) {
			if (s.top() != '(' && s.top() != ')') cout << s.top();
			s.pop();
		}
		cout << '\n';
		if (t) cout << '\n';
	}
}
