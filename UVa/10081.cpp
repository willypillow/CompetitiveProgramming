#include <bits/stdc++.h>
using namespace std;

double dp[101][11];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int k, n;
	while (cin >> k >> n) {
		for (int j = 0; j <= k; j++) dp[1][j] = 1;
		for (int i = 2; i <= n; i++) {
			for (int j = 0; j <= k; j++) {
				dp[i][j] = dp[i - 1][j];
				if (j - 1 >= 0) dp[i][j] += dp[i - 1][j - 1];
				if (j + 1 <= k) dp[i][j] += dp[i - 1][j + 1];
			}
		}
		double ans = 0;
		for (int j = 0; j <= k; j++) ans += dp[n][j];
		cout << fixed << setprecision(5) << 100 * ans / pow(k + 1, n) << '\n';
	}
}
