#include <bits/stdc++.h>
using namespace std;

vector<vector<int> > g;
int dp[201][101] = { 0 }, a[201];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int t; cin >> t;
	while (t--) {
		int n, m; cin >> n >> m;
		g.clear(); g.resize(n + 1);
		for (int i = 0; i < m; i++) {
			int u, v; cin >> u >> v;
			g[u].push_back(v); g[v].push_back(u);
		}
		int len; cin >> len;
		for (int i = 1; i <= len; i++) cin >> a[i];
		for (int i = 1; i <= len; i++) {
			for (int j = 1; j <= n; j++) {
				dp[i][j] = dp[i - 1][j] + (a[i] != j);
				for (int k: g[j]) {
					dp[i][j] = min(dp[i][j], dp[i - 1][k] + (a[i] != j));
				}
			}
		}
		int ans = 1 << 30;
		for (int j = 1; j <= n; j++) ans = min(ans, dp[len][j]);
		cout << ans << '\n';
	}
}
