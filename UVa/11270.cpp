#include <iostream>
#include <cstring>
using namespace std;

uint_fast64_t dp[2][1024];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, m;
	while (cin >> n >> m) {
		if (m > n) swap(m, n);
		memset(dp, 0, sizeof(dp));
		bool dpIdx = 0;
		dp[0][(1 << m) - 1] = 1;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				memset(dp[!dpIdx], 0, sizeof(dp[!dpIdx]));
				for (int k = (1 << m) - 1; k >= 0; k--) {
					int left = (j ? ((k >> (j - 1)) & 1) : 1), up = (k >> j) & 1;
					if (!up) dp[!dpIdx][k | (1 << j)] += dp[dpIdx][k];
					if (up && !left) dp[!dpIdx][k | (1 << j) | (1 << (j - 1))] += dp[dpIdx][k];
					if (up) dp[!dpIdx][k ^ (1 << j)] += dp[dpIdx][k];
				}
				dpIdx = !dpIdx;
			}
		}
		cout << dp[dpIdx][(1 << m) - 1] << '\n';
	}
}
