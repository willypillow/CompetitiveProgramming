#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cmath>
using std::cin; using std::cout; using std::sort; using std::setprecision;

double PI = acos(-1);
int n;

struct P {
	double x, y;
	bool operator <(const P &rhs) const {
		return (x != rhs.x) ? (x < rhs.x) : (y < rhs.y);
	}
} p[2400], hull[2400];

double cross(P o, P a, P b) {
	return (a.x - o.x) * (b.y - o.y) - (a.y - o.y) * (b.x - o.x);
}

int convexHull() {
	sort(p, p + n * 4);
	int m = 0;
	for (int i = 0; i < n * 4; i++) {
		while (m >= 2 && cross(hull[m - 2], hull[m - 1], p[i]) <= 0) m--;
		hull[m++] = p[i];
	}
	for (int i = n * 4 - 2, t = m; i >= 0; i--) {
		while (m > t && cross(hull[m - 2], hull[m - 1], p[i]) <= 0) m--;
		hull[m++] = p[i];
	}
	return m - 1;
}

double area(int m) {
	double ans = 0;
	for (int i = 2; i < m; i++) {
		ans += cross(hull[0], hull[i - 1], hull[i]);
	}
	return ans / 2;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		cin >> n;
		double boxArea = 0;
		for (int i = 0; i < n; i++) {
			double x, y, w, h, angle;
			cin >> x >> y >> w >> h >> angle;
			boxArea += w * h;
			double rad = angle * PI / 180.0,
				halfWSin = w / 2 * sin(rad), halfWCos = w / 2 * cos(rad),
				halfHSin = h / 2 * sin(rad), halfHCos = h / 2 * cos(rad);
			P lEdge = P { x - halfWCos, y + halfWSin },
				rEdge = P { x + halfWCos, y - halfWSin };
			P p1 = P { lEdge.x + halfHSin, lEdge.y + halfHCos },
				p2 = P { lEdge.x - halfHSin, lEdge.y - halfHCos },
				p3 = P { rEdge.x + halfHSin, rEdge.y + halfHCos },
				p4 = P { rEdge.x - halfHSin, rEdge.y - halfHCos };
			p[i * 4] = p1, p[i * 4 + 1] = p2, p[i * 4 + 2] = p3, p[i * 4 + 3] = p4;
		}
		int m = convexHull();
		cout << std::fixed << setprecision(1) << 100 * boxArea / area(m) << " %\n";
	}
}
