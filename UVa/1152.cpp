#include <iostream>
#include <unordered_map>

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif 
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  int t;
  std::unordered_map<int, int> map;
  std::cin >> t;
  while (t--) {
    map.clear();
    int ans = 0, n, arr[4][4000];
    std::cin >> n;
    for (int i = 0; i < n; i++) {
      std::cin >> arr[0][i] >> arr[1][i] >> arr[2][i] >> arr[3][i];
    }
    for (int a = 0; a < n; a++) {
      for (int b = 0; b < n; b++) map[arr[0][a] + arr[1][b]]++;
    }
    for (int c = 0; c < n; c++) {
      for (int d = 0; d < n; d++) ans += map[-arr[2][c] - arr[3][d]];
    }
    std::cout << ans << '\n';
    if (t) std::cout << '\n';
  }
}
