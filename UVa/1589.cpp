#include <iostream>

const char MX[4] = { -1, 0, 1, 0 };
const char MY[4] = { 0, -1, 0, 1 };

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n, bgx, bgy;
  while (std::cin >> n >> bgx >> bgy) {
    if (!n) break;
    char types[10];
    int xs[10], ys[10];
    for (int i = 0; i < n; i++) std::cin >> types[i] >> xs[i] >> ys[i];
    bool fail = false;
    for (int i = 0; i < 4 && !fail; i++) {
      int nbgx = bgx + MX[i], nbgy = bgy + MY[i];
      if (nbgx < 1 || nbgx > 3 || nbgy < 4 || nbgy > 6) continue;
      bool kill = false;
      for (int k = 0; k < n && !kill; k++) {
        switch (types[k]) {
        case 'G':
          if (nbgy == ys[k]) {
            bool block = false;
            for (int m = 0; m < n && !block; m++) {
              if (ys[k] == ys[m] && xs[m] > nbgx && xs[m] < xs[k]) block = true;
            }
            if (!block) kill = true;
          }
          break;
        case 'R':
          if (xs[k] != nbgx || ys[k] != nbgy) {
            if (xs[k] == nbgx) {
              bool block = false;
              for (int m = 0; m < n && !block; m++) {
                int min, max;
                if (ys[k] > nbgy) { min = nbgy; max = ys[k]; }
                else { max = nbgy; min = ys[k]; }
                if (xs[k] == xs[m] && ys[m] > min && ys[m] < max) block = true;
              }
              if (!block) kill = true;
            }
            if (ys[k] == nbgy) {
              bool block = false;
              for (int m = 0; m < n && !block; m++) {
                int min, max;
                if (xs[k] > nbgx) { min = nbgx; max = xs[k]; }
                else { max = nbgx; min = xs[k]; }
                if (ys[k] == ys[m] && xs[m] > min && xs[m] < max) block = true;
              }
              if (!block) kill = true;
            }
          }
          break;
        case 'H': {
          int xdiff = abs(xs[k] - nbgx), ydiff = abs(ys[k] - nbgy);
          if (xdiff == 2 && ydiff == 1 || xdiff == 1 && ydiff == 2) {
            bool block = false;
            int xx, xy;
            if (xdiff == 1) { xx = xs[k]; xy = (ys[k] + nbgy) / 2; }
            else { xx = (xs[k] + nbgx) / 2; xy = ys[k]; }
            for (int m = 0; m < n && !block; m++) {
              if (xs[m] == xx && ys[m] == xy) block = true;
            }
            if (!block) kill = true;
          }
          break;
        }
        case 'C':
          if (xs[k] == nbgx) {
            int block = 0;
            for (int m = 0; m < n ; m++) {
              int min, max;
              if (ys[k] > nbgy) { min = nbgy; max = ys[k]; }
              else { max = nbgy; min = ys[k]; }
              if (xs[k] == xs[m] && ys[m] > min && ys[m] < max) block++;
            }
            if (block == 1) kill = true;
          }
          if (ys[k] == nbgy) {
            int block = 0;
            for (int m = 0; m < n ; m++) {
              int min, max;
              if (xs[k] > nbgx) { min = nbgx; max = xs[k]; }
              else { max = nbgx; min = xs[k]; }
              if (ys[k] == ys[m] && xs[m] > min && xs[m] < max) block++;
            }
            if (block == 1) kill = true;
          }
          break;
        }
      }
      if (!kill) fail = true;
    }
    if (fail) puts("NO");
    else puts("YES");
  }
}
