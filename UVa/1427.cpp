#include <iostream>
#include <queue>
using namespace std;

const int INF = 1 << 30;
int happy[110][10100], times[110][10100], sum[10100], sumTime[10100],
	dp[110][10100], dpL[10100], dpR[10100];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, m, k;
	while (cin >> n >> m >> k && n) {
		for (int i = 0; i <= n; i++) {
			for (int j = 1; j <= m; j++) cin >> happy[i][j];
		}
		for (int i = 0; i <= n; i++) {
			for (int j = 1; j <= m; j++) cin >> times[i][j];
		}
		for (int j = 0; j <= m; j++) dp[n + 1][j] = 0;
		for (int i = n; i >= 0; i--) {
			deque<int> q;
			for (int j = 0; j <= m; j++) {
				sum[j] = (j ? sum[j - 1] : 0) + happy[i][j];
				sumTime[j] = (j ? sumTime[j - 1] : 0) + times[i][j];
				int last = dp[i + 1][j] - sum[j];
				while (!q.empty() && dp[i + 1][q.back()] - sum[q.back()] <= last) {
					q.pop_back();
				}
				q.push_back(j);
				while (!q.empty() && sumTime[j] - sumTime[q.front()] > k) q.pop_front();
				if (i == n) {
					dpL[j] = max(-INF, sum[j] - sum[q.front()]);
				} else {
					dpL[j] = max(dp[i + 1][j], dp[i + 1][q.front()] + sum[j] - sum[q.front()]);
				}
			}
			while (!q.empty()) q.pop_back();
			sum[m + 2] = sumTime[m + 2] = 0;
			for (int j = m + 1; j >= 1; j--) {
				sum[j] = sum[j + 1] + happy[i][j];
				sumTime[j] = sumTime[j + 1] + times[i][j];
				int last = dp[i + 1][j - 1] - sum[j];
				while (!q.empty() && dp[i + 1][q.back() - 1] - sum[q.back()] <= last) {
					q.pop_back();
				}
				q.push_back(j);
				while (!q.empty() && sumTime[j] - sumTime[q.front()] > k) q.pop_front();
				if (i == n) {
					dpR[j] = max(-INF, sum[j] - sum[q.front()]);
				} else {
					dpR[j] = max(dp[i + 1][j - 1], dp[i + 1][q.front() - 1] + sum[j] - sum[q.front()]);
				}
				dp[i][j - 1] = max(dpL[j - 1], dpR[j]);
			}
		}
		int ans = 0;
		for (int j = 0; j <= m; j++) ans = max(ans, dp[0][j]);
		cout << ans << '\n';
	}
}
