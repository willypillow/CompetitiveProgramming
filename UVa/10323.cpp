#include <iostream>
using std::cin; using std::cout;

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  unsigned long long x = 1, fac[25];
  int low = 0, n;
  while (x < 10000) x *= ++low;
  fac[0] = x;
  int hi = low;
  while (fac[hi - low] <= 6227020800ULL) {
    hi++;
    fac[hi - low] = fac[hi - low - 1] * hi;
  }
  while (cin >> n) {
    if (n < 0) {
      if (-n % 2 == 1) cout << "Overflow!\n";
      else cout << "Underflow!\n";
    } else {
      if (n < low) cout << "Underflow!\n";
      else if (n >= hi) cout << "Overflow!\n";
      else cout << fac[n - low] << '\n';
    }
  }
}
