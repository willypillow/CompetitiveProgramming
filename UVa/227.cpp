#include <iostream>
#include <cstring>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);

  for (int i = 1;; i++) {
    char arr[5][6];
    int blankX = -1, blankY = -1;
    std::cin.getline(arr[0], 6);
    if (arr[0][0] == 'Z') break;
    for (int j = 0; j < 5; j++) {
      char *ptr = arr[j];
      for (int k = 0; blankX == -1 && k < 5; k++, ptr++) {
        if (*ptr == '\0') *ptr = ' ';
        if (*ptr == ' ') {
          blankX = j;
          blankY = k;
          break;
        }
      }
      if (j < 4) std::cin.getline(arr[j + 1], 6);
    }

    char cmd;
    bool fail = false;
    while (std::cin >> cmd && cmd != '0') {
      switch (cmd) {
      case 'A':
        if (blankX - 1 < 0) fail = true;
        else {
          arr[blankX][blankY] = arr[blankX - 1][blankY];
          arr[blankX - 1][blankY] = ' ';
          blankX--;
        }
        break;
      case 'B':
        if (blankX + 1 > 4) fail = true;
        else {
          arr[blankX][blankY] = arr[blankX + 1][blankY];
          arr[blankX + 1][blankY] = ' ';
          blankX++;
        }
        break;
      case 'L':
        if (blankY - 1 < 0) fail = true;
        else {
          arr[blankX][blankY] = arr[blankX][blankY - 1];
          arr[blankX][blankY - 1] = ' ';
          blankY--;
        }
        break;
      case 'R':
        if (blankY + 1 > 4) fail = true;
        else {
          arr[blankX][blankY] = arr[blankX][blankY + 1];
          arr[blankX][blankY + 1] = ' ';
          blankY++;
        }
        break;
      default:
        fail = true;
      }
      if (fail) {
        std::cin.ignore(100, '0');
        break;
      }
    }
    std::cin.ignore(100, '\n');

    if (i != 1) std::cout << '\n';
    std::cout << "Puzzle #" << i << ":\n";
    if (fail) {
      std::cout << "This puzzle has no final configuration.\n";
    } else {
      for (int j = 0; j < 5; j++) {
        for (int k = 0; k < 5; k++) {
          std::cout << arr[j][k];
          if (k != 4) std::cout << ' ';
        }
        std::cout << '\n';
      }
    }
  }
}
