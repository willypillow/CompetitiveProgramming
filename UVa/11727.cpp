#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  std::cin >> n;
  for (int i = 1; i <= n; i++) {
    int a, b, c, res;
    std::cin >> a >> b >> c;
    if (a > b) {
      if (b > c) res = b;
      else if (c > a) res = a;
      else res = c;
    } else { // b > a
      if (c > b) res = b;
      else if (a > c) res = a;
      else res = c;
    }
    std::cout << "Case " << i << ": " << res << '\n';
  }
}
