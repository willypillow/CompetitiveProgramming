#include <iostream>
using std::cin; using std::cout;

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  while (cin >> n && n) {
    for (int i = 1; i < n; i++) {
      int cur = 0, count = 1;
      bool used[110] = { true, false };
      while (cur != 12) {
        for (int j = 0; j < i; j++) {
          do { cur = (cur + 1) % n; } while (used[cur]);
        }
        used[cur] = true;
        count++;
      }
      if (count == n) {
        cout << i << '\n';
        break;
      }
    }
  }
}
