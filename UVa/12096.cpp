#include <iostream>
#include <map>
#include <set>
#include <stack>
#include <algorithm>

std::map<std::set<int>, int> cache;
std::set<int> toSet[10000];
int topId = 0;

static inline int toId(const std::set<int> &s) {
  if (cache.count(s)) return cache[s];
  toSet[topId] = s;
  return cache[s] = topId++;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int t, empty = toId(std::set<int>());
  std::cin >> t;
  std::stack<int> s;
  while (t--) {
    int n;
    std::cin >> n;
    while (n--) {
      char cmd[100];
      std::cin >> cmd;
      if (cmd[0] == 'P') s.push(empty);
      else if(cmd[0] == 'D') s.push(s.top());
      else {
        int id1 = s.top(); s.pop();
        std::set<int> tmp, &s1 = toSet[id1], &s2 = toSet[s.top()]; s.pop();
        if (cmd[0] == 'U') {
          std::set_union(s1.begin(), s1.end(), s2.begin(), s2.end(),
            std::inserter(tmp, tmp.begin()));
        } else if (cmd[0] == 'I') {
          std::set_intersection(s1.begin(), s1.end(), s2.begin(), s2.end(),
            std::inserter(tmp, tmp.begin()));
        } else {
          tmp = s2;
          tmp.insert(id1);
        }
        s.push(toId(tmp));
      }
      std::cout << toSet[s.top()].size() << '\n';
    }
    std::cout << "***\n";
  }
}
