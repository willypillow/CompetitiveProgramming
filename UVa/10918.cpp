#include <bits/stdc++.h>
using namespace std;

int dp[31][3][8];

int main() {
	int n;
	dp[0][0][0] = dp[0][1][0] = dp[0][2][0] = 1;
	for (int i = 1; i <= 30; i++) {
		for (int j = 0; j < 3; j++) {
			int *last = (j ? dp[i][j - 1] : dp[i - 1][2]);
			for (int k = (1 << 3) - 1; k >= 0; k--) {
				if (k & (1 << j)) {
					dp[i][j][k ^ (1 << j)] += last[k];
				} else {
					if (j > 0 && (k & (1 << (j - 1)))) {
						dp[i][j][k ^ (1 << (j - 1))] += last[k];
					}
					dp[i][j][k | (1 << j)] += last[k];
				}
			}
		}
	}
	while (cin >> n && n != -1) {
		cout << dp[n][2][0] << '\n';
	}
}

