#include <algorithm>
#include <iostream>

struct Proposal {
  char name[81];
  double price;
  int reqMet;
  bool operator <(const Proposal &rhs) {
    return (reqMet != rhs.reqMet) ? (reqMet > rhs.reqMet) : (price < rhs.price);
  }
} proposals[1000];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n, p, cases = 0;
  while (std::cin >> n >> p && n) {
    std::cin.ignore(1000, '\n');
    for (int i = 0; i < n; i++) std::cin.ignore(1000, '\n');
    for (int i = 0; i < p; i++) {
      std::cin.getline(proposals[i].name, 81);
      std::cin >> proposals[i].price >> proposals[i].reqMet;
      std::cin.ignore(1000, '\n');
      for (int j = 0; j < proposals[i].reqMet; j++) std::cin.ignore(1000, '\n');
    }
    std::sort(proposals, proposals + p);
    if (cases) std::cout << '\n';
    std::cout << "RFP #" << ++cases << '\n' << proposals[0].name << '\n';
  }
}
