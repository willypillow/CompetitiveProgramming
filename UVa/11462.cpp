#include <iostream>
using std::cin; using std::cout;

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  while (cin >> n && n) {
    int arr[101] = { 0 }, tmp;
    while (n--) {
      cin >> tmp;
      arr[tmp]++;
    }
    bool first = true;
    for (int i = 1; i < 100; i++) {
      while (arr[i]--) {
        if (!first) cout << ' ';
        first = false;
        cout << i;
      }
    }
    cout << '\n';
  }
}
