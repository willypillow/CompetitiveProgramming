#include <iostream>
#include <cstdio>
#include <cmath>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int t;
  std::cin >> t;
  while (t--) {
    int n;
    std::cin >> n;
    putchar(std::abs((n * 315 + 36962) / 10 % 10) + '0');
    putchar('\n');
  }
}
