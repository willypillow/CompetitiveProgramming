#include <cmath>
#include <cstring>
#include <iomanip>
#include <iostream>

static const int MAXN = 1000;

int n;
double ansIn, ansOut;
bool vis[MAXN];
struct Enemy {
  int x, y, r;
} enemies[MAXN];

static inline bool isConnected(int i, int j) {
  int x = abs(enemies[i].x - enemies[j].x),
    y = abs(enemies[i].y - enemies[j].y),
    r = enemies[i].r + enemies[j].r;
  x *= x; y *= y; r *= r;
  return (x + y < r);
}

bool dfs(int e) {
  vis[e] = true;
  if (enemies[e].y - enemies[e].r <= 0) return true;
  if (enemies[e].x - enemies[e].r <= 0) {
    int &r = enemies[e].r, &x = enemies[e].x;
    double newAns = enemies[e].y - sqrt(r * r - x * x);
    ansIn = (ansIn < newAns) ? ansIn : newAns;
  } else if (enemies[e].x + enemies[e].r >= 1000) {
    int &r = enemies[e].r, x = 1000 - enemies[e].x;
    double newAns = enemies[e].y - sqrt(r * r - x * x);
    ansOut = (ansOut < newAns) ? ansOut : newAns;
  }
  bool fail = false;
  for (int i = 0; i < n && !fail; i++) {
    if (!vis[i] && isConnected(i, e)) fail = dfs(i); 
  }
  return fail;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif 
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  while (std::cin >> n) {
    for (int i = 0; i < n; i++) {
      vis[i] = false;
      std::cin >> enemies[i].x >> enemies[i].y >> enemies[i].r;
    }
    bool fail = false;
    ansIn = ansOut = 1000;
    for (int i = 0; i < n && !fail; i++) {
      if (!vis[i] && enemies[i].y + enemies[i].r >= 1000) fail = dfs(i);
    }
    if (fail) std::cout << "IMPOSSIBLE\n";
    else {
      std::cout << std::fixed << std::setprecision(2) <<
        "0.00 " << ansIn << " 1000.00 " << ansOut << '\n';
    }
  }
}
