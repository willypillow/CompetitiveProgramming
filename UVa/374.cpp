#include <iostream>

inline long long powMod(int b, int p, int m) {
  if (p == 0) return 1;
  long long ans = powMod(b, p / 2, m);
  ans = ans * ans % m;
  if (p & 1) ans = ans * b % m;
  return ans;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int b, p, m;
  while (std::cin >> b >> p >> m) {
    std::cout << powMod(b, p, m) << '\n';
  }
}
