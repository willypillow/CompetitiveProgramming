#include <bits/stdc++.h>
using namespace std;

const long double PI = 4 * atan((long double)1.0);

int main() {
    cin.tie(0); ios_base::sync_with_stdio(0);
    double r, h;
    while (cin >> r >> h) {
        if (r * 2 < h) {
            double t = r * r * PI * (h - r * 2) * 2;
            double d = r * 2;
            t += d * d * d * 0.90412966547310824605;
            cout << setprecision(4) << fixed << t << '\n';
        } else {
            long double eps = 1e-9;
            long double tmp = 0, dd = r * eps, hh = h / 2, hh2 = hh * hh, r2 = r * r;
            long double z = sqrt(r2 - hh2), w = z;
            long double xxx = 1.0 / 3.0 * r2 - 1.0 / 3.0 * w * w * w / r;
            xxx = r2 * (r - w) / r - xxx;
            xxx += w / r * hh2;
            long double rat = xxx / hh2;
            cout << setprecision(4) << fixed << PI * r * r * h * 2 - h * h * r * 2 * rat << '\n';
        }
    }
}
