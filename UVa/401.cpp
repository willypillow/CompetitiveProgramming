#include <iostream>
#include <cstring>

char map[] = "________________________________________________51SE_Z__8________A___3__HIL_JM_O___2TUVWXY5_____________________________________________________________________________________________________________________________________________________________________";

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  char str[100];
  while (std::cin >> str) {
    int len = strlen(str);
    bool mir = true, pal = true;
    for (int i = 0; i < len; i++) {
      if (str[i] != str[len - i - 1]) mir = false;
      if (str[i] != map[str[len - i - 1]]) pal = false;
      if (!mir && !pal) break;
    }
    std::cout << str << " -- is " << (mir ? (pal ? "a mirrored palindrome" : "a regular palindrome") : (pal ? "a mirrored string" : "not a palindrome")) << ".\n\n";
  }
}

