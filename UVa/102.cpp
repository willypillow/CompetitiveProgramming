#include <cstdio>

int perm[][3] = { {0, 4, 8}, {0, 5, 7}, {1, 3, 8}, {1, 5, 6}, {2, 3, 7}, {2, 4, 6} };
char str[][4] = { "BGC", "BCG", "GBC", "GCB", "CBG", "CGB" };

int main() {
  int a[9];
  while (scanf("%d", &a[0]) != -1) {
    for (int i = 1; i < 9; i++) scanf("%d", &a[i]);
    char *pnt = str[0];
    int max = a[perm[0][0]] + a[perm[0][1]] + a[perm[0][2]];

    for (int i = 1; i < 6; i++) {
      int nxt = a[perm[i][0]] + a[perm[i][1]] + a[perm[i][2]];
      if (nxt > max) {
        max = nxt;
        pnt = str[i];
      } else if (nxt == max) {
        int choose;
        for (int j = 0; j < 3; j++) {
          if (str[i][j] != pnt[j]) {
            choose = (str[i][j] < pnt[j]) ? 1 : 0;
            break;
          }
        }
        if (choose) pnt = str[i];
      }
    }
    int sum = 0;
    for (int i = 0; i < 9; i++) sum += a[i];
    printf("%s %d\n", pnt, sum - max);
  }
  return 0;
}
