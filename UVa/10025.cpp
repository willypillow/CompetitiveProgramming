#include <bits/stdc++.h>
using namespace std;

typedef long long LL;

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	LL k;
	int t; cin >> t;
	while (t--) {
		cin >> k;
		if (k < 0) k = -k;
		long long l = 1, r = k;
		if (k == 0) l = r = 3;
		while (l != r) {
			long long m = (l + r) >> 1;
			if ((LL)m * (m + 1) / 2 >= k) r = m;
			else l = m + 1;
		}
		int ans = l;
		if (k % 2 == (LL)l * (l + 1) / 2 % 2) ans = l;
		else if ((l + 1) % 2 == 0) ans += 2;
		else ans += 1;
		cout << ans << '\n';
		if (t) cout << '\n';
	}
}
