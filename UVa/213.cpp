#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  char str[1000];
  while (std::cin.getline(str, 1000)) {
    for (;;) {
      char l1, l2, l3;
      std::cin >> l1 >> l2 >> l3;
      int len = ((l1 - '0') << 2) + ((l2 - '0') << 1) + (l3 - '0');
      if (!len) break;
      for (;;) {
        int pos = 0;
        char c;
        for (int i = len - 1; i >= 0; i--) {
          std::cin >> c;
          pos += (c - '0') << i;
        }
        if (pos == (1 << len) - 1) break;
        pos += (1 << len) - len - 1;
        putchar(str[pos]);
      }
    }
    putchar('\n');
    std::cin.get(); // Ignore '\n'
  }
}
