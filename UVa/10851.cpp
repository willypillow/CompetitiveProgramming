#include <iostream>
#include <cstring>
#include <cstdio>
using std::cin; using std::cout;

char str[10][100], ans[100];

int main() {
    int n; cin >> n >> std::ws;
    while (n--) {
        memset(ans, 0, sizeof(ans));
        for (int i = 0; i < 10; i++) cin >> str[i];
        int len = strlen(str[0]) - 2;
        for (int j = 1; j <= len; j++) {
            for (int i = 1; i <= 9; i++) {
                ans[j] |= (str[i][j] == '\\') << (i - 1);
            }
        }
        for (int i = 1; i <= len; i++) cout << ans[i];
        putchar('\n');
        cin.ignore(100, '\n');
    }
}
