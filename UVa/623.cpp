#include <iostream>
#include <cstring>
#include <iomanip>

const unsigned int SIZE = 250, LEN = 12;
const unsigned long long BASE = 1000000000000LL;

struct Product {
  unsigned long long data[SIZE] = { 0 }; // Int overflows!
  Product mult(int y) {
    unsigned int carry = 0;
    for (int j = 0; j < SIZE; j++) {
      data[j] *= y;
      data[j] += carry;
      carry = data[j] / BASE;
      data[j] %= BASE;
    }
  }
  void prnt() {
    unsigned int i;
    for (i = SIZE - 1; i >= 0 && data[i] == 0; i--);
    if (i == -1) std::cout << "0\n";
    else {
        std::cout << data[i];
        while (i-- > 0) {
          std::cout << std::setfill('0') << std::setw(LEN) << data[i];
        }
        std::cout << '\n';
    }
  }
};

Product dp[1001];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  dp[0].data[0] = 1;
  unsigned int top = 1, n;
  while (std::cin >> n) {
    if (n >= top) {
      do {
        memcpy(&dp[top], &dp[top - 1], sizeof(Product));
        dp[top].mult(top);
      } while (top++ < n);
    }
    std::cout << n << "!\n";
    dp[n].prnt();
  }
}
