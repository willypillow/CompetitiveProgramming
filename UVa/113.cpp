#include <iostream>
#include <cmath>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  long double p, n;
  while (std::cin >> p >> n) {
    std::cout << (int)round(pow(n, 1 / p)) << '\n';
  }
}
