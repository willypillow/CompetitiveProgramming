#include <iostream>
#include <cmath>
#include <algorithm>

struct Particle {
  int x, y, r;
  double rad;
  bool operator <(const Particle &rhs) const {
    return rad < rhs.rad;
  }
} particles[1010], sorted[1010];

int cross(Particle &a, Particle &b) {
  return a.x * b.y - a.y * b.x;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif 
  int n;
  while (std::cin >> n && n) {
    int ans = 0;
    for (int i = 0; i < n; i++) {
      std::cin >> particles[i].x >> particles[i].y >> particles[i].r;
    }
    for (int i = 0; i < n; i++) {
      int sTop = 0;
      for (int j = 0; j < n; j++) {
        if (i == j) continue;
        sorted[sTop].x = particles[j].x - particles[i].x;
        sorted[sTop].y = particles[j].y - particles[i].y;
        if (particles[j].r) {
          sorted[sTop].x = -sorted[sTop].x, sorted[sTop].y = -sorted[sTop].y;
        }
        sorted[sTop].rad = std::atan2(sorted[sTop].y, sorted[sTop].x);
        sTop++;
      }
      std::sort(sorted, sorted + sTop);
      int start = 0, end = 0, points = 2;
      while (start < sTop) {
        if (start == end) { end = (end + 1) % sTop; points++; }
        while (start != end && cross(sorted[start], sorted[end]) >= 0) {
          end = (end + 1) % sTop;
          points++;
        }
        points--;
        ans = std::max(ans, points);
        start++;
      }
    }
    std::cout << ans << '\n';
  }
}

 
