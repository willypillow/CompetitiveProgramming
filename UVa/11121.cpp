#include <bits/stdc++.h>
using namespace std;

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	int cases = 0;
	int t; cin >> t;
	long long n;
	while (t--) {
		cin >> n;
		bool neg = false;
		if (n < 0) {
			n *= -2;
			neg = true;
		}
		string ans;
		for (long long i = 0, t = 1; (1LL << i) <= n; i++, t *= -2) {
			if ((n - t) % (t * 2) == 0) {
				ans.push_back('1');
				n -= t;
			} else ans.push_back('0');
		}
		if (neg) ans = string(ans.begin() + 1, ans.end());
		reverse(ans.begin(), ans.end());
		if (ans.size() == 0) ans.push_back('0');
		cout << "Case #" << ++cases << ": " << ans << '\n';
	}
}

