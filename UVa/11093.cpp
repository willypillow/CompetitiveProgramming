#include <iostream>

int p[100001], q[100001];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif 
  int t;
  std::cin >> t;
  for (int cases = 1; cases <= t; cases++) {
    int n, ans = -1;
    std::cin >> n;
    for (int i = 0; i < n; i++) std::cin >> p[i];
    for (int i = 0; i < n; i++) std::cin >> q[i];
    for (int i = 0; i < n; i++) {
      int j = i;
      bool fail = false;
      for (int gas = 0; j < i + n; j++) {
        gas = gas + p[j % n] - q[j % n];
        if (gas < 0) { fail = true; break; }
      }
      if (!fail) {
        ans = i + 1;
        break;
      } else {
        i = j;
      }
    }
    std::cout << "Case " << cases << ": ";
    if (ans != -1) std::cout << "Possible from station " << ans << '\n';
    else std::cout << "Not possible\n";
  }
}
      

