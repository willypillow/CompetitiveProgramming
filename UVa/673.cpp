#include <iostream>

static inline char toLeft(char c) {
  switch (c) {
  case ')': return '(';
  case ']': return '[';
  default: return '\0';
  }
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  std::cin >> n;
  std::cin.ignore(100, '\n');
  while (n--) {
  	char stack[200], str[200];
  	int top = 0;
  	std::cin.getline(str, 200);
  	bool fail = false;
  	for (char *p = str; *p != '\0'; p++) {
  	  if (*p == '(' || *p == '[') stack[top++] = *p;
  	  else if (!top || stack[--top] != toLeft(*p)) { fail = true; break; }
  	}
  	puts((fail || top) ? "No" : "Yes");
  }
}
