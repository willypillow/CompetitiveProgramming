#include <bits/stdc++.h>
using namespace std;

int xs[100002], ys[100002], w[100002], cost[100002];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		int c, n; cin >> c >> n;
		for (int i = 1; i <= n; i++) cin >> xs[i] >> ys[i] >> w[i];
		for (int i = 2; i <= n; i++) {
			cost[i] = cost[i - 1] + abs(xs[i] - xs[i - 1]) + abs(ys[i] - ys[i - 1]);
			w[i] += w[i - 1];
		}
		deque<pair<int, int> > q;
		q.push_back({ xs[1] + ys[1] - cost[1], 0 });
		for (int i = 1; i <= n; i++) {
			while (w[i] - w[q.front().second] > c) q.pop_front();
			int x = q.front().first + cost[i] + xs[i] + ys[i];
			int v = x + xs[i + 1] + ys[i + 1] - cost[i + 1];
			while (q.size() && q.back().first >= v) q.pop_back();
			q.push_back({ v, i });
		}
		cout << q.back().first - xs[n + 1] - ys[n + 1] + cost[n + 1]
				<< (t ? "\n\n" : "\n");
	}
}
