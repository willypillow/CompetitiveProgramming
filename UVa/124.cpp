#include <iostream>
#include <cstring>
#include <sstream>
#include <string>
using namespace std;

bool vis[30], g[30][30], use[30];
char ans[30];
int n;

void dfs(int cur) {
	if (cur == n) {
		ans[cur] = '\0';
		cout << ans << '\n';
		return;
	}
	for (int i = 0; i < 26; i++) {
		bool fail = false;
		if (vis[i] || !use[i]) continue;
		for (int j = 0; j < 26; j++) {
			if (g[i][j] && vis[j]) {
				fail = true;
				break;
			}
		}
		if (fail) continue;
		vis[i] = true;
		ans[cur] = i + 'a';
		dfs(cur + 1);
		vis[i] = false;
	}
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	string str;
	bool first = true;
	while (getline(cin, str)) {
		if (!first) cout << '\n';
		first = false;
		n = 0;
		memset(vis, 0, sizeof(vis)); memset(use, 0, sizeof(use));
		memset(g, false, sizeof(g));
		stringstream ss(str);
		char c, c2;
		while (ss >> c) {
			if (!isalpha(c)) continue;
			use[c - 'a'] = true;
		}
		for (int i = 0; i < 26; i++) {
			if (use[i]) n++;
		}
		getline(cin, str);
		ss = stringstream(str);
		while (ss >> c >> c2) g[c - 'a'][c2 - 'a'] = true;
		dfs(0);
	}
}
