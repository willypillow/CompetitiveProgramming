#include <iostream>
#include <cstring>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  bool isRep[3001];
  int n;
  while (std::cin >> n) {
    memset(isRep, 0, sizeof(bool) * n);
    bool isJolly = true;
    int lastInt, thisInt;
    std::cin >> lastInt;
    for (int i = 1; i < n; i++) {
      std::cin >> thisInt;
      int diff = lastInt - thisInt;
      diff = (diff > 0) ? diff : -diff;
      if (diff >= n || isRep[diff]) {
        isJolly = false;
        break;
      } else {
        isRep[diff] = true;
      }
      lastInt = thisInt;
    }
    if (isJolly) {
      std::cout << "Jolly\n";
    } else {
      std::cin.ignore(100000, '\n');
      std::cout << "Not jolly\n";
    }
  }
}
