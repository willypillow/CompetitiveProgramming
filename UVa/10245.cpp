#include <bits/stdc++.h>
using namespace std;

struct P { double x, y; } p[10000];

double dist(double x0, double y0, double x1, double y1) {
	return (x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0);
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	int n;
	while (cin >> n && n) {
		double ans = 1E20;
		for (int i = 0; i < n; i++) {
			cin >> p[i].x >> p[i].y;
			for (int j = 0; j < i; j++) {
				ans = min(ans, dist(p[i].x, p[i].y, p[j].x, p[j].y));
			}
		}
		if (ans >= 1E8 - 1E-8) cout << "INFINITY\n";
		else cout << fixed << setprecision(4) << sqrt(ans) << '\n';
	}
}
