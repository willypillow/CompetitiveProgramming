#include <bits/stdc++.h>
using namespace std;

int arr[1001];

static inline void update(int &diff, int &ans, int nq, int ai, int aj) {
	int newDiff = abs(nq - aj);
	if (newDiff < diff) {
		diff = newDiff;
		ans = ai + aj;
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int n, cases = 0;
	while (cin >> n && n) {
		for (int i = 0; i < n; i++) cin >> arr[i];
		//sort(arr, arr + n);
		int m; cin >> m;
		cout << "Case " << ++cases << ":\n";
		while (m--) {
			int q; cin >> q;
			int ans = -(1 << 30);
			bool ok = false;
			/*for (int i = 0, j = 0; i < n; i++) {
				int nq = q - arr[i];
				while (j < n && arr[j] < nq) j++;
				if (i != j && j < n) update(diff, ans, nq, arr[i], arr[j]);
				if (i != j - 1 && j) update(diff, ans, nq, arr[i], arr[j - 1]);
				if (i != j + 1 && j + 1 < n) update(diff, ans, nq, arr[i], arr[j + 1]);
				if (i != j - 2 && j >= 2) update(diff, ans, nq, arr[i], arr[j - 2]);
			}*/
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					if (i == j) continue;
					if (!ok || abs(arr[i] + arr[j] - q) < abs(ans - q)) {
						ans = arr[i] + arr[j];
						ok = true;
					}
				}
			}
			cout << "Closest sum to " << q << " is " << ans << ".\n";
		}
	}
}
