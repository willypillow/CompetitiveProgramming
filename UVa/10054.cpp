#include <cstring>
#include <vector>
#include <iostream>

struct P { int dst, node; };
std::vector<P> nodes[51];
bool vis[1000];
int p[51], n;

void dfs(int src) {
  for (P p: nodes[src]) {
    if (vis[p.node]) continue;
    vis[p.node] = true;
    dfs(p.dst);
    std::cout << p.dst << ' ' << src << '\n';
  }
}

void init() {
  memset(vis, false, sizeof(vis));
  for (int i = 0; i <= 50; i++) p[i] = i, nodes[i].clear();
}

int find(int x) {
  return (x == p[x]) ? x : find(p[x]);
}

int bond(int x, int y) {
  int xp = find(x), yp = find(y);
  if (xp != yp) {
    if (xp < yp) p[xp] = yp;
    else p[yp] = xp;
  }
  return p[xp];
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif 
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  int t;
  std::cin >> t;
  for (int cases = 1; cases <= t; cases++) {
    int edgeCnt[51] = { 0 }, root = 0;
    std::cin >> n;
    init();
    for (int i = 0; i < n; i++) {
      int x, y;
      std::cin >> x >> y;
      edgeCnt[x]++; edgeCnt[y]++;
      root = bond(x, y);
      nodes[x].push_back(P { y, i }); nodes[y].push_back(P { x, i });
    }
    bool fail = false;
    for (int i = 1; i <= 50; i++) {
      if (edgeCnt[i] % 2 != 0 ||
          (edgeCnt[i] != 0 && find(i) != root)) {
        fail = true;
        break;
      }
    }
    if (cases != 1) std::cout << '\n';
    std::cout << "Case #" << cases << '\n';
    if (fail) std::cout << "some beads may be lost\n";
    else dfs(root);
  }
}
