#include <bits/stdc++.h>
using namespace std;

int pa[50001];

int find(int x) {
	return (pa[x] == x) ? x : (pa[x] = find(pa[x]));
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int n, m, cases = 0;
	while (cin >> n >> m && n) {
		int ans = n;
		for (int i = 1; i <= n; i++) pa[i] = i;
		while (m--) {
			int x, y; cin >> x >> y;
			int px = find(x), py = find(y);
			if (px != py) {
				pa[px] = py;
				ans--;
			}
		}
		cout << "Case " << ++cases << ": " << ans << '\n';
	}
}
