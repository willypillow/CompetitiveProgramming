#include <iostream>

int main() {
  char c, str[100], *ptr = str;
  while ((c = getchar()) != -1) {
    if (c == ' ' || c == '\n') {
      while (ptr-- != str) putchar(*ptr);
      ptr = str;
      putchar(c);
    } else {
      *ptr++ = c;
    }
  }
}
