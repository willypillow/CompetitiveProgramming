#include <bits/stdc++.h>
using namespace std;

struct Point { double x, y; } point[110];
struct Edge {
	int u, v;
	double dist;
	bool operator <(const Edge &rhs) const {
		return dist < rhs.dist;
	}
};
vector<Edge> edges;
int par[110];

double dist(const Point &l, const Point &r) {
	return sqrt((r.x - l.x) * (r.x - l.x) + (r.y - l.y) * (r.y - l.y));
}

int find(int x) {
	return (par[x] == x) ? x : (par[x] = find(par[x]));
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	int t; cin >> t;
	while (t--) {
		edges.clear();
		int n; cin >> n;
		for (int i = 0; i < n; i++) {
			par[i] = i;
			cin >> point[i].x >> point[i].y;
			for (int j = 0; j < i; j++) {
				edges.push_back(Edge { i, j, dist(point[i], point[j]) });
			}
		}
		sort(edges.begin(), edges.end());
		double ans = 0;
		for (int i = 0, cnt = 0; cnt < n && i < edges.size(); i++) {
			Edge &e = edges[i];
			int pU = find(e.u), pV = find(e.v);
			if (pU == pV) continue;
			par[pU] = pV;
			cnt++;
			ans += e.dist;
		}
		cout << fixed << setprecision(2) << ans << '\n';
		if (t) cout << '\n';
	}
}
