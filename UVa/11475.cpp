#include <iostream>
#include <algorithm>
#include <cstring>
using std::cin; using std::cout; using std::reverse_copy;

char str[100001], reverseStr[100001];

bool isMatch(int len) {
  char tmp = reverseStr[len];
  reverseStr[len] = '\0';
  bool res = strstr(str, reverseStr) != NULL;
  reverseStr[len] = tmp;
  return res;
}

bool cmp(int subLen, int len) {
  char tmp = reverseStr[subLen];
  reverseStr[subLen] = '\0';
  bool res = strcmp(str + len - subLen, reverseStr) == 0;
  reverseStr[subLen] = tmp;
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  while (cin.getline(str, 100001)) {
    int len = strlen(str);
    reverse_copy(str, str + len, reverseStr);
    reverseStr[len] = '\0';
    int l = 0, r = len;
    while (l < r) {
      int m = (l + r + 1) >> 1;
      if (isMatch(m)) l = m;
      else r = m - 1;
    }
    while (!cmp(l, len)) l--;
    cout << str << reverseStr + l << '\n';
  }
}

