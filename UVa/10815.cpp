#include <iostream>
#include <string>
#include <set>
#include <sstream>
#include <cstring>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  std::set<std::string> words;
  char str[1000];
  while (std::cin.getline(str, 1000)) {
    int len = strlen(str);
    for (int i = 0; i < len; i++) {
      if (isalpha(str[i])) str[i] = tolower(str[i]);
      else str[i] = ' ';
    }
    std::stringstream ss(str);
    std::string buf;
    while (ss >> buf) words.insert(buf);
  }
  for (std::string s : words) std::cout << s << '\n';
}
