#include <iostream>
#include <queue>
#include <cstring>

struct P {
  int x, y;
  inline int id() {
    return ((x - 1) << 4) + (y - 1);
  }
  bool operator ==(const P &rhs) const {
    return x == rhs.x && y == rhs.y;
  }
  bool operator !=(const P &rhs) const {
    return x != rhs.x || y != rhs.y;
  }
};

struct Node {
  P edges[4];
  int eTop;
  inline void addEdge(P &e) {
    edges[eTop++] = e;
  }
};

struct State {
  P pos[3] = { 0 };
  int steps = 0;
  bool isFwd;
  inline int id() {
    return (pos[0].id() << 16) + (pos[1].id() << 8) + pos[2].id();
  }
  bool operator ==(const State &rhs) const {
    return pos[0] == rhs.pos[0] && pos[1] == rhs.pos[1] && pos[2] == rhs.pos[2];
  }
};

int w, h, n;
int vis[256 << 16];
Node nodes[20][20] = { 0 };
State *curState, res[1000], orig;
int rTop;

void dfsMoves(int depth) {
  if (depth == n) {
    for (int i = 0; i < n; i++) {
      for (int j = i + 1; j < n; j++) {
        if ((curState->pos[i] == orig.pos[j] &&
            curState->pos[j] == orig.pos[i]) ||
            curState->pos[i] == curState->pos[j]) {
          return;
        }
      }
    }
    res[rTop++] = *curState;
    return;
  }
  P &p = curState->pos[depth];
  p = orig.pos[depth];
  dfsMoves(depth + 1);
  Node &nd = nodes[p.x][p.y];
  for (int i = 0; i < nd.eTop; i++) {
    curState->pos[depth] = nd.edges[i];
    dfsMoves(depth + 1);
  }
}

static inline int isVis(State &s) {
  int &v = vis[s.id()];
  if (v) return v;
  v = s.steps * (s.isFwd ? 1 : -1);
  return 0;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  while (std::cin >> w >> h >> n && w) {
    std::cin.ignore(100, '\n');
    memset(vis, false, sizeof(vis)); memset(nodes, 0, sizeof(nodes));
    char field[20][20] = { '\0' };
    P srcs[3] = { 0 }, dsts[3] = { 0 };
    for (int i = 1; i <= h; i++) std::cin.getline(field[i] + 1, 20);
    for (int i = 1; i <= h; i++) {
      for (int j = 1; j <= w; j++) {
        if (field[i][j] != '#') {
          P cur = P { i, j };
          if (islower(field[i][j])) srcs[field[i][j] - 'a'] = P { i, j };
          else if (isupper(field[i][j])) dsts[field[i][j] - 'A'] = P { i, j };
          nodes[i - 1][j].addEdge(cur); nodes[i + 1][j].addEdge(cur);
          nodes[i][j - 1].addEdge(cur); nodes[i][j + 1].addEdge(cur);
        }
      }
    }
    State root, goal;
    root.pos[0] = srcs[0]; root.pos[1] = srcs[1]; root.pos[2] = srcs[2];
    goal.pos[0] = dsts[0]; goal.pos[1] = dsts[1]; goal.pos[2] = dsts[2];
    root.isFwd = true, goal.isFwd = false;
    std::queue<State> q;
    q.push(root);
    q.push(goal);
    int ans = -1;
    while (!q.empty()) {
      State cur = q.front(); q.pop();
      rTop = 0, curState = &cur, orig = cur;
      dfsMoves(0);
      for (int i = 0; i < rTop; i++) {
        State &s = res[i];
        int visStat = isVis(s);
        if (!visStat) {
          ++s.steps;
          q.push(s);
        } else if ((visStat < 0) == s.isFwd) {
          ans = s.steps + abs(visStat) + 2;
          goto End;
        }
      }
    }
End:
    std::cout << ans << '\n';
  }
}
