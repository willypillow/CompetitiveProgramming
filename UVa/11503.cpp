#include <bits/stdc++.h>
using namespace std;

unordered_map<string, int> dict;
int pa[200000], cnt[200000];

int toId(string str) {
	if (dict.count(str)) return dict[str];
	int nId = dict.size();
	return dict[str] = nId;
}

int find(int x) {
	return (pa[x] == x) ? x : (pa[x] = find(pa[x]));
}

int merge(int a, int b) {
	int ap = find(a), bp = find(b);
	if (ap == bp) return cnt[ap];
	pa[ap] = bp;
	return cnt[bp] += cnt[ap];
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int t; cin >> t;
	while (t--) {
		dict.clear();
		int f; cin >> f;
		for (int i = 0; i < f * 2; i++) {
			pa[i] = i;
			cnt[i] = 1;
		}
		string str1, str2;
		while (f--) {
			cin >> str1 >> str2;
			int a = toId(str1), b = toId(str2);
			cout << merge(a, b) << '\n';
		}
	}
}
