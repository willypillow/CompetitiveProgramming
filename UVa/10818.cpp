#include <bits/stdc++.h>
using namespace std;

const int DX[] = { 0, -1, 1, 0 }, DY[] = { 1, 0, 0, -1 }, INF = 0x5f5f5f5f;
const char DIR[] = "ENSW";

int r, c, dist[21][21], homeX, homeY, order[11];
char path[21][21][110], field[21][21];
bool vis[21][21];
vector<pair<int, int> > dests;

bool check(int x, int y) {
	return 0 <= x && x < r && 0 <= y && y < c &&
		field[x][y] != '#' && field[x][y] != 'X';
}

void dfsCanReach(int x, int y) {
	vis[x][y] = true;
	if (field[x][y] == '*') dests.push_back(make_pair(x, y));
	for (int i = 0; i < 4; i++) {
		int nx = x + DX[i], ny = y + DY[i];
		if (check(nx, ny) && !vis[nx][ny]) dfsCanReach(nx, ny);
	}
}

void bfsPath(int a, int b) {
	int ax = dests[a].first, ay = dests[a].second,
		bx = dests[b].first, by = dests[b].second;
	memset(dist, 0x5f, sizeof(dist));
	queue<pair<int, int> > q;
	dist[ax][ay] = 0;
	q.push(make_pair(ax, ay));
	while (!q.empty()) {
		auto cur = q.front(); q.pop();
		for (int i = 0; i < 4; i++) {
			int nx = cur.first + DX[i], ny = cur.second + DY[i];
			if (check(nx, ny) && dist[nx][ny] == INF) {
				dist[nx][ny] = dist[cur.first][cur.second] + 1;
				if (nx == bx && ny == by) goto End;
				q.push(make_pair(nx, ny));
			}
		}
	}
End:
	int d = dist[bx][by], x = bx, y = by;
	while (d--) {
		int i;
		for (i = 0; i < 4; i++) {
			int nx = x + DX[i], ny = y + DY[i];
			if (check(nx, ny) && dist[nx][ny] == dist[x][y] - 1) {
				x = nx; y = ny;
				break;
			}
		}
		path[a][b][d] = DIR[3 - i];
	}
	path[a][b][dist[bx][by]] = '\0';
}


int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	while (cin >> r >> c && r) {
		cin.ignore(100, '\n');
		dests.clear();
		memset(vis, false, sizeof(vis));
		for (int i = 0; i < r; i++) {
			cin.getline(field[i], 22);
			char *c;
			if ((c = strchr(field[i], 'S'))) homeX = i, homeY = c - field[i];
		}
		dests.push_back(make_pair(homeX, homeY));
		dfsCanReach(homeX, homeY);
		if (dests.size() == 1) {
			cout << "Stay home!\n";
			continue;
		}
		for (unsigned int i = 0; i < dests.size(); i++) {
			for (unsigned int j = 0; j < dests.size(); j++) bfsPath(i, j);
		}
		for (unsigned int i = 0; i < dests.size() - 1; i++) order[i] = i + 1;
		string ans = string(1000, 'x');
		do {
			string tmp;
			int cur = 0;
			for (unsigned int i = 0; i < dests.size() - 1; i++) {
				tmp.append(path[cur][order[i]]);
				cur = order[i];
				if (tmp.size() > ans.size()) goto Next;
			}
			tmp.append(path[cur][0]);
			if (tmp.size() < ans.size() || (tmp.size() == ans.size() && tmp < ans)) {
				ans = tmp;
			}
Next:;
		} while (next_permutation(order, order + dests.size() - 1));
		cout << ans << '\n';
	}
}
