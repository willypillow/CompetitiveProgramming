#include <iostream>
#include <cstring>
#include <vector>
using namespace std;

int timer, low[1100], dfn[1100];
bool isBridge[1100000], hasPrint[1100000], vis[1100];
vector<pair<int, int> > edges;
vector<vector<int> > g;

void dfs(int u, int p) {
	low[u] = dfn[u] = ++timer;
	for (int v: g[u]) {
		auto &e = edges[v];
		if (e.second == p) continue;
		if (dfn[e.second] == 0) {
			dfs(e.second, u);
			low[u] = min(low[u], low[e.second]);
			if (low[e.second] > dfn[u]) {
				isBridge[v]  = isBridge[v ^ 1] = true;
			}
		} else {
			low[u] = min(low[u], dfn[e.second]);
		}
	}
}

void print(int u) {
	vis[u] = true;
	for (int v: g[u]) {
		auto &e = edges[v];
		if (!isBridge[v]) {
			if (!hasPrint[v]) {
				cout << u << ' ' << e.second << '\n';
				hasPrint[v] = hasPrint[v ^ 1] = true;
			}
			if (!vis[e.second]) print(e.second);
		}
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int n, m, cases = 0;
	while (cin >> n >> m && n) {
		edges.clear(); g.clear(); g.resize(n + 1);
		memset(dfn, 0, sizeof(dfn)); memset(low, 0, sizeof(low));
		memset(isBridge, false, sizeof(isBridge)); memset(vis, false, sizeof(vis));
		memset(hasPrint, false, sizeof(hasPrint));
		timer = 0;
		for (int i = 0, u, v; i < m; i++) {
			cin >> u >> v;
			edges.push_back(make_pair(u, v));
			g[u].push_back(edges.size() - 1);
			edges.push_back(make_pair(v, u));
			g[v].push_back(edges.size() - 1);
		}
		dfs(1, 1);
		cout << ++cases << "\n\n";
		for (int i = 1; i <= n; i++) {
			if (!vis[i]) print(i);
		}
		for (unsigned int i = 0; i < edges.size(); i += 2) {
			if (isBridge[i]) {
				cout << edges[i].first << ' ' << edges[i].second << '\n' <<
					edges[i].second << ' ' << edges[i].first << '\n';
			}
		}
		cout << "#\n";
	}
}
