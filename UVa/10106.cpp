#include <iostream>
#include <cstring>
#include <iomanip>
using std::cin; using std::cout; using std::setw; using std::setfill;

struct BigInt {
  static const int CELLSIZE = 4, CELLSIZE10 = 10000;
  int arr[130] = { 0 }, max = 0;
  static BigInt toInt(char *strOrg) {
    char str[300];
    strcpy(str, strOrg);
    int len = strlen(str), max = len / CELLSIZE;
    BigInt b;
    for (int i = 0; i < max; i++) {
      b.arr[i] = atoi(str + len - CELLSIZE);
      str[len - CELLSIZE] = '\0';
      len -= CELLSIZE;
    }
    b.arr[max] = atoi(str);
    if (b.arr[max] == 0) max--;
    b.max = max;
    return b;
  }
  BigInt mult(const BigInt &rhs) {
    BigInt b;
    if (max == -1 || rhs.max == -1) {
      b.max = 0;
      b.arr[0] = 0;
      return b;
    }
    for (int i = 0; i <= max; i++) {
      int carry = 0;
      for (int j = 0; j <= rhs.max; j++) {
        b.arr[i + j] += arr[i] * rhs.arr[j] + carry;
        carry = b.arr[i + j] / CELLSIZE10;
        b.arr[i + j] %= CELLSIZE10;
      }
      b.arr[i + rhs.max + 1] = carry;
    }
    b.max = max + rhs.max;
    if (b.arr[b.max + 1]) b.max++;
    return b;
  }
  void print() {
    cout << arr[max];
    for (int i = max - 1; i >= 0; i--) {
      cout << setw(CELLSIZE) << setfill('0') << arr[i];
    }
    cout << '\n';
  }
};

int main() {
  char n1[300], n2[300];
  while (cin.getline(n1, 300) && cin.getline(n2, 300)) {
    BigInt b1 = BigInt::toInt(n1), b2 = BigInt::toInt(n2);
    BigInt res = b1.mult(b2);
    res.print();
  }
}
