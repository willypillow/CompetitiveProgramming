#include <iostream>
#include <cstring>
#include <vector>
#include <queue>
using std::cin; using std::cout; using std::priority_queue; using std::greater; using std::vector; using std::move;

struct Node {
  int at, dist;
  bool operator >(const Node &rhs) const {
    return dist > rhs.dist;
  }
};
struct Patch {
  int time, depWild, dep, fixWild, fix;
  bool canPatchTo(int stat) {
    return ((stat ^ dep) & ~depWild) == 0;
  }
  int patchTo(int stat) {
    return ((stat & fixWild) | fix);
  }
} patches[105];
int dist[1 << 20];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int cases = 0, n, m;
  while (cin >> n >> m && (n || m)) {
    memset(dist, 0x6f, sizeof(int) * (1 << n));
    for (int i = 0; i < m; i++) {
      char dep[25], fix[25];
      cin >> patches[i].time >> dep >> fix;
      patches[i].depWild = patches[i].dep =
        patches[i].fixWild = patches[i].fix = 0;
      for (int j = 0; j < n; j++) {
        if (dep[j] == '0') patches[i].depWild |= 1 << j;
        else if (dep[j] == '+') patches[i].dep |= 1 << j;
        if (fix[j] == '0') patches[i].fixWild |= 1 << j;
        else if (fix[j] == '+') patches[i].fix |= 1 << j;
      }
    }
    vector<Node> cont; cont.reserve(2000);
    priority_queue<Node, vector<Node>, greater<Node> > q(greater<Node>(), move(cont));
    q.push(Node { (1 << n) - 1, 0 });
    dist[(1 << n) - 1] = 0;
    while (!q.empty()) {
      Node cur = q.top(); q.pop();
      if (cur.dist != dist[cur.at]) continue;
      for (int i = 0; i < m; i++) {
        if (patches[i].canPatchTo(cur.at)) {
          int res = patches[i].patchTo(cur.at);
          if (dist[res] > dist[cur.at] + patches[i].time) {
            dist[res] = dist[cur.at] + patches[i].time;
            q.push(Node { res, dist[res] });
          }
        }
      }
    }
    cout << "Product " << ++cases << '\n';
    if (dist[0] != 0x6f6f6f6f) {
      cout << "Fastest sequence takes " << dist[0] << " seconds.\n\n";
    } else {
      cout << "Bugs cannot be fixed.\n\n";
    }
  }
}
