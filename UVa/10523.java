import java.util.Scanner;
import java.math.BigInteger;

class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
		Scanner cin = new Scanner(System.in);
		while (cin.hasNext()) {
			int n = cin.nextInt();
			int a = cin.nextInt();
			BigInteger sum = BigInteger.valueOf(0);
			for (int i = 1; i <= n; i++) {
				sum = sum.add(BigInteger.valueOf(a).pow(i).multiply(BigInteger.valueOf(i)));
			}
			System.out.println(sum);
		}
	}
}
