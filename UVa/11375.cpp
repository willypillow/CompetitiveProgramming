#include <iostream>
#include <cstring>
#include <iomanip>

const unsigned int SIZE = 30, LEN = 16;
const unsigned long long BASE = 10000000000000000LL;

struct BigInt {
  unsigned long long data[SIZE] = { 0 }; // Int overflows!
  BigInt add(BigInt y) {
    unsigned int carry = 0;
    for (int j = 0; j < SIZE; j++) {
      data[j] += y.data[j] + carry;
      carry = data[j] / BASE;
      data[j] %= BASE;
    }
  }
  void prnt() {
    int i;
    for (i = SIZE - 1; i >= 0 && data[i] == 0; i--);
    if (i == -1) std::cout << "0\n";
    else {
        std::cout << data[i];
        while (i-- > 0) {
          std::cout << std::setfill('0') << std::setw(LEN) << data[i];
        }
        std::cout << '\n';
    }
  }
};

BigInt dp[2500];
const int matches[10] = {6, 2, 5, 5, 4, 5, 6, 3, 7, 6};

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  dp[0].data[0] = 1; // dp[0] = 1
  for (int i = 0; i < 2001; i++) {
    for (int j = 0; j < 10; j++) {
      if(!i && !j) continue; // Skip prefix '0'
      dp[i + matches[j]].add(dp[i]);
    }
  }
  dp[6].add(dp[0]); // Increment 1, adding back the sequence "0"
  for (int i = 2; i < 2001; i++) dp[i].add(dp[i - 1]);
  int n;
  while (std::cin >> n) dp[n].prnt();
}
