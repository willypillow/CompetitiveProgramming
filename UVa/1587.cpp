#include <iostream>
#include <algorithm>

struct Pair {
  int a, b;
  bool operator<(const Pair& rhs) {
    return (a == rhs.a) ? (b < rhs.b) : (a < rhs.a);
  }
  bool operator!=(const Pair& rhs) {
    return (a != rhs.a || b != rhs.b);
  }
  friend std::istream &operator>>(std::istream& is, Pair& rhs) {
    int tmp1, tmp2;
    is >> tmp1 >> tmp2;
    if (tmp1 > tmp2) { rhs.a = tmp1; rhs.b = tmp2; }
    else { rhs.a = tmp2; rhs.b = tmp1; }
    return is;
  }
};

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  Pair arr[6];
  while (std::cin >> arr[0]) {
    for (int i = 1; i < 6; i++) std::cin >> arr[i];
    std::sort(arr, arr + 6);
    bool success = true;
    for (int i = 0; i < 6; i += 2) {
      if (arr[i] != arr[i + 1]) {
        success = false;
        break;
      }
    }
    success &= arr[0].a == arr[4].b &&
      arr[0].b == arr[2].b && arr[2].a == arr[4].a;
    if (success) puts("POSSIBLE");
    else puts("IMPOSSIBLE");
  }
}
