#include <bits/stdc++.h>
using namespace std;

vector<vector<int> > g;
vector<int> vis;

void dfs(int x, int col, int &a1, int &a2, int &fail) {
	if (col == 1) a1++;
	else a2++;
	vis[x] = col;
	for (int y: g[x]) {
		if (!vis[y]) dfs(y, 3 - col, a1, a2, fail);
		if (vis[y] == col) fail = true;
	}
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	int t; cin >> t;
	while (t--) {
		int n; cin >> n;
		g.clear(); g.resize(n + 1); vis.clear(); vis.resize(n + 1);
		for (int i = 1, j, k; i <= n; i++) {
			cin >> k;
			while (k--) {
				cin >> j;
				if (j > n) continue;
				g[i].push_back(j); g[j].push_back(i);
			}
		}
		int ans = 0;
		for (int i = 1; i <= n; i++) {
			if (!vis[i]) {
				int a1 = 0, a2 = 0, fail = 0;
				dfs(i, 1, a1, a2, fail);
				if (!fail) ans += max(a1, a2);
			}
		}
		cout << ans << '\n';
	}
}

