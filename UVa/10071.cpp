#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int v, t;
  while (std::cin >> v >> t) {
    std::cout << 2 * v * t << "\n";
  }
}

