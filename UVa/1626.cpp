#include <iostream>
#include <cstring>
using std::cin; using std::cout; using std::min;

const int MAX = 1 << 28;
char str[101];
int dp[102][102];

bool match(char x, char y) {
  return (x == '(' && y == ')') || (x == '[' && y == ']');
}

void print(int i, int j) {
  if (i == j) {
    if (str[i] == '(' || str[i] == ')') cout << "()";
    else cout << "[]";
  } else if (i < j) {
    if (match(str[i], str[j]) && dp[i][j] == dp[i + 1][j - 1]) {
      cout << str[i];
      print(i + 1, j - 1);
      cout << str[j];
    } else {
      for (int k = i; k < j; k++) {
        if (dp[i][j] == dp[i][k] + dp[k + 1][j]) {
          print(i, k); print(k + 1, j);
          break;
        }
      }
    }
  }
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int t; cin >> t >> std::ws;
  while (cin.getline(str, 101)) {
    int len = strlen(str);
    for (int i = 0; i <= len; i++) dp[i + 1][i] = 0, dp[i][i] = 1;
    for (int i = len - 2; i >= 0; i--) {
      for (int j = i + 1; j < len; j++) {
        dp[i][j] = MAX;
        if (match(str[i], str[j])) dp[i][j] = dp[i + 1][j - 1];
        for (int k = i; k < j; k++) {
          dp[i][j] = min(dp[i][j], dp[i][k] + dp[k + 1][j]);
        }
      }
    }
    print(0, len - 1);
    cout << '\n';
  }
}
