#include <iostream>
#include <algorithm>
#include <cstring>

struct Rook { int l[2], r[2], res[2]; } orig[5010], *rooks[5010];
int n;

bool place(int type) {
  bool vis[5010] = { false };
  std::sort(rooks, rooks + n, [&](const Rook *l, const Rook *r) {
    return (l->r[type] != r->r[type]) ?
      (l->r[type] < r->r[type]) : (l->l[type] < r->l[type]); });
  for (int i = 1; i <= n; i++) {
    Rook search = { {}, { i, i }, {} };
    int p = std::lower_bound(rooks, rooks + n, &search,
      [&](const Rook *l, const Rook *r) {
        return l->r[type] < r->r[type]; }) - rooks;
    while (p < n && (vis[p] || i < rooks[p]->l[type])) p++;
    if (p == n) return false;
    vis[p] = true;
    rooks[p]->res[type] = i;
  }
  return true;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  while (std::cin >> n && n) {
    for (int i = 0; i < n; i++) {
      rooks[i] = &orig[i];
      std::cin >> rooks[i]->l[0] >> rooks[i]->l[1] >>
        rooks[i]->r[0] >> rooks[i]->r[1];
    }
    bool suc = place(0) && place(1);
    if (suc) {
      for (int i = 0; i < n; i++) {
        std::cout << orig[i].res[0] << ' ' << orig[i].res[1] << '\n';
      }
    } else {
      std::cout << "IMPOSSIBLE\n";
    }
  }
}
