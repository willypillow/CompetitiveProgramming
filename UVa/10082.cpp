#include <iostream>

const char mapping[] = "_______________________________________;____M0,.9`12345678_L_-___\\VXSWDFGUHJKNBIO=EARYCQZT'P][__I_______________________________________________________________________________________________________________________________________________________________";

int getChr() {
  static char buf[1 << 20], *p = buf, *end = buf;
  if (p == end) {
    if ((end = buf + fread(buf, 1, 1 << 20, stdin)) == buf) return -1;
    p = buf;
  }
  return *p++;
}

int main() {
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  char c;
  while ((c = getChr()) != -1) {
    if (c == ' ' || c == '\n') std::cout << c;
    else std::cout << mapping[c];
  }
}
