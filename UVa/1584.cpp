#include <iostream>
#include <cstring>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  char str[200];
  int n;
  std::cin >> n;
  while (n--) {
    std::cin >> str;
    int min = 0, len = strlen(str);
    for (int i = 1; i < len; i++) {
      for (int j = 0; j < len; j++) {
        char chr1 = str[(min + j) % len], chr2 = str[(i + j) % len];
        if (chr1 != chr2) {
          if (str[(min + j) % len] > str[(i + j) % len]) min = i;
          break;
        }
      }
    }
    for (int i = 0; i < len; i++) {
      std::cout << str[(min + i) % len];
    }
    std::cout << '\n';
  }
}
