#include <bits/stdc++.h>
using namespace std;

int pa[30001], cnt[30001];

int find(int x) {
	return (pa[x] == x) ? x : (pa[x] = find(pa[x]));
}

void merge(int a, int b) {
	int ap = find(a), bp = find(b);
	if (ap == bp) return;
	pa[ap] = bp;
	cnt[bp] += cnt[ap];
	cnt[ap] = 0;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int t, n, m, x, y;
	cin >> t;
	while (t--) {
		cin >> n >> m;
		for (int i = 1; i <= n; i++) {
			pa[i] = i;
			cnt[i] = 1;
		}
		while (m--) {
			cin >> x >> y;
			merge(x, y);
		}
		int ans = 0;
		for (int i = 1; i <= n; i++) ans = max(ans, cnt[i]);
		cout << ans << '\n';
	}
}
