#include <iostream>
#include <iomanip>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  std::cout << std::fixed << std::setprecision(3);
  int c;
  std::cin >> c;
  while (c--) {
    int n, arr[1000], count = 0;
    float avg = 0;
    std::cin >> n;
    for (int i = 0; i < n; i++) {
      std::cin >> arr[i];
      avg += arr[i];
    }
    avg /= n;
    for (int i = 0; i < n; i++) {
      if (arr[i] > avg) count++;
    }
    std::cout << (float)count / n * 100 << "%\n";
  }
}
