#include <iostream>

int main() {
  int t;
  std::cin >> t;
  for (int cases = 1; cases <= t; cases++) {
    int n, prev, tmp, high = 0, low = 0;
    std::cin >> n >> prev;
    while (--n) {
      std::cin >> tmp;
      if (tmp > prev) high++;
      if (tmp < prev) low++;
      prev = tmp;
    }
    std::cout << "Case " << cases << ": " << high << ' ' << low << '\n';
  }
}

