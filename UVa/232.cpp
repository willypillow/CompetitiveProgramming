#include <iostream>
#include <iomanip>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int r, c, count = 1;
  char arr[15][15] = { "************", 0 };
  while (std::cin >> r && r) {
    if (count != 1) std::cout << '\n';
    std::cout << "puzzle #" << count++ << ":\n";
    std::cin >> c;
    std::cin.get(); // Skip '\n'
    for (int i = 1; i <= r; i++) {
      arr[i][0] = '*';
      std::cin.getline(arr[i] + 1, c + 1);
      arr[i][c + 1] = '*';
    }
    for (int i = 0; i <= c + 1; i++) arr[r + 1][i] = '*';
    int number = 1, acrossWords[50], acrossTop = 0, downWords[50], downTop = 0;
    char *num2Pos[100];
    for (int i = 1; i <= r ; i++) {
      for (int j = 1; j <= c ; j++) {
        if (arr[i][j] == '*') continue;
        if (arr[i - 1][j] == '*' || arr[i][j - 1] == '*') {
          if (arr[i][j - 1] == '*') acrossWords[acrossTop++] = number;
          if (arr[i - 1][j] == '*') downWords[downTop++] = number;
          num2Pos[number++] = &arr[i][j];
        }
      }
    }
    std::cout << "Across\n";
    for (int i = 0; i < acrossTop; i++) {
      int number = acrossWords[i];
      char *ptr = num2Pos[number];
      std::cout << std::setw(3) << number;
      std::cout << '.';
      do {
        std::cout << *ptr++;
      } while (*ptr != '*');
      std::cout << '\n';
    }
    std::cout << "Down\n";
    for (int i = 0; i < downTop; i++) {
      int number = downWords[i];
      char *ptr = num2Pos[number];
      std::cout << std::setw(3) << number;
      std::cout << '.';
      do {
        std::cout << *ptr;
        ptr += sizeof(arr[0]); // To next line
      } while (*ptr != '*');
      std::cout << '\n';
    }
  }
}
