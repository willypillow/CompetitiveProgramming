#include <iostream>

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif 
  int n, cases = 0;
  while (std::cin >> n) {
    long long ans = 0, nums[18];
    for (int i = 0; i < n; i++) std::cin >> nums[i];
    for (int i = 0; i < n; i++) {
      for (int j = i; j < n; j++) {
        long long tmp = 1;
        for (int k = i; k <= j; k++) tmp *= nums[k];
        ans = (tmp > ans) ? tmp : ans;
      }
    }
    std::cout << "Case #" << ++cases <<
      ": The maximum product is " << ans << ".\n\n";
  }
}
