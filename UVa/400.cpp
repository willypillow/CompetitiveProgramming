#include <iostream>
#include <string>
#include <algorithm>
#include <iomanip>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  while (std::cin >> n) {
    int maxLen = 0;
    std::string strs[100];
    for (int i = 0; i < n; i++) {
      std::cin >> strs[i];
      int len = strs[i].length();
      if (len > maxLen) maxLen = len;
    }
    std::sort(strs, strs + n);
    int col = 62 / (maxLen + 2), row = (n % col) ? n / col + 1 : n / col ;
    std::cout << "------------------------------------------------------------\n";
    for (int i = 0; i < row; i++) {
      for (int j = 0; j < col - 1; j++) {
        std::cout << strs[i + row * j];
        int spaces = maxLen + 2 - strs[i + row * j].length();
        for (int k = 0; k < spaces; k++) std::cout << ' ';
      }
      if (i + row * (col - 1) < n) {
        std::cout << strs[i + row * (col - 1)] << '\n';
      } else {
        std::cout << '\n';
      }
    }
  }
}
