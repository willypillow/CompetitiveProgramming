#include <iostream>
#include <cstring>

int arr[401];

bool solve(int pos) {
  int node;
  std::cin >> node;
  if (node == -1) return false;
  arr[pos] += node;
  solve(pos - 1);
  solve(pos + 1);
  return true;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int cases = 0;
  while (solve(200)) {
    std::cout << "Case " << ++cases << ":\n";
    int i = 0;
    while (arr[i] == 0) i++;
    while (arr[i + 1] != 0) std::cout << arr[i++] << ' ';
    std::cout << arr[i] << "\n\n";
    memset(arr, 0, sizeof(arr));
  }
}
