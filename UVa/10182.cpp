#include <bits/stdc++.h>
using namespace std;

const int DX[] = { 0, -1, -1, 0, 1, 1 }, DY[] = { -1, 0, 1, 1, 0, -1 };

int main() {
	int x;
	while (cin >> x) {
		int i, d = 0;
		for (i = 1; i < x; d++, i += d * 6);
		int nx = d, ny = 0, cnt = 0;
		while (i > x) {
			nx += DX[cnt / d], ny += DY[cnt / d];
			cnt++;
			i--;
		}
		cout << nx << ' ' << ny << '\n';
	}
}
