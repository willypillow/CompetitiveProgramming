#include <bits/stdc++.h>
using namespace std;

vector<vector<int> > g1, g2;
vector<int> fin;
bool vis[1001];
unordered_map<string, int> dict;

int toId(string str) {
	if (dict.count(str)) return dict[str];
	int nId = dict.size();
	return dict[str] = nId;
}

void dfs(int x) {
	vis[x] = true;
	for (int y: g1[x]) {
		if (!vis[y]) dfs(y);
	}
	fin.push_back(x);
}

void dfsScc(int x, int curCol) {
	vis[x] = true;
	for (int y: g2[x]) {
		if (!vis[y]) {
			dfsScc(y, curCol);
		}
	}
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	for (int p, k; cin >> p >> k >> ws && p; ) {
		dict.clear(); fin.clear();
		g1.clear(); g2.clear(); g1.resize(p + 1); g2.resize(p + 1);
		string str;
		for (int i = 0; i < p; i++) {
			getline(cin, str);
			toId(str);
		}
		for (int i = 0; i < k; i++) {
			getline(cin, str);
			int u = toId(str);
			getline(cin, str);
			int v = toId(str);
			g1[u].push_back(v); g2[v].push_back(u);
		}
		memset(vis, false, sizeof(vis));
		for (int i = 0; i < p; i++) {
			if (!vis[i]) dfs(i);
		}
		int col = 0;
		memset(vis, false, sizeof(vis));
		for (int i = fin.size() - 1; i >= 0; i--) {
			if (!vis[fin[i]]) dfsScc(fin[i], col++);
		}
		cout << col << '\n';
	}
}
