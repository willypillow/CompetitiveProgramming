#include <iostream>
#include <cstring>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  char nums[128][128] = { 0 };
  int n = 0, ans[128] = { 0 };
  while (std::cin >> nums[n] && nums[n][0] != '0') n++;
  for (int i = 0; i < n; i++) {
    int len = strlen(nums[i]), carry = 0, k = 0;
    for (int j = len - 1; j >= 0; j--, k++) {
      ans[k] += nums[i][j] - '0';
      if (carry) {
        ans[k] += carry;
        carry = 0;
      }
      if (ans[k] >= 10) {
        carry = ans[k] / 10;
        ans[k] %= 10;
      }
    }
    while (carry) {
      ans[k] += carry;
      carry = 0;
      if (ans[k] >= 10) {
        carry = ans[k] / 10;
        ans[k] %= 10;
      }
      k++;
    }
  }
  int pos = 127;
  while (ans[pos] == 0 && pos >= 0) pos--;
  for (int i = pos; i >= 0; i--) std::cout << ans[i];
  std::cout << '\n';
}
