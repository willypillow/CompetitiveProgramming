#include <iostream>
#include <cstring>
#include <algorithm>
using std::cout; using std::cin;

int arr[101][101], nxt[101][101];
long long dp[101][101];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int m, n;
  while (std::cin >> m >> n && m) {
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) cin >> arr[i][j];
    }
    int firstRow = -1;
    long long cost = 1ULL << 40;
    for (int j = n - 1; j >= 0; j--) {
      for (int i = 0; i < m; i++) {
        if (j == n - 1) {
          dp[i][j] = arr[i][j];
        } else {
          int rows[3];
          if (i == 0) rows[0] = 0, rows[1] = 1, rows[2] = m - 1;
          else if (i == m - 1) rows[0] = 0, rows[1] = m - 2, rows[2] = m - 1;
          else rows[0] = i - 1, rows[1] = i, rows[2] = i + 1;
          dp[i][j] = 1ULL << 40;
          for (int k = 0; k < 3 && rows[k] < m; k++) {
            long long x = dp[rows[k]][j + 1] + arr[i][j];
            if (x < dp[i][j]) dp[i][j] = x, nxt[i][j] = rows[k];
          }
        }
        if (j == 0 && dp[i][j] < cost) cost = dp[i][j], firstRow = i;
      }
    }
    cout << firstRow + 1;
    for (int i = 1, r = nxt[firstRow][0]; i < n; r = nxt[r][i], i++) {
      cout << ' ' << r + 1;
    }
    cout << '\n' << cost << '\n';
  }
}
