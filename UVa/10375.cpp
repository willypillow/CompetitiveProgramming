#include <iostream>
#include <cstring>
#include <cmath>
#include <iomanip>
using std::cin; using std::cout; using std::setprecision;

const int MAXN = 10000;
bool notPrime[10001] = { false };
int primes[1230], pTop = 0, cnt[1230];

void initPrime() {
  for (int i = 2; i <= MAXN; i++) {
    if (notPrime[i]) continue;
    for (int j = i + i; j <= MAXN; j += i) notPrime[j] = true;
  }
  for (int i = 2; i <= MAXN; i++) {
    if (!notPrime[i]) primes[pTop++] = i;
  }
}

void add(int n, int d) {
  for (int i = 1; i <= n; i++) {
    for (int t = i, j = 0; t != 1 && j < pTop; j++) {
      while (t % primes[j] == 0) {
        t /= primes[j];
        cnt[j] += d;
      }
    }
  }
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  initPrime();
  int p, q, r, s;
  while (cin >> p >> q >> r >> s) {
    memset(cnt, 0, sizeof(cnt));
    add(p, 1); add(p - q, -1); add(q, -1);
    add(r, -1); add(r - s, 1); add(s, 1);
    double ans = 1;
    for (int i = 0; i < pTop; i++) ans *= pow(primes[i], cnt[i]);
    cout << std::fixed << setprecision(5) << ans << '\n';
  }
}
