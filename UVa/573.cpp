#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int h, u, d, f;
  while (std::cin >> h >> u >> d >> f && h) {
    double newU = u, minus = u * (f / 100.0), curH = 0;
    int day;
    for (day = 1; curH >= 0; day++) {
      if (newU > 0) {
        curH += newU;
        newU -= minus;
      }
      if (curH > h) break;
      curH -= d;
    }
    if (curH < 0) std::cout << "failure on day " << day - 1 << '\n';
    else std::cout << "success on day " << day << '\n';
  }
}
