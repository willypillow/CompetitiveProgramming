#include <iostream>

static inline long long c(int n, int k) {
  if (k == 0) return 1;
  return c(n, k - 1) * (n - k + 1) / k;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif 
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  int n, k;
  while (std::cin >> n >> k) {
    long long ans = 1;
    int left = n;
    for (int i = 0; i < k; i++) {
      int z;
      std::cin >> z;
      if (left - z < z) ans *= c(left, left - z);
      else ans *= c(left, z);
      left -= z;
    }
    std::cout << ans << '\n';
  }
}
