#include <iostream>

char field[100][101];
int m, n;

void dfs(int x, int y) {
  if (x < 0 || x >= m || y < 0 || y >= n || field[x][y] != '@') return;
  field[x][y] = '*';
  for (int i = -1; i <= 1; i++) {
    for (int j = -1; j <= 1; j++) dfs(x + i, y + j);
  }
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  while (std::cin >> m >> n && m) {
    int ans = 0;
    for (int i = 0; i < m; i++) std::cin >> field[i];
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        if (field[i][j] == '@') {
          ans++;
          dfs(i, j);
        }
      }
    }
    std::cout << ans << '\n';
  }
}
