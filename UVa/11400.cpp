#include <iostream>
#include <algorithm>
using std::cin; using std::cout; using std::sort; using std::min;

struct Cat {
  int v, k, c, l;
  bool operator <(const Cat &rhs) const {
    return v < rhs.v;
  }
} cat[1000];
int dp[1000];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  for (int n; cin >> n && n; ) {
    for (int i = 0; i < n; i++) {
      cin >> cat[i].v >> cat[i].k >> cat[i].c >> cat[i].l;
    }
    sort(cat, cat + n);
    dp[0] = 0;
    for (int i = 1; i <= n; i++) {
      int cnt = 0;
      dp[i] = 1 << 30;
      for (int j = i - 1; j >= 0; j--) {
        cnt += cat[j].l;
        dp[i] = min(dp[i], dp[j] + (cnt * cat[i - 1].c) + cat[i - 1].k);
      }
    }
    cout << dp[n] << '\n';
  }
}
