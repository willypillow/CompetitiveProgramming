import java.util.Scanner;
import java.lang.Math;

class Main
{
  public static void main (String[] args) throws java.lang.Exception
  {
    Scanner cin = new Scanner(System.in).useDelimiter("\\s+|e");
    double arrA[][] = new double[11][31];
    int arrB[][] = new int[11][31];
    for (int m = 1; m <= 10; m++) {
      for (int e = 0; e <= 30; e++) {
        double logVal = Math.log10(1 - Math.pow(2, -m)) + Math.log10(2) * (Math.pow(2, e) - 1);
        int b = (int)logVal;
        arrA[m][e] = Math.pow(10, logVal - b); arrB[m][e] = b;
      }
    }
    while (cin.hasNext()) {
      double a = cin.nextDouble(), b = cin.nextInt(), min = 1000;
      if (a == 0 && b == 0) break;
      int minM = 0, minE = 1;
      for (int m = 1; m <= 10; m++) {
        for (int e = 0; e <= 30; e++) {
          if (b == arrB[m][e]) {
            double diff = Math.abs(a - arrA[m][e]);
            if (diff < min) {
              min = diff;
              minM = m; minE = e;
            }
          }
        }
      }
      System.out.println((minM - 1) + " " + minE);
    }
  }
}
