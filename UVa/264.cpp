#include <iostream>
using namespace std;

int main()  {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n;
	while (cin >> n) {
		int x, acc = 0;
		for (x = 1; acc < n; x++) (acc += x);
		acc -= --x;
		int a = n - acc, b = x + 1 - a;
		if (x & 1) swap(a, b);
		cout << "TERM " << n << " IS " << a << '/' << b << '\n';
	}
}
