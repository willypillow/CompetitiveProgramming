#include <iostream>
#include <cstring>

char res[100];
int n, l, count;

bool dfs(int cur) {
  if (count++ == n) {
    for (int i = 0; i < cur; i++) {
      if (i && i % 4 == 0) {
        if (i != 64) std::cout << ' ';
        else std::cout << '\n';
      }
      std::cout << res[i];
    }
    std::cout << '\n' << cur << '\n';
    return true;
  }
  for (int i = 0; i < l; i++) {
    res[cur] = 'A' + i;
    for (int j = 1; j * 2 <= cur + 1; j++) {
      bool same = true;
      for (int k = 0; k < j; k++) {
        if (res[cur - k] != res[cur - k - j]) { same = false; break; }
      }
      if (same) goto NextChar;
    }
    if (dfs(cur + 1)) return true;
NextChar:;
  }
  return false;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif 
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  while (std::cin >> n >> l && n) {
    count = 0;
    dfs(0);
  }
}
