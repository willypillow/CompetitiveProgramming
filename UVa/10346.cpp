#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n, k;
  while (std::cin >> n >> k) {
    int ans = n;
    while (n >= k) {
      ans += n / k;
      n = n / k + n % k;
    }
    std::cout << ans << '\n';
  }
}
