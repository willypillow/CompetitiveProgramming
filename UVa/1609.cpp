#include <iostream>
#include <cstring>
#include <forward_list>

// WEAK -> can be beaten by 1, STRONG -> can beat 1
enum Type { WEAK = 0, STRONG = 1, BEAT_STRONG = 2 };

bool matrix[1030][1030];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int n;
  std::forward_list<int> s[3];
  while (std::cin >> n) {
    s[WEAK].clear(); s[STRONG].clear(); s[BEAT_STRONG].clear();
    Type type[1024] = { WEAK };
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        char c;
        std::cin >> c;
        matrix[i][j] = (c == '1');
      }
    }
    for (int i = 1; i < n; i++) { // Skip player 1
      if (matrix[0][i]) {
        type[i] = (type[i] == BEAT_STRONG) ? BEAT_STRONG : WEAK;
      } else {
        type[i] = STRONG;
        for (int j = 1; j < n; j++) {
          if (matrix[j][i] && type[j] != STRONG) type[j] = BEAT_STRONG;
        }
      }
    }
    for (int i = 1; i < n; i++) s[type[i]].push_front(i);
    for (int iter = n; iter != 1; iter >>= 1) {
      bool use[1030] = { false };
      for (auto i = s[STRONG].begin(); i != s[STRONG].end();) {
        int &cur = *i++, opponent = -1;
        for (int beater: s[BEAT_STRONG]) {
          if (matrix[beater][cur] && !use[beater]) {
            use[cur] = use[beater] = true;
            opponent = beater;
            break;
          }
        }
        if (opponent != -1) {
          s[STRONG].remove(cur);
          std::cout << cur + 1 << ' ' << opponent + 1 << '\n';
        }
      }
      if (!s[WEAK].empty()) { // Try to match 0 to a WEAK
        int lose = *s[WEAK].begin();
        use[0] = use[lose] = true;
        s[WEAK].erase_after(s[WEAK].before_begin());
        std::cout << "1 " << lose + 1 << '\n';
      } else { // Match 0 to a BEAT_STRONG
        int opponent = -1;
        for (int match: s[BEAT_STRONG]) {
          if (!use[match]) {
            use[0] = use[match] = true;
            opponent = match;
            break;
          }
        }
        s[BEAT_STRONG].remove(opponent);
        std::cout << "1 " << opponent + 1 << '\n';
      }
      // Match STRONG to each other
      for (auto i = s[STRONG].begin(); i != s[STRONG].end();) {
        int a = *i++;
        if (i == s[STRONG].end()) break;
        int b = *i++;
        use[a] = use[b] = true;
        s[STRONG].remove((matrix[a][b]) ? b : a);
        std::cout << a + 1 << ' ' << b + 1 << '\n';
      }
      for (int i = 0, prev = -1; i < 3; i++) { // Match what's left
        for (auto it = s[i].begin(); it != s[i].end();) {
          int &cur = *it++;
          if (use[cur]) continue;
          if (prev != -1) {
            int lose = matrix[prev][cur] ? cur : prev;
            s[type[lose]].remove(lose);
            std::cout << prev + 1 << ' ' << cur + 1 << '\n';
            prev = -1;
          } else {
            prev = cur;
          }
        }
      }
    }
  }
}
