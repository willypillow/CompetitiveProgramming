#include <iostream>

unsigned long long c(int n, int m) {
  if (m == 0) return 1;
  return (unsigned long long)(n - m + 1) * c(n, m - 1) / m;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  int n, m;
  while (std::cin >> n >> m && n) {
    int newM = (n - m < m) ? (n - m) : m;
    std::cout << n << " things taken " << m << " at a time is " << c(n, newM) << " exactly.\n";
  }
}
