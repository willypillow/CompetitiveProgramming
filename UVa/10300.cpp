#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  std::cin >> n;
  for (int i = 0; i < n; i++) {
    int farmers;
    unsigned long long budget = 0;
    std::cin >> farmers;
    for (int j = 0; j < farmers; j++) {
        int siz, animals, eco;
        std::cin >> siz >> animals >> eco;
        budget += (long long)siz * eco;
    }
    std::cout << budget << '\n';
  }
}

