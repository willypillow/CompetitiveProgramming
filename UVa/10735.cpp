#include <bits/stdc++.h>
using namespace std;

const int INF = 0x5f5f5f5f;
int cnt[110], dist[110], flowS, flowT, id;
bool vis[510];

struct P {
	int to, id;
	char type;
};
struct Edge {
	int from, to, cap;
};
vector<Edge> edges;
vector<vector<P> > g;
vector<vector<int> > gFlow;
vector<int> ans;

void addEdge(int u, int v, int cap) {
	edges.push_back(Edge { u, v, cap });
	gFlow[u].push_back(edges.size() - 1);
	edges.push_back(Edge { v, u, 0 });
	gFlow[v].push_back(edges.size() - 1);
}

int bfs() {
	memset(dist, 0x5f, sizeof(dist));
	dist[flowS] = 0;
	queue<int> q;
	q.push(flowS);
	while (!q.empty()) {
		int cur = q.front(); q.pop();
		for (int i: gFlow[cur]) {
			Edge &e = edges[i];
			if (e.cap > 0 && dist[e.to] == INF) {
				dist[e.to] = dist[cur] + 1;
				if (e.to == flowT) return dist[e.to];
				q.push(e.to);
			}
		}
	}
	return INF;
}

int dfs(int cur, int f) {
	if (cur == flowT) return f;
	vis[cur] = true;
	for (int i: gFlow[cur]) {
		Edge &e = edges[i];
		if (e.cap > 0 && dist[e.to] == dist[cur] + 1 && !vis[e.to]) {
			int newF = dfs(e.to, min(f, e.cap));
			if (newF) {
				e.cap -= newF;
				edges[i ^ 1].cap += newF;
				return newF;
			}
		}
	}
	return 0;
}

int dinic() {
	int flow = 0;
	while (bfs() < INF) {
		for (;;) {
			memset(vis, false, sizeof(vis));
			int f = dfs(flowS, 1 << 30);
			if (!f) break;
			flow += f;
		}
	}
	return flow;
}

void dfsEuler(int cur) {
	for (P &y: g[cur]) {
		if (vis[y.id]) continue;
		vis[y.id] = true;
		dfsEuler(y.to);
		ans.push_back(y.to);
	}
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int t; cin >> t;
	while (t--) {
		int v, e; cin >> v >> e;
		memset(vis, false, sizeof(vis)); memset(cnt, 0, sizeof(cnt));
		ans.clear(); g.clear(); g.resize(v + 1); gFlow.clear(); gFlow.resize(v + 3);
		edges.clear();
		id = 0; flowS = v + 1; flowT = v + 2;
		for (int i = 0, u, v; i < e; i++) {
			char type;
			cin >> u >> v >> type;
			cnt[u]++; cnt[v]--;
			if (type == 'D') g[u].push_back(P { v, ++id });
			else addEdge(u, v, 1);
		}
		bool fail = false;
		int total = 0;
		for (int i = 1; i <= v; i++) {
			if (cnt[i] % 2 == 1) {
				fail = true;
				break;
			}
			if (cnt[i] > 0) {
				addEdge(flowS, i, cnt[i] / 2);
				total += cnt[i] / 2;
			} else {
				addEdge(i, flowT, -cnt[i] / 2);
			}
		}
		if (fail || dinic() != total) {
			cout << "No euler circuit exist\n";
		} else {
			for (unsigned int i = 0; i < edges.size(); i += 2) {
				Edge &e = edges[i];
				if (e.from > v || e.to > v) break;
				if (e.cap > 0) g[e.from].push_back(P { e.to, ++id });
				else g[e.to].push_back(P { e.from, ++id });
			}
			memset(vis, false, sizeof(vis));
			dfsEuler(1);
			cout << ans[0];
			for (int i = ans.size() - 1; i >= 0; i--) cout << ' ' << ans[i];
			cout << '\n';
		}
		if (t) cout << '\n';
	}
}
