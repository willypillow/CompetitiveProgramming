#include <iostream>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int t;
  std::cin >> t;
  while (t--) {
    int n, arr[10] = { 0 };
    std::cin >> n;
    int digit = 1, lastDigit = 0;
    while (n) {
      int mod = n % 10, div = n / 10;
      if (mod == 0) arr[0] += (div - 1) * digit + lastDigit + 1;
      else {
        arr[0] += div * digit;
        arr[mod] += div * digit + lastDigit + 1;
      }
      int lt = (div + 1) * digit;
      int gt = div * digit;
      for (int i = 1; i < mod; i++) arr[i] += lt;
      for (int i = mod + 1; i < 10; i++) arr[i] += gt;
      lastDigit += mod * digit;
      n /= 10;
      digit *= 10;
    }
    for (int i = 0; i < 10; i++) {
      std::cout << arr[i];
      if (i != 9) std::cout << ' ';
    }
    std::cout << '\n';
  }
}
