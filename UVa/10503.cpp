#include <iostream>
#include <vector>
#include <map>
#include <cstring>
#include <set>
using namespace std;

map<int, int> par;

int find(int x) {
	return par[x] == x ? x : par[x] = find(par[x]);
}

int main()  {
	int n;
	while (cin >> n && n) {
		int m, la, lb, ra, rb; cin >> m >> la >> lb >> ra >> rb;
		vector<pair<int, int> > edges;
		for (int i = 0; i < m; i++) {
			int a, b; cin >> a >> b;
			edges.push_back(make_pair(a, b));
		}
		bool ok = false;
		for (int i = 0; i < (1 << m); i++) {
			if (__builtin_popcount(i) != n) continue;
			map<int, int> cnt;
			par.clear();
			for (int j = 0; j < (int)edges.size(); j++) {
				if (i & (1 << j)) {
					int a = edges[j].first, b = edges[j].second;
					cnt[a]++, cnt[b]++;
					if (!par.count(a)) par[a] = a;
					if (!par.count(b)) par[b] = b;
					int pa = find(a), pb = find(b);
					if (pa != pb) par[pa] = pb;
				}
			}
			set<int> pars;
			for (map<int, int>::iterator it = par.begin(); it != par.end(); it++) {
				pars.insert(find(it->first));
			}
			bool fail = !(cnt.count(lb) && cnt.count(ra) && pars.size() == 1 && cnt[lb] % 2 == cnt[ra] % 2);
			int odd = 0;
			for (map<int, int>::iterator it = cnt.begin(); it != cnt.end(); it++) {
				if (it->first != lb && it->first != ra && it->second % 2) fail = true;
				if (it->second % 2) odd++;
			}
			if (lb == ra && cnt[lb] % 2) fail = true;
			if (lb != ra && odd == 0) fail = true;
			if (!fail) {
				ok = true;
				break;
			}
		}
		cout << (ok ? "YES\n" : "NO\n");
	}
}
