#include <bits/stdc++.h>
using namespace std;

vector<int> enemy[10001];
bool isEnemy[10001][10001];
int pa[10001];

int find(int x) {
	return (pa[x] == x) ? x : (pa[x] = find(pa[x]));
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int n, c, x, y;
	while (cin >> n) {
		memset(isEnemy, false, sizeof(isEnemy));
		for (int i = 0; i < n * 2; i++) {
			pa[i] = i;
		}
		while (cin >> c >> x >> y && c) {
			int px0 = find(x), px1 = find(x + n), py0 = find(y), py1 = find(y + n);
			switch (c) {
			case 1:
				if (px0 == py1) cout << "-1\n";
				else {
					pa[px0] = py0;
					pa[px1] = py1;
				}
				break;
			case 2:
				if (px0 == py0) cout << "-1\n";
				else {
					pa[px0] = py1;
					pa[py0] = px1;
				}
				break;
			case 3:
				cout << (px0 == py0) << '\n';
				break;
			case 4:
				cout << (px0 == py1) << '\n';
			}
		}
	}
}
