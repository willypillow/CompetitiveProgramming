#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
using std::cin; using std::cout; using std::sort; using std::vector;

const int MAXN = 100001;
int dp[MAXN], n, t;
vector<int> edges[MAXN];

int dfs(int x) {
  if (dp[x]) return dp[x];
  if (!edges[x].size()) return 1;
  vector<int> tmp(edges[x].size());
  for (unsigned int i = 0; i < edges[x].size(); i++) {
    tmp[i] = dfs(edges[x][i]);
  }
  sort(tmp.begin(), tmp.end());
  int ans = 0, cnt = (edges[x].size() * t - 1) / 100 + 1;//, cnt = ceil(edges[x].size() * t / 100.0);
  for (int i = 0; i < cnt; i++) ans += tmp[i];
  return dp[x] = ans;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  while (cin >> n >> t && n) {
    for (int i = 0; i <= n; i++) { edges[i].clear(); dp[i] = 0; }
    for (int boss, i = 1; i <= n; i++) {
      cin >> boss;
      edges[boss].push_back(i);
    }
    cout << dfs(0) << '\n';
  }
}
