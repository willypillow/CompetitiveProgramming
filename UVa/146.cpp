#include <iostream>
#include <algorithm>
#include <cstring>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  char str[100];
  while (std::cin.getline(str, 100) && str[0] != '#') {
    int last = strlen(str) - 1;
    bool suc = false;
    for (int i = last; i > 0; i--) {
      if (str[i] > str[i - 1]) {
        suc = true;
        int nextPos = i;
        char next = 'z' + 1;
        for (int j = i; j <= last; j++) {
          if (str[j] > str[i - 1] && next > str[j]) next = str[j], nextPos = j;
        }
        char tmp = str[i - 1];
        str[i - 1] = next;
        str[nextPos] = tmp;
        std::sort(str + i, str + last + 1);
        break;
      }
    }
    if (suc) std::cout << str << '\n';
    else std::cout << "No Successor\n";
  }
}

