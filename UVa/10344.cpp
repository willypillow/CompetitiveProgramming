#include <bits/stdc++.h>
using namespace std;

int calc(int x, int y, int op) {
	switch (op) {
	case 0: return x + y;
	case 1: return x - y;
	case 2: return x * y;
	}
	return 0;
}

int main() {
	int a[5];
	while (cin >> a[0] >> a[1] >> a[2] >> a[3] >> a[4] && a[0]) {
		sort(a, a + 5);
		bool ok = false;
		do {
			for (int i = 0; i < 3; i++) {
				int t1 = calc(a[0], a[1], i);
				for (int j = 0; j < 3; j++) {
					int t2 = calc(t1, a[2], j);
					for (int k = 0; k < 3; k++) {
						int t3 = calc(t2, a[3], k);
						for (int m = 0; m < 3; m++) {
							int t4 = calc(t3, a[4], m);
							if (t4 == 23) {
								ok = true;
								goto Finish;
							}
						}
					}
				}
			}
		} while (next_permutation(a, a + 5));
Finish:
		cout << (ok ? "Possible\n" : "Impossible\n");
	}
}

