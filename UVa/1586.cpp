#include <iostream>
#include <iomanip>
#include <cstdio>

static inline double toMass(char c) {
  switch (c) {
  case 'C':
    return 12.01;
  case 'H':
    return 1.008;
  case 'O':
    return 16.00;
  case 'N':
    return 14.01;
  }
}

int getChr() {
  static char buf[1 << 20], *p = buf, *end = buf;
  if (p == end) {
    if ((end = buf + fread(buf, 1, 1 << 20, stdin)) == buf) return -1;
    p = buf;
  }
  return *p++;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int add = 0;
  double sum = 0;
  bool isBlank = true;
  char c, lastE = '\0';
  std::cout << std::fixed << std::setprecision(3);
  while (getChr() != '\n');
  while ((c = getChr()) != -1) {
    if (isupper(c)) {
      isBlank = false;
      if (isupper(lastE)) {
        sum += toMass(lastE) * (add ? add : 1);
        add = 0;
      }
      lastE = c;
    } else if (isdigit(c)) {
      isBlank = false;
      add = add * 10 + (c - '0');
    } else if (!isBlank) {
      if (isupper(lastE)) sum += toMass(lastE) * (add ? add : 1);
      std::cout << sum << '\n';
      sum = 0;
      add = 0;
      lastE = '\0';
      isBlank = true;
    }
  }
}
