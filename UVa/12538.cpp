#include <bits/stdc++.h>
#include <ext/rope>
using namespace std;
using namespace __gnu_cxx;

const int MAXN = 50010;

rope<char> *r[MAXN], pool[MAXN], *ptr = pool;
int timer = 0, d = 0;

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int n;
	while (cin >> n) {
		ptr = pool;
		r[0] = new(ptr++) rope<char>();
		timer = d = 0;
		for (int i = 0; i < n; i++) {
			int cmd; cin >> cmd;
			if (cmd == 1) {
				char s[110];
				int p; cin >> p >> s;
				p -= d;
				timer++;
				r[timer] = new(ptr++) rope<char>(*r[timer - 1]);
				r[timer]->insert(p, s);
			} else if (cmd == 2) {
				int p, c; cin >> p >> c;
				p -= d; c -= d;
				timer++;
				r[timer] = new(ptr++) rope<char>(*r[timer - 1]);
				r[timer]->erase(p - 1, c);
			} else {
				int v, p, c; cin >> v >> p >> c;
				v -= d; p -= d; c -= d;
				for (int i = p - 1, to = p - 1 + c; i < to; i++) {
					if (r[v]->at(i) == 'c') d++;
					cout << r[v]->at(i);
				}
				cout << '\n';
			}
		}
	}
}
