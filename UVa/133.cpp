#include <iostream>
#include <iomanip>

int next(int start, int dist, int s, int n, int *arr) {
  while (dist--) {
    do {
      start = (start + s + n - 1) % n  + 1; // Skip arr[0]
    } while (arr[start] == 0);
  }
  return start;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n, k, m;
  while (std::cin >> n >> k >> m && n) {
    int arr[100], left = n;
    for (int i = 1; i <= n; i++) arr[i] = i;
    int victim1 = n, victim2 = 1; // Imagine n and 1 are the last victims
    while (left) {
      victim1 = next(victim1, k, 1, n, arr);
      victim2 = next(victim2, m, -1, n, arr);
      std::cout << std::setw(3) << victim1;
      arr[victim1] = 0;
      left--;
      if (victim1 != victim2) {
        std::cout << std::setw(3) << victim2;
        arr[victim2] = 0;
        left--;
      }
      if (left) std::cout << ',';
    }
    std::cout << '\n';
  }
}
