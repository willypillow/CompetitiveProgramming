#include <iostream>
#include <algorithm>
#include <set>

static const int MAXN = 10;
static const int DIRS[4][2] = { { -1, 0 }, { 0, 1 }, { 1, 0 }, { 0, -1 } };
struct P {
  int x, y;
  bool operator <(const P &rhs) const {
    return (x != rhs.x) ? (x < rhs.x) : (y < rhs.y);
  }
};
typedef std::set<P> Poly;
std::set<Poly> s[MAXN];
int ans[MAXN][MAXN + 1][MAXN + 1];

static inline Poly normalize(Poly &po) {
  int minX = 1 << 30, minY = 1 << 30;
  Poly newPo;
  for (const P &p: po) minX = std::min(minX, p.x), minY = std::min(minY, p.y);
  for (const P &p: po) newPo.insert(P { p.x - minX, p.y - minY });
  return newPo;
}

static inline Poly rotate(Poly &po) {
  Poly newPo;
  for (const P &p: po) newPo.insert(P { p.y, -p.x });
  return normalize(newPo);
}

static inline Poly flip(Poly &po) {
  Poly newPo;
  for (const P &p: po) newPo.insert(P { p.y, p.x });
  return normalize(newPo);
}

static inline void insert(int n, P &newP, Poly po) {
  if (po.count(newP)) return;
  po.insert(newP);
  po = normalize(po);
  for (int i = 0; i < 4; i++) {
    if (s[n].count(po)) return;
    po = rotate(po);
  }
  if (s[n].count(po)) return;
  po = flip(po);
  for (int i = 0; i < 4; i++) {
    if (s[n].count(po)) return;
    po = rotate(po);
  }
  s[n].insert(po);
}

static inline void init() {
  Poly root;
  root.insert(P { 0, 0 });
  s[0].insert(root);
  for (int i = 1; i < MAXN; i++) {
    for (const Poly &po: s[i - 1]) {
      for (const P &p: po) {
        for (int j = 0; j < 4; j++) {
          P newP = P { p.x + DIRS[j][0], p.y + DIRS[j][1] };
          insert(i, newP, po);
        }
      }
    }
  }
  for (int i = 0; i < MAXN; i++) {
    for (int w = 1; w <= MAXN; w++) {
      for (int h = w; h <= MAXN; h++) {
        int cnt = 0;
        for (const Poly &po: s[i]) {
          int maxX = 0, maxY = 0;
          for (const P &p: po) maxX = std::max(maxX, p.x), maxY = std::max(maxY, p.y);
          if (std::min(maxX, maxY) < w && std::max(maxX, maxY) < h) cnt++;
        }
        ans[i][w][h] = cnt;
      }
    }
  }
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  init();
  int n, w, h;
  while (std::cin >> n >> w >> h) {
    if (w > h) { int tmp = w; w = h; h = tmp; }
    std::cout << ans[n - 1][w][h] << '\n';
  }
}
