#include <iostream>

static inline uint_fast32_t binReverse(uint_fast32_t x) {
  x = (x & 0xFFFF0000) >> 16 | (x & 0x0000FFFF) << 16;
  x = (x & 0xFF00FF00) >> 8 | (x & 0x00FF00FF) << 8;
  x = (x & 0xF0F0F0F0) >> 4 | (x & 0x0F0F0F0F) << 4;
  x = (x & 0xCCCCCCCC) >> 2 | (x & 0x33333333) << 2;
  x = (x & 0xAAAAAAAA) >> 1 | (x & 0x55555555) << 1;
  return x;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int_fast32_t n;
  std::cin >> n;
  while (n--) {
    uint_fast32_t d, i;
    std::cin >> d >> i;
    i = (i << 1) - 1;
    std::cout << (binReverse(i) >> (32 - d)) << '\n';
  }
}
