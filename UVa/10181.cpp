#include <iostream>
#include <cstdio>
#include <queue>
#include <cmath>

const int dirs[][2] = { { -1, 0 }, { 0, -1 }, { 0, 1 }, { 1, 0 } };
const char dirToChar[] = { 'U', 'L', 'R', 'D' };

struct State {
	int grid[4][4], spaceX, spaceY, stepCnt = 0, hVal, lastStep = -1;
	char steps[51];
	bool operator <(const State &rhs) const {
		return stepCnt + hVal > rhs.stepCnt + rhs.hVal;
	}
	void swap(int x0, int y0, int x1, int y1) {
		int tmp = grid[x0][y0];
		grid[x0][y0] = grid[x1][y1];
		grid[x1][y1] = tmp;
	}
};

inline void input(State &init) {
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			std::cin >> init.grid[i][j];
			if (init.grid[i][j] == 0) init.spaceX = i, init.spaceY = j;
		}
	}
}

inline bool isSolvable(const State &init) {
	const int *p = &init.grid[0][0];
	int reverseCnt = 0;
	for (int i = 0; i < 16; i++) {
		if (p[i] == 0) continue;
		for (int j = i + 1; j < 16; j++) {
			if (p[j] < p[i] && p[j] != 0) reverseCnt++;
		}
	}
	return (init.spaceX + reverseCnt) % 2;
}

inline int singleH(const State &s, int x, int y) {
	if (x == s.spaceX && y == s.spaceY) return 0;
	return std::abs(x - (s.grid[x][y] - 1) / 4) +
		std::abs(y - (s.grid[x][y] - 1) % 4);
}

inline int h(const State &s) {
	int manhattanDist = 0;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) manhattanDist += singleH(s, i, j);
	}
	return manhattanDist;
}

inline void print(State &s) {
	char bkup = s.steps[s.stepCnt];
	s.steps[s.stepCnt] = '\0';
	puts(s.steps);
	s.steps[s.stepCnt] = bkup;
}

inline void solvePrint(const State &init) {
	std::priority_queue<State> q;
	q.push(init);
	while (!q.empty()) {
		State cur = q.top(); q.pop();
		if (cur.hVal == 0) {
			print(cur);
			break;
		}
		if (cur.stepCnt >= 50) continue;
		for (int i = 0; i < 4; i++) {
			if (i + cur.lastStep == 3) continue;
			int newX = cur.spaceX + dirs[i][0], newY = cur.spaceY + dirs[i][1];
			if (newX < 0 || newX >= 4 || newY < 0 || newY >= 4) continue;
			State next = cur;
			next.swap(cur.spaceX, cur.spaceY, newX, newY);
			next.spaceX = newX, next.spaceY = newY;
			next.lastStep = i;
			next.steps[next.stepCnt++] = dirToChar[i];
			next.hVal -= singleH(cur, newX, newY);
			next.hVal += singleH(next, cur.spaceX, cur.spaceY);
			q.push(next);
		}
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t;
	std::cin >> t;
	State init;
	while (t--) {
		input(init);
		init.hVal = h(init);
		if (isSolvable(init)) solvePrint(init);
		else puts("This puzzle is not solvable.");
	}
}
