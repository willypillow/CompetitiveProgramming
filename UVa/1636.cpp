#include <iostream>
#include <cstring>
#include <cmath>
using std::cin; using std::cout;

int main() {
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
  char str[110];
  while (cin >> str) {
    int zero = 0, one = 0, zeroone = 0, n = strlen(str);
    for (int i = 0; i < n; i++) {
      if (str[i] == '1') one++;
      else {
        zero++;
        if (str[i + 1] == '1') zeroone++;
      }
    }
		if (str[n - 1] == '0' && str[0] == '1') zeroone++;
    //double rot = (double)one / (zero + one), shoot = (double)zeroone / zero;
		int rot = one * zero, shoot = zeroone * (zero + one);
    if (rot == shoot) puts("EQUAL");
    else if (rot > shoot) puts("SHOOT");
    else puts("ROTATE");
  }
}
