#include <iostream>
#include <cstring>
#include <algorithm>
using std::cin; using std::cout; using std::sort;

struct P {
  int x, y;
  bool operator <(const P &rhs) const {
    return x < rhs.x;
  }
} out[1100000], s[1010];
// For s, x -> height / y -> column, For out, x -> perimeter / y -> count
char field[1010][1010];
int count[2001];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int t; cin >> t;
  while (t--) {
    memset(count, 0, sizeof(count));
    int n, m, height[1010] = { 0 }, sTop = 0;
    cin >> n >> m;
    for (int i = 0; i < n; i++) cin >> field[i];
    for (int i = 0; i < n; i++) {
      sTop = 0;
      for (int j = 0; j < m; j++) {
        if (field[i][j] == '#') {
          height[j] = sTop = 0;
          continue;
        }
        height[j]++;
        int col = j;
        while (sTop > 0 && s[sTop - 1].x > height[j]) {
          col = s[sTop - 1].y;
          sTop--;
        }
        if (sTop == 0 || height[j] - col > s[sTop - 1].x - s[sTop - 1].y) {
          s[sTop++] = P { height[j], col };
        }
        count[(s[sTop - 1].x + (j - s[sTop - 1].y + 1))]++;
      }
    }
    for (int i = 2; i <= n + m; i++) {
      if (count[i]) cout << count[i] << " x " << i * 2 << '\n';
    }
  }
}
