#include <iostream>
#include <cstring>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  char str[100001];
  while (std::cin >> str + 1) {
    int len = strlen(str + 1), cur = 0, last = 0, nxt[100001] = { 0 };
    for (int i = 1; i <= len; i++) {
      if (str[i] == '[') cur = 0;
      else if (str[i] == ']') cur = last;
      else {
        nxt[i] = nxt[cur];
        nxt[cur] = i;
        if (cur == last) last = i;
        cur = i;
      }
    }
    for (int i = nxt[0]; i != 0; i = nxt[i]) std::cout << str[i];
    std::cout << '\n';
  }
}
