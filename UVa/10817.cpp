#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
using std::cin; using std::cout; using std::min; using std::string; using std::stringstream;

const int INF = 1 << 28;
int dp[120][256][256], s, m, n;
struct Teacher { int c, s; } tchr[120];

int dfs(int c, int t0, int t1, int t2) {
  if (c == n + m) return (t2 == (1 << s) - 1) ? 0 : INF;
  int &ans = dp[c][t1][t2];
  if (ans >= 0) return ans;
  ans = (c >= m) ? dfs(c + 1, t0, t1, t2) : INF;
  int nT1 = t0 & tchr[c].s, nT2 = t1 & tchr[c].s;
  ans = min(ans, tchr[c].c + dfs(c + 1, t0 ^ nT1, (t1 ^ nT2) | nT1, t2 | nT2));
  return ans;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  while (cin >> s >> m >> n >> std::ws && s) {
    memset(dp, -1, sizeof(dp));
    for (int t, i = 0; i < m + n; i++) {
      string line;
      getline(cin, line);
      stringstream ss(line);
      ss >> tchr[i].c;
      tchr[i].s = 0;
      while (ss >> t) tchr[i].s |= 1 << (t - 1);
    }
    cout << dfs(0, (1 << s) - 1, 0, 0) << '\n';
  }
}

