#include <iostream>
#include <cstring>
#include <algorithm>

const char toLetter[] = { 'W', 'A', 'K', 'J', 'S', 'D' };

int m, n, canvas[202][202];

void fillWhite(int x, int y) {
  if (x < 0 || x >= m || y < 0 || y >= n || canvas[x][y] != 0) return;
  canvas[x][y] = -1;
  fillWhite(x + 1, y); fillWhite(x, y + 1);
  fillWhite(x - 1, y); fillWhite(x, y - 1);
}

int findHoles(int x, int y) {
  if (x < 0 || x >= m || y < 0 || y >= n) return 0;
  if (canvas[x][y] == 1) {
    canvas[x][y] = -1;
    return findHoles(x + 1, y) + findHoles(x, y + 1) +
      findHoles(x - 1, y) + findHoles(x, y - 1);
  } else if (canvas[x][y] == 0) {
    fillWhite(x, y);
    return 1;
  } else return 0;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int rawM, rawN, cases = 0;
  while (std::cin >> rawM >> rawN && rawM) {
    memset(canvas, 0, sizeof(canvas));
    m = rawM + 2, n = rawN * 4 + 2;
    for (int i = 1; i <= rawM; i++) {
      for (int j = 0; j < rawN; j++) {
        char c;
        std::cin >> c;
        int bin;
        if (isdigit(c)) bin = c - '0';
        else bin = c - 'a' + 10;
        canvas[i][j * 4 + 1] = bin >> 3;
        canvas[i][j * 4 + 2] = (bin >> 2) & 1;
        canvas[i][j * 4 + 3] = (bin >> 1) & 1;
        canvas[i][j * 4 + 4] = bin & 1;
      }
    }
    fillWhite(0, 0);
    char ans[10000], aTop = 0;
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        if (canvas[i][j] == 1) ans[aTop++] = toLetter[findHoles(i, j)];
      }
    }
    std::sort(ans, ans + aTop);
    std::cout << "Case " << ++cases << ": ";
    for (int i = 0; i < aTop; i++) std::cout << ans[i];
    std::cout << '\n';
  }
}
