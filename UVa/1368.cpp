#include <iostream>

const char int2Dna[] = "ACGT";

static inline int dnaToInt(char c) {
  switch (c) {
  case 'A': return 0;
  case 'C': return 1;
  case 'G': return 2;
  case 'T': return 3;
  }
}

static inline int arrMax(int *arr) {
  int maxPos, max = 0;
  for (int i = 0; i < 4; i++) {
    if (arr[i] > max) {
      maxPos = i;
      max = arr[i];
    }
  }
  return maxPos;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int t;
  std::cin >> t;
  while (t--) {
    int m, n, count[1000][4] = { 0 }, distance = 0;
    std::cin >> m >> n;
    for (int i = 0; i < m; i++) {
      std::cin.ignore(1000, '\n');
      for (int j = 0; j < n; j++) {
        char in;
        std::cin.get(in);
        count[j][dnaToInt(in)]++;
      }
    }
    for (int i = 0; i < n; i++) {
      int maxPos = arrMax(count[i]);
      std::cout << int2Dna[maxPos];
      distance += count[i][0] + count[i][1] +
        count[i][2] + count[i][3] - count[i][maxPos];
    }
    std::cout << '\n' << distance << '\n';
  }
}
