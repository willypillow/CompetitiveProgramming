#include <iostream>

int edges[200][200];

bool dfs(int node, int color, int *tops, int *colors) {
  int *colorPtr = &colors[node];
  if (!*colorPtr) *colorPtr = color;
  else if (*colorPtr != color) return false;
  else return true;
  bool res = true;
  int newColor = color ^ 2, top = tops[node];
  for (int i = 0; i < top; i++) {
    if (!dfs(edges[node][i], newColor, tops, colors)) {
      res = false;
      break;
    }
  }
  return res;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  while (std::cin >> n && n) {
    int tops[200] = { 0 }, colors[200] = { 0 };
    int l;
    std::cin >> l;
    while (l--) {
      int a, b;
      std::cin >> a >> b;
      if (a < b) edges[a][tops[a]++] = b;
      else edges[b][tops[b]++] = a;
    }
    if (dfs(0, 1, tops, colors)) puts("BICOLORABLE.");
    else puts("NOT BICOLORABLE.");
  }
}
