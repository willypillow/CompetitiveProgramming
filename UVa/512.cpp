#include <iostream>

struct Command {
  char cmd[8];
  int r1, c1, r2, c2, a, x[16];
} cmds[16384];

static inline bool process(int& qR, int& qC, int cmdCount) {
  for (int i = 0; i < cmdCount; i++) {
    Command *ptr = &cmds[i];
    if (ptr->cmd[0] == 'E') { // EX
      if (ptr->r1 == qR && ptr->c1 == qC) {
        qR = ptr->r2; qC = ptr->c2;
      } else if (ptr->r2 == qR && ptr->c2 == qC) {
        qR = ptr->r1; qC = ptr->c1;
      }
    } else {
      int a = ptr->a, addR = 0, addC = 0,
        type = ((ptr->cmd[0] == 'I') << 1) | (ptr->cmd[1] == 'R');
      switch (type) {
      case 3: // IR
        for (int i = 0; i < a; i++) {
          if (ptr->x[i] <= qR) addR++;
        }
        break;
      case 2: // IC
        for (int i = 0; i < a; i++) {
          if (ptr->x[i] <= qC) addC++;
        }
        break;
      case 1: // DR
        for (int i = 0; i < a; i++) {
          if (ptr->x[i] == qR) return false;
          if (ptr->x[i] < qR) addR--;
        }
        break;
      case 0: // DC
        for (int i = 0; i < a; i++) {
          if (ptr->x[i] == qC) return false;
          if (ptr->x[i] < qC) addC--;
        }
        break;
      }
      qR += addR; qC += addC;
    }
  }
  return true;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int r, c, count = 0;
  while (std::cin >> r >> c && r) {
    if (count) std::cout << '\n';
    std::cout << "Spreadsheet #" << ++count << '\n';
    int cmdCount;
    std::cin >> cmdCount;
    for (int i = 0; i < cmdCount; i++) {
      std::cin >> cmds[i].cmd;
      if (cmds[i].cmd[0] == 'E') { // EX
        std::cin >> cmds[i].r1 >> cmds[i].c1 >> cmds[i].r2 >> cmds[i].c2;
      } else {
        std::cin >> cmds[i].a;
        for (int j = 0; j < cmds[i].a; j++) std::cin >> cmds[i].x[j];
      }
    }
    int queryCount;
    std::cin >> queryCount;
    while (queryCount--) {
      int qR, qC;
      std::cin >> qR >> qC;
      std::cout << "Cell data in (" << qR << ',' << qC << ") ";
      if (!process(qR, qC, cmdCount)) std::cout << "GONE\n";
      else std::cout << "moved to (" << qR << ',' << qC << ")\n";
    }
  }
}
