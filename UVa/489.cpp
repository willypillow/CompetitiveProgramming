#include <iostream>
#include <cstring>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int round;
  while (std::cin >> round && round != -1) {
    std::cout << "Round " << round << '\n';
    char str1[200], str2[200];
    std::cin >> str1 >> str2;
    int chance = 7, len1 = strlen(str1), len2 = strlen(str2), guessLeft = len1;
    for (int i = 0; i < len2; i++) {
      bool fail = true;
      for (int j = 0; j < len1; j++) {
        if (str2[i] == str1[j]) {
          fail = false;
          guessLeft--;
          str1[j] = ' ';
        }
      }
      if (fail) chance--;
      if (!chance || !guessLeft) break;
    }
    if (!chance) std::cout << "You lose.\n";
    else if (!guessLeft) std::cout << "You win.\n";
    else std::cout << "You chickened out.\n";
  }
}
