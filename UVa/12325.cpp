#include <iostream>

unsigned long long n, s[2], v[2];

static inline unsigned long long search(int obj, int max) {
  unsigned long long ans = 0;
  for (int i = 0; i <= max; i++) {
    ans = std::max(ans, i * v[obj] + (n - i * s[obj]) / s[1-obj] * v[1 - obj]);
  }
  return ans;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif 
  int t;
  std::cin >> t;
  for (int i = 1; i <= t; i++) {
    unsigned long long ans;
    std::cin >> n >> s[0] >> v[0] >> s[1] >> v[1];
    if (s[0] * s[1] < n) {
      if (s[1] * v[0] > s[0] * v[1]) ans = search(1, s[0]);
      else ans = search(0, s[1]);
    } else {
      if (s[0] > s[1]) ans = search(0, n / s[0]);
      else ans = search(1, n / s[1]);
    }
    std::cout << "Case #" << i << ": " << ans << '\n';
  }
}
