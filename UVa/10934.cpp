#include <bits/stdc++.h>
using namespace std;

long long dp[101][64];

int main() {
	dp[0][0] = 0;
	for (int i = 1; i <= 100; i++) {
		dp[i][0] = 0;
		for (int j = 1; j < 64; j++) {
			dp[i][j] = dp[i - 1][j - 1] + 1 + dp[i][j - 1];
		}
	}
	for (long long k, n; cin >> k >> n && k; ) {
		int idx = lower_bound(dp[k], dp[k] + 64, n) - dp[k];
		if (idx == 64) cout << "More than 63 trials needed.\n";
		else cout << idx << '\n';
	}
}
