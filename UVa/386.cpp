#include <iostream>
#include <map>
#include <algorithm>
#include <vector>

struct P {
  int x, y;
};

struct Cube {
  int a, b, c, d;
  bool operator <(const Cube &rhs) {
    return (a != rhs.a) ? (a < rhs.a) :
      (b != rhs.b) ? (b < rhs.b) :
      (c != rhs.c) ? (c < rhs.c) :
      (d < rhs.d);
  }
};

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  std::multimap<int, P> ad, cb;
  for (int a = 2; a <= 200; a++) {
    for (int d = 2; d < a; d++) {
      ad.insert(std::make_pair(a * a * a - d * d * d, P { a, d }));
    }
  }
  for (int c = 2; c < 200 ; c++) {
    for (int b = 2; b <= c; b++) {
      cb.insert(std::make_pair(c * c * c + b * b * b, P { c, b }));
    }
  }
  std::vector<Cube> ans;
  for (auto i = ad.begin(), j = cb.begin(); i != ad.end() && j != cb.end();) {
    if (i->first < j->first) i++;
    else if (i->first > j->first) j++;
    else {
      int key = i->first;
        if (j->second.x <= i->second.y) {
          ans.push_back(Cube { i->second.x, j->second.y, j->second.x, i->second.y });
        }
        int minus = 0;
        if ((++i)->first != key) { i--; minus++; }
        if ((++j)->first != key) { j--; minus++; }
        if (minus == 2) { i++; j++; }
    }
  }
  std::sort(ans.begin(), ans.end());
  for (auto i: ans) {
    std::cout << "Cube = " << i.a << ", Triple = ("
      << i.b << ',' << i.c << ',' << i.d << ")\n";
  }
}
