#include <iostream>

char sub[1000000], str[1000000];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  while (std::cin >> sub >> str) {
    char *subP = sub, *strP = str;
    for (;*strP != '\0'; strP++) {
      if (*subP == '\0') break;
      if (*subP == *strP) subP++;
    }
    if (*subP == '\0') puts("Yes");
    else puts("No");
  }
}
