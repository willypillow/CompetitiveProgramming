#include <iostream>
#include <cstring>

int edges[101][101], eTops[101], route[101], n, m, rTop;
bool vis[101];

void dfs(int node) {
  vis[node] = true;
  for (int i = 0; i < eTops[node]; i++) {
    int next = edges[node][i];
    if (!vis[next]) dfs(next);
  }
  route[--rTop] = node;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  while (std::cin >> n >> m && n) {
    memset(eTops, 0, sizeof(eTops));
    memset(vis, 0, sizeof(vis));
    rTop = n;
    bool isAfter[101] = { false };
    while (m--) {
      int before, after;
      std::cin >> before >> after;
      edges[before][eTops[before]++] = after;
      isAfter[after] = true;
    }
    for (int i = 1; i <= n; i++) {
      if (!isAfter[i] && !vis[i]) dfs(i);
    }
    for (int i = 0; i < n - 1; i++) std::cout << route[i] << ' ';
    std::cout << route[n - 1] << '\n';
  }
}
