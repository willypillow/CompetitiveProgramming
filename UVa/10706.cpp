#include <bits/stdc++.h>
using namespace std;

int main() {
	int t; cin >> t;
	while (t--) {
		int n; cin >> n;
		n--;
		string str;
		for (int i = 1;; i++) {
			str += to_string(i);
			if (n < str.size()) break;
			n -= str.size();
		}
		cout << str[n] << '\n';
	}
}
