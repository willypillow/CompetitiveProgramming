# CompetitiveProgramming

These are the source code for my submissions on online judges and various competitions.

[UVa](https://uva.onlinejudge.org/)

[CodeChef](https://www.codechef.com/)

[CodeForces](http://codeforces.com/)

[KhCodeNew](http://khcode.new.m-school.tw/)

[LA](https://icpcarchive.ecs.baylor.edu/)

[POI](http://main.edu.pl/en)

[TIOJ](http://tioj.infor.org/)

[ZeroJudge](https://zerojudge.tw/)

YTP: [A competition with online preliminary and on-site finals](http://www.tw-ytp.com/). Problem descriptions included.

TOI: Taiwanese Olympiad of Informatics. Problem descriptions included. (Only a few currently)

Note: Due to the fact that most commits are from my laptop, this repo is unsigned. Please use with care :P
