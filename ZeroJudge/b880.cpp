#include <bits/stdc++.h>
using namespace std;

const uint32_t INF = 1E9, MAXN = 1E6 + 5;

uint32_t x[MAXN], y[MAXN];

struct Node {
	uint32_t minVal, ans, lazy, origMin;
} st[MAXN * 4];

void build(int id, int l, int r) {
	st[id].minVal = INF;
	st[id].lazy = UINT_MAX;
	if (l == r) {
		st[id].ans = y[l] + INF;
		st[id].origMin = x[l] + y[l];
	} else {
		int m = (l + r) >> 1;
		build(id << 1, l, m); build(id << 1 | 1, m + 1, r);
		st[id].ans = min(st[id << 1].ans, st[id << 1 | 1].ans);
		st[id].origMin = min(st[id << 1].origMin, st[id << 1 | 1].origMin);
	}
}

void push(int id) {
	if (st[id << 1].minVal > st[id].lazy) {
		st[id << 1].ans -= st[id << 1].minVal - st[id].lazy;
		st[id << 1].minVal = st[id].lazy;
	}
	if (st[id << 1 | 1].minVal > st[id].lazy) {
		st[id << 1 | 1].ans -= st[id << 1 | 1].minVal - st[id].lazy;
		st[id << 1 | 1].minVal = st[id].lazy;
	}

	st[id << 1].lazy = min(st[id << 1].lazy, st[id].lazy);
	st[id << 1 | 1].lazy = min(st[id << 1 | 1].lazy, st[id].lazy);
	st[id].lazy = UINT_MAX;
}

void pull(int id) {
	if (st[id << 1].ans < st[id << 1 | 1].ans) {
		st[id].ans = st[id << 1].ans;
		st[id].minVal = st[id << 1].minVal;
	} else {
		st[id].ans = st[id << 1 | 1].ans;
		st[id].minVal = st[id << 1 | 1].minVal;
	}
}

void modify(int id, int l, int r, int qL, int qR, uint32_t d) {
	if (qL <= l && r <= qR) {
		if (st[id].minVal > d) {
			st[id].ans -= st[id].minVal - d;
			st[id].minVal = d;
		}
		st[id].lazy = min(st[id].lazy, d);
	} else {
		if (st[id].lazy < UINT_MAX) push(id);
		int m = (l + r) >> 1;
		if (qL <= m) modify(id << 1, l, m, qL, qR, d);
		if (m < qR) modify(id << 1 | 1, m + 1, r, qL, qR, d);
		pull(id);
	}
}

uint32_t query(int id, int l, int r, int qL, int qR) {
	if (l != r && st[id].lazy < UINT_MAX) {
		push(id);
		pull(id);
	}
	if (qL <= l && r <= qR) {
		return min(st[id].ans, st[id].origMin);
	} else {
		int m = (l + r) >> 1;
		uint32_t t = UINT_MAX;
		if (qL <= m) t = min(t, query(id << 1, l, m, qL, qR));
		if (m < qR) t = min(t, query(id << 1 | 1, m + 1, r, qL, qR));
		return t;
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 1; i <= n; i++) cin >> x[i];
	for (int i = 1; i <= n; i++) cin >> y[i];
	build(1, 1, n);
	int m; cin >> m;
	while (m--) {
		char c; cin >> c;
		if (c == 'q') {
			int l, r; cin >> l >> r;
			cout << query(1, 1, n, l, r) << '\n';
		} else {
			int l, r, d; cin >> l >> r >> d;
			modify(1, 1, n, l, r, d);
		}
	}
}
