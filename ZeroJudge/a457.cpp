#include <iostream>
#include <algorithm>
using std::cin; using std::cout; using std::sort;

const int MAXK = 100000;

struct Score {
  int s, r;
  bool operator <(const Score &rhs) const {
    return (s != rhs.s ) ? (s < rhs.s) : (r < rhs.r);
  }
} scores[MAXK];

struct Node {
  int key, pri, count;
  Node *left, *right;
} nodes[MAXK];
int nTop;

uint32_t state[4] = { 123456789, 234567890, 345678901, 456789012 };
int rand() {
  uint32_t t = state[3];
  t ^= t << 11; t ^= t >> 8;
  state[3] = state[2]; state[2] = state[1]; state[1] = state[0];
  t = t ^ state[0] ^ (state[0] >> 19);
  return state[0] = t;
}

static inline int getCount(Node *n) {
  return n ? n->count : 0;
}

Node* newNode(int key) {
  Node *n = &nodes[nTop++];
  n->key = key;
  n->pri = rand();
  n->count = 1;
  n->left = n->right = NULL;
  return n;
}

Node* leftRot(Node *n) {
  Node *root = n->right;
  n->right = root->left;
  root->left = n;
  n->count = getCount(n->left) + getCount(n->right) + 1;
  root->count = getCount(root->left) + getCount(root->right) + 1;
  return root;
}

Node* rightRot(Node *n) {
  Node *root = n->left;
  n->left = root->right;
  root->right = n;
  n->count = getCount(n->left) + getCount(n->right) + 1;
  root->count = getCount(root->left) + getCount(root->right) + 1;
  return root;
}

Node* insert(Node *root, int key, int *count) {
  if (!root) return newNode(key);
  root->count++;
  if (key < root->key) {
    *count += getCount(root->right) + 1;
    root->left = insert(root->left, key, count);
    if (root->left->pri > root->pri) root = rightRot(root);
  } else {
    root->right = insert(root->right, key, count);
    if (root->right->pri > root->pri) root = leftRot(root);
  }
  return root;
}

int main() {
  int k, m;
  while (cin >> k >> m) {
    nTop = 0;
    for (int i = 0; i < k; i++) cin >> scores[i].s;
    for (int i = 0; i < k; i++) cin >> scores[i].r;
    sort(scores, scores + k);
    Node *root = NULL;
    long long int ans = 0;
    for (int i = 0; i < k; i++) {
      int count = 0;
      root = insert(root, scores[i].r, &count);
      ans += count;
    }
    cout << ans << '\n';
  }
}

