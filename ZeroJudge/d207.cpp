#include <iostream>
#include <cstdio>
#include <queue>
#include <cmath>

const int dirs[][2] = { { 0, -1 }, { -1, 0 }, { 1, 0 }, { 0, 1 } };
const int WEIGHTM = 114;

int singleH[4][4][16] = { 0 };
int grid[4][4], spaceX, spaceY;

inline void swap(int x0, int y0, int x1, int y1) {
  int tmp = grid[x0][y0];
  grid[x0][y0] = grid[x1][y1];
  grid[x1][y1] = tmp;
}

inline void input() {
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      std::cin >> grid[i][j];
      if (grid[i][j] == 0) spaceX = i, spaceY = j;
    }
  }
}

inline bool isSolvable() {
  const int *p = &grid[0][0];
  int reverseCnt = 0;
  for (int i = 0; i < 16; i++) {
    if (p[i] == 0) continue;
    for (int j = i + 1; j < 16; j++) {
      if (p[j] < p[i] && p[j] != 0) reverseCnt++;
    }
  }
  return (spaceX + reverseCnt) % 2;
}

inline int h() {
  int manhattanDist = 0;
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) manhattanDist += singleH[i][j][grid[i][j]];
  }
  return manhattanDist;
}

int ida(int stepCnt, int hVal, int maxDepth, int lastStep) {
  if (hVal == 0) {
    std::cout << stepCnt << '\n';
    return 0;
  }
  if (WEIGHTM * hVal / 100 + stepCnt > maxDepth) {
    return WEIGHTM * hVal / 100 + stepCnt;
  }
  int minDepth = 100000, x = spaceX, y = spaceY;
  stepCnt++;
  for (int i = 0; i < 4; i++) {
    if (i + lastStep == 3) continue;
    int newX = x + dirs[i][0], newY = y + dirs[i][1];
    if (newX < 0 || newX >= 4 || newY < 0 || newY >= 4) continue;
    int data = grid[newX][newY];
    swap(x, y, newX, newY);
    spaceX = newX, spaceY = newY;
    int newH = hVal - singleH[newX][newY][data] + singleH[x][y][data];
    int depth = ida(stepCnt, newH, maxDepth, i);
    if (depth == 0) return 0;
    if (depth < minDepth) minDepth = depth;
    swap(x, y, newX, newY);
  }
  spaceX = x, spaceY = y;
  return minDepth;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      for (int k = 0; k < 15; k++) {
        singleH[i][j][k + 1] = std::abs(i - k / 4) + std::abs(j - k % 4);
      }
    }
  }
  int t;
  std::cin >> t;
  while (t--) {
    input();
    int hVal = h();
    if (isSolvable()) {
      int depth = WEIGHTM * hVal / 100;
      while (depth != 0) {
        depth = ida(0, hVal, depth, -1);
      }
    }
    else std::cout << "This puzzle is not solvable.\n";
  }
}
