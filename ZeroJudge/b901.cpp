#include <iostream>
#include <iomanip>
using std::cin; using std::cout; using std::setw;

// 1 * 2 -> 2 / 8 * 2 * 2 -> 2 / 4 * 2 * 2 * 2 -> 2 / 2 * 2 * 2 * 2 * 2 -> 2
static const int START[] = { 1, 8, 4, 2 };
static const int MULT[][4] = { { 0, 0, 0, 0 }, { 1, 1, 1, 1}, { 2, 4, 8, 6 }, { 3, 9, 7, 1 }, { 4, 6, 4, 6 }, { 5, 5, 5, 5 }, { 6, 6, 6, 6 }, { 7, 9, 3, 1 }, { 8, 4, 2, 6 }, { 9, 1, 9, 1 } };

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  unsigned long long n;
  int t;
  cin >> t;
  while (t--) {
    cin >> n;
    unsigned long long nn = n, fiveCnt = 0;
    int ans = 0;
    while (nn) fiveCnt += (nn /= 5); // Count the factor of fives
    ans = START[fiveCnt % 4]; // 5 cancels out 2,which repeats every 4 times
    nn = n;
    while (nn) {
      for (unsigned int i = 2; i <= 9; i++) { // Skip 1 for speed
        if (i == 5) continue;
        unsigned long long cnt = (nn / 10) + ((nn % 10 >= i) ? 1 : 0); // i in last digit
        if (cnt) {
          cnt = (cnt - 1) % 4; // All digits repeat within a period of 4
          ans = (ans * MULT[i][cnt]) % 10;
        }
      }
      nn /= 5; // Process stuff like 2 * 35 = (2 * 5) * 7
    }
    cout << ans << '\n';
  }
}
