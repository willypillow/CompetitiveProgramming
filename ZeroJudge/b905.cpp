#include <bits/stdc++.h>
using namespace std;

char pattern[3000010], str[3000010];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t; cin >> t >> std::ws;
	while (t--) {
		cin.getline(pattern, 3000010); cin.getline(str, 3000010);
		int pl = 0, pr = strlen(pattern) - 1, sl = 0, sr = strlen(str) - 1;
		char *p = strchr(pattern, '*');
		bool fail = false;
		if (!p) {
			fail = strcmp(pattern, str) != 0;
			goto End;
		}
		while (pattern[pl] != '*') {
			if (sl > sr || pattern[pl] != str[sl]) {
				fail = true;
				goto End;
			}
			pl++; sl++;
		}
		while (pattern[pr] != '*') {
			if (sr < 0 || pattern[pr] != str[sr]) {
				fail = true;
				goto End;
			}
			pr--; sr--;
		}
		while (pl < pr) {
			pl++;
			int nxt = strchr(pattern + pl, '*') - pattern;
			pattern[nxt] = '\0';
			char *sNxt = strstr(str + sl, pattern + pl);
			if (!sNxt) {
				fail = true;
				goto End;
			}
			sl = sNxt - str + (nxt - pl);
			if (sl > sr + 1) {
				fail = true;
				goto End;
			}
			pl = nxt;
		}
End:
		if (fail || sl > sr + 1) cout << "No\n";
		else cout << "Yes\n";
	}
}
