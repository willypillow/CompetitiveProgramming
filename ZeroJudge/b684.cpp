#include <iostream>
#include <cstring>

enum Type { NONE = 0, BACK, FRONT, BOTH };
Type reverse(Type t) {
  return (t == BACK) ? FRONT : BACK;
}

struct Seq {
  Type type;
  bool root;
} seq[1000010];

int arr[1000010];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n;
  while (std::cin >> n) {
    memset(seq, 0, sizeof(seq));
    for (int i = 0; i < n; i++) std::cin >> arr[i];
    for (int i = 0; i < n; i++) {
      int cur = arr[i];
      if (seq[cur - 1].type != BACK && seq[cur - 1].type != NONE) {
        if (seq[cur - 1].type == BOTH) seq[cur - 1].type = FRONT;
        seq[cur].type = FRONT;
      } else if (seq[cur + 1].type != FRONT && seq[cur + 1].type != NONE) {
        if (seq[cur + 1].type == BOTH) seq[cur + 1].type = BACK;
        seq[cur].type = BACK;
      } else {
        seq[cur].type = BOTH;
        seq[cur].root = true;
      }
    }
    Type last = BACK;
    int ans = -1;
    for (int i = 1; i <= n; i++) {
      if (seq[i].root) {
        ans += (seq[i].type == last && seq[i].type != BOTH) ? 2 : 1;
        last = (seq[i].type == BOTH) ? reverse(last) : seq[i].type;
      }
    }
    std::cout << ans << '\n';
  }
}
