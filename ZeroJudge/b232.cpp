#include <iostream>
using std::cin; using std::cout;

long long dp[800][800] = { 0 };

long long dfs(int n, int min) {
  if (n == 0) return 1;
  if (dp[n][min]) return dp[n][min];
  for (int i = min; i <= n; i += 2) {
    dp[n][min] += dfs(n - i, i);
  }
  return dp[n][min];
}

int main() {
  int t; cin >> t;
  while (t--) {
    int n; cin >> n;
    cout << dfs(n, 1) << '\n';
  }
}

