#include <bits/stdc++.h>
using namespace std;

const int MAXN = 3E5 + 10;

int64_t sum[1 << 20], lazy[1 << 20], ans[MAXN], arr[MAXN];
int opL[MAXN], opR[MAXN], opV[MAXN];
vector<tuple<int, int, int, int> > event;

void modify(int id, int l, int r, int qL, int qR, int val) {
	if (qL <= l && r <= qR) {
		lazy[id] += val;
	} else {
		int m = (l + r) >> 1;
		lazy[id << 1] += lazy[id];
		lazy[id << 1 | 1] += lazy[id];
		lazy[id] = 0;
		if (qL <= m) modify(id << 1, l, m, qL, min(m, qR), val);
		if (m < qR) modify(id << 1 | 1, m + 1, r, max(qL, m), qR, val);
		sum[id] = sum[id << 1] + sum[id << 1 | 1] +
			lazy[id << 1] * (m - l + 1) + lazy[id << 1 | 1] * (r - m);
	}
}

int64_t query(int id, int l, int r, int qL, int qR) {
	if (qL <= l && r <= qR) {
		return sum[id] + lazy[id] * (r - l + 1);
	} else {
		int m = (l + r) >> 1;
		int64_t t = lazy[id] * (qR - qL + 1);
		if (qL <= m) t += query(id << 1, l, m, qL, min(m, qR));
		if (m < qR) t += query(id << 1 | 1, m + 1, r, max(qL, m + 1), qR);
		return t;
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	for (int i = 1; i <= n; i++) cin >> arr[i];
	for (int i = 1; i <= n; i++) arr[i] += arr[i - 1];
	for (int i = 1; i <= m; i++) cin >> opL[i] >> opR[i] >> opV[i];
	int k; cin >> k;
	for (int i = 1; i <= k; i++) {
		int l, r, tl, tr; cin >> l >> r >> tl >> tr;
		if (tl - 1 != 0) event.push_back(make_tuple(tl - 1, l, r, -i));
		event.push_back(make_tuple(tr, l, r, i));
		ans[i] += arr[r] - arr[l - 1];
	}
	sort(event.begin(), event.end());
	auto it = event.begin();
	for (int i = 1; i <= m; i++) {
		modify(1, 1, n, opL[i], opR[i], opV[i]);
		while (it != event.end() && get<0>(*it) == i) {
			if (get<3>(*it) < 0) {
				ans[-get<3>(*it)] -= query(1, 1, n, get<1>(*it), get<2>(*it));
			} else {
				ans[get<3>(*it)] += query(1, 1, n, get<1>(*it), get<2>(*it));
			}
			it++;
		}
	}
	for (int i = 1; i <= k; i++) cout << ans[i] << '\n';
}
