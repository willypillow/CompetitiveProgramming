#include <bits/stdc++.h>
using namespace std;

bool stat[300001];

int main() {
	int n, ans = 0;
	cin >> n;
	for (int i = 1; i <= n; i++) cin >> stat[i];
	for (int i = 1; i <= n; i++) {
		if (stat[i]) {
			for (int j = i; j <= n; j += i) stat[j] = !stat[j];
			ans++;
		}
	}
	cout << ans << '\n';
}
