#include <bits/stdc++.h>
using namespace std;

int main() {
	int t; cin >> t;
	while (t--) {
		long long a, b, c; cin >> a >> b >> c;
		if (a == 1) cout << b + c << '\n';
		if (a == 2) cout << b - c << '\n';
		if (a == 3) cout << b * c << '\n';
		if (a == 4) cout << b / c << '\n';
	}
}
