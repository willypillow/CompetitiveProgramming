#include <iostream>
using std::cin; using std::cout;

const int MOD = 10000000;
int dp[5001] = { 0 }, aux[5001] = { 0 };

int dfs(int n);

int auxFunc(int n) {
  if (n == 0) return 1;
  if (aux[n]) return aux[n];
  for (int i = 0; i <= n; i++) {
    int j = n - i;
    long long tmp = dfs(i);
    tmp = tmp * dfs(j) % MOD;
    aux[n] = (aux[n] + tmp) % MOD;
  }
  return aux[n];
}

int dfs(int n) {
  if (n <= 1) return 1;
  if (dp[n]) return dp[n];
  for (int i = 0; i < n; i++) {
    int j = n - 1 - i;
    long long tmp = dfs(i);
    tmp = tmp * auxFunc(j) % MOD;
    dp[n] = (dp[n] + tmp) % MOD;
  }
  return dp[n];
}

int main() {
  for (int n; cin >> n; ) {
    cout << dfs(n) << '\n';
  }
}
