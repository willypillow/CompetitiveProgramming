#include <iostream>
#include <cmath>
using std::cin; using std::cout;

const double EPS = 1E-8;

int main() {
  int n;
  while (cin >> n && n) {
    int cnt = 0;
    for (int i = 2; i < n; i++) {
      double res = (n * 2.0 / i + 1 - i) / 2;
      if (res < 0 || fabs(res - 0) < EPS) break;
      if (fabs(res - round(res)) < EPS) {
        cnt++;
      }
    }
    cout << cnt << '\n';
  }
}
