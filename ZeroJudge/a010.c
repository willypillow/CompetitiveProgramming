#include <stdio.h>

main() {
  int input;
  while (scanf("%d", &input) != EOF) {
    int i, j = 0;
    int buffer[100];
    while (input != 1) {
      for (i = 2; i <= input; i++) {
        if (input % i == 0) {
          input /= i;
          buffer[j++] = i;
          break;
        }
      }
    }
    if (j == 0) printf("1");
    else {
      int k;
      for (k = 0; k < j; k++) {
        int count = 1;
        while ((k + count) < j && buffer[k] == buffer[k+count])
          count++;
        if (count == 1) printf("%d", buffer[k]);
        else printf("%d^%d", buffer[k], count);
        k += count - 1;
        if ((k + 1) < j) printf(" * ");
      }
    }
    printf("\n");
  }
  return 0;
}
