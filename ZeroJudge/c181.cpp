#include <bits/stdc++.h>
using namespace std;

int points[300001];
set<int> oppo;
vector<int> self;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 1; i <= n; i++) cin >> points[i];
	for (int i = 0; i < n / 2; i++) {
		int x; cin >> x;
		oppo.insert(x);
	}
	self.reserve(n / 2 + 1);
	for (int i = 1; i <= n; i++) {
		if (!oppo.count(i)) self.push_back(i);
	}
	sort(self.begin(), self.end(), [](int &l, int &r) {
		return points[l] > points[r];
	});
	long long ans = 0;
	for (int x: self) {
		auto it = oppo.lower_bound(x);
		if (it == oppo.begin()) {
			ans -= points[x];
			it = oppo.lower_bound(n + 1);
			oppo.erase(--it);
		} else {
			ans += points[x];
			oppo.erase(--it);
		}
	}
	cout << ans << '\n';
}
