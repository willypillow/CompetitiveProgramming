#include <stdio.h>
#include <math.h>

main() {
  int num, res;
  while (scanf("%d", &num) != EOF) {
    if (num >= 40) res = 100;
    else if (num > 20) res = 80 + (num - 20);
    else if (num > 10) res = 60 + (num - 10) * 2;
    else res = num * 6;
    printf("%d\n", res);
  }
  return 0;
}
