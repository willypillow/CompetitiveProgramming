#include <bits/stdc++.h>
using namespace std;

const int INF = 0x3f3f3f3f;

struct P {
	int x, y;
	bool operator <(const P &rhs) const {
		return y > rhs.y;
	}
};
vector<vector<P> > g;
int cnt[110], dist[110];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int n, m, cases = 0;
	while (cin >> n >> m) {
		g.clear(); g.resize(n + 1);
		while (m--) {
			int x, y, d; cin >> x >> y >> d;
			g[x].push_back(P { y, d }); g[y].push_back(P { x, d });
		}
		cout << "Set #" << ++cases << '\n';
		int p; cin >> p;
		while (p--) {
			memset(cnt, 0, sizeof(cnt)); memset(dist, 0x3f, sizeof(dist));
			int s, t, k; cin >> s >> t >> k;
			priority_queue<P> q;
			q.push(P { s, 0 });
			int ans = INF;
			while (!q.empty()) {
				P cur = q.top(); q.pop();
				if (cnt[cur.x] >= k || dist[cur.x] == cur.y) continue;
				dist[cur.x] = cur.y;
				cnt[cur.x]++;
				if (cur.x == t && cnt[cur.x] == k) {
					ans = cur.y;
					break;
				}
				for (P p: g[cur.x]) {
					q.push(P { p.x, cur.y + p.y });
				}
			}
			if (ans < INF) cout << ans << '\n';
			else cout << "?\n";
		}
	}
}
