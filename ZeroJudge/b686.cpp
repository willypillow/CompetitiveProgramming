#include <iostream>
#include <algorithm>
#include <cstring>

static const int MAX = 0x3f3f3f3f;
int dist[501][501][501], origToSort[501];

struct A {
  int id, val;
  bool operator <(const A &rhs) {
    return val > rhs.val;
  }
} a[510];
bool cmpA(const A &l, const A &r) { return l.val > r.val; }

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif 
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n, m, r;
  while (std::cin >> n >> m >> r) {
    for (int i = 0; i <= n; i++) for (int j = 0; j <= n; j++) for (int k = 0; k <= n; k++) dist[i][j][k] = MAX;
    for (int i = 0; i < n; i++) { std::cin >> a[i].val; a[i].id = i + 1; }
    std::sort(a, a + n);
    for (int i = 0; i < n; i++) origToSort[a[i].id] = i;
    for (int i = 0; i < m; i++) {
      int s, t, c;
      std::cin >> s >> t >> c;
      if (s > n || t > n) continue;
      s = origToSort[s]; t = origToSort[t];
      dist[0][s][t] = std::min(dist[0][s][t], c);
      dist[0][t][s] = std::min(dist[0][t][s], c);
    }
    for (int k = 0; k < n; k++) {
      for (int s = 0; s < n; s++) {
        for (int e = 0; e < n; e++) {
          if (dist[k][s][e] != MAX || (dist[k][s][k] + dist[k][k][e]) != MAX) {
            dist[k + 1][s][e] = std::min(dist[k][s][e],
              dist[k][s][k] + dist[k][k][e]);
          }
        }
      }
    }
    while (r--) {
      int g, p, q;
      std::cin >> g >> p >> q;
      if (p > n || q > n) { std::cout << "-1\n"; continue; }
      p = origToSort[p]; q = origToSort[q];
      int availCnt = std::upper_bound(a, a + n, A { 0, g }, cmpA) - a;
      if (a[p].val < g || a[q].val < g || dist[availCnt][p][q] == MAX) {
        std::cout << "-1\n";
      } else {
        std::cout << dist[availCnt][p][q] << '\n';
      }
    }
  }
}
