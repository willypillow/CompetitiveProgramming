#include <iostream>
#include <vector>
#include <queue>
using namespace std;
const int DIR[][2] = { { -2, -1 }, { -2, 1 }, { -1, -2 }, { -1, 2 }, { 1, -2 }, { 1, 2 }, { 2, -1 }, { 2, 1 } };

int main()  {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	char _s[3], _e[3];
	while (cin >> _s >> _e) {
		int s = ((_s[0] - 'a') << 3) | (_s[1] - '1');
		int e = ((_e[0] - 'a') << 3) | (_e[1] - '1');
		char t[3];
		bool block[8][8] = { false };
		while (cin >> t && t[0] != 'x') block[t[0] - 'a'][t[1] - '1'] = true;
		queue<pair<int, vector<int> > > q;
		vector<int> v;
		q.push(make_pair(s, v));
		vector<vector<int> > ans;
		int ansL = 1E9;
		while (!q.empty()) {
			auto c = q.front(); q.pop();
			if (c.second.size() > ansL) break;
			if (c.first == e) {
				ans.push_back(c.second);
				ansL = c.second.size();
			} else {
				for (int i = 0; i < 8; i++) {
					int nx = (c.first >> 3) + DIR[i][0], ny = (c.first & 0b111) + DIR[i][1];
					if (nx < 0 || nx >= 8 || ny < 0 || ny >= 8) continue;
					if (block[nx][ny]) continue;
					auto nv = c.second;
					int np = nx << 3 | ny;
					nv.push_back(np);
					q.push(make_pair(np, nv));
				}
			}
		}
		cout << "The shortest solution is " << ansL << " move(s).\n";
		for (int i = 0; i < (int)ans.size(); i++) {
			cout << "Solution: " << _s;
			for (int j = 0; j < (int)ans[i].size(); j++) {
				cout << ' ' <<
					(char)((ans[i][j] >> 3) + 'a') << (char)((ans[i][j] & 0b111) + '1');
			}
			cout << '\n';
		}
	}
}
