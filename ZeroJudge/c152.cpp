#include <iostream>
#include <vector>
#include <cstring>
#include <queue>
using std::cin; using std::cout; using std::priority_queue; using std::vector;

struct Edge {
	int node;
	double weight[2];
};

vector<vector<Edge> > edges;

struct Node {
	int node;
	double dist;
	bool operator <(const Node &rhs) const {
		return dist > rhs.dist;
	}
} parent[10001];

double dist[10001];

double solve(int src, int dst, int type) {
	priority_queue<Node> q;
	q.push(Node { src, 0 });
	dist[src] = 0;
	while (!q.empty()) {
		Node cur = q.top(); q.pop();
		if (cur.dist > dist[cur.node]) continue;
		for (Edge e: edges[cur.node]) {
			double newDist = dist[cur.node] + e.weight[type];
			if (newDist < dist[e.node]) {
				dist[e.node] = newDist;
				parent[e.node] = Node { cur.node, e.weight[0] };
				q.push(Node { e.node, newDist });
			}
		}
	}
	return dist[dst];
}

int calcDist(int src, int dst) {
	if (dst == src) return 0;
	return calcDist(src, parent[dst].node) + parent[dst].dist;
}

void init() {
	memset(dist, 0x5f, sizeof(dist));
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		init();
		int n, m, src, dst;
		cin >> n >> m >> src >> dst;
		edges.clear(); edges.resize(n + 1);
		for (int i = 0, a, b, d, v; i < m; i++) {
			cin >> a >> b >> d >> v;
			edges[a].push_back(Edge{ b, { d, (double)d/v } });
			edges[b].push_back(Edge{ a, { d, (double)d/v } });
		}
		cout << solve(src, dst, 0) << ' ';
		init();
		solve(src, dst, 1);
		cout << calcDist(src, dst) << '\n';
	}
}
