#include <iostream>
#include <algorithm>
#include <vector>

typedef std::pair<int, int> P;

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int n, cases = 0;
  while (std::cin >> n) {
    std::vector<P> points, ans;
    while (n--) {
      int x, y;
      std::cin >> x >> y;
      points.push_back(std::make_pair(x, y));
    }
    std::sort(points.begin(), points.end(), std::greater<P>());
    int curX = -1, curY = -1;
    for (P &p: points) {
      if (p.first == curX) continue;
      if (curY < p.second) {
        ans.push_back(std::make_pair(p.first, p.second));
        curX = p.first;
        curY = p.second;
      }
    }
    std::cout << "Case " << ++cases << ":\n" <<
      "Dominate Point: " << ans.size() << '\n';
    for (auto i = ans.rbegin(); i != ans.rend(); ++i) {
      std::cout << '(' << i->first << ',' << i->second << ")\n";
    }
  }
}
