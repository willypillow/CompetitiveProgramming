#include <bits/stdc++.h>
using namespace std;

const uint32_t MAXN3 = 1000000;

uint32_t notPrime[MAXN3 + 1];
vector<uint32_t> primes;

static inline uint64_t powMod(uint64_t x, uint64_t e, uint64_t m) {
	__uint128_t t = x, ret = 1;
	for (; e; e >>= 1, t = t * t % m) {
		if (e & 1) ret = ret * t % m;
	}
	return ret;
}

static inline bool isPrime(uint64_t x) {
	const uint32_t as = 7;
	const uint32_t a[7] = { 2, 325, 9375, 28178, 450775, 9780504, 1795265022 };
	uint64_t t = x - 1;
	uint32_t r = 0;
	while (!(t & 1)) {
		t >>= 1;
		r++;
	}
	for (uint32_t i = 0; i < as; i++) {
		if (!(a[i] % t)) continue;
		bool ok = false;
		__uint128_t tt = powMod(a[i], t, x);
		if (tt == 1) continue;
		for (uint32_t i = 0; i < r; i++, tt = (tt * tt % x)) {
			if (tt == x - 1) {
				ok = true;
				break;
			}
		}
		if (!ok) return false;
	}
	return true;
}

static inline void genPrimes() {
	primes.reserve(50000);
	for (uint32_t i = 2; i <= MAXN3; i++) {
		if (!notPrime[i]) primes.push_back(i);
		for (uint32_t p: primes) {
			if (i * p > MAXN3) break;
			notPrime[i * p] = p;
		}
	}
}

static inline int d(uint64_t n) {
	uint32_t ret = 1, thres = ceil(pow(n, 1.0 / 3));
	for (uint32_t p: primes) {
		if (n <= MAXN3 || p > thres) break;
		uint32_t tmp = ret;
		while (!(n % p)) {
			n /= p;
			ret += tmp;
		}
	}
	if (n != 1) {
		if (n <= MAXN3) {
			uint32_t tmp = ret, last = -1;
			while (notPrime[n]) {
				if (notPrime[n] != last) {
					last = notPrime[n];
					tmp = ret;
				}
				ret += tmp;
				n /= notPrime[n];
			}
			if (last == n) ret += tmp;
			else ret <<= 1;
		} else if (isPrime(n)) {
			ret <<= 1;
		} else {
			uint64_t t = sqrt(n);
			if (t * t == n) ret *= 3;
			else ret <<= 2;
		}
	}
	return ret;
}

int32_t main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	genPrimes();
	uint64_t n;
	while (cin >> n && n) {
		while (!(n & 1)) n >>= 1;
		cout << d(n) - 1 << '\n';
	}
}
