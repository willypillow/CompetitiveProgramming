#include <stdio.h>

main() {
  int year;
  while(scanf("%d", &year) != EOF) {
    printf("%s\n", (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) ? "閏年" : "平年");
  }
  return 0;
}
