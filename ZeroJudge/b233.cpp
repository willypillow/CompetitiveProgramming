#include <iostream>
#include <cstring>
#include <vector>
#include <set>
using std::cin; using std::cout; using std::vector; using std::set;

vector<int> edges[30001];
set<int> edgeSet[30001];
int discover[30001], low[30001], cnt[30001];
int dfs(int u, int depth, int parent) {
  discover[u] = low[u] = depth;
  for (int v: edges[u]) {
    if (!discover[v]) {
      cnt[u] += dfs(v, depth + 1, u);
      low[u] = std::min(low[u], low[v]);
    } else if (v != parent) {
      low[u] = std::min(low[u], discover[v]);
    }
  }
  return cnt[u] + 1;
}

int main() {
  for (int n, m; cin >> n >> m; ) {
    memset(discover, 0, sizeof(discover)); memset(low, 0, sizeof(low));
    memset(cnt, 0, sizeof(cnt));
    for (int i = 1; i <= n; i++) edges[i].clear();
    for (int i = 0, u, v; i < m; i++) {
      cin >> u >> v;
      if (edgeSet[u].count(v)) continue;
      edges[u].push_back(v); edges[v].push_back(u);
      edgeSet[u].insert(v); edgeSet[v].insert(u);
    }
    int root, ans = 0, ansNode = 0;
    cin >> root;
    int total = dfs(root, 1, -1);
    for (int u = 1; u <= n; u++) {
      if (u == root || !discover[u]) continue;
      int sum = 0;
      for (int v: edges[u]) {
        if (discover[v] == discover[u] + 1 && low[v] >= discover[u]) {
          sum += cnt[v] + 1;
        }
      }
      if (sum > ans) ans = sum, ansNode = u;
    }
    if (ansNode) cout << ansNode << ' ' << total - ans << '\n';
    else cout << "0\n";
  }
}
