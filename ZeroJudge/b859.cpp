#include <iostream>

struct S { int h, w; bool use; } s[20010];
int ans[20010];

bool gt(const S &l, const S &r) {
  return (l.h > r.h) && (l.w > r.w);
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  int t;
  std::cin >> t;
  while (t--) {
    int aTop = 0, n, m;
    std::cin >> n >> m;
    for (int i = 0; i < n; i++) {
      std::cin >> s[i].h >> s[i].w;
      s[i].use = true;
    }
    S start = s[m - 1];
    for (int i = 0; i < n; i++) {
      if (!gt(s[i], start)) s[i].use = false;
    }
    for (int i = 0; i < n; i++) {
      if (!s[i].use) continue;
      for (int j = 0; j < n; j++) {
        if (i == j || !s[j].use) continue;
        if (gt(s[i], s[j])) {
          s[j].use = false;
        }
      }
    }
    for (int i = 0; i < n; i++) {
      if (s[i].use) ans[aTop++] = i + 1;
    }
    if (aTop == 0) {
      aTop = 1;
      ans[0] = m;
    }
    std::cout << aTop << '\n';
    for (int i = 0, first = true; i < aTop; i++) {
      if (!first) std::cout << ' ';
      first = false;
      std::cout << ans[i];
    }
    std::cout << '\n';
  }
}
