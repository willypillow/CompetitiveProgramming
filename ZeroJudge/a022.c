#include <stdio.h>

main() {
  char str[1000];
  while (scanf("%s", str) != EOF) {
    int i, len = strlen(str), ans = 1;
    for (i = 0; i < len / 2; i++) {
      if (str[i] != str[len - i - 1]) {
        ans = 0;
        break;
      }
    }
    printf("%s\n", ans ? "yes" : "no");
  }
  return 0;
}
