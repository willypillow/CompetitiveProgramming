#include <iostream>
#include <cstring>
#include <algorithm>

int arr[110], dpArr[110][1000000], sum, m, n;

int dp(int depth, int cap) {
  if (depth >= n || cap <= 0 || cap < arr[depth]) return 0;
  if (dpArr[depth][cap]) return dpArr[depth][cap];
  return dpArr[depth][cap] =
    std::max(dp(depth + 1, cap - arr[depth]) + arr[depth], dp(depth + 1, cap));
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  while (std::cin >> m >> n) {
    while (m--) {
      sum = 0; memset(dpArr, 0, sizeof(dpArr));
      for (int i = 0; i < n; i++) {
        std::cin >> arr[i];
        sum += arr[i];
      }
      if (sum & 1) {
        puts("No");
      } else {
        sum >>= 1;
        int res = dp(0, sum);
        if (res == sum) puts("Yes");
        else puts("No");
      }
    }
  }
}
