#include <stdio.h>

main() {
  int num;
  while (scanf("%d", &num) != EOF) {
    printf("%d\n", 2 + num * (num + 1) - 2 * num);
  }
  return 0;
}
