#include <bits/stdc++.h>
using namespace std;

int main()
{
	long long int n,m;
	while(cin>>n>>m){
		int t=0,sum=0;
		do{
			sum+=n;
			n++;
			++t;
		} while (sum <= m);
		cout<<t<<endl;
	}
}
