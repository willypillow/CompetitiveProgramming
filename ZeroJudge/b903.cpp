#include <iostream>
#include <algorithm>
using std::cin; using std::cout; using std::sort;

struct Match {
  int a, b, e;
  bool operator <(const Match &rhs) const {
    return (a != rhs.a) ? a < rhs.a : b > rhs.b;
  }
} match[300000];

struct Node {
  Node *l, *r;
  long long val;
} nodes[700000];
int nTop = 0;

Node* newNode() {
  Node *n = &nodes[nTop++];
  n->l = n->r = NULL;
  n->val = 0;
  return n;
}

Node* build(int l, int r) {
  Node *n = newNode();
  if (l == r) return n;
  int m = (l + r) / 2;
  n->l = build(l, m);
  n->r = build(m + 1, r);
  return n;
}

void modify(Node *n, int l, int r, int pos, long long val) {
  if (l == r) {
    n->val = val;
  } else {
    int m = (l + r) / 2;
    if (pos <= m) modify(n->l, l, m, pos, val);
    else modify(n->r, m + 1, r, pos, val);
    n->val = std::max(n->l->val, n->r->val);
  }
}

long long query(Node *n, int l, int r, int queryL, int queryR) {
  if (r < queryL || l > queryR) return -10000000;
  if (l >= queryL && r <= queryR) return n->val;
  int m = (l + r) / 2;
  return std::max(query(n->l, l, m, queryL, queryR),
    query(n->r, m + 1, r, queryL, queryR));
}

int main() {
  for (int n, m, k; cin >> n >> m >> k; ) {
    nTop = 0;
    for (int i = 0, a, b, e; i < k; i++) {
      cin >> a >> b >> e;
      match[i].a = a - 1, match[i].b = b - 1, match[i].e = e;
    }
    sort(match, match + k);
    Node *root = build(0, m - 1);
    for (int i = 0; i < k; i++) {
      int b = match[i].b;
      long long t1 = query(root, 0, m - 1, 0, b - 1),
        t2 = query(root, 0, m - 1, b, b);
      modify(root, 0, m - 1, b, std::max(t1 + match[i].e, t2));
    }
    cout << root->val << '\n';
  }
}
