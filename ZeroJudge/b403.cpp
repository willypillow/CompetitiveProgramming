#include <bits/stdc++.h>
using namespace std;

namespace std {
	template <> struct hash<pair<int, int> > {
		size_t operator()(const pair<int, int> &p) const {
			return hash<int>()(p.first ^ p.second);
		}
	};
}

int n, blkSz, blks, a[100000], nxt[100000], tmpPos[100000];
int64_t f[100000], hashs[1000001], lazy[10000];
bool vis[100000];
unordered_map<pair<int, int>, int> mp;

int blkStart(int b) { return min(b * blkSz, n); }

void build() {
	blkSz = (int)sqrt(n), blks = (n + blkSz - 1) / blkSz;
	for (int i = 0; i < n; i++) mp[{i / blkSz, f[i]}]++;
}

void update(int b, int v) {
	for (int i = blkStart(b); i < blkStart(b + 1); i++) mp[{b, f[i]}] += v;
}

void modify(int l, int r, int64_t val) {
	int lBlk = l / blkSz, rBlk = r / blkSz;
	if (lBlk == rBlk) {
		update(lBlk, -1);
		for (int i = l; i <= r; i++) f[i] ^= val;
		update(lBlk, 1);
	} else {
		update(lBlk, -1), update(rBlk, -1);
		for (int i = l; i < blkStart(lBlk + 1); i++) f[i] ^= val;
		for (int i = blkStart(rBlk); i <= r; i++) f[i] ^= val;
		update(lBlk, 1), update(rBlk, 1);
		for (int i = lBlk + 1; i < rBlk; i++) lazy[i] ^= val;
	}
}

int query(int l, int r, int64_t val) {
	int lBlk = l / blkSz, rBlk = r / blkSz, ret = 0;
	if (lBlk == rBlk) {
		for (int i = l; i <= r; i++) {
			if ((f[i] ^ lazy[lBlk]) == val) ret++;
		}
	} else {
		for (int i = l; i < blkStart(lBlk + 1); i++) {
			if ((f[i] ^ lazy[lBlk]) == val) ret++;
		}
		for (int i = blkStart(rBlk); i <= r; i++) {
			if ((f[i] ^ lazy[rBlk]) == val) ret++;
		}
		for (int i = lBlk + 1; i < rBlk; i++) ret += mp[{i, lazy[i] ^ val}];
	}
	return ret;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	mt19937_64 rng(71229487);
	for (int i = 1; i <= 1000000; i++) hashs[i] = rng();
	cin >> n;
	for (int i = 0; i < n; i++) cin >> a[i];
	memset(tmpPos, -1, sizeof(tmpPos));
	for (int i = n - 1; i >= 0; i--) {
		nxt[i] = tmpPos[a[i]];
		tmpPos[a[i]] = i;
	}
	int64_t ans = 0;
	f[0] = 0, vis[a[0]] = true, ans++;
	for (int i = 1; i < n; i++) {
		if (vis[a[i]]) f[i] = f[i - 1] ^ hashs[a[i]];
		else f[i] = f[i - 1], vis[a[i]] = true;
		if (!f[i]) ans++;
	}
	build();
	for (int i = 1; i < n; i++) {
		if (nxt[i - 1] != -1) modify(nxt[i - 1], n - 1, hashs[a[i - 1]]);
		ans += query(i, n - 1, 0);
	}
	cout << ans << '\n';
}
