#include <stdio.h>

main() {
  int n, m;
  while(scanf("%d %d", &n, &m) != EOF) {
    printf("%d\n", m + ((m==n) ? 0 : 1));
  }
  return 0;
}
