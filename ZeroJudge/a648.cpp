#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1), SAMPLES = 2000;

int x[100], y[100], r[100];

static inline double dist2(double x0, double y0, double x1, double y1) {
	return pow(x1 - x0, 2) + pow(y1 - y0, 2);
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		int n; cin >> n;
		for (int i = 0; i < n; i++) cin >> x[i] >> y[i] >> r[i];
		double ans = 0;
		for (int i = 0; i < n; i++) {
			int t = 0, contact[100], cTop = 0;
			for (int j = 0; j < n; j++) {
				if (i == j) continue;
				if (dist2(x[i], y[i], x[j], y[j]) < pow(r[i] + r[j], 2)) {
					contact[cTop++] = j;
				}
			}
			for (int j = 0; j < SAMPLES; j++) {
				bool fail = false;
				double rad = 2 * PI * j / SAMPLES;
				double nx = x[i] + r[i] * cos(rad), ny = y[i] + r[i] * sin(rad);
				for (int k = 0; k < cTop; k++) {
					int idx = contact[k];
					if (dist2(nx, ny, x[idx], y[idx]) < pow(r[idx], 2)) {
						fail = true;
						break;
					}
				}
				if (!fail) t++;
			}
			ans += 2 * PI * r[i] * t / SAMPLES;
		}
		int e = log10(ans);
		ans = ans / pow(10, e);
		cout << fixed << setprecision(2) << ans << ' ' << e << '\n';
	}
}
