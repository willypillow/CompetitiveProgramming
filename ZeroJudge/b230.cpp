#include <iostream>
using std::cin; using std::cout;

const int MAXA = 100;
bool notConv[3 * MAXA * MAXA] = { false };
int cTop = 0, conv[3000];

int main() {
  for (int a = 1; a < MAXA; a++) {
    for (int b = a + 1; b < MAXA; b++) {
      for (int c = b + 1; c < MAXA; c++) {
        notConv[a * b + b * c + c * a] = true;
      }
    }
  }
  int ma = MAXA - 3, mb = MAXA - 2, mc = MAXA - 1, top = ma * mb + mb * mc + mc * ma;
  for (int i = 1; i <= top; i++) {
    if (!notConv[i]) {
      conv[cTop++] = i;
    }
  }
  for (int k; cin >> k; ) {
    cout << conv[k - 1] << '\n';
  }
}
