#include <bits/stdc++.h>
using namespace std;

int n, k, price[200000];

bool solve(int x) {
	int bkup = x, ans = 1;
	for (int i = 0; i < n; i++) {
		if (x < price[i]) {
			ans++;
			x = bkup;
		}
		if (x < price[i]) return false;
		x -= price[i];
	}
	return ans <= k;
}

int main() {
	cin >> n >> k;
	long long l = 0, r = 0;
	for (int i = 0; i < n; i++) {
		cin >> price[i];
		r += price[i];
	}
	while (l != r) {
		int m = (l + r) >> 1;
		if (solve(m)) r = m;
		else l = m + 1;
	}
	cout << l << '\n';
}
