#include <stdio.h>

main() {
  int arr[9][9];
  while (scanf("%d", arr) != EOF) {
    int i, j, collide = 0;
    int nums[10] = { 0 };
    for (i = 1; i < 81; i++) scanf("%d", (int*)arr+i);

    for (i = 0; i < 9; i++) {
      for (j = 0; j < 9; j++) {
        if (!nums[arr[i][j]]) nums[arr[i][j]] = 1;
        else {
          collide = 1;
          break;
        }
      }
      for (j = 0; j < 10; j++) nums[j] = 0;
    }

    for (i = 0; i < 9; i++) {
      for (j = 0; j < 9; j++) {
        if (!nums[arr[j][i]]) nums[arr[j][i]] = 1;
        else {
          collide = 1;
          break;
        }
      }
      for (j = 0; j < 10; j++) nums[j] = 0;
    }

    for (i = 0; i < 9; i++) {
      for (j = 0; j < 9; j++) {
        if (!nums[arr[i/3*3+j/3][i%3*3+j%3]]) nums[arr[i/3*3+j/3][i%3*3+j%3]] = 1;
        else {
          collide = 1;
          break;
        }
      }
      for (j = 0; j < 10; j++) nums[j] = 0;
    }

    printf("%s\n", collide? "no": "yes");
  }
  return 0;
}
