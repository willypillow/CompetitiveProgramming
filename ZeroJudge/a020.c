#include <stdio.h>

const char LETTERS[] = {10 ,11, 12, 13, 14, 15, 16, 17, 34, 18, 19, 20, 21,
                      22, 35, 23, 24, 25, 26, 27, 28, 29, 32, 30, 31, 33};

main() {
  char str[100], chr[] = "X";
  while (scanf("%s", str) != EOF) {
    int i, res = LETTERS[str[0] - 'A'];
    res = res / 10 + (res % 10) * 9;
    for (i = 1; i < 9; i++) {
      chr[0] = str[i];
      res += atoi(chr) * (9 - i);
    }
    chr[0] = str[9];
    res += atoi(chr);
    printf("%s\n", res % 10 ? "fake" : "real");
  }
  return 0;
}
