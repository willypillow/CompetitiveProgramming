#include <bits/stdc++.h>
using namespace std;

int main() {
	int t; cin >> t;
	for (int ii = 1; ii <= t; ii++) {
		int a, b; cin >> a >> b;
		int ans = 0;
		for (int i = a; i <= b; i++) {
			int x = round(sqrt(i));
			if (fabs(x * x - i) < 1E-8) ans += i;
		}
		cout << "Case " << ii << ": " << ans << '\n';
	}
}
