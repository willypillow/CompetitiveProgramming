#include <iostream>
#include <algorithm>
using std::cin; using std::cout; using std::sort;

struct Task {
  int t1, t2;
  bool operator <(const Task &rhs) const {
    return (t2 != rhs.t2) ? t2 > rhs.t2 : t1 < rhs.t1;
  }
} t[1001];

int main() {
  for (int n; cin >> n; ) {
    int max = 0;
    for (int i = 0; i < n; i++) cin >> t[i].t1 >> t[i].t2;
    sort(t, t + n);
    for (int i = 1; i < n; i++) t[i].t1 += t[i - 1].t1;
    for (int i = 0; i < n; i++) {
      max = std::max(max, t[i].t1 + t[i].t2);
    }
    cout << max << '\n';
  }
}
