#include <bits/stdc++.h>
using namespace std;

pair<int, int> pile[100001];
int dp[100001];

inline bool canMake(int x) {
	switch (x) {
	case 0: case 4: case 7: case 8: case 11:
	case 12: case 14: case 15: case 16:
		return true;
	default:
		return false;
	}
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	for (int i = 1; i <= n; i++) cin >> pile[i].second >> pile[i].first;
	sort(pile + 1, pile + 1 + n);
	for (int i = 1, j = 0, prefMx = 0; i <= n; i++) {
		if (pile[i].first <= 17 && !canMake(pile[i].first)) {
			dp[i] = -2E9;
		} else {
			while (j < i && pile[i].first - pile[j].first > 17) {
				prefMx = max(prefMx, dp[j++]);
			}
			for (int k = i - 1; k >= j; k--) {
				if (canMake(pile[i].first - pile[k].first)) dp[i] = max(dp[i], dp[k]);
			}
			dp[i] = max(dp[i], prefMx) + pile[i].second;
		}
	}
	cout << *max_element(dp, dp + 1 + n) << '\n';
}
