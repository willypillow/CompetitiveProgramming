#include <iostream>
#include <algorithm>
#include <cstring>

int n, m, arr[30], side;
bool vis[30];

int dfs(int pos, int sideCnt, int sum) {
  if (sideCnt == n - 1) return true;
  if (sum) {
    for (int i = pos; i < m; i++) {
      if (vis[i] || sum + arr[i] > side) continue;
      vis[i] = true;
      if (sum + arr[i] < side) {
        if (dfs(i + 1, sideCnt, sum + arr[i])) return true;
      } else {
        if (dfs(i + 1, sideCnt + 1, 0)) return true;
      }
      vis[i] = false;
    }
  } else {
    int i;
    for (i = sideCnt; vis[i] && i < m; i++);
    vis[i] = true;
    if (arr[i] < side) {
      if (dfs(i + 1, sideCnt, arr[i])) return true;
    } else {
      if (dfs(i + 1, sideCnt + 1, 0)) return true;
    }
    vis[i] = false;
  }
  return false;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0); 
  int t;
  std::cin >> t;
  while (t--) {
    memset(vis, false, sizeof(vis));
    int max = 0, sum = 0;
    std::cin >> n >> m;
    for (int i = 0; i < m; i++) {
      std::cin >> arr[i];
      sum += arr[i];
      max = std::max(max, arr[i]);
    }
    side = sum / n;
    if (sum % n != 0 || max > side) std::cout << "0\n";
    else {
      std::sort(arr, arr + m, std::greater<int>());
      std::cout << dfs(0, 0, 0) << '\n';
    }
  }
}
