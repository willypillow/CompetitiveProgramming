#include <bits/stdc++.h>
using namespace std;

int main() {
	string str;
	while (getline(cin, str)) {
		stringstream ss(str);
		long long ans = 0, x;
		while (ss >> x) ans += x;
		cout << ans << '\n';
	}
}
