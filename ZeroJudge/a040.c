#include <stdio.h>
#include <math.h>

main() {
  int n, m, i;
  while(scanf("%d %d", &n, &m) != EOF) {
    int isExist = 0;
    for (i = n; i <= m; i++) {
      int num = i, out = 0, digits = 0;
      while (num) { num /= 10; digits++; }
      num = i;
      while (num) {
        int digit = num % 10;
        out += pow(digit, digits);
        num /= 10;
      }
      if (i == out) {
        printf("%d ", i);
        isExist = 1;
      }
    }
    if (!isExist) printf("none");
    printf("\n");
  }
  return 0;
}

