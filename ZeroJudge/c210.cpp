#include <bits/stdc++.h>
using namespace std;

vector<int> toFront;
bool mov[1000010];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int64_t n, m; cin >> n >> m;
	for (int i = n; i > 0; i--) {
		if (m >= i - 1) {
			m -= i - 1;
			toFront.push_back(i);
			mov[i] = true;
		}
	}
	bool first = true;
	for (int x: toFront) {
		if (!first) cout << ' ';
		first = false;
		cout << x;
	}
	for (int i = 1; i <= n; i++) {
		if (!mov[i]) {
			if (!first) cout << ' ';
			first = false;
			cout << i;
		}
	}
	cout << '\n';
}
