#include <stdio.h>

main() {
  int input;
  while (scanf("%d", &input) != EOF) {
    if (input == 0) {
      printf("0");
      break;
    }
    int i;
    int isZero = 1;
    for (i = 0; input != 0; i++) {
      if ((input % 10 == 0) && isZero) {
        input /= 10;
        continue;
      }
      isZero = 0;
      printf("%d", input % 10);
      input /= 10;
    }
    printf("\n");
  }
  return 0;
}
