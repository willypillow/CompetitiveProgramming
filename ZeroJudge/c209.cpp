#include <bits/stdc++.h>
using namespace std;

uint64_t arr[110];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	for (int n; cin >> n; ) {
		for (int i = 0; i < n; i++) cin >> arr[i];
		int groupRel[65] = { 0 };
		bool fail = false;
		for (int i = 1; i < n; i++) {
			bool rel = arr[i - 1] < arr[i];
			if (arr[i - 1] == arr[i]) fail = true;
			int type = 63 - __builtin_clzll(arr[i - 1] ^ arr[i]);
			if (!fail) {
				if (groupRel[type]) {
					if (groupRel[type] != rel + 1) fail = true;
				} else {
					groupRel[type] = rel + 1;
				}
			}
		}
		int cnt = 0;
		for (int i = 0; i < 60; i++) cnt += (groupRel[i] == 0);
		cout << (fail ? 0 : (1LL << cnt)) << '\n';
	}
}
