#include <stdio.h>
#include <math.h>

main() {
  int a, b, c;
  while (scanf("%d %d %d", &a, &b, &c) != EOF) {
    if (b * b - (4 * a * c) > 0) printf("Two different roots x1=%d , x2=%d\n", (int)((-b+sqrt((double)b*b-4*a*c))/(a*2)), (int)(-b-sqrt(b*b-4*a*c))/(a*2));
    else if (b * b - (4 * a * c) == 0) printf("Two same roots x=%d\n", -b/(a*2));
    else printf("No real root\n");
  }
  return 0;
}
