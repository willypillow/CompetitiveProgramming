#include <bits/stdc++.h>
using namespace std;

const int MAXN = 5E5 + 10;

vector<int> edges1[MAXN], edges2[MAXN], edges3[MAXN], edges4[MAXN], fin;
int scc[MAXN], sccSz[MAXN], size[MAXN], mxChild[MAXN], arr[MAXN], c1, c2;
bool vis[MAXN], ccVis[MAXN], chainVis[MAXN];
map<pair<int, int>, int> szList;
vector<pair<int, int> > szVect;
set<int> useSet;

void dfs1(int x) {
	vis[x] = true;
	for (int y: edges1[x]) {
		if (!vis[y]) dfs1(y);
	}
	fin.push_back(x);
}

void dfs2(int x, int color) {
	scc[x] = color;
	sccSz[color]++;
	for (int y: edges2[x]) {
		if (!scc[y]) dfs2(y, color);
	}
}

int dfs4(int x, int p) {
	size[x] = sccSz[x];
	for (int y: edges4[x]) {
		if (y != p) size[x] = max(size[x], dfs4(y, x) + sccSz[x]);
	}
	return size[x];
}

void decomp(int x, int p) {
	int mx = 0;
	for (int y: edges4[x]) {
		if (y == p) continue;
		mx = max(mx, size[y]);
		decomp(y, x);
	}
	for (int y: edges4[x]) {
		if (y != p && size[y] == mx) {
			mxChild[x] = y;
			break;
		}
	}
}

void calcSz(int x, int p, int curc1, int curc2) {
	if (mxChild[x]) calcSz(mxChild[x], x, curc1, curc2);
	szList[{ curc1, curc2 }] = max(szList[{ curc1, curc2 }], size[x]);
	for (int y: edges4[x]) {
		if (y != p && y != mxChild[x]) calcSz(y, x, curc1, ++c2);
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, k; cin >> n >> k;
	for (int i = 1; i <= n; i++) {
		cin >> arr[i];
		edges1[i].push_back(arr[i]);
		edges2[arr[i]].push_back(i);
	}
	for (int i = 2; i <= n; i++) {
		if (!vis[i]) dfs1(i);
	}
	int color = 0;
	for (int i = fin.size() - 1; i >= 0; i--) {
		if (!scc[fin[i]]) dfs2(fin[i], ++color);
	}
	for (int x = 2; x <= n; x++) {
		for (int y: edges1[x]) {
			if (scc[x] == scc[y]) continue;
			edges3[scc[x]].push_back(scc[y]);
			edges4[scc[y]].push_back(scc[x]);
		}
	}
	for (int i = 1; i <= color; i++) {
		sort(edges3[i].begin(), edges3[i].end());
		edges3[i].resize(
			unique(edges3[i].begin(), edges3[i].end()) - edges3[i].begin());
		sort(edges4[i].begin(), edges4[i].end());
		edges4[i].resize(
			unique(edges4[i].begin(), edges4[i].end()) - edges4[i].begin());
	}
	for (int i = 1; i <= color; i++) {
		if (!edges3[i].size()) dfs4(i, i);
	}
	for (int i = 1; i <= color; i++) {
		if (!edges3[i].size()) {
			decomp(i, i);
			calcSz(i, i, ++c1, ++c2);
		}
	}
	for (auto p: szList) szVect.push_back({ p.second, p.first.first });
	sort(szVect.begin(), szVect.end());
	int i, sum = 0, cnt = 0;
	for (i = szVect.size() - 1; i >= 0; i--) {
		if (ccVis[szVect[i].second]) continue;
		sum += szVect[i].first;
		ccVis[szVect[i].second] = true;
		chainVis[i] = true;
	}
	for (i = szVect.size() - 1; sum < k - 1; i--) {
		if (chainVis[i]) continue;
		sum += szVect[i].first;
		cnt++;
	}
	cout << c1 + cnt + (k > 1) << '\n';
}
