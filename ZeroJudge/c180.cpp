#include <bits/stdc++.h>
using namespace std;

const int64_t INF = 1E18;
const int MAXN = 3E5 + 5;
typedef tuple<int, int, int> TIII;

vector<TIII> event;
int64_t arr[MAXN];
pair<int64_t, int64_t> st[MAXN * 4]; // (max, lazy)

void build(int id, int l, int r) {
	if (l == r) {
		st[id].first = arr[l];
	} else {
		int m = (l + r) >> 1;
		build(id << 1, l, m); build(id << 1 | 1, m + 1, r);
		st[id].first = max(st[id << 1].first, st[id << 1 | 1].first);
	}
}

void modify(int id, int l, int r, int qL, int qR, int v) {
	if (qL <= l && r <= qR) {
		st[id].second += v;
	} else {
		int m = (l + r) >> 1;
		if (qL <= m) modify(id << 1, l, m, qL, min(m, qR), v);
		if (m < qR) modify(id << 1 | 1, m + 1, r, max(qL, m), qR, v);
		st[id].first = max(st[id << 1].first + st[id << 1].second,
			st[id << 1 | 1].first + st[id << 1 | 1].second);
	}
}

int64_t queryMax(int id, int l, int r, int qL, int qR) {
	if (qL <= l && r <= qR) return st[id].first + st[id].second;
	st[id << 1].first += st[id].second;
	st[id << 1 | 1].first += st[id].second;
	st[id].first += st[id].second;
	st[id].second = 0;
	int m = (l + r) >> 1;
	int64_t t = -INF;
	if (qL <= m) t = max(t, queryMax(id << 1, l, m, qL, min(m, qR)));
	if (m < qR) t = max(t, queryMax(id << 1 | 1, m + 1, r, max(qL, m), qR));
	return t;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	for (int i = 1; i <= n; i++) cin >> arr[i];
	for (int i = 2; i <= n; i++) arr[i] += arr[i - 1];
	event.reserve(m);
	for (int i = 0; i < m; i++) {
		int b, c, d; cin >> b >> c >> d;
		event.push_back(make_tuple(b, c, d));
	}
	sort(event.begin(), event.end(), [](const TIII &l, const TIII &r) { return get<0>(l) > get<0>(r); });
	build(1, 1, n);
	int64_t ans = -INF;
	auto it = event.begin();
	for (int i = n; i > 0; i--) {
		while (it != event.end() && get<0>(*it) == i) {
			modify(1, 1, n, get<1>(*it), n, get<2>(*it));
			it++;
		}
		ans = max(ans, queryMax(1, 1, n, i, n) - arr[i - 1]);
	}
	cout << ans << '\n';
}
