#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E5 + 10, MOD = 1E5;

vector<int64_t> st[MAXN * 4];
int64_t arr[MAXN];

void build(int id, int l, int r) {
	if (l == r) {
		st[id] = vector<int64_t> { arr[l] };
	} else {
		int m = (l + r) >> 1;
		build(id << 1, l, m); build(id << 1 | 1, m + 1, r);
		st[id].resize(st[id << 1].size() + st[id << 1 | 1].size());
		merge(st[id << 1].begin(), st[id << 1].end(),
			st[id << 1 | 1].begin(), st[id << 1 | 1].end(), st[id].begin());
	}
}

int query(int id, int l, int r, int qL, int qR, int k) {
	if (qL <= l && r <= qR) {
		return upper_bound(st[id].begin(), st[id].end(), k) - st[id].begin();
	} else {
		int m = (l + r) >> 1, t = 0;
		if (qL <= m) t += query(id << 1, l, m, qL, min(m, qR), k);
		if (m < qR) t += query(id << 1 | 1, m + 1, r, max(qL, m), qR, k);
		return t;
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	for (int i = 1; i <= n; i++) cin >> arr[i];
	build(1, 1, n);
	for (int i = 1; i <= n; i++) arr[i] += arr[i - 1];
	int totK = 0;
	while (m--) {
		int op; cin >> op;
		if (op == 1) {
			int k; cin >> k;
			totK = (totK + k) % MOD;
		} else {
			int l, r; cin >> l >> r;
			// x + totK > 1E6 => x > 1E6 - totK
			int t = query(1, 1, n, l, r, MOD - totK);
			int64_t ans = arr[r] - arr[l - 1];
			ans += (int64_t)totK * (r - l + 1);
			ans -= (int64_t)MOD * (r - l + 1 - t);
			cout << ans << '\n';
		}
	}
}
