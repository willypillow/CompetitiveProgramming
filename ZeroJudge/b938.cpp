#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace std;
using namespace __gnu_pbds;

typedef tree<int, null_type, less<int>, rb_tree_tag,
	tree_order_statistics_node_update> Set;

int main() {
	int n, m; cin >> n >> m;
	Set s;
	for (int i = 1; i <= n; i++) s.insert(i);
	while (m--) {
		int k; cin >> k;
		if (s.find(k) == s.end() || s.order_of_key(k) >= s.size() - 1) {
			cout << "0u0 ...... ?\n";
		} else {
			int idx = s.order_of_key(k);
			auto it = s.find_by_order(idx + 1);
			cout << *it << '\n';
			s.erase(it);
		}
	}
}
