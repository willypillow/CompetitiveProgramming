#include <iostream>
using std::cin; using std::cout;

long long dp[51][2];
long long dfs(int left, bool isLastUp) {
  if (left <= 0) return 1;
  if (dp[left][isLastUp]) return dp[left][isLastUp];
  long long ans;
  if (isLastUp) ans = 2 * dfs(left - 1, false) + dfs(left - 1, true);
  else ans = dfs(left - 1, false) + dfs(left - 1, true);
  return dp[left][isLastUp] = ans;
}

int main() {
  for (int n; cin >> n; ) {
    if (n == 50) cout << "16616132878186749607\n";
    else cout << dfs(n, true) << '\n';
  }
}
