#include <stdio.h>

main() {
  int month, day;
  while( scanf("%d %d", &month, &day) == 2) {
    int res = (month * 2 + day) % 3;
    switch(res) {
    case 0:
      printf("普通\n");
      break;
    case 1:
      printf("吉\n");
      break;
    case 2:
      printf("大吉\n");
      break;
    }
  }
  return 0;
}
