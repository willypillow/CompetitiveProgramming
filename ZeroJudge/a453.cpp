#include <iostream>
#include <cmath>
using std::cin; using std::cout;

bool isInt(double x) {
  return fabs(round(x) - x) < 1.0E-6;
}

int main() {
  int t; cin >> t;
  while (t--) {
    int a, b, c;
    cin >> a >> b >> c;
    double res = b * b - 4 * a * c;
    if (isInt(sqrt(res))) cout << "Yes\n";
    else cout << "No\n";
  }
}
