#include <iostream>
using namespace std;

typedef long long LL;

LL m;

LL left(LL x) {
	while (x >= m * 2) x /= 2;
	return x;
}

LL dfs(LL x) {
	if (x < m) return 0;
	if (x < m * 2) return 1;
	return 2 * dfs(x / 2) + (left(x) + 1 == m * 2 && x % 2 == 1);
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	LL n;
	while (cin >> n >> m) {
		cout << dfs(n) << '\n';
	}
}
