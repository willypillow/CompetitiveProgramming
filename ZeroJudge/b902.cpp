#include <iostream>
using std::cin; using std::cout; using std::swap;

bool win(long long a, long long b) {
  if (a < b) return win(b, a);
  if (a % 2 == 1) return b % 2 == 0;
  if (b % 2 == 1) return a % 2 == 0;
  if (b == 0) return a % 2;
  long long cnt = a / b;
  if (cnt % 2 == 1) return !win(b, a % b);
  else return win(b, a % b);
  //return !win(a - 1, b) || !win(a, b - 1) || !win(a - b, b);
}

int main() {
  int t; cin >> t;
  while (t--) {
    long long a, b; cin >> a >> b;
    cout << (win(a, b) ? ">//<\n" : ">\\\\<\n");
  }
}
