#include <stdio.h>

main() {
  int num;
  int buf[100];
  int top;
  while (scanf("%d", &num) != EOF) {
    top = 0;
    do {
      buf[top++] = num % 2;
      num /= 2;
    } while (num != 0);
    for (; top > 0; top--) printf("%d", buf[top-1]);
    printf("\n");
  }
  return 0;
}
