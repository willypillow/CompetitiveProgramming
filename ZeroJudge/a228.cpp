#include <bits/stdc++.h>
using namespace std;

const int MOD = 1000000007;
long long dp[2][1 << 12];
int field[12][12];

int main() {
	int cases = 0;
	int t; cin >> t;
	while (t--) {
		int n, m; cin >> n >> m;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) cin >> field[i][j];
		}
		int dpIdx = 0;
		memset(dp[dpIdx], 0, sizeof(dp[dpIdx]));
		dp[dpIdx][0] = 1;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				memset(dp[!dpIdx], 0, sizeof(dp[!dpIdx]));
				for (int k = (1 << (m + 1)) - 1; k >= 0; k--) {
					int p = 1 << j, q = 1 << (j + 1), tx = (k & p) != 0, ty = (k & q) != 0;
					if (field[i][j]) {
						dp[!dpIdx][k] = dp[dpIdx][k ^ p ^ q];
						if (tx ^ ty) dp[!dpIdx][k] += dp[dpIdx][k];
					} else {
						dp[!dpIdx][k] = tx | ty ? 0 : dp[dpIdx][k];
					}
				}
				dpIdx = !dpIdx;
			}
			memset(dp[!dpIdx], 0, sizeof(dp[!dpIdx]));
			for (int j = 0; j < (1 << m); j++) dp[!dpIdx][j << 1] = dp[dpIdx][j] % MOD;
			dpIdx = !dpIdx;
		}
		cout << "Case " << ++cases << ": " << dp[dpIdx][0] << '\n';
	}
}
