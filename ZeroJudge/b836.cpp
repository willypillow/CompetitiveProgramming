#include <iostream>
using namespace std;

int main() {
	int n, m;
	while (scanf("%d%d", &n, &m) != EOF) {
		if (!m) n = 0;
		else for (int i = 1; n > 0; i += m) n -= i;
		if (n == 0) cout << "Go Kevin!!\n";
		else cout << "No Stop!!\n";
	}
}
