#include <iostream>
#include <cstring>
#include <queue>
using namespace std;

struct G {
	int n, d;
	bool operator <(const G &rhs) const {
		return d > rhs.d;
	}
};
vector<vector<G> > g;
bool isK[300001];
int dist[300001], ks[300001];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int n, m, k;
	cin >> n >> m >> k;
	g.resize(n + 1);
	for (int i = 0, x, y, d; i < m; i++) {
		cin >> x >> y >> d;
		g[x].push_back(G { y, d });
		g[y].push_back(G { x, d });
	}
	for (int i = 0; i < k; i++) {
		cin >> ks[i];
		isK[ks[i]] = true;
	}
	int ans = 1 << 30;
	memset(dist, 0x5f, sizeof(dist));
	for (int i = 0; i < k; i++) {
		priority_queue<G> q;
		q.push(G { ks[i], 0 });
		dist[ks[i]] = 0;
		int res = 1 << 30;
		while (!q.empty()) {
			G cur = q.top(); q.pop();
			if (dist[cur.n] < cur.d) continue;
			if (cur.d > ans) break;
			if (cur.n != ks[i] && isK[cur.n]) { res = cur.d; break; }
			for (G &e: g[cur.n]) {
				int newD = e.d + cur.d;
				if (newD < dist[e.n]) {
					dist[e.n] = newD;
					q.push(G { e.n, newD });
				}
			}
		}
		ans = min(ans, res);
	}
	cout << ans << '\n';
}
