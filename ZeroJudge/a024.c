#include <iostream>
using namespace std;
int main() {
    int m, n;
    while (cin >> m >> n) {
        while (n != 0) {
            int t = n;
            n = m % n;
            m = t;
        }
        cout << m << endl;
    }
}
