#include <iostream>
#include <set>
#include <vector>
#include <queue>
#include <string>
#include <cstring>
using namespace std;

long long dp[1000][1000];

long long dfs(int left, int last) {
	if (!left) return 1;
	if (dp[left][last]) return dp[left][last];
	long long ans = 0;
	for (int i = min(left, last); i > 0; i--) {
		ans += dfs(left - i, i);
	}
	return dp[left][last] = ans;
}

int main()  {
	int n;
	while (cin >> n) {
		memset(dp, 0, sizeof(dp));
		cout << dfs(n, n) << '\n';
	}
}
