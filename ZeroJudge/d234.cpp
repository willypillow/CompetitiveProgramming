#include <iostream>
#include <cstring>
using std::cin; using std::cout;

bool canMake[20010];

int main() {
  for (int a, b, c; cin >> a >> b >> c; ) {
    memset(canMake, false, sizeof(canMake));
    for (int i = 0; i * a <= c * 2; i++) {
      for (int j = 0; i * a + j * b <= c * 2; j++) {
        canMake[i * a + j * b] = true;
      }
    }
    bool fail = false;
    for (int i = c; i <= c * 2; i++) {
      if (!canMake[i]) {
        fail = true;
        break;
      }
    }
    cout << (fail ? "No\n" : "Yes\n");
  }
}
