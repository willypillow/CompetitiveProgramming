#include <stdio.h>

main() {
  int width, height, i, j;
  int arr[100][100];
  while (scanf("%d %d", &width, &height) != EOF) {
    for (i = 0; i < width; i++) {
      for (j = 0; j < height; j++) {
        scanf("%d", &(arr[i][j]));
      }
    }
    for (j = 0; j < height; j++) {
      for (i = 0; i < width; i++) {
        printf("%d ", arr[i][j]);
      }
      printf("\n");
    }
  }
  return 0;
}
