#include <iostream>
using std::cin; using std::cout;

long long powMod(int x, int e, int m) {
  if (e == 0) return 1;
  long long t = powMod(x, e / 2, m);
  t = t * t % m;
  if (e & 1) t = t * x % m;
  return t;
}

bool millRab(int n, int a) {
  int s = n - 1, r = 0;
  while ((s & 1) == 0) {
    s >>= 1;
    r++;
  }
  long long x = powMod(a, s, n);
  if (x == 1) return true;
  for (int i = 0; i < r; i++, x = x * x % n) {
    if (x == n - 1) return true;
  }
  return false;
}


bool test(int n) {
  if (n == 2 || n == 3 || n == 5 || n == 7) return true;
  if (n == 1) return false;
  for (int i = 2; i < 1000 && i < n; i++) {
    if (n % i == 0) return false;
  }
  return millRab(n, 2) && millRab(n, 3) && millRab(n, 5) && millRab(n, 7);
}

int main() {
  for (int n; cin >> n; ) {
    cout << (test(n) ? "質數\n" : "非質數\n");
  }
}
