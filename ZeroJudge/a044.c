#include <stdio.h>
#include <math.h>

main() {
  int num;
  while (scanf("%d", &num) != EOF) {
    printf("%d\n", 1 + num * (num * num + 5) / 6);
  }
  return 0;
}
