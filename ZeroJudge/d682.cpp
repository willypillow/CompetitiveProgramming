#include <iostream>
#include <cstring>
using std::cin; using std::cout; using std::max;

struct Player {
  int cost, value;
} player[51][51];

int m, n, p, dp[51][10001] = { 0 };

int dfs(int cur, int money) {
  if (cur < 0) return 0;
  if (dp[cur][money]) return dp[cur][money];
  int res = dfs(cur - 1, money);
  for (int i = 0; i < p; i++) {
    if (money < player[cur][i].cost) continue;
    int tmp = dfs(cur - 1, money - player[cur][i].cost) + player[cur][i].value;
    res = max(res, tmp);
  }
  return dp[cur][money] = res;
}

int main() {
  cin >> m >> n >> p;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < p; j++) {
      cin >> player[i][j].cost >> player[i][j].value;
    }
  }
  cout << dfs(n - 1, m) << '\n';
}
