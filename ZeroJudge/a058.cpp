#include <bits/stdc++.h>
using namespace std;
int main() {
	int n; cin >> n;
	int cnt[3] = { 0 };
	while (n--) {
		int x; cin >> x;
		cnt[x % 3]++;
	}
	cout << cnt[0] << ' ' << cnt[1] << ' ' << cnt[2] << '\n';
}
