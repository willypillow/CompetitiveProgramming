#include <iostream>
using std::cin; using std::cout; using std::fill; using std::max;

struct Task {
  int len, depCnt, dep[1001];
} task[1001];
bool start[1001];
int dp[1001];

int dfs(int x) {
  if (dp[x]) return dp[x];
  int res = 0;
  for (int i = 0; i < task[x].depCnt; i++) {
    res = max(res, dfs(task[x].dep[i]));
  }
  return dp[x] = res + task[x].len;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int t; cin >> t;
  while (t--) {
    fill(dp, dp + 1001, 0); fill(start, start + 1001, true);
    int n, ans = 0;
    cin >> n;
    for (int i = 1; i <= n; i++) {
      cin >> task[i].len >> task[i].depCnt;
      for (int j = 0; j < task[i].depCnt; j++) {
        cin >> task[i].dep[j];
        start[task[i].dep[j]] = false;
      }
    }
    for (int i = 1; i <= n; i++) {
      if (start[i]) ans = max(ans, dfs(i));
    }
    cout << ans << '\n';
  }
}
