#include <iostream>
#include <algorithm>

struct P {
  int x, y;
  bool operator <(const P &rhs) const {
    return x < rhs.x;
  }
} h[100010];
int arr[100010], factor[200010];

bool cmpY(const P &lhs, const P &rhs) {
  return lhs.y < rhs.y;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  long long n, m;
  while (std::cin >> n >> m) {
    int dTop = 0, hTop = 0;
    long long ans = 0;
    for (int i = 0; i < n; i++) std::cin >> arr[i];
    for (int i = 1; (long long)i * i <= m; i++) {
      if (m % i == 0) factor[dTop++] = i;
    }
    int back = ((long long)factor[dTop - 1] * factor[dTop - 1] == m) ? dTop - 2 : dTop - 1;
    for (; back >= 0; back--) {
      factor[dTop++] = m / factor[back];
    }
    for (int i = 0; i < n; i++) {
      if (arr[i] == 0) { h[0] = P { i, 0}; hTop = 1; continue; }
      int pos = std::lower_bound(h, h + hTop, P { 0, arr[i] }, cmpY) - h;
      hTop -= hTop - pos;
      h[hTop++] = P { i, arr[i] };
      int minW = (m % arr[i]) ? (m / arr[i] + 1) : (m / arr[i]);
      int j = std::lower_bound(factor, factor + dTop, minW) - factor;
      for (; factor[j] <= i + 1 && j < dTop; j++) {
        P *p = std::lower_bound(h, h + hTop, P { i - factor[j] + 1, 0 });
        if ((long long)p->y * factor[j] >= m) {
          ans += p->y - (m / factor[j]) + 1;
        }
      }
    }
    std::cout << ans << '\n';
  }
}
