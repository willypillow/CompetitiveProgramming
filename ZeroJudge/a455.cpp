#include <iostream>
#include <algorithm>
using std::cin; using std::cout; using std::sort;

int box[60], item[10010], m, n, itemSum[10010] = { 0 };

bool check(int cur, int prevBox) {
  if (cur < 0) return true;
  int iStart = (prevBox != -1 && item[cur] == item[cur + 1]) ? prevBox : 0;
  for (int i = iStart; i < m; i++) {
    if (box[i] < item[cur]) continue;
    box[i] -= item[cur];
    bool res = check(cur - 1, i);
    box[i] += item[cur];
    if (res) return true;
  }
  return false;
}

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  while (cin >> m >> n) {
    int boxSum = 0;
    for (int i = 0; i < m; i++) { cin >> box[i]; boxSum += box[i]; }
    for (int j = 0; j < n; j++) cin >> item[j];
    sort(item, item + n);
    for (int i = 1; i <= n; i++) itemSum[i] = item[i - 1] + itemSum[i - 1];
    int l = 0, r = n;
    while (l != r) {
      int m = (l + r) / 2 + 1;
      if (itemSum[m] <= boxSum && check(m - 1, -1)) l = m;
      else r = m - 1;
    }
    cout << l << '\n';
  }
}
