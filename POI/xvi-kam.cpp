#include <bits/stdc++.h>
using namespace std;

int a[1001];

int main() {
	int t; cin >> t;
	while (t--) {
		int n, ans = 0; cin >> n;
		for (int i = 0; i < n; i++) cin >> a[i];
		for (int i = n - 1; i > 0; i -= 2) ans ^= (a[i] - a[i - 1]);
		if (n & 1) ans ^= a[0];
		cout << (ans ? "TAK\n" : "NIE\n");
	}
}
