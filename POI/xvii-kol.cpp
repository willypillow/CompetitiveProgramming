#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E5 + 1;
typedef pair<int, int> PII;

int par[MAXN * 2], order[MAXN], parAns[MAXN * 2], ans[MAXN];
bool done[MAXN];
set<PII> segs;

int find(int x) {
	return (par[x] == x) ? x : (par[x] = find(par[x]));
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int n; cin >> n;
	for (int i = 1; i <= n * 2; i++) par[i] = i;
	int next = 1;
	bool fail = false;
	for (int i = 1; i <= n; i++) {
		cin >> order[i];
		done[order[i]] = true;
		while (done[next]) next++;
		while (!segs.empty() && segs.begin()->first < next) segs.erase(segs.begin());
		if (!segs.empty() && segs.begin()->first < order[i]) {
			int first0 = segs.begin()->first, first1 = first0 + n, x1 = order[i] + n;
			for (set<PII>::iterator it1 = segs.begin(), it2; it1 != segs.end(); ) {
				it2 = it1++;
				if (it1 == segs.end() || it1->first > order[i]) break;
				int a0 = it2->first, b0 = it1->first, a1 = a0 + n, b1 = b0 + n;
				int pa0 = find(a0), pb0 = find(b0), pa1 = find(a1), pb1 = find(b1);
				if (pa0 == pb1 || pb0 == pa1) {
					fail = true;
					goto End;
				}
				par[pa0] = pb0; par[pa1] = pb1;
				if (it2->second + 1 == it1->first) {
					PII t = *it2;
					t.second = it1->second;
					segs.erase(it1); segs.erase(it2);
					it1 = segs.insert(t).first;
				}
			}
			int pf0 = find(first0), pf1 = find(first1),
				px0 = find(order[i]), px1 = find(x1);
			if (pf0 == px0 || pf1 == px1) {
				fail = true;
				goto End;
			}
			par[pf0] = px1; par[pf1] = px0;
		}
		segs.insert(make_pair(order[i], order[i]));
	}
End:
	if (fail) {
		cout << "NIE\n";
	} else {
		for (int i = 1; i <= n; i++) {
			int i1 = i + n, pi0 = find(i), pi1 = find(i1);
			if (!parAns[pi0] || parAns[pi0] == 1) {
				parAns[pi0] = 1; parAns[(pi0 > n) ? (pi0 - n) : (pi0 + n)] = 2;
				ans[i] = 1;
			} else {
				parAns[pi1] = 1; parAns[(pi1 > n) ? (pi1 - n) : (pi1 + n)] = 2;
				ans[i] = 2;
			}
		}
		cout << "TAK\n";
		for (int i = 1; i < n; i++) cout << ans[order[i]] << ' ';
		cout << ans[order[n]] << '\n';
	}
}
