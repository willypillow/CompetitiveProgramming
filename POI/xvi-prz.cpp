#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E5 + 1, MOD = 1E5 + 3;
typedef bitset<101> Hash;
typedef pair<int, int> PII;
typedef unsigned PP;

int cntX[101], cntY[101], xs[MAXN], ys[MAXN], maxD = 0, n, m;
vector<PP> ids[MOD];
vector<bool> vals[MOD];

void insert(PP id, bool v) {
	int bucket = id % MOD;
	ids[bucket].push_back(id);
	vals[bucket].push_back(v);
	maxD++;
}

int get(PP id) {
	int bucket = id % MOD;
	for (int i = 0; i < (int)ids[bucket].size(); i++) {
		if (ids[bucket][i] == id) return vals[bucket][i];
	}
	return -1;
}

bool f(Hash hashX, Hash hashY, int lX, int rX, int lY, int rY, int d) {
	if (hashX != hashY) return false;
	if (hashX.count() == 1 && hashY.count() == 1) return true;
	PP p = (unsigned)rX * MAXN + rY;
	int dpRes = get(p);
	if (dpRes != -1) return dpRes;
	int i, j;
	Hash pX = hashX, pY = hashY;
	for (i = rX; i >= lX; i--) {
		cntX[xs[i]]--;
		if (!cntX[xs[i]]) {
			pX[xs[i]] = false;
			break;
		}
	}
	for (j = rY; j >= lY; j--) {
		cntY[ys[j]]--;
		if (!cntY[ys[j]]) {
			pY[ys[j]] = false;
			break;
		}
	}
	bool res = f(pX, pY, lX, i, lY, j, d + 1);
	for (; i <= rX; i++) cntX[xs[i]]++;
	for (; j <= rY; j++) cntY[ys[j]]++;
	if (!res) {
		insert(p, false);
		return false;
	}
	Hash sX = hashX, sY = hashY;
	for (i = lX; i <= rX; i++) {
		cntX[xs[i]]--;
		if (!cntX[xs[i]]) {
			sX[xs[i]] = false;
			break;
		}
	}
	for (j = lY; j <= rY; j++) {
		cntY[ys[j]]--;
		if (!cntY[ys[j]]) {
			sY[ys[j]] = false;
			break;
		}
	}
	res = f(sX, sY, i, rX, j, rY, d + 1);
	for (; i >= lX; i--) cntX[xs[i]]++;
	for (; j >= lY; j--) cntY[ys[j]]++;
	insert(p, res);
	return res;
}

int main() {
	int t; cin >> t;
	while (t--) {
		memset(cntX, 0, sizeof(cntX)); memset(cntY, 0, sizeof(cntY));
		for (int i = 0; i < MOD; i++) {
			ids[i].clear(); vals[i].clear();
		}
		int n, m; cin >> n >> m;
		maxD = 0;
		Hash hashX, hashY;
		for (int i = 0; i < n; i++) {
			cin >> xs[i];
			hashX[xs[i]] = true;
			cntX[xs[i]]++;
		}
		for (int i = 0; i < m; i++) {
			cin >> ys[i];
			hashY[ys[i]] = true;
			cntY[ys[i]]++;
		}
		cout << f(hashX, hashY, 0, n - 1, 0, m - 1, 0) << '\n';
		//cout << maxD << '\n';
	}
}
