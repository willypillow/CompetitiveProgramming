#include <bits/stdc++.h>
using namespace std;

typedef long long LL;
const int MAXN = 1E6 + 10, INF = 1E9;

int cost[MAXN], orig[MAXN], invOrig[MAXN], dest[MAXN];
bool vis[MAXN];

LL dfs(int x, int *mn, int *cnt) {
	vis[x] = true;
	int cur = orig[x];
	*mn = min(*mn, cost[cur]);
	*cnt += 1;
	if (!vis[invOrig[dest[x]]]) return dfs(invOrig[dest[x]], mn, cnt) + cost[cur];
	return cost[cur];
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, globalMin = INF;
	cin >> n;
	for (int i = 1; i <= n; i++) {
		cin >> cost[i];
		globalMin = min(globalMin, cost[i]);
	}
	for (int i = 1; i <= n; i++) {
		cin >> orig[i];
		invOrig[orig[i]] = i;
	}
	for (int i = 1; i <= n; i++) cin >> dest[i];
	LL ans = 0;
	for (int i = 1; i <= n; i++) {
		if (vis[i]) continue;
		int mn = INF, cnt = 0;
		LL sum = dfs(i, &mn, &cnt);
		ans += min((LL)mn * (cnt - 2) + sum, (LL)globalMin * (cnt + 1) + sum + mn);
	}
	cout << ans << '\n';
}
