#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E5 + 10, MAXK = 20 + 2;

vector<int> g[MAXN];
long long need[MAXN][MAXK], potential[MAXN][MAXK], ans, n, s, k;

void dfs(int x, int p) {
	need[x][0] = 1;
	for (int idx = 0; idx < (int)g[x].size(); idx++) {
		int y = g[x][idx];
		if (y == p) continue;
		dfs(y, x);
		for (int i = 0; i < k; i++) need[x][i + 1] += need[y][i];
		for (int i = 0; i < k; i++) potential[x][i] += potential[y][i + 1];
	}
	long long t = (need[x][k] + s - 1) / s;
	ans += t;
	potential[x][k] += t * s;
	for (int i = 0; i <= k; i++) {
		if (!potential[x][i]) continue;
		for (int j = i; j >= i - 1 || (x == 1 && j >= 0); j--) {
			long long d = min(potential[x][i], need[x][j]);
			potential[x][i] -= d;
			need[x][j] -= d;
		}
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	cin >> n >> s >> k;
	for (int i = 1, x, y; i < n; i++) {
		cin >> x >> y;
		g[x].push_back(y); g[y].push_back(x);
	}
	dfs(1, 1);
	long long t = 0;
	for (int i = 0; i <= k; i++) t += need[1][i];
	ans += (t + s - 1) / s;
	cout << ans << '\n';
}
