#include <iostream>
#include <cstring>
using namespace std;

int ans[110], par[110][20000];
bool can[110][20000], val[110][20000];

int main() {
	int t; cin >> t;
	while (t--) {
		memset(can, false, sizeof(can));
		int n, i = 0;
		cin >> n;
		if (n == 1) {
			cout << "1\n";
			continue;
		}
		for (i = 0; ;i++) {
			for (int j = 0; j < n; j++) {
				if (j == 0 || can[i][j]) {
					can[i + 1][j * 10 % n] = true;
					par[i + 1][j * 10 % n] = j;
					val[i + 1][j * 10 % n] = 0;
					can[i + 1][(j * 10 + 1) % n] = true;
					par[i + 1][(j * 10 + 1) % n] = j;
					val[i + 1][(j * 10 + 1) % n] = 1;
					if (j != 0 && (j * 10 % n == 0 || (j * 10 + 1) % n == 0)) goto End;
				}
			}
		}
End:
		int p = 0, top = 0;
		do {
			ans[top++] = val[i + 1][p];
			p = par[i + 1][p];
			i--;
		} while (p);
		for (int i = top - 1; i >= 0; i--) cout << ans[i];
		cout << '\n';
	}
}
