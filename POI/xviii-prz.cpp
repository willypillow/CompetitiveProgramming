#include <iostream>
#include <vector>
using namespace std;

const int MAXN = 2010;

struct Move {
	Move(char _m, int _val): m(_m), val(_val) {}
	char m;
	int val;
};
vector<Move> moves, ansMoves;
int n, pos[MAXN], at[MAXN], shift = 0;

int getPos(int x) {
	return (pos[x] + shift) % n;
}

int getAt(int x) {
	return at[(x - shift + n) % n];
}

void print() {
	for (int i = 0; i < n; i++) cout << getAt(i) << ' ';
	cout << '\n';
}

void a(int cnt) {
	moves.push_back(Move('a', cnt));
	shift = (shift + cnt) % n;
}

void b(int cnt) {
	moves.push_back(Move('b', cnt));
	for (int i = 0; i < cnt; i++) {
		int &p1 = at[(0 - shift + n) % n], &p2 = at[(1 - shift + n) % n],
			&p3 = at[(2 - shift + n) % n];
		pos[p1] = (pos[p1] + 1) % n;
		pos[p2] = (pos[p2] + 1) % n;
		pos[p3] = (pos[p3] - 2 + n) % n;
		int t = p3;
		p3 = p2; p2 = p1; p1 = t;
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> at[i];
		pos[at[i]] = i;
	}
	for (int i = 2; i <= n - 2; i++) {
		if (getPos(i) == (getPos(i - 1) + 1) % n) continue;
		if (getPos(i) != 0) a(n - getPos(i));
		while (getPos(i - 1) < n - 2) {
			a(2);
			b(1);
		}
		if (getPos(i - 1) == n - 2) {
			a(1);
			b(2);
		}
	}
	if (getPos(1) != 0) a(n - getPos(1));
	if (n % 2 == 1 && getPos(n) != (getPos(n - 1) + 1) % n) {
		cout << "NIE DA SIE\n";
	} else {
		while (getPos(n) != (getPos(n - 1) + 1) % n) {
			a(n - getPos(n - 1));
			b(2);
		}
		a(n - getPos(1));
		for (unsigned int i = 0; i < moves.size(); i++) {
			int cnt = moves[i].val;
			while (i < moves.size() && moves[i].m == moves[i + 1].m) {
				i++;
				cnt += moves[i].val;
			}
			if (moves[i].m == 'a') cnt %= n;
			else cnt %= 3;
			if (!cnt) continue;
			ansMoves.push_back(Move(moves[i].m, cnt));
		}
		cout << ansMoves.size() << '\n';
		for (unsigned int i = 0; i < ansMoves.size(); i++) {
			if (i) cout << ' ';
			cout << ansMoves[i].val << ansMoves[i].m;
		}
		cout << '\n';
	}
}
