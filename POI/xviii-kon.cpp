#include <iostream>
#include <cstring>
using namespace std;

const int MAXN = 5001;

int n, vis[MAXN * 2], fail[MAXN * 2], cnt[2];
bool g[MAXN][MAXN];

int inv(int x) {
	return (x >= n) ? (x - n) : (x + n);
}

bool dfs(int x) {
	if (vis[inv(x)]) return false;
	if (vis[x]) return true;
	vis[x] = 1;
	if (x >= n) {
		for (int i = 0; i < n; i++) {
			if (g[inv(x)][i] && !dfs(i)) return false;
		}
	} else {
		for (int i = n; i < n * 2; i++) {
			if (x != inv(i) && !g[x][inv(i)] && !dfs(i)) return false;
		}
	}
	return true;
}

bool solve() {
	for (int i = 0; i < n; i++) {
		if (!vis[i] && !vis[inv(i)]) {
			if (!dfs(i)) {
				memset(vis, 0, sizeof(vis));
				if (!dfs(inv(i))) return false;
			}
		}
	}
	return true;
}

int main() {
	cin >> n;
	for (int i = 0, k, x; i < n; i++) {
		cin >> k;
		while (k--) {
			cin >> x;
			g[i][x - 1] = true;
		}
	}
	if (!solve()) {
		cout << "0\n";
	} else {
		int ans = 0;
		memset(fail, -1, sizeof(fail));
		for (int i = 0; i < n; i++) {
			if (vis[i]) {
				cnt[0]++;
				for (int j = n; j < n * 2; j++) {
					if (vis[j] && g[i][inv(j)]) {
						if (fail[i] == -1) fail[i] = j;
						else fail[i] = -2;
					}
				}
			}
		}
		for (int i = n; i < n * 2; i++) {
			if (vis[i]) {
				cnt[1]++;
				for (int j = 0; j < n; j++) {
					if (vis[j] && !g[inv(i)][j]) {
						if (fail[i] == -1) fail[i] = j;
						else fail[i] = -2;
					}
				}
			}
		}
		for (int i = 0; i < n * 2; i++) {
			if (cnt[i >= n] > 1 && vis[i] && fail[i] == -1) ans++;
		}
		if (cnt[0] && cnt[1]) {
			ans++;
			for (int i = 0; i < n; i++) {
				for (int j = n; j < n * 2; j++) {
					if (!vis[i] || !vis[j]) continue;
					if ((fail[i] == -1 || fail[i] == j) && (fail[j] == -1 || fail[j] == i)) ans++;
				}
			}
		}
		cout << ans << '\n';
	}
}
