#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

const int MAXN = 1000010;
int ansL[MAXN], ansR[MAXN], sum[MAXN], tCnt[MAXN];
char str[MAXN];

struct Query {
	int val, id;
	bool operator <(const Query &rhs) const {
		return val < rhs.val;
	}
} query[MAXN];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, m;
	cin >> n >> m >> str;
	sum[0] = (str[0] == 'T') + 1;
	for (int i = 1; i < n; i++) sum[i] = sum[i - 1] + (str[i] == 'T') + 1;
	tCnt[n] = 0;
	for (int i = n - 1; i >= 0; i--) {
		tCnt[i] = (str[i] == 'T') ? (tCnt[i + 1] + 1) : 0;
	}
	for (int i = 0; i < m; i++) {
		cin >> query[i].val;
		query[i].id = i;
	}
	sort(query, query + m);
	memset(ansR, -1, sizeof(ansR));
	for (int i = 0, p = 0; i < m; i++) {
		int &id = query[i].id;
		while (p < n && sum[p] < query[i].val) p++;
		if (p >= n) break;
		if (sum[p] == query[i].val) {
			ansL[id] = 0, ansR[id] = p;
		} else {
			int d = min(tCnt[0], tCnt[p + 1]);
			int t1 = d, t2 = p + d;
			ansL[id] = t1 + 1;
			if (str[t1] == 'W') ansR[id] = t2;
			else if (str[t2 + 1] == 'W') ansR[id] = t2 + 1;
		}
	}
	for (int i = 0; i < m; i++) {
		if (ansR[i] == -1) cout << "NIE\n";
		else cout << ansL[i] + 1 << ' ' << ansR[i] + 1 << '\n';
	}
}
