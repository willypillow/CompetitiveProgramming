#include <iostream>
#include <cmath>
using namespace std;

const int MAXN = 500010;

int h[MAXN];
double ans[MAXN][2];

void build(int l, int r, int searchL, int searchR, int isLeft) {
	if (l > r) return;
	int m = (l + r) >> 1, maxPos = 0;
	double maxVal = 0;
	if (isLeft) {
		for (int i = searchL; i <= searchR && i <= m; i++) {
			double t = sqrt(m - i) + h[i];
			if (t >= maxVal) {
				maxVal = t;
				maxPos = i;
			}
		}
	} else {
		for (int i = searchR; i >= searchL && i >= m; i--) {
			double t = sqrt(i - m) + h[i];
			if (t >= maxVal) {
				maxVal = t;
				maxPos = i;
			}
		}
	}
	ans[m][isLeft] = maxVal;
	build(l, m - 1, searchL, maxPos, isLeft);
	build(m + 1, r, maxPos, searchR, isLeft);
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int n; cin >> n;
	for (int i = 0; i < n; i++) cin >> h[i];
	build(0, n - 1, 0, n - 1, 0); build(0, n - 1, 0, n - 1, 1);
	for (int i = 0; i < n; i++) {
		cout << (int)ceil(max(ans[i][0], ans[i][1]) - h[i]) << '\n';
	}
}
