#include <iostream>
#include <vector>
using namespace std;

const int MAXN = 2E5 + 10;

int ans[MAXN] = { 0 };
vector<vector<int> > g;

void dfs(int cur, int type) {
	ans[cur] = type;
	for (unsigned int i = 0; i < g[cur].size(); i++) {
		if (!ans[g[cur][i]]) dfs(g[cur][i], 3 - type);
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	g.resize(n + 1);
	for (int i = 0, a, b; i < m; i++) {
		cin >> a >> b;
		g[a].push_back(b); g[b].push_back(a);
	}
	bool fail = false;
	for (int i = 1; i <= n; i++) {
		if (g[i].size() == 0) {
			fail = true;
			break;
		}
	}
	if (fail) {
		cout << "NIE\n";
	} else {
		cout << "TAK\n";
		for (int i = 1; i <= n; i++) {
			if (!ans[i]) dfs(i, 1);
		}
		for (int i = 1; i <= n; i++) {
			if (ans[i] == 1) cout << "K\n";
			else if (ans[i] == 2) cout << "S\n";
			else cout << "N\n";
		}
	}
}
