#include <iostream>
#include <vector>
using namespace std;

const int INF = 1 << 30, MAXN = 300010;

struct Shower { int l, r, a; } shower[MAXN];
vector<vector<int> > g;
int ans[MAXN], n, m, k, lefts[MAXN], rights[MAXN], id[MAXN], goal[MAXN];
long long bit[MAXN], tmpSum[MAXN], cur[MAXN];

void add(int p, int v) {
	while (p <= m) {
		bit[p] += v;
		p += (p & -p);
	}
}

void addRange(int l, int r, int v) {
	add(l, v); add(r + 1, -v);
}

long long sum(int p) {
	long long ans = 0;
	while (p > 0) {
		ans += bit[p];
		p -= (p & -p);
	}
	return ans;
}

void solve(int idHead, int idTail, int l, int r) {
	if (idHead > idTail || l > r) return;
	if (l == r) {
		for (int i = idHead; i <= idTail; i++) ans[id[i]] = l;
		return;
	}
	int mid = (l + r) >> 1, lTop = 0, rTop = 0;
	for (int i = l; i <= mid; i++) {
		if (shower[i].l <= shower[i].r) {
			addRange(shower[i].l, shower[i].r, shower[i].a);
		} else {
			addRange(shower[i].l, m, shower[i].a);
			addRange(1, shower[i].r, shower[i].a);
		}
	}
	for (int i = idHead; i <= idTail; i++) {
		tmpSum[id[i]] = 0;
		for (unsigned int j = 0; j < g[id[i]].size(); j++) {
			int x = g[id[i]][j];
			tmpSum[id[i]] += sum(x);
			if (cur[id[i]] + tmpSum[id[i]] >= goal[id[i]]) break;
		}
		if (cur[id[i]] + tmpSum[id[i]] >= goal[id[i]]) {
			lefts[lTop++] = id[i];
		} else {
			rights[rTop++] = id[i];
			cur[id[i]] += tmpSum[id[i]];
		}
	}
	for (int i = 0; i < lTop; i++) id[idHead + i] = lefts[i];
	for (int i = 0; i < rTop; i++) id[idHead + lTop + i] = rights[i];
	for (int i = l; i <= mid; i++) {
		if (shower[i].l <= shower[i].r) {
			addRange(shower[i].l, shower[i].r, -shower[i].a);
		} else {
			addRange(shower[i].l, m, -shower[i].a);
			addRange(1, shower[i].r, -shower[i].a);
		}
	}
	solve(idHead, idHead + lTop - 1, l, mid);
	solve(idHead + lTop, idTail, mid + 1, r);
}

int main() {
	cin >> n >> m;
	g.resize(n + 1);
	for (int i = 1, x; i <= m; i++) {
		cin >> x;
		g[x].push_back(i);
	}
	for (int i = 1; i <= n; i++) {
		cin >> goal[i];
		id[i] = i;
	}
	cin >> k;
	for (int i = 1; i <= k; i++) {
		cin >> shower[i].l >> shower[i].r >> shower[i].a;
	}
	k++;
	shower[k].l = 1; shower[k].r = m; shower[k].a = INF;
	solve(1, n, 1, k);
	for (int i = 1; i <= n; i++) {
		if (ans[i] != k) cout << ans[i] << '\n';
		else cout << "NIE\n";
	}
}
