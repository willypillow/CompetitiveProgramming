#include <iostream>
#include <vector>
using namespace std;

const int MAXN = 5000;

int lev[MAXN], cnt = 0, idTop = 0, n;
bool firstGeno = true, fail = false;

struct Node {
	static Node pool[MAXN], *ptr;
	Node *l, *r;
	int id;
	Node() {}
	Node(int d) {
		id = ++idTop;
		if (id > n * 2 || d > lev[cnt]) {
			fail = true;
			return;
		}
		if (d == lev[cnt]) {
			cnt++;
			l = r = NULL;
		} else {
			if (!fail) l = new(ptr++) Node(d + 1);
			if (!fail) r = new(ptr++) Node(d + 1);
		}
	}
	void printGeno(int p) {
		if (firstGeno) cout << p;
		else cout << ' ' << p;
		firstGeno = false;
		if (l) l->printGeno(id);
		if (r) r->printGeno(id);
	}
	void printBrace() {
		if (!l && !r) {
			cout << "()";
			return;
		}
		cout << "(";
		if (l) l->printBrace();
		if (r) r->printBrace();
		cout << ")";
	}
} Node::pool[MAXN], *Node::ptr;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	cin >> n;
	for (int i = 0; i < n; i++) cin >> lev[i];
	Node::ptr = Node::pool;
	Node root = Node(0);
	if (cnt == n && !fail) {
		root.printGeno(0);
		cout << '\n';
		root.printBrace();
		cout << '\n';
	} else {
		cout << "NIE\n";
	}
}
