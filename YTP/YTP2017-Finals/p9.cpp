#include <bits/stdc++.h>
using namespace std;

const int MAXN = 500, INF = 0x3f3f3f3f;
const int DX[] = { 1, 0, -1, 0 }, DY[] = { 0, 1, 0, -1 };

vector<pair<int, int> > edges[MAXN];
int arr[MAXN][MAXN], dis[MAXN];
int g[MAXN][MAXN];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int m, n; cin >> m >> n;
	memset(arr, 0x3f, sizeof(arr));
	for (int i = 1; i <= m; i++) {
		for (int j = 1; j <= n; j++) cin >> arr[i][j];
	}
	memset(g, 0x3f, sizeof(g));
	for (int i = 1; i <= m; i++) {
		for (int j = 1; j <= n; j++) {
			for (int k = 0; k < 4; k++) {
				int ni = i + DX[k], nj = j + DY[k];
				if (abs(arr[ni][nj] - arr[i][j]) <= 1) {
					//edges[ni * 21 + nj].push_back({ i * 21 + j, abs(arr[ni][nj] - arr[i][j]) });
					g[ni * 21 + nj][i * 21 + j] = abs(arr[ni][nj] - arr[i][j]);
				}
			}
		}
	}
	/*memset(dis, 0x3f, sizeof(dis));
	priority_queue<pair<int, int> > q;
	q.push({ 0, 1 * 21 + 1 });
	while (!q.empty()) {
		auto cur = q.top(); q.pop();
		if (dis[cur.second] < cur.first) continue;
		if (cur.second == m * 21 + n) {
			cout << cur.first << '\n';
			return 0;
		}
		for (auto nxt: edges[cur.second]) {
			if (dis[nxt.first] > cur.first + nxt.second) {
				dis[nxt.first] = cur.first + nxt.second;
				q.push({ cur.first + nxt.second, nxt.first });
			}
		}
	}*/
	for (int k = 0; k < MAXN; k++) {
		for (int i = 0; i < MAXN; i++) {
			for (int j = 0; j < MAXN; j++) {
				g[i][j] = min(g[i][j], g[i][k] + g[k][j]);
			}
		}
	}
	if (g[1 * 21 + 1][m * 21 + n] >= INF) cout << "NA\n";
	else cout << g[1 * 21 + 1][m * 21 + n] << '\n';
}
