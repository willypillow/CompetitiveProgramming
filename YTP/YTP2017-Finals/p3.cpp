#include <bits/stdc++.h>
using namespace std;

int main() {
	//std::cin.tie(0);
	//std::ios_base::sync_with_stdio(0);
	string str; cin >> str;
	for (char c: str) {
		if (c == 'z' || c == 'Z' || str.size() > 5) {
			cout << "Error\n";
			return 0;
		}
	}
	int ans = 0;
	for (int i = 0; i < str.size(); i++) {
		if (isupper(str[i])) str[i] = tolower(str[i]);
		ans = ans * 25 + str[i] - 'a' + 1;
	}
	
	printf("%06X\n", ans);
}
