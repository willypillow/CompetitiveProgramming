#include <bits/stdc++.h>
using namespace std;

const int MAXN = 2000;

int visit[MAXN], low[MAXN], arr[MAXN];
int t = 0;
int ans = 0;
vector<int> edges[MAXN];
 
void dfs(int p, int i)
{
    visit[i] = low[i] = ++t;
 
    int child = 0;
    bool ap = false;
 
		for (int j: edges[i]) {
			if (j != p) {
				if (visit[j]) low[i] = min(low[i], visit[j]);
				else {
					child++;
					dfs(i, j);
					low[i] = min(low[i], low[j]);
					if (low[j] >= visit[i]) ap = true;
				}
			}
		}
 
    if ((i == p && child > 1) || (i != p && ap))
        ans++;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 0; i < n; i++) cin >> arr[i];
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < i; j++) {
			if (__gcd(arr[i], arr[j]) != 1) {
				edges[i].push_back(j);
				edges[j].push_back(i);
			}
		}
	}
	for (int i = 0; i < n; i++) {
		if (!visit[i]) {
			dfs(i, i);
		}
	}
	cout << ans << '\n';
}

