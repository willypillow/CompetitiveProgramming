#include <bits/stdc++.h>
using namespace std;

const int MAXN = 5000;

bool notPrime[5010];
vector<int> primes;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	for (int i = 2; i <= MAXN; i++) {
		if (!notPrime[i]) {
			for (int j = i + i; j <= MAXN; j += i) notPrime[j] = true;
			primes.push_back(i);
		}
	}
	int n; cin >> n;
	notPrime[1] = true;
	if (notPrime[n]) {
		for (int i = n; i >= 1; i--) {
			if (notPrime[i]) cout << i << ' ';
		}
	} else {
		for (int i = n; i >= 1; i--) {
			if (!notPrime[i]) cout << i << ' ';
		}
	}
	cout << '\n';
}
