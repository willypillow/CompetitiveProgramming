#include <bits/stdc++.h>
using namespace std;

int main() {
	//std::cin.tie(0);
	//std::ios_base::sync_with_stdio(0);
	string s;
	cin >> s;
	for (int i = 0; i < s.size(); i++) {
		if (s[i] == ',') s[i] = ' ';
	}
	stringstream ss(s);
	double a, t1, t2, b, t3; ss >> a >> t1 >> t2 >> b >> t3;
	double mid = a * t1;
	double ans = 0.5 * a * t1 * t1 + mid * t2 + mid * t3 - 0.5 * b * t3 * t3;
	cout << fixed << setprecision(1) << ans << '\n';
}
