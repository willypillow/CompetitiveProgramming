#include <bits/stdc++.h>
using namespace std;

int arr[10000], n;

bool solve(int m) {
	priority_queue<int, vector<int>, greater<int> > q;
	for (int i = 0; i < m; i++) q.push(0);
	for (int i = 0; i < n; ) {
		int bus = q.top(); q.pop();
		if (bus - arr[i] > 30) {
			return false;
		}
		int ini = arr[i], j = i + 1;
		while (j - i < 7 && arr[j] - ini <= 30) j++;
		i = j;
		q.push(arr[j - 1] + 90);
	}
	return true;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	cin >> n;
	for (int i = 0; i < n; i++) {
		int x; cin >> x;
		int hr = x / 100, mn = x % 100;
		arr[i] = hr * 60 + mn;
	}
	int l = 2, r = 1E4;
	for (int i = l; i <= r; i++) {
		if (solve(i)) {
			cout << i - 2 << '\n';
			return 0;
		}
	}
}
