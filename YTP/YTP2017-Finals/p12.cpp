#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E4;

vector<int> edges1[MAXN], edges2[MAXN];
vector<int> fin;
int scc[MAXN], sccSz[MAXN];
bool vis[MAXN];

void dfs1(int x) {
	vis[x] = true;
	for (int y: edges1[x]) {
		if (!vis[y]) dfs1(y);
	}
	fin.push_back(x);
}

void dfs2(int x, int color) {
	scc[x] = color;
	sccSz[color]++;
	for (int y: edges2[x]) {
		if (!scc[y]) dfs2(y, color);
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	for (int i = 0; i < m; i++) {
		int u, v; cin >> u >> v;
		edges1[u].push_back(v);
		edges2[v].push_back(u);
	}
	for (int i = 1; i <= n; i++) {
		if (!vis[i]) dfs1(i);
	}
	int colo = 0;
	for (int i = fin.size() - 1; i >= 0; i--) {
		if (!scc[fin[i]]) dfs2(fin[i], ++colo);
	}
	int ans = 0;
	for (int i = 1; i <= colo; i++) ans = max(ans, sccSz[i]);
	cout << ans << '\n';
}

