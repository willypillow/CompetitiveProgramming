#include <bits/stdc++.h>
using namespace std;

int dp[100], arr[100];

int val(int j, int i) {
	int t = 0, t2 = 0;
	for (int k = j + 1; k <= i; k++) {
		t += arr[k];
		t2 = t2 * 10 + arr[k];
	}
	t = t * 2 + 1;
	if (!t) return 1E9;
	return (t2 % t + 1) * (i - j);
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	string s; cin >> s;
	int n = s.size();
	for (int i = 1; i <= n; i++) arr[i] = s[i - 1] - '0';
	for (int i = 1; i <= n; i++) {
		dp[i] = 1E9;
		for (int j = max(0, i - 6); j < i; j++) {
			dp[i] = min(dp[i], dp[j] + val(j, i));
		}
	}
	cout << dp[n] << '\n';
}

