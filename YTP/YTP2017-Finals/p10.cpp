#include <bits/stdc++.h>
using namespace std;

int arr[11][11];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	string s; cin >> s;
	for (int i = 0; i < (int)s.size(); i++) {
		if (s[i] == ',') s[i] = ' ';
	}
	stringstream ss(s);
	int q; ss >> q;
	int t;
	while (ss >> t) {
		int x = t / 10, y = t % 10;
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				int l = round(sqrt((i - x) * (i - x) + (j - y) * (j - y)));
				if (l <= 5) arr[i][j] += 5 - l;
			}
		}
	}
	cout << setw(2) << setfill('0') << arr[q / 10][q % 10];
	cout << '\n';
}
