#include <bits/stdc++.h>
using namespace std;

struct Calc {
	int a, b, c;
	char t;
} calc[200];

int doc(int a, int b, char t) {
	if (t == '+') return a + b;
	if (t == '-') return a - b;
	if (t == '*') return a * b;
	return a / b;
}

int rep(int x, int a, int b) {
	string str = to_string(x);
	for (int i = 0; i < (int)str.size(); i++) {
		if (str[i] == a + '0') str[i] = b + '0';
		else if (str[i] == b + '0') str[i] = a + '0';
	}
	return atoi(str.c_str());
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n = 0;
	bool fail = false;
	string str;
	while (getline(cin, str)) {
		int i;
		for (i = 0; isdigit(str[i]); i++) calc[n].a = calc[n].a * 10 + str[i] - '0';
		calc[n].t = str[i++];
		for (; isdigit(str[i]); i++) calc[n].b = calc[n].b * 10 + str[i] - '0';
		i++;
		for (; isdigit(str[i]); i++) calc[n].c = calc[n].c * 10 + str[i] - '0';
		if (doc(calc[n].a, calc[n].b, calc[n].t) != calc[n].c) fail = true;
		n++;
	}
	if (!fail) {
		cout << "0\n";
	} else {
		vector<pair<int, int> > suc;
		for (int i = 0; i <= 9; i++) {
			for (int j = i + 1; j <= 9; j++) {
				bool fail = false;
				for (int k = 0; k < n; k++) {
					int a = rep(calc[k].a, i, j), b = rep(calc[k].b, i, j), c = rep(calc[k].c, i, j);
					if ((b == 0 && calc[k].t == '/') || doc(a, b, calc[k].t) != c) {
						fail = true;
						break;
					}
				}
				if (!fail) suc.push_back({ i, j });
			}
		}
		if (suc.size() != 1) {
			cout << "100\n";
		} else {
			cout << suc[0].first << suc[0].second << '\n';
		}
	}
}
