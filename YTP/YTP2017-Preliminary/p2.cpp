#include <bits/stdc++.h>
using namespace std;

int main() {
	//std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, l;
	while (cin >> n >> l) {
		double deg = 2 * 3.14159265359 / n / 2;
		double ans = l * 0.5 / sin(deg);
		cout << setprecision(3) << fixed << ans << '\n';
	}
}
