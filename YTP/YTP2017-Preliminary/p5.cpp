#include <bits/stdc++.h>
using namespace std;

string ino, posto;

string build(int il, int ir, int pl, int pr) {
	int i;
	for (i = il; i <= ir; i++) {
		if (ino[i] == posto[pr]) break;
	}
	int lnum = i - il;
	string str = string(1, posto[pr]);
	if (il <= i - 1) str += build(il, i - 1, pl, pl + lnum - 1);
	if (i + 1 <= ir) str += build(i + 1, ir, pl + lnum, pr - 1);
	return str;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int t; cin >> t;
	while (t--) {
		int n; cin >> n >> ino >> posto;
		cout << build(0, n - 1, 0, n - 1) << '\n';
	}
}
