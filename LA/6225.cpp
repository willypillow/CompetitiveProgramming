#include <bits/stdc++.h>
using namespace std;

int main() {
	for (int n; cin >> n; ) {
		set<int> s;
		int ans = 0;
		for (long long i = n; s.size() < 10; i += n, ans++) {
			long long t = i;
			while (t) {
				s.insert(t % 10);
				t /= 10;
			}
		}
		cout << ans << '\n';
	}
}
