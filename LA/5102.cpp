#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

struct P { double x, y; } c[100];

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  srand(time(0));
  while (cin >> c[0].x >> c[0].y && fabs(c[0].x + 1) > 1E-7) {
    int n = 4;
    for (int i = 1; i < n; i++) cin >> c[i].x >> c[i].y;
    double x = 500, y = 500, temp = 500, ans = 1.0E15;
    while (temp > 1.0E-9) {
      int rnd = rand();
      double nx = x + (temp * cos(rnd)), ny = y + (temp * sin(rnd)), tot = 0;
      for (int i = 0; i < n; i++) {
        double dx = nx - c[i].x, dy = ny - c[i].y;
        tot += sqrt(dx * dx + dy * dy);
      }
      if (tot < ans) ans = tot, x = nx, y = ny;
      else temp *= 0.99;
    }
    cout << fixed << setprecision(4) << ans << '\n';
  }
}
