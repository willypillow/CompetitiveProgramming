#include <bits/stdc++.h>
using namespace std;

const int MOD = 1E9 + 7;

int main() {
	int t; cin >> t;
	while (t--) {
		int f[3], n; cin >> f[0] >> f[1] >> n;
		f[2] = f[1] - f[0];
		n--;
		int ans = f[n % 3] * (n / 3 % 2 ? -1 : 1);
		ans = (ans % MOD + MOD) % MOD;
		cout << ans << '\n';
	}
}
