#include <bits/stdc++.h>
using namespace std;

int ans[101];

struct P {
	static P cur;
	int x, y, t, id;
	bool operator <(const P &rhs) const {
		int x0 = P::cur.x, y0 = P::cur.y, x1 = rhs.x, y1 = rhs.y;
		int cross = (x1 - x0) * (y - y0) - (x - x0) * (y1 - y0);
		return cross < 0;
	}
} p[201], P::cur;

void dfs(int l, int r) {
	if (r < l) return;
	P::cur = p[l];
	int curId = l;
	for (int i = l + 1; i <= r; i++) {
		if (p[i].y < P::cur.y) P::cur = p[curId = i];
	}
	swap(p[curId], p[l]);
	sort(p + l + 1, p + r + 1);
	int i, cnt[2] = { 0 };
	for (i = l + 1; i <= r; i++) {
		if (P::cur.t != p[i].t && cnt[0] == cnt[1]) {
			if (!P::cur.t) ans[P::cur.id] = p[i].id;
			else ans[p[i].id] = P::cur.id;
			break;
		}
		cnt[p[i].t]++;
	}
	dfs(l + 1, i - 1); dfs(i + 1, r);
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n;
	bool first = true;
	while (cin >> n) {
		for (int i = 0; i < n; i++) {
			cin >> p[i].x >> p[i].y;
			p[i].t = 0;
			p[i].id = i + 1;
		}
		for (int i = 0; i < n; i++) {
			cin >> p[n + i].x >> p[n + i].y;
			p[n + i].t = 1;
			p[n + i].id = i + 1;
		}
		dfs(0, n + n - 1);
		if (!first) cout << '\n';
		first = false;
		for (int i = 1; i <= n; i++) cout << ans[i] << '\n';
	}
}
