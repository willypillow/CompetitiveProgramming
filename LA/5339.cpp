#include <bits/stdc++.h>
using namespace std;

typedef __int128 ULL;
char str1[110], str2[110];
ULL fib[151];

ULL toInt(char *str) {
	int n = strlen(str);
	ULL ans = 0;
	for (int i = 0; i < n; i++) {
		if (str[i] == '1') {
			ans += fib[n - i - 1];
		}
	}
	return ans;
}

string toFib(ULL x) {
	string str;
	for (int i = 150; i >= 0; i--) {
		if (x >= fib[i]) {
			str.push_back('1');
			x -= fib[i];
		} else if (str.size()) {
			str.push_back('0');
		}
	}
	if (!str.size()) str.push_back('0');
	return str;
}

int main() {
#ifdef OJ_DEBUG
	freopen("tmp", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	fib[0] = 1; fib[1] = 2;
	for (int i = 2; i <= 150; i++) fib[i] = fib[i - 1] + fib[i - 2];
	bool first = true;
	while (cin >> str1 >> str2) {
		ULL a = toInt(str1), b = toInt(str2);
		if (!first) cout << '\n';
		first = false;
		cout << toFib(a + b) << '\n';
	}
}
