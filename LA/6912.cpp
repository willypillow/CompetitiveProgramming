#include <bits/stdc++.h>
using namespace std;

bool on[1001];

int main() {
	int t, cases = 0; cin >> t;
	while (t--) {
		int n, k; cin >> n >> k;
		vector<int> trial, must;
		for (int i = 0; i < k; i++) {
			int x; cin >> x;
			if (x <= 31) trial.push_back(x);
			else must.push_back(x);
		}
		sort(trial.begin(), trial.end());
		int ans = 0;
		for (int i = 0; i < (1 << trial.size()); i++) {
			memset(on, false, sizeof(on));
			for (int j = 0; j < (int)trial.size(); j++) {
				if (~i & (1 << j)) continue;
				for (int k = trial[j]; k <= n; k += trial[j]) on[k] ^= 1;
			}
			for (int j: must) {
				int tmp = 0;
				for (int k = j; k <= n; k += j) tmp += on[k] ? -1 : 1;
				if (tmp > 0) {
					for (int k = j; k <= n; k += j) on[k] ^= 1;
				}
			}
			int res = 0;
			for (int j = 1; j <= n; j++) res += on[j];
			ans = max(ans, res);
		}
		cout << "Case #" << ++cases << ": " << ans << '\n';
	}
}
