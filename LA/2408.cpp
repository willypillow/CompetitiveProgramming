#include <bits/stdc++.h>
using namespace std;

int main() {
	int y, m, d, t;
	while (cin >> y >> m >> d >> t) {
		long double delta = 1152921504606846975.0 * t / 60 / 60 / 24 / 365.242199;
		long double t0 = y + m / 12.0 + d / 365.242199;
		cout << fixed << setprecision(1) << t0 + delta << '\n';
	}
}
