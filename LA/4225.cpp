#include <bits/stdc++.h>
using namespace std;

const int primes[] = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47 };
vector<int> ans;

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	long long n;
	while (cin >> n && n) {
		cout << n << " = ";
		if (n <= 2) {
			if (n == 1) cout << "1\n";
			else cout << "1*2\n";
			continue;
		}
		ans.clear();
		long long prod = 1;
		int i = 0;
		while (prod <= n) prod *= primes[i++];
		prod /= primes[--i];
		while (i) {
			ans.push_back(n / prod);
			n %= prod;
			prod /= primes[--i];
		}
		ans.push_back(n);
		auto it = ans.end(); it--;
		bool first = true;
		if (*it) {
			cout << *it;
			first = false;
		}
		it--;
		int pTop = 0;
		string str;
		for (;; it--) {
			str += "*" + to_string(primes[pTop++]);
			if (*it) {
				if (!first) cout << " + ";
				first = false;
				cout << *it << str;
			}
			if (it == ans.begin()) break;
		}
		cout << '\n';
	}
}
