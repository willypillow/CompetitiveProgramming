#include <bits/stdc++.h>
using namespace std;

int dp[1010];

int dfs(int x) {
	if (x <= 1) return 1;
	if (dp[x]) return dp[x];
	int ans = 0;
	for (int i = (x % 2 ? 1 : 0); i <= x; i += 2) {
		ans += dfs((x - i) / 2);
	}
	return dp[x] = ans;
}

int main() {
	int t; cin >> t;
	for (int i = 1; i <= t; i++) {
		int n; cin >> n;
		cout << i << ' ' << dfs(n) << '\n';
	}
}
