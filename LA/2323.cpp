#include <bits/stdc++.h>
using namespace std;

bitset<2017> f, g, h, fg;

int getPoly(bitset<2017> &bs) {
	int n; cin >> n;
	bs.reset();
	for (int i = n - 1; i >= 0; i--) {
		bool x; cin >> x;
		bs[i] = x;
	}
	return n;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		int fn = getPoly(f), gn = getPoly(g), hn = getPoly(h);
		fg.reset();
		for (int i = 0; i < fn; i++) {
			if (f[i]) fg ^= g << i;
		}
		int len = fn + gn - 2;
		for (int i = len; i >= hn - 1; i--) {
			if (fg[i]) fg ^= h << (i - hn + 1);
		}
		for (len = hn; len >= 0 && !fg[len]; len--);
		cout << len + 1;
		if (len < 0) cout << " 0";
		while (len >= 0) cout << ' ' << fg[len--];
		cout << '\n';
	}
}
