#include <bits/stdc++.h>
#include <ext/pb_ds/tree_policy.hpp>
#include <ext/pb_ds/assoc_container.hpp>
using namespace std;
using namespace __gnu_pbds;

int arr[40010];

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	long long n, cases = 0;
	while (cin >> n && n) {
		for (int i = 0; i < n; i++) cin >> arr[i];
		sort(arr, arr + n);
		long long ans = 0;
		for (int i = 0; i < n; i++) {
			ans += arr + n - upper_bound(arr, arr + n, arr[i]);
		}
		cout << "Case " << ++cases << ": " << ans << '\n';
	}
}
