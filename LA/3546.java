import java.util.Scanner;
import java.math.BigInteger;

class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
		Scanner cin = new Scanner(System.in);
		int t = cin.nextInt();
		while (t-- != 0) {
			int n = cin.nextInt();
			int k = cin.nextInt();
			if (n == 1) {
				System.out.println(k);
			} else {
				BigInteger b = BigInteger.valueOf(n * 10 - 1);
				int ok = 0;
				for (int lgx = 0; ok == 0 && lgx <= 100; lgx++) {
					BigInteger tmp = BigInteger.valueOf(10).pow(lgx + 1).subtract(BigInteger.valueOf(n));
					BigInteger a = BigInteger.valueOf(k).multiply(tmp);
					if (a.mod(b).intValue() == 0) {
						BigInteger res = a.divide(b);
						if (Math.floor(Math.log10(res.doubleValue())) == lgx) {
							res = res.multiply(BigInteger.valueOf(10)).add(BigInteger.valueOf(k));
							System.out.println(res.toString());
							ok = 1;
						}
					}
				}
				if (ok == 0) System.out.println(0);
			}
		}
	}
}

