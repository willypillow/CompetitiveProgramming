#include <bits/stdc++.h>
using namespace std;

const int MAXN = 5E8;
int clzSize[32];

string dfs(int n, int cnt) {
	string s = "";
	int l = 0, r = cnt - 1;
	while (n >= clzSize[l] * clzSize[r]) n -= clzSize[l++] * clzSize[r--];
	if (l) s += "(" + dfs(n / clzSize[r], l) + ")";
	s += "X";
	if (r) s += "(" + dfs(n % clzSize[r], r) + ")";
	return s;
}

int main() {
	clzSize[0] = clzSize[1] = 1;
	for (int i = 2; clzSize[i - 1] < MAXN; i++) {
		for (int j = 0; j < i; j++) clzSize[i] += clzSize[j] * clzSize[i - j - 1];
	}
	int n;
	while (cin >> n && n) {
		int idx = 0;
		while (n >= clzSize[idx]) n -= clzSize[idx++];
		cout << dfs(n, idx) << '\n';
	}
}
