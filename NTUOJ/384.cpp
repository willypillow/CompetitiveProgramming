#include <bits/stdc++.h>
using namespace std;

pair<int, int> part[5001];
int ans[5001];

int cross(pair<int, int> v, pair<int, int> w) {
	return v.first * w.second - v.second * w.first;
}

bool isLeft(int idx, int x, int y, int y1, int y2) {
	pair<int, int> v1 = {part[idx].first - x, y1 - y}, v2 = {part[idx].second - x, y2 - y};
	return cross(v1, v2) < 0;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n, m, x1, y1, x2, y2, cases = 0;
	while (cin >> n && n && cin >> m >> x1 >> y1 >> x2 >> y2) {
		memset(ans, 0, sizeof(ans));
		if (cases++) cout << '\n';
		part[0] = {x1, x1};
		for (int i = 1; i <= n; i++) cin >> part[i].first >> part[i].second;
		for (int i = 0; i < m; i++) {
			int x, y; cin >> x >> y;
			int l = 0, r = n + 1;
			while (l + 1 < r) {
				int mid = (l + r) >> 1;
				if (isLeft(mid, x, y, y1, y2)) r = mid;
				else l = mid;
			}
			ans[l]++;
		}
		for (int i = 0; i <= n; i++) cout << i << ": " << ans[i] << '\n';
	}
}
