#include <bits/stdc++.h>
using namespace std;

pair<int, int> p[101];
unordered_map<int, vector<pair<int, int> > > event;

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	for (int timer = 1, m; cin >> m && m; timer++) {
		event.clear();
		cin >> p[0].first >> p[0].second;
		int mn = min(p[0].first, p[0].second), mx = max(p[0].first, p[0].second);
		for (int i = 1; i <= m; i++) {
			if (i == m) {
				p[i] = p[0];
			} else {
				cin >> p[i].first >> p[i].second;
				mn = min({mn, p[i].first, p[i].second});
				mx = max({mx, p[i].first, p[i].second});
			}
			auto p1 = p[i - 1], p2 = p[i];
			if (p1.first > p2.first) swap(p1, p2);
			for (int j = p1.first; j != p2.first; j++) {
				double dx = p2.first - p1.first, dy = p2.second - p1.second;
				double from = 1.0 * (j - p1.first) * dy / dx + p1.second,
					to = 1.0 * (j + 1 - p1.first) * dy / dx + p1.second;
				if (from > to) swap(from, to);
				event[j].push_back({(int)floor(from), (int)ceil(to)});
			}
		}
		int ans = 0;
		for (int i = mn; i <= mx; i++) {
			auto &v = event[i];
			sort(v.begin(), v.end());
			assert(v.size() % 2 == 0);
			int last = -1E9;
			for (int j = 0; j < (int)v.size(); j += 2) {
				ans += v[j + 1].second - max(last, v[j].first);
				last = v[j + 1].second;
			}
		}
		cout << ans << '\n';
	}
}
