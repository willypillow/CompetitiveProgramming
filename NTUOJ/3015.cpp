#include <bits/stdc++.h>
using namespace std;

const int kMaxN = 1010;

using WInt = int64_t;
const WInt kInf = 0x3f3f3f3f;
WInt g[kMaxN][kMaxN];
int n, m;

WInt slack[kMaxN], lx[kMaxN], ly[kMaxN];
int mx[kMaxN], my[kMaxN], vx[kMaxN], vy[kMaxN], timer;
bool vis[kMaxN];

bool Dfs(int x) {
	vx[x] = timer;
	for (int y = 1; y <= n; y++) {
		if (vy[y] == timer) continue;
		auto t = lx[x] + ly[y] - g[x][y];
		if (!t) {
			vy[y] = timer;
			if (!my[y] || Dfs(my[y])) {
				mx[x] = y;
				my[y] = x;
				return true;
			}
		} else {
			slack[y] = min(slack[y], t);
		}
	}
	return false;
}

void Reweight() {
	auto t = kInf;
	for (int y = 1; y <= n; y++) {
		if (vy[y] != timer) t = min(t, slack[y]);
	}
	for (int x = 1; x <= m; x++) {
		if (vx[x] == timer) lx[x] -= t;
	}
	for (int y = 1; y <= n; y++) {
		if (vy[y] == timer) ly[y] += t;
	}
}

void Km() {
	memset(lx, 0, sizeof(lx)), memset(ly, 0, sizeof(ly));
	memset(mx, 0, sizeof(mx)), memset(my, 0, sizeof(my));
	for (int x = 1; x <= m; x++) {
		lx[x] = -kInf;
		for (int y = 1; y <= n; y++) lx[x] = max(lx[x], g[x][y]);
	}
	for (int x = 1; x <= m; x++) {
		for (;;) {
			++timer;
			memset(slack, kInf, sizeof(slack));
			if (Dfs(x)) break;
			else Reweight();
		}
	}
}

int main() {
	cin.tie(nullptr), ios_base::sync_with_stdio(false);
	int k; cin >> n >> m >> k;
	n = m = max(n, m);
	while (k--) {
		WInt p;
		int u, v; cin >> u >> v >> p;
		g[u][v] = max(g[u][v], p);
	}
	Km();
	int64_t ans = 0, cnt = 0;
	for (int i = 1; i <= n; i++) {
		if (g[i][mx[i]] > 0) ans += g[i][mx[i]], ++cnt;
	}
	cout << ans << '\n' << cnt << '\n';
	for (int i = 1; i <= n; i++) {
		if (g[i][mx[i]] > 0) cout << i << ' ' << mx[i] << '\n';
	}
}
