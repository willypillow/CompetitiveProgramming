#include <bits/stdc++.h>
using namespace std;

const int INF = 0x3f3f3f3f;
int dist[3010][2010];
bool isGas[3010];
unordered_map<string, int> mp;
vector<pair<int, int> > g[3010];

int toId(const string &s) {
	if (mp.count(s)) return mp[s];
	int id = (int)mp.size();
	return mp[s] = id;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n, m, cap;
	while (cin >> n >> m >> cap && n) {
		cap *= 10;
		memset(dist, INF, sizeof(dist)), memset(isGas, false, sizeof(isGas));
		for (int i = 0; i < 3010; i++) g[i].clear();
		mp.clear();
		string _src, _dest; cin >> _src >> _dest;
		int src = toId(_src), dest = toId(_dest);
		for (int i = 0; i < n; i++) {
			string _c1, _c2; cin >> _c1 >> _c2;
			int d; cin >> d;
			int c1 = toId(_c1), c2 = toId(_c2);
			g[c1].push_back({c2, d}), g[c2].push_back({c1, d});
		}
		for (int i = 0; i < m; i++) {
			string _s; cin >> _s;
			isGas[toId(_s)] = true;
		}
		priority_queue<tuple<int, int, int>, vector<tuple<int, int, int> >,
			greater<tuple<int, int, int> > > pq;
		dist[src][cap] = 0;
		pq.push(make_tuple(0, cap, src));
		while (pq.size()) {
			auto cur = pq.top(); pq.pop();
			if (get<2>(cur) == dest) break;
			if (dist[get<2>(cur)][get<1>(cur)] < get<0>(cur)) continue;
			if (isGas[get<2>(cur)]) get<1>(cur) = cap;
			for (auto &p: g[get<2>(cur)]) {
				if (get<1>(p) > get<1>(cur)) continue;
				auto nxt = make_tuple(get<0>(cur) + get<1>(p),
					get<1>(cur) - get<1>(p), get<0>(p));
				if (dist[get<2>(nxt)][get<1>(nxt)] > get<0>(nxt)) {
					dist[get<2>(nxt)][get<1>(nxt)] = get<0>(nxt);
					pq.push(nxt);
				}
			}
		}
		int ans = *min_element(dist[dest], dist[dest] + cap + 1);
		cout << (ans == INF ? -1 : ans) << '\n';
	}
}
