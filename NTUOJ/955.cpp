#include <bits/stdc++.h>
using namespace std;

int field[1001][1001], sum[1002][1002], sum2[1002][1002], row, col, h, w;
vector<int> vals;

bool solve(int m) {
	memset(sum, 0, sizeof(sum)), memset(sum2, 0, sizeof(sum2));
	for (int i = 1; i <= row; i++) {
		for (int j = 1; j <= col; j++) {
			if (field[i][j] < vals[m]) {
				int x = max(1, i - (h - 1)), y = max(1, j - (w - 1)),
						x2 = min(row - (h - 1), i), y2 = min(col - (w - 1), j);
				sum[x2 + 1][y2 + 1]++, sum[x][y2 + 1]--, sum[x2 + 1][y]--, sum[x][y]++;
			}
		}
	}
	for (int i = 1; i <= row; i++) {
		for (int j = 1; j <= col; j++) {
			if (field[i][j] > vals[m]) {
				int x = max(1, i - (h - 1)), y = max(1, j - (w - 1)),
						x2 = min(row - (h - 1), i), y2 = min(col - (w - 1), j);
				sum2[x2 + 1][y2 + 1]++, sum2[x][y2 + 1]--, sum2[x2 + 1][y]--, sum2[x][y]++;
			}
		}
	}
	for (int i = 1; i <= row - (h - 1); i++) {
		for (int j = 1; j <= col - (w - 1); j++) {
			sum[i][j] += sum[i - 1][j] + sum[i][j - 1] - sum[i - 1][j - 1];
			sum2[i][j] += sum2[i - 1][j] + sum2[i][j - 1] - sum2[i - 1][j - 1];
			if (sum[i][j] >= sum2[i][j]) return true;
		}
	}
	return false;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	for (int cases = 1; cases <= t; cases++) {
		cin >> row >> col >> h >> w;
		vals.clear();
		for (int i = 1; i <= row; i++) {
			for (int j = 1; j <= col; j++) {
				cin >> field[i][j];
				vals.push_back(field[i][j]);
			}
		}
		sort(vals.begin(), vals.end());
		vals.resize(unique(vals.begin(), vals.end()) - vals.begin());
		int l = 0, r = (int)vals.size() - 1;
		while (l != r) {
			int m = (l + r) >> 1;
			if (solve(m)) r = m;
			else l = m + 1;
		}
		cout << "Case #" << cases << ": " << vals[l] << '\n';
	}
}
