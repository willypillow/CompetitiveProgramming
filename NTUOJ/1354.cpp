#include <bits/stdc++.h>
using namespace std;

const int MOD = 10056, MAXN = 1000;

int fact[1001] = {1, 0}, dp[1001][1001];

int stirling(int n, int k) {
	if (k == 1 || k == n) return 1;
	if (dp[n][k] != -1) return dp[n][k];
	return dp[n][k] = (k * stirling(n - 1, k) % MOD + stirling(n - 1, k - 1)) % MOD;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	memset(dp, -1, sizeof(dp));
	for (int i = 1; i <= MAXN; i++) fact[i] = fact[i - 1] * i % MOD;
	int t; cin >> t;
	while (t--) {
		int n; cin >> n;
		int ans = 0;
		for (int i = 1; i <= n; i++) {
			ans = (ans + fact[i] * stirling(n, i) % MOD) % MOD;
		}
		cout << ans << '\n';
	}
}
