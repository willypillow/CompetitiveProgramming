#include <bits/stdc++.h>
#pragma GCC target("avx2","tune=native", "arch=core-avx2")
using namespace std;

int in[1003], out[1003], event[1003], timer, c[1003], l[1003], dp[1003][100];
vector<int> g[1003];

void dfs(int x) {
	in[x] = timer, event[timer++] = x;
	for (int y: g[x]) dfs(y);
	out[x] = timer;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		timer = 0;
		int n, k; cin >> n >> k;
		for (int i = 0; i <= n; i++) g[i].clear();
		for (int i = 1; i <= n; i++) {
			int p; cin >> p >> c[i] >> l[i];
			g[p].push_back(i);
		}
		dfs(0);
		memset(&dp[0][1], 0x8f, sizeof(int) * (&dp[timer][k] - &dp[0][1] + 1));
		for (int i = 0; i < timer; i++) {
			int id = event[i], x = out[id];
			for (int j = 0; j <= k; j++) {
				if (dp[i][j] > dp[x][j]) dp[x][j] = dp[i][j];
				if (j >= c[id] && dp[i + 1][j] < dp[i][j - c[id]] + l[id]) {
					dp[i + 1][j] = dp[i][j - c[id]] + l[id];
				}
			}
		}
		cout << *max_element(&dp[timer][0], &dp[timer][k] + 1) << '\n';
	}
}
