#include <bits/stdc++.h>
using namespace std;

bool stat[200001];
int stL[800010], stR[800010], stM[800010];

void pull(int id, int l, int m, int r) {
	stL[id] = stL[id << 1], stR[id] = stR[id << 1 | 1];
	stM[id] = max(stM[id << 1], stM[id << 1 | 1]);
	if (stat[m] != stat[m + 1]) {
		if (stL[id] == m - l + 1) stL[id] += stL[id << 1 | 1];
		if (stR[id] == r - m) stR[id] += stR[id << 1];
		stM[id] = max(stM[id], stR[id << 1] + stL[id << 1 | 1]);
	}
}

void build(int id, int l, int r) {
	if (l == r) {
		stL[id] = stR[id] = stM[id] = 1;
	} else {
		int m = (l + r) >> 1;
		build(id << 1, l, m), build(id << 1 | 1, m + 1, r);
		pull(id, l, m, r);
	}
}

void update(int id, int l, int r, int p) {
	if (l == r) return;
	int m = (l + r) >> 1;
	if (p <= m) update(id << 1, l, m, p);
	else update(id << 1 | 1, m + 1, r, p);
	pull(id, l, m, r);
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n, q;
	while (cin >> n >> q) {
		memset(stat, 0, sizeof(stat));
		build(1, 1, n);
		while (q--) {
			int x; cin >> x;
			stat[x] ^= 1;
			update(1, 1, n, x);
			cout << stM[1] << '\n';
		}
	}
}
