#include <bits/stdc++.h>
using namespace std;

set<pair<int, int> > st[100001], st2[100001];
int ans[100001], f[100001];
vector<int> g[100001];

void dfs(int x) {
	st[x].clear(), st2[x].clear();
	if (g[x].empty()) {
		st[x].insert({-1, f[x]});
		st2[x].insert({f[x], -1});
		ans[x] = f[x];
		return;
	}
	pair<int, int> mx = {0, 0};
	for (int y: g[x]) {
		dfs(y);
		mx = max(mx, {(int)st[y].size(), y});
	}
	swap(st[x], st[mx.second]), swap(st2[x], st2[mx.second]);
	for (int y: g[x]) {
		if (y != mx.second) {
			for (auto &p: st[y]) {
				auto it = st2[x].lower_bound({p.second, INT_MIN});
				if (it != st2[x].end() && it->first == p.second) {
					auto pr = make_pair(it->second + p.first, p.second);
					st[x].erase({it->second, it->first}), st2[x].erase(it);
					st[x].insert(pr), st2[x].insert({pr.second, pr.first});
				} else {
					st[x].insert(p), st2[x].insert({p.second, p.first});
				}
			}
		}
	}
	{
		auto p = make_pair(-1, f[x]);
		auto it = st2[x].lower_bound({p.second, INT_MIN});
		if (it != st2[x].end() && it->first == p.second) {
			auto pr = make_pair(it->second + p.first, p.second);
			st[x].erase({it->second, it->first}), st2[x].erase(it);
			st[x].insert(pr), st2[x].insert({pr.second, pr.first});
		} else {
			st[x].insert(p), st2[x].insert({p.second, p.first});
		}
	}
	ans[x] = st[x].begin()->second;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		int n; cin >> n;
		for (int i = 0; i <=n; i++) g[i].clear();
		for (int i = 1; i <= n; i++) {
			int p; cin >> p;
			g[p].push_back(i);
		}
		for (int i = 1; i <= n; i++) cin >> f[i];
		dfs(0);
		for (int i = 1; i <= n; i++) cout << ans[i] << ' ';
		cout << '\n';
	}
}
