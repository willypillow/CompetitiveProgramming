#include <bits/stdc++.h>
using namespace std;

struct SplayNode {
	static SplayNode HOLE;
	SplayNode *ch[2], *par;
	bool rev;
	int data, sum;
	SplayNode(): par(&HOLE), rev(false), data(0), sum(0) { ch[0] = ch[1] = &HOLE; }
	bool isRoot() {
		return par == &HOLE || (par->ch[0] != this && par->ch[1] != this);
	}
	void push() {
		if (rev) {
			if (ch[0] != &HOLE) ch[0]->rev ^= 1;
			if (ch[1] != &HOLE) ch[1]->rev ^= 1;
			swap(ch[0], ch[1]);
			rev ^= 1;
		}
	}
	void pushFromRoot() {
		if (!isRoot()) par->pushFromRoot();
		push();
	}
	void pull() {
		sum = data ^ ch[0]->sum ^ ch[1]->sum;
	}
	void rotate() {
		SplayNode *p = par, *gp = p->par;
		bool dir = (p->ch[1] == this);
		par = gp;
		if (!p->isRoot()) gp->ch[gp->ch[1] == p] = this;
		p->ch[dir] = ch[dir ^ 1];
		p->ch[dir]->par = p;
		p->par = this;
		ch[dir ^ 1] = p;
		p->pull(), pull();
	}
	void splay() {
		pushFromRoot();
		while (!isRoot()) {
			if (!par->isRoot()) {
				SplayNode *gp = par->par;
				if ((gp->ch[0] == par) == (par->ch[0] == this)) rotate();
				else par->rotate();
			}
			rotate();
		}
	}
} SplayNode::HOLE, node[20010], edge[40010];

namespace LCT {
	SplayNode *access(SplayNode *x) {
		SplayNode *last = &SplayNode::HOLE;
		while (x != &SplayNode::HOLE) {
			x->splay();
			x->ch[1] = last;
			x->pull();
			last = x;
			x = x->par;
		}
		return last;
	}
	void makeRoot(SplayNode *x) {
		access(x);
		x->splay();
		x->rev ^= 1;
	}
	void link(SplayNode *x, SplayNode *y) {
		makeRoot(x);
		x->par = y;
	}
	SplayNode *findRoot(SplayNode *x) {
		x = access(x);
		while (x->ch[0] != &SplayNode::HOLE) x = x->ch[0];
		x->splay();
		return x;
	}
	SplayNode *query(SplayNode *x, SplayNode *y) {
		makeRoot(x);
		return access(y);
	}
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n, q, cases = 0;
	while (cin >> n >> q && n) {
		for (int i = 0; i <= n; i++) node[i] = SplayNode();
		for (int i = 0; i <= q; i++) edge[i] = SplayNode();
		cout << "Case " << ++cases << ":\n";
		bool conflict = false;
		for (int i = 1, facts = 0; i <= q; i++) {
			char op; cin >> op;
			string str; getline(cin, str);
			if (conflict) continue;
			stringstream ss(str);
			if (op == 'I') {
				facts++;
				vector<int> in;
				int x, pi, qi, vi;
				while (ss >> x) in.push_back(x);
				if (in.size() == 2) pi = in[0] + 1, qi = 0, vi = in[1];
				else pi = in[0] + 1, qi = in[1] + 1, vi = in[2];
				auto r0 = LCT::findRoot(&node[pi]), r1 = LCT::findRoot(&node[qi]);
				if (r0 == r1) {
					auto p = LCT::query(&node[pi], &node[qi]);
					if (p->sum != vi) {
						cout << "The first " << facts << " facts are conflicting.\n";
						conflict = true;
					}
				} else {
					edge[facts].data = edge[facts].sum = vi;
					LCT::link(&node[pi], &edge[facts]), LCT::link(&edge[facts], &node[qi]);
				}
			} else {
				int k; ss >> k;
				map<SplayNode*, vector<int> > in;
				for (int j = 0; j < k; j++) {
					int x; ss >> x;
					x++;
					in[LCT::findRoot(&node[x])].push_back(x);
				}
				auto root0 = LCT::findRoot(&node[0]);
				int ans = 0;
				for (auto &p: in) {
					if (p.second.size() & 1) {
						if (p.first == root0) {
							p.second.push_back(0);
						} else {
							cout << "I don't know.\n";
							ans = -1;
							break;
						}
					}
					for (int j = 0; j < (int)p.second.size(); j += 2) {
						auto tmp = LCT::query(&node[p.second[j]], &node[p.second[j + 1]]);
						ans ^= tmp->sum;
					}
				}
				if (ans != -1) cout << ans << '\n';
			}
		}
		cout << '\n';
	}
}
