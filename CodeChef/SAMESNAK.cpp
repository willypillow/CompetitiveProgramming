#include <bits/stdc++.h>
using namespace std;
 
const int DX[] = { 0, 1, 0, -1 }, DY[] = { 1, 0, -1, 0 };
bool vis[16][16], field[16][16];
 
pair<int, int> pt[4];
 
bool dfs(int x, int y) {
	vis[x][y] = true;
	int cnt = 0;
	for (int i = 0; i < 4; i++) {
		int nx = x + DX[i], ny = y + DY[i];
		if (nx < 0 || nx >= 16 || ny < 0 || ny >= 16) continue;
		if (field[nx][ny]) {
			cnt++;
			if (cnt > 2) return false;
			if (vis[nx][ny]) continue;
			if (!dfs(nx, ny)) return false;
		}
	}
	return true;
}
 
int main() {
	int t; cin >> t;
	while (t--) {
		vector<int> cords;
		for (int i = 0; i < 4; i++) {
			int x, y; cin >> x >> y;
			pt[i] = { x, y };
			cords.push_back(x); cords.push_back(y);
		}
		sort(cords.begin(), cords.end());
		cords.resize(unique(cords.begin(), cords.end()) - cords.begin());
		memset(field, false, sizeof(field));
		memset(vis, false, sizeof(vis));
		for (int i = 0; i < 4; i++) {
			int nx = lower_bound(cords.begin(), cords.end(), pt[i].first) - cords.begin();
			int ny = lower_bound(cords.begin(), cords.end(), pt[i].second) - cords.begin();
			pt[i] = { nx * 2, ny * 2 };
		}
		for (int i = 0; i < 2; i++) {
			if (pt[i * 2].first == pt[i * 2 + 1].first) {
				for (int a = min(pt[i * 2].second, pt[i * 2 + 1].second), b = max(pt[i * 2].second, pt[i * 2 + 1].second); a <= b; a++) field[pt[i * 2].first][a] = true;
			} else {
				for (int a = min(pt[i * 2].first, pt[i * 2 + 1].first), b = max(pt[i * 2].first, pt[i * 2 + 1].first); a <= b; a++) field[a][pt[i * 2].second] = true;
			}
		}
		bool ok = dfs(pt[0].first, pt[0].second);
		for (int i = 0; i < 16; i++) {
			for (int j = 0; j < 16; j++) {
				if (field[i][j]) ok &= vis[i][j];
			}
		}
		cout << (ok ? "yes" : "no") << '\n';
	}
}
 
