#include <bits/stdc++.h>
using namespace std;

char _str[100010], *str;
vector<pair<set<int>, int> > stk;

int getFirst(int64_t id) {
	return *(get<0>(stk[id]).begin()) + get<1>(stk[id]);
}

int getLast(int64_t id) {
	return *(get<0>(stk[id]).rbegin()) + get<1>(stk[id]);
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		stk.clear();
		int n; cin >> n;
		cin >> _str;
		for (str = _str; *str == '#' && str - _str < n; str++);
		n -= (int)(str - _str);
		int64_t ans = 0;
		for (int i = n - 1; i > 0; i--) {
			if (str[i] == '#') {
				if (stk.size() && getFirst(stk.size() - 1) == i + 2) {
					get<0>(stk.back()).insert(i - get<1>(stk.back()));
				} else {
					stk.push_back({ { i }, 0 });
				}
				if (str[i - 1] == '#') {
					ans += get<0>(stk.back()).size();
					get<1>(stk.back())++;
					if (stk.size() > 1 && 
							getLast(stk.size() - 1) + 2 >= getFirst(stk.size() - 2)) {
						auto p1 = stk.back(); stk.pop_back();
						auto p2 = stk.back(); stk.pop_back();
						if (get<0>(p1).size() > get<0>(p2).size()) swap(p1, p2);
						for (int x: get<0>(p1)) get<0>(p2).insert(x + get<1>(p1) - get<1>(p2));
						stk.push_back(p2);
					}
				}
			}
		}
		bool fail = false;
		int cnt = 0;
		for (auto &p: stk) {
			for (int x: p.first) {
				x += p.second;
				if (x >= n - 1) fail = true;
				ans += x - cnt;
				cnt++;
			}
		}
		cout << (fail ? -1 : ans) << '\n';
	}
}
