#include <bits/stdc++.h>
using namespace std;

#define int int64_t

const int MAXN = 1E5 + 10;

int64_t sum[MAXN];
int len[MAXN];

bool solve(int k, int x, int n) {
	int64_t need = (int64_t)k * x - (sum[n] - sum[n - x]);
	int64_t has = n - x;
	return has >= need;
}

int32_t main() {
	int t; cin >> t;
	while (t--) {
		int n, q; cin >> n >> q;
		for (int i = 0; i < n; i++) cin >> len[i];
		sort(len, len + n);
		for (int i = 0; i < n; i++) sum[i + 1] = sum[i] + len[i];
		while (q--) {
			int k; cin >> k;
			int already = n - (lower_bound(len, len + n, k) - len);
			int l = 0, r = n - already + 1;
			while (r - l > 1) {
				int m = (l + r) >> 1;
				if (solve(k, m, n - already)) l = m;
				else r = m;
			}
			//cout << '!';
			cout << already + l << '\n';
		}
	}
}
