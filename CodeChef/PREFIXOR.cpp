#include <bits/stdc++.h>
using namespace std;

const int MAXN = 4E5 + 10;

int sum[32][MAXN][2], arr[MAXN], xorred[MAXN], maxDst[MAXN], n, queryR;
bool isRev[32] = { false };
vector<int> typeToIdx[32];
vector<int64_t> msPreSum[MAXN * 4], msSufSum[MAXN * 4];
vector<pair<int, int> > msTree[MAXN * 4];

bool queryMulti(int l, int r) {
	for (int i = 0; i < 32; i++) {
		int t = sum[i][r][0] - sum[i][l - 1][0];
		if (isRev[i]) {
			if (sum[i][r][1] - sum[i][l - 1][1] != t) return false;
		} else {
			if (t != 0) return false;
		}
	}
	return true;
}

void modifyMulti(int x) {
	for (int i = 0; i < 32; i++) {
		if (x & (1 << i)) isRev[i + 1] = !isRev[i + 1];
	}
}

void buildMs(int id, int l, int r) {
	if (l == r) {
		msTree[id].push_back({ maxDst[l], maxDst[l] - l + 1 });
		msPreSum[id] = { 0, maxDst[l] - l + 1 };
		msSufSum[id] = { l, 0 };
	} else {
		int m = (l + r) >> 1;
		buildMs(id << 1, l, m); buildMs(id << 1 | 1, m + 1, r);
		msTree[id].resize(msTree[id << 1].size() + msTree[id << 1 | 1].size());
		merge(msTree[id << 1].begin(), msTree[id << 1].end(),
			msTree[id << 1 | 1].begin(), msTree[id << 1 | 1].end(),
			msTree[id].begin());
		msPreSum[id].resize(msTree[id].size() + 1);
		for (int i = 1; i <= (int)msTree[id].size(); i++) {
			msPreSum[id][i] = msPreSum[id][i - 1] + msTree[id][i - 1].second;
		}
		msSufSum[id].resize(msTree[id].size() + 1);
		for (int i = msTree[id].size(); i >= 1; i--) {
			msSufSum[id][i - 1] = msSufSum[id][i] +
				(msTree[id][i - 1].first - msTree[id][i - 1].second + 1);
		}
	}
}

int64_t queryMs(int id, int l, int r, int qL, int qR) {
	if (qL <= l && r <= qR) {
		int idx = lower_bound(msTree[id].begin(), msTree[id].end(),
			make_pair(queryR, 0)) - msTree[id].begin();
		return msPreSum[id][idx] +
			(int64_t)(msTree[id].size() - idx) * (queryR + 1) - msSufSum[id][idx];
	} else {
		int m = (l + r) >> 1;
		int64_t t = 0;
		if (qL <= m) t += queryMs(id << 1, l, m, qL, min(m, qR));
		if (m < qR) t += queryMs(id << 1 | 1, m + 1, r, max(qL, m), qR);
		return t;
	}
}

int32_t main() {
#ifdef OJ_DEBUG
	freopen("out2", "r", stdin);
#endif
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	cin >> n;
	for (int i = 1; i <= n; i++) cin >> arr[i];
	for (int i = 1; i <= n; i++) xorred[i] = xorred[i - 1] ^ arr[i];
	for (int i = 1; i < n; i++) {
		int j;
		for (j = 31; j >= 0; j--) {
			if ((xorred[i] & (1 << j)) != (xorred[i + 1] & (1 << j))) {
				typeToIdx[j + 1].push_back(i);
				if (xorred[i] > xorred[i + 1]) sum[j + 1][i][0]++;
				sum[j + 1][i][1]++;
				break;
			}
		}
		if (j < 0) {
			typeToIdx[0].push_back(i);
			sum[0][i][1]++;
		}
	}
	for (int i = 0; i < 32; i++) {
		for (int j = 1; j <= n; j++) {
			sum[i][j][0] += sum[i][j - 1][0];
			sum[i][j][1] += sum[i][j - 1][1];
		}
	}
	for (int i = 1; i < n; i++) {
		int l = i, r = n;
		while (l != r) {
			int m = (l + r) >> 1;
			if (queryMulti(i, m)) l = m + 1;
			else r = m;
		}
		maxDst[i] = l;
		modifyMulti(arr[i]);
	}
	maxDst[n] = n;
	buildMs(1, 1, n);
	int q;
	int64_t lastAns = 0;
	cin >> q;
	while (q--) {
		int l, r; cin >> l >> r;
		l = (l + lastAns) % n + 1;
		queryR = r = (r + lastAns) % n + 1;
		lastAns = queryMs(1, 1, n, l, r);
		cout << lastAns << '\n';
	}
}
