#include <bits/stdc++.h>
using namespace std;
 
int64_t bit[1000010];
int ans[500010], in[500010], out[500010], timer;
vector<int> g[500010], g2[500010];
 
void addBit(int p, int v) {
	while (p <= timer) {
		bit[p] += v;
		p += p & -p;
	}
}
 
int64_t queryBit(int p) {
	int64_t ans = 0;
	while (p > 0) {
		ans += bit[p];
		p -= p & -p;
	}
	return ans;
}
 
void dfs(int x, int p) {
	in[x] = ++timer;
	for (int y: g[x]) {
		if (y == p) continue;
		dfs(y, x);
	}
	out[x] = ++timer;
}
 
void dfs2(int x, int p) {
	ans[x] = queryBit(out[x]);
	addBit(in[x], 1); addBit(out[x] + 1, -1);
	for (int y: g2[x]) {
		if (y == p) continue;
		dfs2(y, x);
	}
	addBit(in[x], -1); addBit(out[x] + 1, 1);
}
 
int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		int n; cin >> n;
		timer = 0;
		for (int i = 1; i <= n; i++) {
			g[i].clear(); g2[i].clear();
		}
		for (int i = 1; i < n; i++) {
			int u, v; cin >> u >> v;
			g[u].push_back(v); g[v].push_back(u);
		}
		for (int i = 1; i < n; i++) {
			int u, v; cin >> u >> v;
			g2[u].push_back(v); g2[v].push_back(u);
		}
		dfs(1, 0);
		dfs2(1, 0);
		for (int i = 1; i < n; i++) cout << ans[i] << ' ';
		cout << ans[n] << '\n';
	}
}
