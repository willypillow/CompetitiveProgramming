#include <bits/stdc++.h>
using namespace std;

int dp[40][40][40][2];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		memset(dp, 0, sizeof(dp));
		int64_t a, b, c; cin >> a >> b >> c;
		int cntA = __builtin_popcountll(a), cntB = __builtin_popcountll(b);
		dp[0][0][0][0] = 1;
		for (int i = 0; i < 32; i++) {
			int64_t cc = !!(c & (1LL << i));
			for (int j = 0; j <= cntA; j++) {
				for (int k = 0; k <= cntB; k++) {
					for (int m = 0; m < 2; m++) {
						{
							auto bb = (1 ^ m ^ cc);
							dp[i + 1][j + 1][k + bb][(1 + bb + m) >> 1] += dp[i][j][k][m];
						}
						{
							auto bb = (m ^ cc);
							dp[i + 1][j][k + bb][(bb + m) >> 1] += dp[i][j][k][m];
						}
					}
				}
			}
		}
		cout << dp[32][cntA][cntB][0] << '\n';
	}
}
