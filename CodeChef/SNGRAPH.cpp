#include <bits/stdc++.h>
using namespace std;

vector<int> g[100010], deg[100010];
int ans[100010], par[100010];
bool enable[100010];

int find(int x) {
	return par[x] == x ? x : par[x] = find(par[x]);
}

bool merge(int x, int y) {
	int px = find(x), py = find(y);
	if (px == py) return false;
	par[px] = py;
	return true;
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	int t; cin >> t;
	while (t--) {
		int n, m; cin >> n >> m;
		for (int i = 0; i <= n; i++) {
			g[i].clear();
			deg[i].clear();
			enable[i] = false;
		}
		for (int i = 0; i < m; i++) {
			int u, v; cin >> u >> v;
			g[u].push_back(v); g[v].push_back(u);
		}
		for (int i = 1; i <= n; i++) par[i] = i;
		for (int i = 1; i <= n; i++) deg[g[i].size()].push_back(i);
		for (int i = n - 1, merges = 0; i >= 0; i--) {
			ans[i] = n - merges - 1;
			for (int x: deg[i]) {
				enable[x] = true;
				for (int y: g[x]) {
					if (!enable[y]) continue;
					if (merge(x, y)) merges++;
				}
			}
		}
		for (int i = 0; i < n - 1; i++) cout << ans[i] << ' ';
		cout << ans[n - 1] << '\n';
	}
}
