object Main extends App {
  val s = io.StdIn.readInt()
  for (_ <- 1 to s) {
    val _ = io.StdIn.readInt()
    val lis = io.StdIn.readLine().split(" ").map(_.toInt).toList
    val n = lis.size / 2
    val lis2 = (1 to n).toList ++: (1 to n + 1).reverse.toList
    if (lis == lis2) println("yes")
    else println("no")
  }
}
