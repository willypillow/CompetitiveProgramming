object Main extends App {
  val r = io.StdIn.readInt()
  for (_ <- 1 to r) {
    val _ = io.StdIn.readInt()
    val pattern = "^\\.*(\\.*H\\.*T\\.*)*\\.*$".r
    if (pattern.findAllIn(io.StdIn.readLine()).size == 0) println("Invalid")
    else println("Valid")
  }
}
