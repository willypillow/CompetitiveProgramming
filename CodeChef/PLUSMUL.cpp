#include <bits/stdc++.h>
using namespace std;
 
const int MOD = 1E9 + 7;
 
int arr[100010], dp[100010];
 
int64_t poww(int64_t x, int64_t e) {
	if (!e) return 1;
	int64_t t = poww(x, e / 2);
	t = t * t % MOD;
	if (e & 1) return t * x % MOD;
	else return t;
}
 
int main() {
	int t; cin >> t;
	while (t--) {
		int n; cin >> n;
		for (int i = 1; i <= n; i++) cin >> arr[i];
		int64_t acc = 0, acc2 = 0, prd = 1;
		for (int i = n; i > 0; i--) {
			if (i != n) {
				acc += dp[i + 1];
				acc2 = acc2 * arr[i] % MOD;
				acc2 = (acc2 + arr[i] * poww(2, n - i - 1) % MOD) % MOD;
			}
			prd = prd * arr[i] % MOD;
			dp[i] = (acc + acc2 + prd) % MOD;
		}
		cout << dp[1] << '\n';
	}
}
