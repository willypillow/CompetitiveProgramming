#include <bits/stdc++.h>
using namespace std;
 
int arr[510][510], tmp[510][510];
 
int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		int n, m; cin >> n >> m;
		memset(arr, -1, sizeof(arr));
		memset(tmp, -1, sizeof(tmp));
		int mx = 0;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++) {
				cin >> arr[i][j];
				mx = max(mx, arr[i][j]);
			}
		}
		queue<tuple<int, int, int> > q;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++) {
				if (arr[i][j] == mx) q.push(make_tuple(i, j, 0));
			}
		}
		int ans = 0;
		while (!q.empty()) {
			int x, y, d;
			tie(x, y, d) = q.front(); q.pop();
			ans = max(ans, d);
			for (int a = -1; a <= 1; a++) {
				for (int b = -1; b <= 1; b++) {
					if (arr[x + a][y + b] < mx && arr[x + a][y + b] != -1) {
						arr[x + a][y + b] = mx;
						q.push(make_tuple(x + a, y + b, d + 1));
					}
				}
			}
		}
		cout << ans << '\n';
	}
}
