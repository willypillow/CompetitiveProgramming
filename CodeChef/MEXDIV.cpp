#include <bits/stdc++.h>
using namespace std;

const int MOD = 1E9 + 7;

int arr[500010], dp[500010], sum[500010];

int main() {
	int n, k; cin >> n >> k;
	for (int i = 1; i <= n; i++) cin >> arr[i];
	map<int, int> m;
	dp[0] = 1;
	for (int i = 1, l = 1; i <= n; i++) {
		sum[i] = (sum[i - 1] + dp[i - 1]) % MOD;
		if (arr[i] <= k) {
			m[arr[i]]++;
			while (l <= n && (int)m.size() == k + 1) {
				while (l <= n && arr[l] > k) l++;
				if (m[arr[l]] == 1) m.erase(arr[l]);
				else m[arr[l]]--;
				l++;
			}
		}
		dp[i] = (sum[i] - sum[l - 1] + MOD) % MOD;
	}
	cout << dp[n] << '\n';
}
