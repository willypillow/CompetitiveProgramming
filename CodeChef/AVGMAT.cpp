#include <bits/stdc++.h>
using namespace std;

int ans[700], n, m;
int bit[2000][400];
int grid[400][400];

int query(int id, int p) {
	int ret = 0;
	for (; p; p -= p & -p) ret += bit[id][p];
	return ret;
}

void add(int id, int p) {
	for (; p <= m; p += p & -p) bit[id][p] += 1;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		memset(ans, 0, sizeof(ans));
		memset(bit, 0, sizeof(bit));
		cin >> n >> m >> ws;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++) {
				char x; cin >> x;
				grid[i][j] = x == '1';
			}
		}
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++) {
				if (grid[i][j]) {
					for (int d = 1; d <= n + m - 2; d++) {
						ans[d] += query(i + j - d + 900, j);
					}
					add(i + j + 900, j);
				}
			}
		}
		memset(bit, 0, sizeof(bit));
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++) {
				if (grid[i][j]) {
					for (int d = 1; d <= n + m - 2; d++) {
						ans[d] += query(i - j - d + 900, m + 1 - j - 1);
					}
					add(i - j + 900, m + 1 - j);
				}
			}
		}
		for (int i = 1; i < n + m - 2; i++) cout << ans[i] << ' ';
		cout << ans[n + m - 2] << '\n';
	}
}
