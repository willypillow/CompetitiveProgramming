#include <bits/stdc++.h>
using namespace std;

int64_t sel(int x) {
	return x * (x - 1);
}

int64_t diag(int n, int m, int x, int y) {
	int64_t ans = 0;
	for (int i = 1; i <= n; i++) {
		if (i + 1 == x + y) ans += sel(y - 1) + sel(min(i, m) - y);
		else ans += sel(min(i, m));
	}
	for (int j = 2; j <= m; j++) {
		if (n + j == x + y) ans += sel(n - x) + sel(m - y);
		else ans += sel(m - j + 1);
	}
	return ans;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		int n, m, x, y; cin >> n >> m >> x >> y;
		if (n < m) swap(n, m), swap(x, y);
		int64_t ans = 0;
		for (int i = 1; i <= n; i++) {
			if (i == x) ans += sel(y - 1) + sel(m - y);
			else ans += sel(m);
		}
		for (int i = 1; i <= m; i++) {
			if (i == y) ans += sel(x - 1) + sel(n - x);
			else ans += sel(n);
		}
		ans += diag(n, m, x, y);
		x = n - x + 1;
		ans += diag(n, m, x, y);
		ans = sel(n * m - 1) - ans;
		cout << ans << '\n';
	}
}
