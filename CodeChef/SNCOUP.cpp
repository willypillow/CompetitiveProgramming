#include <bits/stdc++.h>
using namespace std;

char arr[2][100010];

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	int t; cin >> t;
	while (t--) {
		int n; cin >> n;
		cin >> arr[0] >> arr[1];
		int res1 = 0, res2 = 1;
		for (int i = 0; i < n; i++) {
			if (arr[0][i] == '*' && arr[1][i] == '*') {
				res1 = 1E9;
				break;
			}
			if (arr[0][i] == '*' || arr[1][i] == '*') res1++;
		}
		res1 = max(res1 - 1, 0);
		int last0 = -1, last1 = -1;
		for (int i = 0; i < n; i++) {
			if (arr[0][i] == '*' && arr[1][i] == '*') {
				if (last0 != -1 || last1 != -1) res2++;
				last0 = last1 = i;
			} else if (arr[0][i] == '*') {
				if (last0 != -1) {
					res2++;
					last1 = -1;
				}
				last0 = i;
			} else if (arr[1][i] == '*') {
				if (last1 != -1) {
					res2++;
					last0 = -1;
				}
				last1 = i;
			}
		}
		//cout << res1 <<  ' ' << res2 << 'x';
		cout << min(res1, res2) << '\n';
	}
}
