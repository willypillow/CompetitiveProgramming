#include <bits/stdc++.h>
using namespace std;

struct Snake { int x1, y1, x2, y2; } s[50];

int mov(int x, int y, int k) {
	bool vert = s[k].y1 == s[k].y2;
	if (vert) {
		int t = abs(y - s[k].y1);
		auto m = minmax(s[k].x1, s[k].x2);
		if (x < m.first) return t + (m.first - x);
		else if (x > m.second) return t + (x - m.second);
		else return t;
	} else {
		int t = abs(x - s[k].x1);
		auto m = minmax(s[k].y1, s[k].y2);
		if (y < m.first) return t + (m.first - y);
		else if (y > m.second) return t + (y - m.second);
		else return t;
	}
}

int main() {
	int t; cin >> t;
	while (t--) {
		int n; cin >> n;
		for (int i = 0; i < n; i++) cin >> s[i].x1 >> s[i].y1 >> s[i].x2 >> s[i].y2;
		int ans = 1E9;
		for (int i = 1; i <= 50; i++) {
			for (int j = 1; j <= 50; j++) {
				int tmp = 0;
				for (int k = 0; k < n; k++) tmp = max(tmp, mov(i, j, k));
				ans = min(tmp, ans);
			}
		}
		cout << ans << '\n';
	}
}
