#include <bits/stdc++.h>
using namespace std;
 
int main() {
	int t; cin >> t;
	while (t--) {
		char s[110]; cin >> s;
		int n = strlen(s);
		for (int i = 0; i < n; i++) {
			if (s[i] == 'm') {
				if (i && s[i - 1] == 's') s[i - 1] = 'x';
				else s[i] = 'M';
			} else if (s[i] == 's') {
				if (i && s[i - 1] == 'M') s[i] = 'x';
			}
		}
		int a = 0, b = 0;
		for (int i = 0; i < n; i++) {
			if (s[i] == 'm' || s[i] == 'M') a++;
			if (s[i] == 's') b++;
		}
		if (a > b) puts("mongooses");
		if (a < b) puts("snakes");
		if (a == b) puts("tie");
	}
}
