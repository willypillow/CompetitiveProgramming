#include <bits/stdc++.h>
using namespace std;

#define int int64_t

int64_t dp[100000][2];
int price[100000], c[100000];
vector<int> g[100000];

bool cmp(const pair<int64_t, int64_t> &lhs, const pair<int64_t, int64_t> &rhs) {
	return (lhs.first - lhs.second) < (rhs.first - rhs.second);
}

int64_t dfs(int x, int p, bool hasP) {
	if (dp[x][hasP]) return dp[x][hasP];
	vector<pair<int64_t, int64_t> > tmp;
	for (int y: g[x]) {
		if (y != p) tmp.push_back({ dfs(y, x, false), dfs(y, x, true) });
	}
	sort(tmp.begin(), tmp.end(), cmp);
	int64_t res1 = 0, res2 = price[x];
	for (int i = 0; i < (int)tmp.size(); i++) res2 += tmp[i].second;
	if ((int)tmp.size() + hasP < c[x]) {
		return dp[x][hasP] = res2;
	} else {
		for (int i = 0; i < c[x] - hasP; i++) res1 += tmp[i].first;
		for (int i = c[x] - hasP; i < (int)tmp.size(); i++) res1 += tmp[i].second;
		return dp[x][hasP] = min(res1, res2);
	}
}

int32_t main() {
	int t; cin >> t;
	while (t--) {
		memset(dp, 0, sizeof(dp));
		int n; cin >> n;
		for (int i = 1; i <= n; i++) g[i].clear();
		for (int i = 1; i < n; i++) {
			int u, v; cin >> u >> v;
			g[u].push_back(v); g[v].push_back(u);
		}
		for (int i = 1; i <= n; i++) cin >> price[i];
		for (int i = 1; i <= n; i++) cin >> c[i];
		cout << dfs(1, 0, false) << '\n';
	}
}
