#include <bits/stdc++.h>
#include <x86intrin.h>
using namespace std;

struct PiiHash {
	size_t operator()(const pair<uint32_t, uint32_t> &p) const {
		return p.first * 31 + p.second;
	}
};
template<uint32_t P, uint32_t *AE> struct Hash {
	uint64_t a, val, size;
	Hash(uint64_t _a): a(_a), val(0), size(0) { }
	void calcAe(int n) {
		AE[0] = 1;
		for (int i = 1; i < n; i++) AE[i] = uint32_t(AE[i - 1] * a % P);
	}
	void push_back(char c) {
		size++;
		val = (val * a + c) % P;
	}
	void pop_front(char c) {
		size--;
		val = (val - (uint64_t)AE[size] * c % P + P) % P;
	}
};
char s[10000001], t[1000001];
uint32_t ae1[1000000], ae2[1000000];
unordered_set<pair<uint32_t, uint32_t>, PiiHash> st;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	cin.getline(t, sizeof(t));
	int n; cin >> n >> std::ws;
	int len = (int)strlen(t);
	st.reserve(1 << 20), st.max_load_factor(0.25);
	Hash<1000099999, ae1> h1(uint32_t(__rdtsc() & ((1 << 30) - 1)));
	Hash<0x3f3f3f2f, ae2> h2(uint32_t(__rdtsc() & ((1 << 30) - 1)));
	h1.calcAe(len), h2.calcAe(len);
	for (char *c = t; *c; c++) h1.push_back(*c), h2.push_back(*c);
	st.emplace(h1.val, h2.val);
	for (int i = 0; i < len - 1; i++) {
		h1.pop_front(t[i]), h2.pop_front(t[i]);
		h1.push_back(t[i]), h2.push_back(t[i]);
		st.emplace(h1.val, h2.val);
	}
	while (n--) {
		cin.getline(s, sizeof(s));
		int slen = (int)strlen(s);
		if (slen < len) {
			cout << "0\n";
		} else {
			auto sh1 = h1; sh1.val = sh1.size = 0;
			auto sh2 = h2; sh2.val = sh2.size = 0;
			for (int i = 0; i < len; i++) sh1.push_back(s[i]), sh2.push_back(s[i]);
			int ans = 0;
			if (st.count({sh1.val, sh2.val})) ans++;
			for (int i = len; i < slen; i++) {
				sh1.pop_front(s[i - len]), sh2.pop_front(s[i - len]);
				sh1.push_back(s[i]), sh2.push_back(s[i]);
				if (st.count({sh1.val, sh2.val})) ans++;
			}
			cout << ans << '\n';
		}
	}
}
