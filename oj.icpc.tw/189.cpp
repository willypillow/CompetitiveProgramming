#include <bits/stdc++.h>
using namespace std;

const int64_t INF = 0x3f3f3f3f3f3f3f3fLL;

int64_t timer, field[502][502];
pair<int64_t, int64_t> dist[502][502];

template<class PQ, class Pred>
int64_t dijkstra(PQ &q, Pred isDest) {
	timer++;
	while (q.size()) {
		int64_t dis, x, y; tie(dis, x, y) = q.top(); q.pop();
		if (isDest(x, y) || dis >= INF) return dis;
		if (dist[x][y] < make_pair(-timer, dis)) continue;
		const pair<int, int> D[4] = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};
		for (auto &p: D) {
			auto nx = x + p.first, ny = y + p.second;
			if (make_pair(-timer, dis + field[nx][ny]) < dist[nx][ny]) {
				dist[nx][ny] = {-timer, dis + field[nx][ny]};
				q.push({dis + field[nx][ny], nx, ny});
			}
		}
	}
	return INF;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int q, n, m; cin >> q >> n >> m;
	memset(field, 0x3f, sizeof(field));
	while (q--) {
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++) {
				int64_t x; cin >> x;
				field[i][j] = (x == -1) ? 0 : (x == 0) ? INF : x;
			}
		}
		typedef tuple<int64_t, int64_t, int64_t> Tiii;
		priority_queue<Tiii, vector<Tiii>, greater<Tiii> > pq;
		for (int i = 2; i <= n; i++) pq.push({field[i][1], i, 1});
		for (int j = 1; j < m; j++) pq.push({field[n][j], n, j});
		auto ans = dijkstra(pq, [&](int64_t x, int64_t y) { return x == 1 || y == m; });
		cout << (ans >= INF ? -1 : ans) << '\n';
	}
}
