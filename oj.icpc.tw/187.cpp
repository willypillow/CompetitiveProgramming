#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		int64_t a[4];
		for (int i = 0; i < 4; i++) cin >> a[i];
		int64_t v; cin >> v;
		a[1] += a[2] * 2 + a[3] * 10;
		int64_t per = (v + 5 - 1) / 5, change = per * 5 - v, cnt = a[1] / per;
		a[0] += cnt * change;
		cout << a[0] << '\n';
	}
}
