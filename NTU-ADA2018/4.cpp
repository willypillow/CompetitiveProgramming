#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		int n; cin >> n;
		int d = 1, day = 0;
		while (n) {
			n -= d;
			if (~n & d) d <<= 1;
			day++;
		}
		cout << day << '\n';
	}
}
