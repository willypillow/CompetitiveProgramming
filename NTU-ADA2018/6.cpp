#include <bits/stdc++.h>
using namespace std;

const int MOD = 1E9 + 7;

struct ModInt {
	int x;
	ModInt operator +(const ModInt &that) {
		int res = x + that.x;
		if (res >= MOD) res -= MOD;
		return ModInt{res};
	}
	ModInt operator +=(const ModInt &that) {
		return *this = *this + that;
	}
	ModInt operator =(const int &that) {
		return x = that, *this;
	}
} dp[2][3][3][3][33][2];

#define NEXT dp[i & 1] \
	[(j + (d == 3)) % 3][(k + (d == 6)) % 3] \
	[(m + (d == 9)) % 3][(n * 10 + d) % 33]
#define PREV dp[~i & 1][j][k][m][n]

int solve(const string &x) {
	if (x[0] == '0') return 1;
	memset(dp[0], 0, sizeof(dp[0]));
	dp[0][0][0][0][0][0] = 1;
	for (int i = 1; i <= (int)x.size(); i++) {
		memset(dp[i & 1], 0, sizeof(dp[i & 1]));
		for (int j = 0; j < 3; j++) {
			for (int k = 0; k < 3; k++) {
				for (int m = 0; m < 3; m++) {
					for (int n = 0; n < 33; n++) {
						int d;
						for (d = 0; d < x[i - 1] - '0'; d++) NEXT[1] += PREV[0] + PREV[1];
						NEXT[0] += PREV[0], NEXT[1] += PREV[1];
						for (d = x[i - 1] - '0' + 1; d <= 9; d++) NEXT[1] += PREV[1];
					}
				}
			}
		}
	}
	return (dp[x.size() & 1][0][0][0][0][0] + dp[x.size() & 1][0][0][0][0][1]).x;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	string l, r; cin >> l >> r;
	int i;
	for (i = (int)l.size() - 1; l[i] == '0'; i--) l[i] = '9';
	l[i]--;
	cout << (solve(r) - solve(l) + MOD) % MOD;
}
