#include <bits/stdc++.h>
using namespace std;

pair<int, int> p[100000];

int calc(int x, int y) {
	int c = min(x, y) / 3;
	x -= 3 * c, y -= 3 * c;
	int a = min(x / 2, y), b = min(x - 2 * a, (y - a) / 2);
	return x + y - a - b + 4 * c;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 0; i < n; i++) cin >> p[i].first >> p[i].second;
	for (int i = 0; i < n; i++) {
		int64_t ans = 0;
		for (int j = 0; j < n; j++) {
			int x = abs(p[i].first - p[j].first), y = abs(p[i].second - p[j].second);
			ans += min(calc(x, y), calc(y, x));
		}
		cout << ans << '\n';
	}
}
