#include <bits/stdc++.h>
using namespace std;
using Pt = pair<int, int>;

inline Pt divMod3(int a) {
	int q = (int)ceil(a / 3.0);
	return { q, q * 3 - a };
}

const int MAXN = 100001, TYPECNT = 8;
const pair<function<int(const Pt&)>, function<Pt(const Pt&)> > TYPES[] = {
	{
		[](const Pt &t) { return get<0>(t) * 2 + get<1>(t); },
		[](const Pt &t) { return divMod3(2 * (get<0>(t) - get<1>(t))); }
	}, {
		[](const Pt &t) { return get<0>(t) + get<1>(t) * 2; },
		[](const Pt &t) { return make_pair(get<0>(t), 0); }
	}, {
		[](const Pt &t) { return -get<0>(t) + get<1>(t) * 2; },
		[](const Pt &t) { return divMod3(2 * (get<0>(t) + get<1>(t))); }
	}, {
		[](const Pt &t) { return -get<0>(t) * 2 + get<1>(t); },
		[](const Pt &t) { return make_pair(get<1>(t), 0); }
	}, {
		[](const Pt &t) { return -get<0>(t) * 2 - get<1>(t); },
		[](const Pt &t) { return divMod3(2 * (-get<0>(t) + get<1>(t))); }
	}, {
		[](const Pt &t) { return -get<0>(t) - get<1>(t) * 2; },
		[](const Pt &t) { return make_pair(-get<0>(t), 0); }
	}, {
		[](const Pt &t) { return get<0>(t) - get<1>(t) * 2; },
		[](const Pt &t) { return divMod3(2 * (-get<0>(t) - get<1>(t))); }
	}, {
		[](const Pt &t) { return get<0>(t) * 2 - get<1>(t); },
		[](const Pt &t) { return make_pair(-get<1>(t), 0); }
	}
};
Pt origPt[MAXN];
tuple<int, int, int, pair<int, int> > pt[MAXN], tmpPt[MAXN];
int64_t ans[MAXN];

void cdq(int l, int r) {
	if (l == r) return;
	int m = (l + r) >> 1;
	cdq(l, m), cdq(m + 1, r);
	valarray<int64_t> rem[3] = { { 0, 0 }, { 0, 0 }, { 0, 0 } };
	for (int i = l, tL = l, tR = m + 1; i <= r; i++) {
		if (tL <= m && (tR > r || get<1>(pt[tL]) < get<1>(pt[tR]))) {
			tmpPt[i] = pt[tL++];
			rem[get<3>(tmpPt[i]).second] += { get<3>(tmpPt[i]).first, 1 };
		} else {
			tmpPt[i] = pt[tR++];
			for (int j = 0; j < 3; j++) {
				int curRem = get<3>(tmpPt[i]).second, d = j < curRem;
				ans[get<2>(tmpPt[i])] += rem[j][0] - rem[j][1] * (get<3>(tmpPt[i]).first - d);
			}
		}
	}
	copy_n(tmpPt + l, r - l + 1, pt + l);
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 0; i < n; i++) cin >> origPt[i].first >> origPt[i].second;
	for (int type = 0; type < TYPECNT; type++) {
		for (int i = 0; i < n; i++) {
			int id = type ? get<2>(pt[i]) : i;
			pt[i] = {
				-TYPES[type].first(origPt[id]),
				TYPES[(type + 1) % TYPECNT].first(origPt[id]),
				id,
				TYPES[type].second(origPt[id])
			};
		}
		if (type == 0) sort(pt, pt + n);
		else reverse(pt, pt + n);
		cdq(0, n - 1);
	}
	for (int i = 0; i < n; i++) cout << ans[i] << '\n';
}
