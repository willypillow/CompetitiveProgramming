#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n; cin >> n;
	vector<int> arr(n + 1);
	for_each(arr.begin() + 1, arr.end(), [](int &x) { cin >> x; });
	partial_sum(arr.begin(), arr.end(), arr.begin());
	int ans = -1E9, mn = 1E9;
	for (int x: arr) {
		ans = max(ans, x - mn);
		mn = min(mn, x);
	}
	cout << ans;
}
