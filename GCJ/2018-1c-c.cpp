#include <bits/stdc++.h>
using namespace std;

int64_t w[100010], dp[100010];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	for (int cases = 1; cases <= t; cases++) {
		int n; cin >> n;
		for (int i = 0; i < n; i++) cin >> w[i];
		memset(dp, 0x3f, sizeof(dp));
		int64_t x3f = dp[0];
		dp[0] = 0;
		for (int i = 0; i < n; i++) {
			int idx = (int)(upper_bound(dp, dp + n + 1, w[i] * 6) - dp);
			for (int j = idx - 1; j >= 0; j--) {
				dp[j + 1] = min(dp[j + 1], dp[j] + w[i]);
			}
		}
		int ans = n;
		while (ans > 0 && dp[ans] == x3f) ans--;
		cout << "Case #" << cases << ": " << ans << '\n';
	}
}
