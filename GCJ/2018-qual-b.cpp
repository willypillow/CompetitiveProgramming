#include <bits/stdc++.h>
using namespace std;

int a[100000], b[2][50000], c[100000];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int t; cin >> t;
	for (int cases = 1; cases <= t; cases++) {
		int n; cin >> n;
		for (int i = 0; i < n; i++) {
			cin >> a[i];
			b[i & 1][i >> 1] = a[i];
		}
		sort(a, a + n), sort(b[0], b[0] + ((n + 1) >> 1)), sort(b[1], b[1] + (n >> 1));
		for (int i = 0; i < n; i++) c[i] = b[i & 1][i >> 1];
		cout << "Case #" << cases << ": ";
		int i;
		for (i = 0; i < n; i++) {
			if (a[i] != c[i]) {
				cout << i << '\n';
				break;
			}
		}
		if (i == n) cout << "OK\n";
	}
}
