#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	for (int cases = 1; cases <= t; cases++) {
		int d; cin >> d;
		string p; cin >> p;
		int damage = 0, str = 1, ans = 0, lvl = 0, cnt[30] = {0};
		for (char c: p) {
			if (c == 'C') str <<= 1, lvl++;
			else cnt[lvl]++, damage += str;
		}
		while (damage > d) {
			while (lvl && !cnt[lvl]) lvl--;
			if (!lvl) break;
			cnt[lvl]--, cnt[lvl - 1]++;
			ans++;
			damage -= (1 << (lvl - 1));
		}
		cout << "Case #" << cases << ": ";
		if (damage > d) cout << "IMPOSSIBLE\n";
		else cout << ans << '\n';
	}
}
