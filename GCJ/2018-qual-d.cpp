#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int t; cin >> t;
	for (int cases = 1; cases <= t; cases++) {
		double a; cin >> a;
		double l = 0, r = atan(1);
		while (r - l > 1E-8) {
			auto m = (l + r) / 2;
			if (cos(m) + sin(m) > a) r = m;
			else l = m;
		}
		cout << "Case #" << cases << ":\n";
		cout.precision(12);
		cout << fixed << 0.5 * cos(l) << ' ' << 0.5 * sin(l) << " 0\n";
		cout << fixed << 0.5 * -sin(l) << ' ' << 0.5 * cos(l) << " 0\n";
		cout << "0 0 0.5\n";
	}
}
