#include <bits/stdc++.h>
using namespace std;

string str[2000];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	unordered_set<uint64_t> st;
	st.reserve(20000), st.max_load_factor(0.25);
	for (int cases = 1; cases <= t; cases++) {
		st.clear();
		int n, l; cin >> n >> l;
		for (int i = 0; i < n; i++) {
			cin >> str[i];
			uint64_t h = 0;
			for (int j = 0; j < l; j++) {
				h = h << 5 | (str[i][j] - 'A' + 1);
				st.insert(h);
			}
		}
		string ansStr = "";
		for (int i = 1; i < l; i++) {
			for (int j = 0; j < n; j++) {
				uint64_t h = 0;
				for (int k = 0; k < i; k++) h = h << 5 | (str[j][k] - 'A' + 1);
				h <<= 5;
				for (int k = 0; k < n; k++) {
					if (!st.count(h + (str[k][i] - 'A' + 1))) {
						ansStr = str[j].substr(0, i) + str[k][i];
						goto End;
					}
				}
			}
		}
End:
		cout << "Case #" << cases << ": ";
		if (ansStr.size()) {
			ansStr += str[0].substr(ansStr.size(), l - ansStr.size());
			cout << ansStr << '\n';
		} else {
			cout << "-\n";
		}
	}
}
