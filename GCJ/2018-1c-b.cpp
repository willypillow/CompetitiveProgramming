#include <bits/stdc++.h>
using namespace std;

int cnt[200], choose[200];
bool used[200];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		memset(cnt, 0, sizeof(cnt)), memset(used, false, sizeof(used));
		int n; cin >> n;
		for (int i = 0; i < n; i++) {
			int d; cin >> d;
			pair<int, int> mn = {1E9, -1};
			for (int j = 0; j < d; j++) cin >> choose[j];
			random_shuffle(choose, choose + d);
			for (int j = 0; j < d; j++) {
				int x = choose[j];
				if (!used[x]) mn = min(mn, {cnt[x], x});
				cnt[x]++;
			}
			if (mn.second != -1) used[mn.second] = true;
			cout << mn.second << endl;
		}
	}
}
