#include <bits/stdc++.h>
using namespace std;

bool vis[25][15];

inline bool check(int x, int y) {
	for (int i = -1; i <= 1; i++) {
		for (int j = -1; j <= 1; j++) {
			if (!vis[x + i][y + j]) return true;
		}
	}
	return false;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int t; cin >> t;
	for (int cases = 1; cases <= t; cases++) {
		int a; cin >> a;
		memset(vis, false, sizeof(vis));
		queue<pair<int, int> > q;
		for (int i = 2; i < 22; i++) {
			for (int j = 2; j < 12; j++) q.push({i, j});
		}
		int cnt = 0;
		while (q.size() && cnt < 1000) {
			auto p = q.front(); q.pop();
			if (!check(p.first, p.second)) continue;
			cout << p.first << ' ' << p.second << endl;
			cnt++;
			int x, y; cin >> x >> y;
			if (!x && !y) goto End;
			vis[x][y] = true;
			q.push(p);
		}
		assert(0);
End:;
	}
}
