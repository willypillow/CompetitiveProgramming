#include <bits/stdc++.h>
using namespace std;

int pref[26];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	string str; cin >> str;
	uint64_t dup = 0;
	pref[str[0] - 'a']++;
	for (int i = 1; i < (int)str.size(); i++) {
		dup += pref[str[i] - 'a'];
		pref[str[i] - 'a']++;
	}
	cout << (uint64_t)str.size() * (str.size() - 1) / 2 - dup + 1 << '\n';
}
