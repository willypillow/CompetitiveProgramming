#include <bits/stdc++.h>
using namespace std;

const int MAXN = 2E5 + 10;
const double PI = atan((double)1.0) * 4;

int n, lis[MAXN];

vector<pair<int, int> > fount;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int x1, y1, x2, y2; cin >> x1 >> y1 >> x2 >> y2 >> n;
	if (x1 > x2) swap(x1, x2), swap(y1, y2);
	bool flip = false;
	if (y1 > y2) {
		flip = true;
		y1 = 1E9 - y1, y2 = 1E9 - y2;
	}
	for (int i = 1; i <= n; i++) {
		int x, y; cin >> x >> y;
		if (flip) y = 1E9 - y;
		if (x1 <= x && x <= x2 && y1 <= y && y <= y2) fount.push_back({ x, y });
	}
	sort(fount.begin(), fount.end());
	int ans = 0;
	for (int i = 0; i < (int)fount.size(); i++) {
		int p = lower_bound(lis, lis + ans, fount[i].second) - lis;
		if (p == ans) lis[ans++] = fount[i].second;
		else lis[p] = fount[i].second;
	}
	double gridAns = 100.0 * (x2 - x1 + y2 - y1);
	if (ans == min(x2 - x1, y2 - y1) + 1) {
		gridAns -= 20 - 10 * PI;
		ans--;
	}
	cout << fixed << setprecision(15) << gridAns - ans * (20 - 5 * PI) << '\n';
}
