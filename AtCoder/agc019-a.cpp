#include <bits/stdc++.h>
using namespace std;

const double X[] = { 0.25, 0.5, 1, 2 };

int64_t a[4];
pair<double, int> s[4];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	double n;
	int64_t ans = 0;
	cin >> a[0] >> a[1] >> a[2] >> a[3] >> n;
	for (int i = 0; i < 4; i++) s[i] = { a[i] / X[i], i };
	sort(s, s + 4);
	for (int i = 0; i < 4; i++) {
		int cnt = floor(n / X[s[i].second]);
		ans += cnt * a[s[i].second];
		n -= cnt * X[s[i].second];
	}
	cout << ans << '\n';
}
