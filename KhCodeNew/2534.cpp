#include <bits/stdc++.h>
using namespace std;

bool vis[10000001];
int delta[50][7], n, m;

int apply(int cur, int op) {
	int tmp[7] = { 0 };
	for (int i = n - 1; i >= 0; i--) {
		tmp[i] = cur % 10;
		cur /= 10;
	}
	for (int i = 0; i < n; i++) tmp[i] = (tmp[i] + delta[op][i]) % 10;
	int res = 0;
	for (int i = 0; i < n; i++) res = res * 10 + tmp[i];
	return res;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	cin >> n >> m;
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			int t; cin >> t;
			delta[i][j] = t % 10;
		}
	}
	int s, t; cin >> s >> t;
	queue<pair<int, int> > q;
	q.push({ s, 0 });
	int ans = 1E9;
	while (!q.empty()) {
		pair<int, int> cur = q.front(); q.pop();
		if (cur.first == t) {
			ans = cur.second;
			break;
		}
		for (int i = 0; i < m; i++) {
			int nxt = apply(cur.first, i);
			if (!vis[nxt]) {
				q.push({ nxt, cur.second + 1 });
				vis[nxt] = true;
			}
		}
	}
	cout << ans << '\n';
}
