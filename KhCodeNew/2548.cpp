#include <cstdio>
#include <iostream>
using namespace std;

int main() {
	long long arr[3];
	for (int i = 0; i < 3; i++) cin >> arr[i];
	if (arr[0] <= arr[1] && arr[1] <= arr[2] || arr[0] >= arr[1] && arr[1] >= arr[2]) puts("Yes");
	else puts("No");
}
