#include <iostream>
#include <iomanip>
using namespace std;

int main() {
	int n; cin >> n;
	int cnt[101] = { 0 }, tot = 0;
	double ans = -1;
	for (int i = 0; i < n; i++) {
		int val; cin >> val;
		if (val <= 100 && val >= 0) {
			cnt[val]++; tot++;
			int acc = 0;
			for (int i = 0; i <= 100; i++) {
				if (acc + cnt[i] > tot / 2) {
					ans = i;
					break;
				}
				if (tot % 2 == 0 && acc + cnt[i] == tot / 2) {
					int j;
					for (j = i + 1; ; j++) {
						if (cnt[j]) break;
					}
					ans = ((double)i + j) / 2;
					break;
				}
				acc += cnt[i];
			}
		}
		if (ans < 0) cout << "no data\n";
		else cout << std::fixed << std::setprecision(1) << ans << '\n';
	}
}
