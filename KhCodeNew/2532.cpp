#include <iostream>
#include <cstring>
using std::cin; using std::cout;

const int MAXN = 5001, MOD = 10000019;
int dpSum[MAXN][MAXN], dpCmp[MAXN][MAXN], n, arr[MAXN], dpFirstRow;

int dfs(int lastWidth, int left);

int cmpFirstRow() {
	if (dpFirstRow) return dpFirstRow;
	for (int i = 1; i < n; i++) {
		if (arr[i - 1] > arr[i]) return dpFirstRow = i;
	}
	return dpFirstRow = n;
}

int cmp(int start, int len) {
	if (len == n) return cmpFirstRow();
	int &ans = dpCmp[start][len];
	if (ans) return ans;
	if (start + len + 1 >= n) {
		if (arr[start] > arr[start + len]) return ans = 0;
		for (int i = 1; start + len + i < n; i++) {
			if (arr[start + i] > arr[start + len + i] ||
					arr[start + len + i - 1] > arr[start + len + i]) {
				return ans = i;
			}
		}
		for (int i = 1; i < len; i++) {
			if (arr[start + i - 1] > arr[start + i]) {
				return ans = i;
			}
		}
		return ans = len;
	}
	if (arr[start] > arr[start + len]) return ans = 0;
	if (arr[start] > arr[start + 1] || arr[start + len] > arr[start + len + 1]) {
		return ans = 1;
	}
	int res = cmp(start + 1, len);
	if (res >= len - 1) return ans = len;
	else return ans = res + 1;
}

int sum(int len, int n) {
	if (len == 0) return 0;
	if (n - len < 0) return sum(n, n);
	if (dpSum[len][n]) return dpSum[len][n];
	return dpSum[len][n] = (sum(len - 1, n) + dfs(len, n - len)) % MOD;
}

int dfs(int lastWidth, int left) {
	if (left == 0) return 1;
	int maxW = cmp(n - left - lastWidth, lastWidth);
	return sum(maxW, left);
}

int main() {
	while (cin >> n) {
		dpFirstRow = 0;
		memset(dpCmp, 0, sizeof(dpCmp)); memset(dpSum, 0, sizeof(dpSum));
		for (int i = 0; i < n; i++) cin >> arr[i];
		cout << dfs(n, n) << '\n';
	}
}
