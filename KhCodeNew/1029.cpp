#include <iostream>
using std::cin; using std::cout;

int main() {
	unsigned long long n, max = 0, ma, mb; cin >> n;
	while (n--) {
		unsigned long long a, b;
		cin >> a >> b;
		if (max < a * b) { max = a * b; ma = a; mb = b;}
	}
	cout << ma << ' ' << mb << '\n';
}
