#include <iostream>
#include <cstdio>
using std::cin; using std::cout;

int main() {
	int n; cin >> n;
	while (n--) {
		int a, b, c, d; cin >> a >> b >> c >> d;
		if (c < a) { std::swap(a, c); std::swap(b, d); }
		if (c == a) cout << "Yes\n";
		else cout << ((b >= c) ? "Yes" : "No") << '\n';
	}
}
