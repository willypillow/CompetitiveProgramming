#include <iostream>
using std::cin; using std::cout;

int main() {
	int n, m;
	while (cin >> n >> m) {
		long long ans = 0, tmp = 1;
		for (int i = 1; i <= m; i++) {
			tmp = tmp * n % 32749;
			ans = (ans + tmp) % 32749;
		}
		cout << ans << '\n';
	}
}
