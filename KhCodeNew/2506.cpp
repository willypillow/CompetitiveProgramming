#include <iostream>
using namespace std;

int main() {
	int n, m;
	while (cin >> n >> m >> std::ws) {
		char str[200001]; cin.getline(str, 200001);
		int cnt[26] = { 0 }, ans = 0;
		for (int i = 0; i < m; i++) cnt[str[i] - 'a']++;
		for (int i = m - 1, iter = 0; iter < n; i = (i + 1) % n, iter++) {
			int diff = 0;
			for (int j = 0; diff <= 1 && j < 26; j++) {
				if (cnt[j]) diff++;
			}
			if (diff == 1) { ans++; }
			cnt[str[(i - m + 1 + n) % n] - 'a']--; cnt[str[(i + 1) % n] - 'a']++;
		}
		cout << ans << '\n';
	}
}
