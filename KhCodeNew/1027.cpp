#include <iostream>
#include <cstdio>
using std::cin; using std::cout;

int main() {
	int n; cin >> n;
	while (n--) {
		int x; cin >> x;
		bool prnt = false;
		for (int i = 31; i >= 0; i--) {
			if (prnt) {
				cout << ((x >> i) & 1);
			} else if ((x >> i) & 1) {
				prnt = true;
				cout << '1';
			}
		}
		if (!prnt) cout << '0';
		cout << '\n';
	}
}
