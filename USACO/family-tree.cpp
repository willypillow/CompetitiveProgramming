#include <bits/stdc++.h>
using namespace std;

set<pair<string, string> > g;

int dfsGm(string x, string dest) {
	if (x == dest) return 0;
	for (auto &p: g) {
		if (p.first != x) continue;
		int t = dfsGm(p.second, dest);
		if (t != -1) return t + 1;
	}
	return -1;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
#ifndef OJ_DEBUG
	freopen("family.in", "r", stdin);
	freopen("family.out", "w", stdout);
#endif
	int n; cin >> n;
	string q1, q2; cin >> q1 >> q2;
	while (n--) {
		string u, v; cin >> u >> v;
		g.insert({u, v});
	}
	string q1P, q2P;
	for (auto &p: g) {
		if (p.second == q1) q1P = p.first;
		if (p.second == q2) q2P = p.first;
	}
	if (q1P == q2P) {
		cout << "SIBLINGS\n";
		return 0;
	}
	int cnt = dfsGm(q1, q2);
	if (cnt != -1) {
		if (cnt == 1) {
			cout << q1 << " is the mother of " << q2 << '\n';
		} else {
			string great;
			while (cnt-- > 2) great += "great-";
			cout << q1 << " is the " << great << "grand-mother of " << q2 << '\n';
		}
		return 0;
	}
	swap(q1, q2);
	cnt = dfsGm(q1, q2);
	if (cnt != -1) {
		if (cnt == 1) {
			cout << q1 << " is the mother of " << q2 << '\n';
		} else {
			string great;
			while (cnt-- > 2) great += "great-";
			cout << q1 << " is the " << great << "grand-mother of " << q2 << '\n';
		}
		return 0;
	}
	for (auto &p: g) {
		if (p.second != q2) continue;
		int cnt2 = dfsGm(p.first, q1);
		if (cnt2 != -1) {
			string great;
			while (cnt2-- > 2) great += "great-";
			cout << q2 << " is the " << great << "aunt of " << q1 << '\n';
			return 0;
		}
	}
	swap(q1, q2);
	for (auto &p: g) {
		if (p.second != q2) continue;
		int cnt2 = dfsGm(p.first, q1);
		if (cnt2 != -1) {
			string great;
			while (cnt2-- > 2) great += "great-";
			cout << q2 << " is the " << great << "aunt of " << q1 << '\n';
			return 0;
		}
	}
	for (auto &p: g) {
		if (dfsGm(p.first, q1) != -1 && dfsGm(p.first, q2) != -1) {
			cout << "COUSINS\n";
			return 0;
		}
	}
	cout << "NOT RELATED\n";
}
