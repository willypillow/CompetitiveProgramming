#include <bits/stdc++.h>
using namespace std;

int field[1000], pos[1000], arr[1000];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	memset(field, -1, sizeof(field)), memset(pos, -1, sizeof(pos));
#ifndef OJ_DEBUG
	freopen("milkorder.in", "r", stdin);
	freopen("milkorder.out", "w", stdout);
#endif
	int n, m, k; cin >> n >> m >> k;
	bool front = false;
	for (int i = 0; i < m; i++) {
		cin >> arr[i];
		if (arr[i] == 1) front = 1;
	}
	for (int i = 0; i < k; i++) {
		int c, p; cin >> c >> p;
		field[p] = c, pos[c] = p;
	}
	if (front) {
		int last = 1;
		for (int i = 0; i < m; i++) {
			if (pos[arr[i]] != -1) {
				last = pos[arr[i]];
			} else {
				while (field[last] != -1) last++;
				field[last] = arr[i], pos[arr[i]] = last;
			}
		}
	} else {
		int last = n;
		for (int i = m - 1; i >= 0; i--) {
			if (pos[arr[i]] != -1) {
				last = pos[arr[i]];
			} else {
				while (field[last] != -1) last--;
				field[last] = arr[i], pos[arr[i]] = last;
			}
		}
	}
	if (pos[1] != -1) {
		cout << pos[1] << '\n';
	} else {
		int i = 1;
		for (; field[i] != -1; i++);
		cout << i << '\n';
	}
}
