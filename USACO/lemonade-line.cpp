#include <bits/stdc++.h>
using namespace std;

int a[100000];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
#ifndef OJ_DEBUG
	freopen("lemonade.in", "r", stdin);
	freopen("lemonade.out", "w", stdout);
#endif
	int n; cin >> n;
	for (int i = 0; i < n; i++) cin >> a[i];
	sort(a, a + n, greater<int>());
	int cur = 0;
	for (int i = 0; i < n; i++) {
		if (cur <= a[i]) cur++;
	}
	cout << cur;
}
