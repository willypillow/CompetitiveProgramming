#include <bits/stdc++.h>
using namespace std;

char field[4][4];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
#ifndef OJ_DEBUG
	freopen("tttt.in", "r", stdin);
	freopen("tttt.out", "w", stdout);
#endif
	for (int i = 0; i < 3; i++) cin >> field[i];
	set<int> ans1;
	set<pair<int, int> > ans2;
	for (int i = 0; i < 3; i++) {
		set<char> st;
		for (int j = 0; j < 3; j++) st.insert(field[i][j]);
		if (st.size() == 1) ans1.insert(*st.begin());
		else if (st.size() == 2) ans2.insert({*st.begin(), *st.rbegin()});
	}
	for (int i = 0; i < 3; i++) {
		set<char> st;
		for (int j = 0; j < 3; j++) st.insert(field[j][i]);
		if (st.size() == 1) ans1.insert(*st.begin());
		else if (st.size() == 2) ans2.insert({*st.begin(), *st.rbegin()});
	}
	{
		set<char> st;
		for (int j = 0; j < 3; j++) st.insert(field[j][j]);
		if (st.size() == 1) ans1.insert(*st.begin());
		else if (st.size() == 2) ans2.insert({*st.begin(), *st.rbegin()});
	}
	{
		set<char> st;
		for (int j = 0; j < 3; j++) st.insert(field[j][2 - j]);
		if (st.size() == 1) ans1.insert(*st.begin());
		else if (st.size() == 2) ans2.insert({*st.begin(), *st.rbegin()});
	}
	cout << ans1.size() << '\n' << ans2.size() << '\n';
}
