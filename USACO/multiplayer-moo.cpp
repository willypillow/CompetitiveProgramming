#include <bits/stdc++.h>
using namespace std;

const int DX[] = {-1, 0, 1, 0}, DY[] = {0, 1, 0, -1};

int vis[300][300], id[300][300], timer;
set<int> has[300][300];

pair<int, int> dfs(int x, int y, int c1, int c2) {
	if (vis[x][y] == timer) return {0, 0};
	vis[x][y] = timer;
	has[x][y].insert(id[x][y] == c1 ? c2 : c1);
	pair<int, int> ret = {id[x][y] == c2, id[x][y] == c1};
	for (int i = 0; i < 4; i++) {
		int nx = x + DX[i], ny = y + DY[i];
		if (id[nx][ny] == c1 || id[nx][ny] == c2) {
			auto p = dfs(nx, ny, c1, c2);
			ret.first += p.first, ret.second += p.second;
		}
	}
	return ret;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
#ifndef OJ_DEBUG
	freopen("multimoo.in", "r", stdin);
	freopen("multimoo.out", "w", stdout);
#endif
	memset(id, -1, sizeof(id));
	int n; cin >> n;
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= n; j++) cin >> id[i][j];
	}
	int ans = 0, ans2 = 0;
	timer++;
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= n; j++) {
			auto p = dfs(i, j, id[i][j], id[i][j]);
			ans = max(ans, p.first);
		}
	}
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= n; j++) {
			for (int k = 0; k < 4; k++) {
				int nx = i + DX[k], ny = j + DY[k];
				if (id[nx][ny] == -1) continue;
				if (!has[nx][ny].count(id[i][j]) && id[nx][ny] != id[i][j]) {
					timer++;
					auto p = dfs(i, j, id[i][j], id[nx][ny]);
					ans2 = max(ans2, p.first + p.second);
				}
			}
		}
	}
	cout << ans << '\n' << ans2;
}
