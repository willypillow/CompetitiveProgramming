#include <bits/stdc++.h>
using namespace std;

pair<int, int> a[100000];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
#ifndef OJ_DEBUG
	freopen("sort.in", "r", stdin);
	freopen("sort.out", "w", stdout);
#endif
	int n; cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> a[i].first;
		a[i].second = i;
	}
	sort(a, a + n);
	int ans = 0;
	for (int i = 0; i < n; i++) ans = max(ans, a[i].second - i);
	cout << ans + 1;
}
