#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		string s[2]; cin >> s[0] >> s[1];
		int i;
		for (i = 0; i < (1 << 3); i++) {
			map<char, int> mp;
			for (int j = 0; j < 3; j++) {
				mp[s[!!(i & (1 << j))][j]]++;
			}
			if (mp['b'] == 2 && mp['o'] == 1) break;
		}
		cout << (i != (1 << 3) ? "yes\n" : "no\n");
	}
}