#include <bits/stdc++.h>
using namespace std;

pair<int, int> ps[100];
vector<pair<int, int> > v;
int ans[100010];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int p, s; cin >> p >> s;
	for (int j = 1; j <= p; j++) {
		for (int i = 0; i < s; i++) cin >> ps[i].first;
		for (int i = 0; i < s; i++) cin >> ps[i].second;
		sort(ps, ps + s);
		int cnt = 0;
		for (int i = 1; i < s; i++) cnt += ps[i - 1].second > ps[i].second;
		v.push_back({cnt, j});
	}
	sort(v.begin(), v.end());
	for (int i = 0; i < p; i++) cout << v[i].second << '\n';
}