#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	set<int64_t> st;
	for (int i = 0; i < 30; i++) {
		for (int j = 0; j < 30; j++) {
			if (i != j) st.insert((1 << i) + (1 << j));
		}
	}
	st.insert(INT_MIN), st.insert(INT_MAX);
	while (t--) {
		int n; cin >> n;
		auto nxt = st.lower_bound(n), prv = prev(nxt);
		cout << min(*nxt - n, n - *prv) << '\n';
	}
}