#include <bits/stdc++.h>
using namespace std;

const int MOD = 1E9 + 7;

int64_t powMod(int64_t x, int64_t e, int64_t m) {
	if (!e) return 1;
	if (!m) m = MOD;
	auto t = powMod(x, e >> 1, m);
	if (e & 1) return t * t % m * x % m;
	else return t * t % m;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		int64_t a, b, n; cin >> a >> b >> n;
		int64_t m = abs(a - b), x = powMod(a, n, m) + powMod(b, n, m);
		if (m > x) swap(m, x);
		cout << __gcd(m, x) % MOD << '\n';
	}
}