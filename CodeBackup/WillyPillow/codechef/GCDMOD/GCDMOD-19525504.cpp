#include <bits/stdc++.h>
using namespace std;

const int MOD = 1E9 + 7;

__int128_t powMod(__int128_t x, __int128_t e, __int128_t m) {
	if (!e) return 1;
	auto t = powMod(x, e >> 1, m);
	if (e & 1) return t * t % m * x % m;
	else return t * t % m;
}

__int128_t gcd(__int128_t a, __int128_t b) {
	if (a < b) swap(a, b);
	return b ? gcd(b, a % b) : a;

}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		int64_t a, b, n; cin >> a >> b >> n;
		__int128_t x = abs(a - b), m = (x ? x : MOD),
			y = powMod(a, n, m) + powMod(b, n, m);
		cout << (int)(gcd(x, y) % MOD) << '\n';
	}
}