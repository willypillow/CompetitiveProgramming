#include <bits/stdc++.h>
using namespace std;

const int MAXC = 5E6;

int cnt[MAXC + 1], notPrime[MAXC + 1];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	for (int i = 2; i <= MAXC; i++) {
		if (notPrime[i]) continue;
		notPrime[i] = i;
		for (int j = i + i; j <= MAXC; j += i) notPrime[j] = i;
	}
	for (int i = 1; i <= MAXC; i++) {
		int x = i;
		while (x != 1) {
			x /= notPrime[x];
			cnt[i]++;
		}
		cnt[i] += cnt[i - 1];
	}
	int t; cin >> t;
	while (t--) {
		int a, b; cin >> a >> b;
		cout << cnt[a] - cnt[b] << '\n';
	}
}