#include <bits/stdc++.h>
using namespace std;

int64_t ss[100010], a[100010], b[100010];
set<pair<int64_t, int64_t> > stA, stB;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int64_t n, s; cin >> n >> s;
	int64_t ans = 0, cntA = 0, cntB = 0;
	for (int i = 0; i < n; i++) {
		cin >> ss[i] >> a[i] >> b[i];
		if (a[i] > b[i]) {
			cntA += ss[i];
			ans += a[i] * ss[i];
			stA.insert({ a[i] - b[i], i });
		} else {
			cntB += ss[i];
			ans += b[i] * ss[i];
			stB.insert({ b[i] - a[i], i });
		}
	}
	int64_t t = (cntA + cntB + s - 1) / s;
	if (t == (cntA + s - 1) / s + (cntB + s - 1) / s) {
		cout << ans;
	} else {
		int64_t ans2 = ans, ans3 = ans;
		{
			int64_t d = min((s - (cntA % s)) % s, cntB % s);
			auto it = stB.begin();
			while (d) {
				int64_t x = min(ss[it->second], d);
				ans2 -= it->first * x;
				d -= x;
				it++;
			}
		}
		{
			int64_t d2 = min(cntA % s, (s - (cntB % s)) % s);
			auto it2 = stA.begin();
			while (d2) {
				int64_t x = min(ss[it2->second], d2);
				ans3 -= it2->first * x;
				d2 -= x;
				it2++;
			}
		}
		cout << max(ans2, ans3);
	}
}