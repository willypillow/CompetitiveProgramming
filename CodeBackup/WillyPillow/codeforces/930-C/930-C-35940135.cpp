#include <bits/stdc++.h>
using namespace std;

int bit[100010], a[100010], front[100010], back[100010], n, m;

void add(int p, int v) {
	for (; p <= m; p += p & -p) bit[p] += v;
}

int query(int p, int ret = 0) {
	for (; p; p -= p & -p) ret += bit[p];
	return ret;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	cin >> n >> m;
	while (n--) {
		int l, r; cin >> l >> r;
		add(l, 1), add(r + 1, -1);
	}
	for (int i = 1; i <= m; i++) a[i] = query(i);
	vector<int> v1 = {a[1]};
	front[1] = 1, back[1] = 1;
	for (int i = 2; i <= m; i++) {
		int x = a[i];
		if (x >= v1.back()) v1.push_back(x);
		else *upper_bound(v1.begin(), v1.end(), x) = x;
		front[i] = v1.size();
	}
	vector<int> v2 = {a[m]};
	for (int i = m - 1; i; i--) {
		int x = a[i];
		if (x >= v2.back()) v2.push_back(x);
		else *upper_bound(v2.begin(), v2.end(), x) = x;
		back[i] = v2.size();
	}
	int ans = 0;
	for (int i = 1; i < m; i++) ans = max(ans, front[i] + back[i + 1]);
	cout << ans;
}