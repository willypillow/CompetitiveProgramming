#include <bits/stdc++.h>
using namespace std;

int a[100001], n, m;

bool solve(int64_t timer) {
	int64_t people = m, left = 0;
	for (int i = n; i; i--) {
		int64_t rem = 0;
		while (rem < a[i] && people >= 0) {
			if (!left) people--, left = timer - i;
			int64_t mod = min(a[i] - rem, left);
			rem += mod, left -= mod;
		}
		if (people < 0) return false;
	}
	return true;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	cin >> n >> m;
	for (int i = 1; i <= n; i++) cin >> a[i];
	int64_t l = 0, r = 1E18;
	while (l != r) {
		int64_t mid = (l + r) >> 1;
		if (solve(mid)) r = mid;
		else l = mid + 1;
	}
	cout << l;
}