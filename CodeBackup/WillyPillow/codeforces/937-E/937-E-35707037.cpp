#include <bits/stdc++.h>
using namespace std;

vector<int> ans;
stack<char> tmp;
deque<char> q;
int cntS[26], cntT[26];

void shift(int x) {
	ans.push_back(x);
	while (x--) {
		tmp.push(q.back());
		q.pop_back();
	}
	while (tmp.size()) {
		q.push_front(tmp.top());
		tmp.pop();
	}
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	string s, t; cin >> s >> t;
	for (int i = 0; i < n; i++) cntS[s[i] - 'a']++, cntT[t[i] - 'a']++;
	for (int i = 0; i < 26; i++) {
		if (cntS[i] != cntT[i]) {
			cout << -1;
			return 0;
		}
	}
	for (char c: s) q.push_back(c);
	bool rev = 0;
	int idx;
	for (idx = 0; idx < n; idx++) {
		if (s[idx] == t[0]) break;
	}
	shift(n - idx - 1), shift(1);
	for (int i = 1; i < n; i++) {
		for (idx = i; idx < n; idx++) {
			if (q[idx] == t[i]) break;
		}
		if (!rev) shift(n - i), shift(idx), shift(1);
		else shift(n - idx), shift(idx - i), shift(i + 1);
		rev = !rev;
	}
	if (rev) shift(n);
	cout << ans.size() << '\n';
	for (int x: ans) cout << x << ' ';
}