#include <bits/stdc++.h>
using namespace std;

int a[10000];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, m, k; cin >> n >> m >> k;
	for (int i = 0; i <= m; i++) cin >> a[i];
	int ans = 0;
	for (int i = 0; i < m; i++) {
		int t = a[i] ^ a[m];
		if (__builtin_popcount(t) <= k) ans++;
	}
	cout << ans;
}