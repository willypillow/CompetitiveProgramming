#include <bits/stdc++.h>
using namespace std;

int dp[5002][5002], mx[5002][5002], n;

void calcF(int depth) {
	for (int i = 1; i + depth <= n; i++) {
		dp[depth][i] = dp[depth - 1][i] ^ dp[depth - 1][i + 1];
		mx[depth][i] = max({mx[depth - 1][i], mx[depth - 1][i + 1], dp[depth][i]});
	}
	if (depth < n) calcF(depth + 1);
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	cin >> n;
	for (int i = 1; i <= n; i++) {
		cin >> dp[0][i];
		mx[0][i] = dp[0][i];
	}
	calcF(1);
	int q; cin >> q;
	while (q--) {
		int l, r; cin >> l >> r;
		cout << mx[r - l][l] << '\n';
	}
}