#include <bits/stdc++.h>
using namespace std;

int a[100], ans[100], d[100][2], suf[100];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 0; i < n; i++) cin >> a[i];
	d[n - 1][0] = d[n - 1][1] = a[n - 1];
	for (int i = n - 1; i >= 0; i--) suf[i] = suf[i + 1] + a[i];
	for (int i = n - 2; i >= 0; i--) {
		for (int j = 0; j < 2; j++) {
			d[i][j] = 0;
			for (int k = i; k <= n; k++) {
				d[i][j] = max(d[i][j], a[k] + suf[k + 1] - d[k + 1][j ^ 1]);
			}
		}
	}
	cout << suf[0] - d[0][0] << ' ' << d[0][0];
}