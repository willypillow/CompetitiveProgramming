#include <bits/stdc++.h>
using namespace std;

int vect[26][5010][26];
int tot[26][5010];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	string s; cin >> s;
	for (int i = 0; i < (int)s.size(); i++) {
		int idx = s[i] - 'a';
		for (int j = 0; j < (int)s.size(); j++) {
			vect[idx][j][s[(i + j) % s.size()] - 'a']++;
			tot[idx][j]++;
		}
	}
	double ans = 0;
	for (int i = 0; i < (int)s.size(); i++) {
		int idx = s[i] - 'a';
		pair<double, int> choose = make_pair(0, -1);
		for (int j = 0; j < (int)s.size(); j++) {
			double res = 0;
			for (int k = 0; k < 26; k++) {
				if (vect[idx][j][k] == 1) {
					res += 1.0 / tot[idx][j];
				}
			}
			choose = max(choose, {res, j});
		}
		ans += choose.first;
	}
	cout << fixed << setprecision(10) << ans / (double)s.size();
}