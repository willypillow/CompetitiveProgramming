object Main extends App {
  scala.io.StdIn.readInt()
  val arr = scala.io.StdIn.readLine().split(" ").map(_.toInt).sorted
  val d = arr.zip(arr.tail).map(t => Math.abs(t._2 - t._1))
  val min = d.min
  println(min + " " + d.count(_ == min))
}