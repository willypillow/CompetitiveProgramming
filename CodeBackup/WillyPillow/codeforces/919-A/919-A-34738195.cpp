#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	double price = 1E9;
	for (int i = 0; i < n; i++) {
		int a, b; cin >> a >> b;
		price = min(price, (double)a / b);
	}
	cout << setprecision(12) << fixed << price * m;
}