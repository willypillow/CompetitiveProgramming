#include <bits/stdc++.h>
using namespace std;

const int MOD = 1E9 + 7;

int fact[1000010], invFact[1000010];
string a, b;

inline int mul(int l, int r) { return (int)((int64_t)l * r % MOD); }
inline int add(int l, int r) { return (l + r) % MOD; }

inline int pow(int x, int r) {
	if (!r) return 1;
	int t = pow(x, r >> 1);
	if (r & 1) return mul(t, mul(t, x));
	else return mul(t, t);
}

inline int inv(int x) { return pow(x, MOD - 2); }

inline int f(string &s) {
	int cnt[26] = { 0 }, ans = 0;
	for (char c: a) cnt[c - 'a']++;
	for (int i = 0; i < (int)s.size(); i++) {
		int c = fact[s.size() - i - 1];
		for (int j = 0; j < 26; j++) c = mul(c, invFact[cnt[j]]);
		for (int j = 0; j < (int)(s[i] - 'a'); j++) {
			if (cnt[j]) ans = add(ans, mul(c, cnt[j]));
		}
		if (cnt[s[i] - 'a']-- == 0) break;
	}
	return ans;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	cin >> a >> b;
	fact[0] = invFact[0] = 1;
	for (int i = 1; i <= (int)a.size(); i++) {
		fact[i] = mul(fact[i - 1], i);
		invFact[i] = mul(invFact[i - 1], inv(i));
	}
	cout << ((f(b) - f(a) - 1) % MOD + MOD) % MOD;
}