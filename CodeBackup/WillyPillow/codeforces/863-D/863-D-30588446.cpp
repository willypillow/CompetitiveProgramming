#include <bits/stdc++.h>
using namespace std;

int a[200010], type[200010], lIdx[200010], rIdx[200010];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, q, m; cin >> n >> q >> m;
	for (int i = 1; i <= n; i++) cin >> a[i];
	for (int i = 0; i < q; i++) cin >> type[i] >> lIdx[i] >> rIdx[i];
	for (int i = 0; i < m; i++) {
		int b; cin >> b;
		for (int j = q - 1; j >= 0; j--) {
			if (type[j] == 2) {
				if (lIdx[j] <= b && b <= rIdx[j]) {
					b = rIdx[j] - (b - lIdx[j]);
				}
			} else {
				if (lIdx[j] <= b && b <= rIdx[j]) {
					if (lIdx[j] == b) b = rIdx[j];
					else b -= 1;
				}
			}
		}
		cout << a[b] << ' ';
	}
}