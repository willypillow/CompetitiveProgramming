import java.util.*;
import java.io.*;
import java.math.*;
public class Main {
	public static void main (String[] args) throws java.lang.Exception {
		MyScanner cin = new MyScanner();
		out = new PrintWriter(new BufferedOutputStream(System.out));
		long n = cin.nextLong(), q = cin.nextInt();
		BigInteger bn = BigInteger.valueOf(n);
		for (int i = 0; i < q; i++) {
			long x = cin.nextLong();
			if (x % 2 == 1) {
				out.println((x + 1) / 2);
			} else {
				long ans = -1;
				BigInteger bx = BigInteger.valueOf(x);
				for (int k = 0; x + (1L << k) <= (n * 2); k++) {
					BigInteger d = BigInteger.valueOf((1L << k) - 1);
					BigInteger d2 = BigInteger.valueOf(2 * ((1L << k) - 1));
					BigInteger t = bn.multiply(d2).add(bx);
					if (t.longValue() % (1L << k) != 0) {
						continue;
					}
					long dd = t.divide(BigInteger.valueOf(1L << k)).longValue();
					if (dd % 2 == 0) {
						continue;
					} else {
						ans = (dd + 1) / 2;
						break;
					}
				}
				out.println(ans);
			}
		}
		out.close();
	}
	public static PrintWriter out;

	//-----------MyScanner class for faster input----------
	public static class MyScanner {
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}

		long nextLong() {
			return Long.parseLong(next());
		}

		double nextDouble() {
			return Double.parseDouble(next());
		}

		String nextLine(){
			String str = "";
			try {
				str = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return str;
		}

	}
}