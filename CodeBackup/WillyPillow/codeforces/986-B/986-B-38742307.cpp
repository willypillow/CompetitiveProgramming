#include <bits/stdc++.h>
using namespace std;

int bit[1000001];
int a[1000001];

int query(int p, int ret = 0) {
	for (; p; p -= p & -p) ret += bit[p];
	return ret;
}

void add(int p, int n) {
	for (; p <= n; p += p & -p) bit[p]++;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n; cin >> n;
	int64_t ans = 0;
	for (int i = 1; i <= n; i++) {
		int x; cin >> x;
		a[i] = x;
		ans += query(n - x + 1);
		add(n - x + 1, n);
	}
	cout << ((ans % 2 == n % 2) ? "Petr" : "Um_nik");
}