#include <bits/stdc++.h>
using namespace std;

int p[100000];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int k; cin >> k;
	int n;
	for (n = 1; p[n - 1] <= 100000; n++) p[n] = p[n - 1] + n;
	multiset<int> cnts;
	for (int i = n - 1; i > 0; i--) {
		while (k >= p[i]) {
			k -= p[i];
			cnts.insert(i);
		}
	}
	char ch = 'a';
	for (int c: cnts) {
		for (int i = 0; i <= c; i++) cout << ch;
		ch++;
	}
	if (cnts.size() == 0) cout << 'a';
}