#include <bits/stdc++.h>
using namespace std;

vector<int> st[2], vect[200010];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	string s; cin >> s;
	for (int i = 0; i < (int)s.size(); i++) {
		if (s[i] == '1') {
			if (st[0].empty()) {
				cout << -1;
				return 0;
			}
			vect[st[0].back()].push_back(i);
			st[1].push_back(st[0].back()), st[0].pop_back();
		} else {
			if (st[1].empty()) {
				st[0].push_back(i);
				vect[i].push_back(i);
			} else {
				vect[st[1].back()].push_back(i);
				st[0].push_back(st[1].back()), st[1].pop_back();
			}
		}
	}
	if (st[1].size()) {
		cout << -1;
		return 0;
	}
	int ans = 0;
	for (int i = 0; i < (int)s.size(); i++) ans += !vect[i].empty();
	cout << ans << '\n';
	for (int i = 0; i < (int)s.size(); i++) {
		if (vect[i].empty()) continue;
		cout << vect[i].size();
		for (int j: vect[i]) cout << ' ' << j + 1;
		cout << '\n';
	}
}