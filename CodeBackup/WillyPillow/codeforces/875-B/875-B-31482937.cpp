#include <bits/stdc++.h>
using namespace std;

int n, bit[300001];

int query(int p) {
	int ret = 0;
	for (; p > 0; p -= p & -p) ret += bit[p];
	return ret;
}

void modify(int p, int v) {
	for (; p <= n; p += p & -p) bit[p] += v;
}

int bs() {
	int l = 0, r = n;
	while (l != r) {
		int m = (l + r + 1) >> 1;
		if (query(m) == m) l = m;
		else r = m - 1;
	}
	return l;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	cin >> n;
	cout << '1';
	for (int i = 1; i <= n; i++) {
		int p; cin >> p;
		modify(n - p + 1, 1);
		cout << ' ' << i - bs() + 1;
	}
}