#include <bits/stdc++.h>
using namespace std;

const int MAXN = 5E6, MOD = 1E9 + 7;

int minFac[MAXN + 10];
uint64_t dp[MAXN + 10];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	for (int i = 2; i <= MAXN; i++) {
		if (!minFac[i]) {
			minFac[i] = i;
			for (int j = i + i; j <= MAXN; j += i) {
				if (!minFac[j]) minFac[j] = i;
			}
		}
	}
	for (uint64_t i = 2; i <= MAXN; i++) {
		dp[i] = dp[i / minFac[i]] + i * (minFac[i] - 1) / 2;
	}
	int t, l, r; cin >> t >> l >> r;
	uint64_t mult = 1, ans = 0;
	while (l <= r) {
		ans = (ans + (mult * (dp[l++] % MOD)) % MOD) % MOD;
		mult = mult * t % MOD;
	}
	cout << ans << '\n';
}