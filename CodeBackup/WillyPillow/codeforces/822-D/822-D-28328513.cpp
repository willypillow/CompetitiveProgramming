#include <bits/stdc++.h>
using namespace std;

const int MAXN = 5E6 + 10, MOD = 1E9 + 7;

uint64_t dp[MAXN];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t, l, r; cin >> t >> l >> r;
	memset(dp, 0x3f, sizeof(dp));
	dp[1] = 0;
	for (int i = 1; i <= r; i++) {
		for (int j = 2, x = i + i; x <= r; j++, x += i) {
			auto tmp = (uint64_t)x * (j - 1) / 2;
			//if (!dp[x] || dp[x] > dp[i] + tmp) dp[x] = dp[i] + tmp;
			dp[x] = min(dp[x], dp[i] + tmp);
		}
	}
	long long mult = 1, ans = 0;
	for (int i = l; i <= r; i++) {
		ans = (ans + (dp[i] % MOD) * mult) % MOD;
		mult = mult * t % MOD;
	}
	cout << ans << '\n';
}