#include <bits/stdc++.h>
using namespace std;

pair<int, int> pt[2001];

void normal(int &a, int &b) {
	if (a && b) {
		if (a < 0) a = -a, b = -b;
		int g = __gcd(abs(a), abs(b));
		a /= g, b /= g;
	} else {
		if (a) a = 1;
		else if (b) b = 1;
	}
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	unordered_map<int, int> revC;
	for (int i = 2; i <= 2000; i++) {
		revC[i * (i - 1) / 2] = i;
	}
	int64_t n; cin >> n;
	int64_t ans = n * (n - 1) * (n - 2) / 6;
	for (int i = 0; i < n; i++) cin >> pt[i].first >> pt[i].second;
	map<tuple<int, int, int, int> , int> mp;
	for (int i = 0; i < n; i++) {
		for (int j = i + 1; j < n; j++) {
			int dx = pt[i].first - pt[j].first, dy = pt[i].second - pt[j].second;
			normal(dx, dy);
			if (dx) {
				int a = pt[i].second * dx - pt[i].first * dy, b = dx;
				normal(a, b);
				mp[{ dx, dy, a, b }]++;
			} else {
				mp[{ dx, dy, pt[i].first, 0 }]++;
			}
		}
	}
	for (auto &p: mp) {
		int64_t k = revC[p.second];
		ans -= k * (k - 1) * (k - 2) / 6;
	}
	cout << ans;
}