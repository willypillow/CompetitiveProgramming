#include <bits/stdc++.h>
using namespace std;

int ms[100010], need[100010];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 1; i <= n; i++) cin >> ms[i];
	for (int i = n; i >= 1; i--) {
		need[i] = max(need[i + 1] - 1, ms[i] + 1);
	}
	for (int i = 1; i <= n; i++) {
		need[i] = max(need[i], need[i - 1]);
	}
	int64_t ans = 0;
	for (int i = 1; i <= n; i++) {
		ans += need[i] - ms[i] - 1;
	}
	cout << ans;
}