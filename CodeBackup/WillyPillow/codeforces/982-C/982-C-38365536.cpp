#include <bits/stdc++.h>
using namespace std;

vector<int> g[100010];
int ans;

int dfs(int x, int p) {
	int ret = 1;
	for (int y: g[x]) {
		if (y == p) continue;
		int tmp = dfs(y, x);
		if (tmp % 2 == 0) ans++;
		else ret += tmp;
	}
	return ret;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	if (n & 1) {
		cout << -1;
		return 0;
	} else {
		for (int i = 1; i < n; i++) {
			int u, v; cin >> u >> v;
			g[u].push_back(v), g[v].push_back(u);
		}
		dfs(1, 1);
		cout << ans;
	}
}