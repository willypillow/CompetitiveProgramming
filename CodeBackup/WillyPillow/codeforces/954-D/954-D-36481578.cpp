#include <bits/stdc++.h>
using namespace std;

int dist[1010], dist2[1010];
bool has[1010];
vector<int> g[1010], vect[1010], vect2[1010];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, m, s, t; cin >> n >> m >> s >> t;
	for (int i = 0; i < m; i++) {
		int u, v; cin >> u >> v;
		g[u].push_back(v), g[v].push_back(u);
	}
	priority_queue<pair<int, int> > q;
	memset(dist, 0x3f, sizeof(dist));
	q.push({dist[s] = 0, s});
	while (q.size()) {
		auto x = q.top(); q.pop();
		if (dist[x.second] < x.first) continue;
		vect[dist[x.second]].push_back(x.second);
		for (int y: g[x.second]) {
			if (dist[y] > x.first + 1) {
				q.push({dist[y] = x.first + 1, y});
			}
		}
	}
	memset(dist2, 0x3f, sizeof(dist2));
	q.push({dist2[t] = 0, t});
	while (q.size()) {
		auto x = q.top(); q.pop();
		if (dist2[x.second] < x.first) continue;
		vect2[dist2[x.second]].push_back(x.second);
		for (int y: g[x.second]) {
			if (dist2[y] > x.first + 1) {
				q.push({dist2[y] = x.first + 1, y});
			}
		}
	}
	int64_t ans = 0;
	for (int i = 1; i <= n; i++) {
		memset(has, false, sizeof(has));
		for (int j: g[i]) has[j] = true;
		for (int j = i + 1; j <= n; j++) {
			if (has[j]) continue;
			if (dist[i] + dist2[j] + 1 >= dist[t] &&
					dist2[i] + dist[j] + 1 >= dist[t]) {
				ans++;
			}
		}
	}
	cout << ans;
}