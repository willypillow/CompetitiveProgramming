#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	string s; cin >> s;
	for (;;) {
		int k = 0;
		for (int i = 1; i < (int)s.size(); i++) {
			if (s[i] + s[i - 1] == 'R' + 'U') {
				s.erase(s.begin() + i);
				s[i - 1] = 'D';
				k++;
			}
		}
		if (!k) break;
	}
	cout << s.size();
}