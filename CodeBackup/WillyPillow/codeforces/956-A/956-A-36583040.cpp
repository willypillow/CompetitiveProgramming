#include <bits/stdc++.h>
using namespace std;

char field[100][100];
int visX[100], visY[100], n, m;

void doNo() {
	cout << "No";
	exit(0);
}

void dfs(int x, int y, int cur) {
	if (visX[x] && visX[x] != cur) doNo();
	if (visY[y] && visY[y] != cur) doNo();
	visX[x] = visY[y] = cur;
	field[x][y] = 'V';
	for (int i = 0; i < n; i++) {
		if (field[i][y] == '#') dfs(i, y, cur);
	}
	for (int i = 0; i < m; i++) {
		if (field[x][i] == '#') dfs(x, i, cur);
	}
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	cin >> n >> m;
	for (int i = 0; i < n; i++) cin >> field[i];
	int cnt = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (field[i][j] == '#') {
				dfs(i, j, ++cnt);
			}
		}
	}
	for (int i = 1; i <= cnt; i++) {
		for (int j = 0; j < n; j++) {
			for (int k = 0; k < m; k++) {
				if (visX[j] == i && visY[k] == i && field[j][k] == '.') doNo();
			}
		}
	}
	cout << "Yes";
}