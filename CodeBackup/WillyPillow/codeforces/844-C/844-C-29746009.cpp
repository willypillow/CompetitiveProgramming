#include <bits/stdc++.h>
using namespace std;

set<int> s;
pair<int, int> a[100010];
vector<int> v[100010];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 0; i < n; i++) {
		int x; cin >> x;
		a[i] = { x, i };
	}
	sort(a, a + n);
	for (int i = 0; i < n; i++) s.insert(i);
	int ans = 0;
	while (!s.empty()) {
		int x = *s.begin();
		while (s.count(x)) {
			s.erase(x);
			v[ans].push_back(x);
			x = a[x].second;
		}
		ans++;
	}
	cout << ans << '\n';
	for (int i = 0; i < ans; i++) {
		cout << v[i].size();
		for (int x: v[i]) cout << ' ' << x + 1;
		cout << '\n';
	}
}