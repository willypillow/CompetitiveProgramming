#include <bits/stdc++.h>
using namespace std;

int64_t ans[200001], lazy[200001], a[200001];
multiset<int64_t> st[200001];
vector<pair<int, int> > g[200001];

void dfs(int x, int par) {
	for (auto p: g[x]) {
		if (p.first == par) continue;
		dfs(p.first, x);
		lazy[p.first] += p.second;
		while (st[p.first].size() && *st[p.first].begin() < lazy[p.first]) {
			st[p.first].erase(st[p.first].begin());
		}
	}
	pair<int, int> mx = {0, 0};
	for (auto p: g[x]) {
		if (p.first == par) continue;
		mx = max(mx, {st[p.first].size(), p.first});
	}
	swap(st[x], st[mx.second]), lazy[x] = lazy[mx.second];
	for (auto p: g[x]) {
		if (p.first == par) continue;
		for (int64_t v: st[p.first]) st[x].insert(v - lazy[p.first] + lazy[x]);
	}
	ans[x] = (int)st[x].size();
	st[x].insert(a[x] + lazy[x]);
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 1; i <= n; i++) cin >> a[i];
	for (int i = 2; i <= n; i++) {
		int p, w; cin >> p >> w;
		g[p].push_back({i, w});
	}
	dfs(1, 1);
	for (int i = 1; i <= n; i++) cout << ans[i] << ' ';
}