#include <bits/stdc++.h>
using namespace std;

vector<int> chain[100001];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, k; cin >> n >> k;
	int b = 0;
	for (int i = 0; i < k; i++) {
		int x; cin >> x;
		while (x--) {
			int y; cin >> y;
			chain[i].push_back(y);
		}
	}
	int segs = 0, moves = 0;
	for (int i = 0; i < k; i++) {
		segs++;
		for (int j = chain[i].size() - 1; j; j--) {
			if (chain[i][j] - chain[i][0] == j && chain[i][0] == 1) break;
			segs++;
			moves++;
		}
	}
	cout << moves + segs - 1;
}