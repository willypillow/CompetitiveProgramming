#include <bits/stdc++.h>
using namespace std;

bool vis[100];

int main() {
	int a, b, c, d;
	cin >> a >> b >> c >> d;
	for (int t = b;; t += a) {
		if (t - d < 0) continue;
		int m = (t - d) % c;
		if (m == 0) {
			cout << t << '\n';
			break;
		} else if (vis[m]) {
			cout << "-1\n";
			break;
		}
		vis[m] = true;
	}
}