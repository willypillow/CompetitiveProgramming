#include <bits/stdc++.h>
using namespace std;

int a[100010], sum = 0;

int main() {
	int n; cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> a[i];
		if (a[i] > 0) sum += a[i];
	}
	if (sum & 1) {
		cout << sum;
	} else {
		int ans = INT_MIN;
		for (int i = 0; i < n; i++) {
			if (a[i] % 2) {
				if (a[i] > 0) ans = max(ans, sum - a[i]);
				else ans = max(ans, sum + a[i]);
			}
		}
		cout << ans << '\n';
	}
}