#include <bits/stdc++.h>
using namespace std;

int arr[200];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	bool fail = false;
	for (int i = 0, last = -100, cur = 0; i < n; i++) {
		int x; cin >> x;
		if (cur == 0) {
			if (last == x) cur = 1;
			if (last > x) cur = 2;
		}
		if (cur == 1) {
			if (last > x) cur = 2;
			if (last < x) fail = true;
		}
		if (cur == 2) {
			if (last <= x) fail = true;
		}
		last = x;
	}
	cout << (fail ? "NO\n" : "YES\n");
}