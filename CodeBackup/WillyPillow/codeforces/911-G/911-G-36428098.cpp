#include <bits/extc++.h>
using namespace std;
using namespace __gnu_pbds;

mt19937 rng;
struct Treap {
	Treap *lc, *rc;
	uint32_t val, pri;
	Treap(): lc(nullptr), rc(nullptr), val(0), pri(rng()) { }
};

Treap *join(Treap *a, Treap *b) {
	if (!a || !b) return a ? a : b;
	if (a->pri < b->pri) {
		a->rc = join(a->rc, b);
		return a;
	} else {
		b->lc = join(a, b->lc);
		return b;
	}
}

void split(uint32_t p, Treap *n, Treap *&a, Treap *&b) {
	if (!n) {
		a = b = NULL;
		return;
	}
	if (n->val <= p) {
		a = n;
		split(p, n->rc, a->rc, b);
	} else {
		b = n;
		split(p, n->lc, a, b->lc);
	}
}

Treap *root[101], pool[1000000], *ptr = pool;
int ans[200001];

int getMin(Treap *n) { return n->lc ? getMin(n->lc) : n->val; }

Treap *merge(Treap *a, Treap *b) {
	if (!a || !b) return a ? a : b;
	int minA = getMin(a), minB = getMin(b);
	if (minA > minB) swap(a, b);
	Treap *tmp = nullptr;
	split(minB, a, a, tmp);
	return merge(join(a, b), tmp);
}

void dfsAns(Treap *n, int answer) {
	if (!n) return;
	ans[n->val] = answer;
	dfsAns(n->lc, answer), dfsAns(n->rc, answer);
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 1; i <= n; i++) {
		int x; cin >> x;
		Treap *tmp = new(ptr++) Treap();
		tmp->val = i;
		root[x] = join(root[x], tmp);
	}
	int q; cin >> q;
	while (q--) {
		int l, r, x, y; cin >> l >> r >> x >> y;
		Treap *tmp = nullptr, *tmp2 = nullptr;
		split(l - 1, root[x], root[x], tmp);
		split(r, tmp, tmp, tmp2);
		root[x] = join(root[x], tmp2);
		root[y] = merge(root[y], tmp);
	}
	for (int i = 1; i <= 100; i++) dfsAns(root[i], i);
	for (int i = 1; i <= n; i++) cout << ans[i] << ' ';
}