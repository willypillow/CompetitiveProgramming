#include <bits/stdc++.h>
using namespace std;

int vis[200001], p[200001];

int dfs(int x) {
	vis[x] = 2;
	int ret;
	if (x == p[x] || vis[p[x]] == 1) ret = 0;
	else if (vis[p[x]] == 2) ret = x;
	else ret = dfs(p[x]);
	vis[x] = 1;
	return ret;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 1; i <= n; i++) cin >> p[i];
	vector<int> root, cut;
	for (int i = 1; i <= n; i++) {
		if (i == p[i]) root.push_back(i);
		else if (!vis[i]) {
			int t = dfs(i);
			if (t) cut.push_back(t);
		}
	}
	int ans = 0;
	if (root.empty()) {
		int x = cut.back(); cut.pop_back();
		p[x] = x;
		root.push_back(x);
		ans++;
	}
	while (root.size() > 1) {
		p[root.back()] = root[0];
		root.pop_back();
		ans++;
	}
	while (cut.size()) {
		p[cut.back()] = root[0];
		cut.pop_back();
		ans++;
	}
	cout << ans << '\n';
	for (int i = 1; i <= n; i++) cout << p[i] << ' ';
}