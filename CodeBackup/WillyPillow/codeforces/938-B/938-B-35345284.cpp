#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	int ans = 0;
	for (int i = 0; i < n; i++) {
		int x; cin >> x;
		ans = max(ans, min(x - 1, 1000000 - x));
	}
	cout << ans;
}