#include <bits/stdc++.h>
using namespace std;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	set<string> st;
	while (n--) {
		string s; cin >> s;
		cout << (st.count(s) ? "YES\n" : "NO\n");
		st.insert(s);
	}
}