#include <bits/stdc++.h>
using namespace std;

const int MAXN = 3E5, MAXN4 = MAXN * 4;

int stLeft[MAXN4], stMid[MAXN4], stRight[MAXN4];
int64_t a[MAXN];

void modify(int id, int l, int r, int p, int64_t v) {
	if (l == r) {
		a[l] += v;
		stLeft[id] = stMid[id] = stRight[id] = (a[l] != 0);
	} else {
		int m = (l + r) >> 1;
		if (p <= m) modify(id << 1, l, m, p, v);
		else modify(id << 1 | 1, m + 1, r, p, v);
		stMid[id] = max(stMid[id << 1], stMid[id << 1 | 1]);
		stLeft[id] = stLeft[id << 1], stRight[id] = stRight[id << 1 | 1];
		if (a[m] && a[m + 1] && (a[m] > 0) >= (a[m + 1] > 0)) {
			stMid[id] = max(stMid[id], stRight[id << 1] + stLeft[id << 1 | 1]);
			if (stLeft[id] == m - l + 1) stLeft[id] += stLeft[id << 1 | 1];
			if (stRight[id] == r - m) stRight[id] += stRight[id << 1];
		}
	}
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 0; i < n; i++) cin >> a[i];
	int m; cin >> m;
	adjacent_difference(a, a + n, a);
	for (int i = 1; i < n; i++) modify(1, 1, n - 1, i, 0);
	while (m--) {
		int l, r, v; cin >> l >> r >> v;
		if (l > 1) modify(1, 1, n - 1, l - 1, v);
		if (r < n) modify(1, 1, n - 1, r, -v);
		cout << stMid[1] + 1 << '\n';
	}
}