#include <bits/stdc++.h>
using namespace std;

const int MAXN = 3E5, MAXN4 = MAXN * 4;

int stLeft[MAXN4], stMid[MAXN4], stRight[MAXN4];
int64_t a[MAXN];

void pull(int id, int l, int r, int m) {
	stMid[id] = max(stMid[id << 1], stMid[id << 1 | 1]);
	stLeft[id] = stLeft[id << 1], stRight[id] = stRight[id << 1 | 1];
	if (a[m] && a[m + 1] && (a[m] > 0) >= (a[m + 1] > 0)) {
		stMid[id] = max(stMid[id], stRight[id << 1] + stLeft[id << 1 | 1]);
		if (stLeft[id] == m - l + 1) stLeft[id] += stLeft[id << 1 | 1];
		if (stRight[id] == r - m) stRight[id] += stRight[id << 1];
	}
}

void build(int id, int l, int r) {
	if (l == r) {
		stLeft[id] = stMid[id] = stRight[id] = 1;
	} else {
		int m = (l + r) >> 1;
		build(id << 1, l, m), build(id << 1 | 1, m + 1, r);
		pull(id, l, r, m);
	}
}

void modify(int id, int l, int r, int p, int v) {
	if (l == r) {
		a[l] += v;
	} else {
		int m = (l + r) >> 1;
		if (p <= m) modify(id << 1, l, m, p, v);
		else modify(id << 1 | 1, m + 1, r, p, v);
		pull(id, l, r, m);
	}
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 0; i < n; i++) cin >> a[i];
	adjacent_difference(a, a + n, a);
	int m; cin >> m;
	if (n != 1) build(1, 1, n - 1);
	while (m--) {
		int l, r, v; cin >> l >> r >> v;
		if (l > 1) modify(1, 1, n - 1, l - 1, v);
		if (r < n) modify(1, 1, n - 1, r, -v);
		cout << max({stLeft[1], stMid[1], stRight[1]}) + 1 << '\n';
	}
}