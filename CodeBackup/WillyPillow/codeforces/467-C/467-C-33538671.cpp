#include <bits/stdc++.h>
using namespace std;

int64_t a[5010], sum[5010], d[5010][5010];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, m, k; cin >> n >> m >> k;
	for (int i = 1; i <= n; i++) {
		cin >> a[i];
		sum[i] = sum[i - 1] + a[i];
	}
	memset(d, -1, sizeof(d));
	for (int i = 1; i <= k; i++) {
		pair<int64_t, int> pref = { 0, 0 };
		for (int j = m; j <= n; j++) {
			pref = max(pref, { d[i - 1][j - m], j - m });
			if (pref.first != -1) {
				d[i][j] = pref.first + sum[j] - sum[j - m];
			}
		}
	}
	int64_t ans = -1;
	for (int i = 0; i <= n; i++) ans = max(ans, d[k][i]);
	cout << ans;
}