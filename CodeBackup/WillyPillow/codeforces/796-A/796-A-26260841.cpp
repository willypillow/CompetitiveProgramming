#include <bits/stdc++.h>
using namespace std;

int a[101];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, m, k; cin >> n >> m >> k;
	int ans = -1;
	for (int i = 1; i <= n; i++) cin >> a[i];
	for (int d = 1; d <= n; d++) {
		if (m + d <= n && a[m + d] && a[m + d] <= k) {
			ans = d;
			break;
		}
		if (m - d >= 1 && a[m - d] && a[m - d] <= k) {
			ans = d;
			break;
		}
	}
	cout << ans * 10 << '\n';
}