#include <bits/stdc++.h>
using namespace std;

typedef pair<long long, int> PII;
const int MAXN = 3E5 + 10;

vector<int> g[MAXN];
int str[MAXN];
set<pair<int, int> > m;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 1; i <= n; i++) {
		cin >> str[i];
		str[i] += 2;
		m.insert(make_pair(str[i], i));
	}
	for (int i = 1; i < n; i++) {
		int u, v; cin >> u >> v;
		g[u].push_back(v); g[v].push_back(u);
	}
	int ans = 1 << 30;
	for (int i = 1; i <= n; i++) {
		m.erase(make_pair(str[i], i));
		m.insert(make_pair(str[i] - 2, i));
		for (int y: g[i]) {
			m.erase(make_pair(str[y], y));
			m.insert(make_pair(str[y] - 1, y));
		}
		auto it = m.end(); it--;
		ans = min(ans, it->first);
		for (int y: g[i]) {
			m.erase(make_pair(str[y] - 1, y));
			m.insert(make_pair(str[y], y));
		}
		m.erase(make_pair(str[i] - 2, i));
		m.insert(make_pair(str[i], i));
	}
	cout << ans << '\n';
}