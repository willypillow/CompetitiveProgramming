#include <bits/stdc++.h>
using namespace std;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	while (n--) {
		uint64_t a, b; cin >> a >> b;
		uint64_t g = round(pow(a * b, 1.0 / 3));
		if (a % g == 0 && b % g == 0 && a / g * b / g == g) cout << "Yes\n";
		else cout << "No\n";
	}
}