#include <bits/stdc++.h>
using namespace std;

static inline uint_fast64_t fast_atoi(const char *p, const uint_fast64_t len) {
	uint_fast64_t res = 0;
	switch (len) {
		case 10: res += (*p++ & 15) * 1000000000;
		case 9: res += (*p++ & 15) * 100000000;
		case 8: res += (*p++ & 15) * 10000000;
		case 7: res += (*p++ & 15) * 1000000;
		case 6: res += (*p++ & 15) * 100000;
		case 5: res += (*p++ & 15) * 10000;
		case 4: res += (*p++ & 15) * 1000;
		case 3: res += (*p++ & 15) * 100;
		case 2: res += (*p++ & 15) * 10;
	}
	res += (*p & 15);
	return res;
}

bool getInt(uint_fast64_t *res) {
	char numBuf[11];
	uint_fast64_t i = 0;
	while (true) {
		static char buf[1 << 20], *p = buf, *end = buf;
		if (p == end) {
			if ((end = buf + fread(buf, 1, 1 << 20, stdin)) == buf) break;
			p = buf;
		}
		char t = *p++;
		if ((unsigned)(t - '0') > 10u) {
			if (i) break;
			else continue;
		}
		numBuf[i++] = t;
	}
	if (!i) return false;
	*res = fast_atoi(numBuf, i);
	return true;
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	uint_fast64_t n; getInt(&n);
	while (n--) {
		uint_fast64_t a, b; getInt(&a); getInt(&b);
		uint_fast64_t g = round(pow(a * b, 1.0 / 3));
		cout << (a * b == g * g * g && a % g == 0 && b % g == 0 ? "Yes\n" : "No\n");
	}
}