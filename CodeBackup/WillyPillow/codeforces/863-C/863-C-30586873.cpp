#include <bits/stdc++.h>
using namespace std;

int64_t movv[2][4][4], ans[2], ansA[100], ansB[100];
map<pair<int64_t, int64_t>, int64_t> mp;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int64_t k, a, b; cin >> k >> a >> b;
	for (int t = 0; t < 2; t++) {
		for (int i = 1; i <= 3; i++) {
			for (int j = 1; j <= 3; j++) cin >> movv[t][i][j];
		}
	}
	int64_t cnt;
	for (cnt = 1; cnt <= k; cnt++) {
		if (mp.count({ a, b })) break;
		int pt = (a == b) ? 0 : (a == b + 1 || (a == 1 && b == 3)) ? 1 : -1;
		ansA[cnt] = ansA[cnt - 1], ansB[cnt] = ansB[cnt - 1];
		if (pt == 1) ansA[cnt]++;
		else if (pt == -1) ansB[cnt]++;
		mp[{ a, b }] = cnt;
		int64_t ta = a, tb = b;
		a = movv[0][ta][tb], b = movv[1][ta][tb];
	}
	int64_t pt = mp[{ a, b }] - 1, cyc = cnt - pt - 1;
	k -= pt;
	ans[0] = ansA[pt] + (ansA[cnt - 1] - ansA[pt]) * (k / cyc) + (k % cyc != 0) * (ansA[pt + k % cyc] - ansA[pt]);
	ans[1] = ansB[pt] + (ansB[cnt - 1] - ansB[pt]) * (k / cyc) + (k % cyc != 0) * (ansB[pt + k % cyc] - ansB[pt]);
	cout << ans[0] << ' ' << ans[1];
}