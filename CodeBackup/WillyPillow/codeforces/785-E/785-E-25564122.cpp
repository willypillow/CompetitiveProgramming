#include <iostream>
#include <cassert>
#include <algorithm>
using namespace std;

typedef long long LL;
const int MAXN = 200010, MAXQ = 400000;

struct Query {
	int pos, val, type, time, id;
	Query() {}
	Query(int _pos, int _val, int _type, int _time, int _id):
		pos(_pos), val(_val), type(_type), time(_time), id(_id) {}
	bool operator <(const Query &rhs) const {
		return (pos != rhs.pos) ? (pos < rhs.pos) : (val < rhs.val);
	}
} query[MAXQ], tmp[MAXQ];
int n, q, qTop = 0, timer = 0, pos[MAXN];
LL bit[MAXN], ans[MAXN];

void add(int p, LL v) {
	while (p <= n) {
		bit[p] += v;
		p += (p & -p);
	}
}

LL sum(int p) {
	LL ans = 0;
	while (p > 0) {
		ans += bit[p];
		p -= (p & -p);
	}
	return ans;
}

void solve(int l, int r) {
	if (l == r) return;
	int m = (l + r) >> 1;
	for (int i = l; i <= r; i++) {
		if (query[i].time <= m) add(query[i].val, query[i].type);
		else ans[query[i].id] += query[i].type * (sum(n) - sum(query[i].val));
	}
	for (int i = l; i <= r; i++) {
		if (query[i].time <= m) add(query[i].val, -query[i].type);
	}
	for (int i = r; i >= l; i--) {
		if (query[i].time <= m) add(query[i].val, query[i].type);
		else ans[query[i].id] += query[i].type * sum(query[i].val - 1);
	}
	for (int i = r; i >= l; i--) {
		if (query[i].time <= m) add(query[i].val, -query[i].type);
	}
	int lTop = l, rTop = m + 1;
	for (int i = l; i <= r; i++) {
		if (query[i].time <= m) tmp[lTop++] = query[i];
		else tmp[rTop++] = query[i];
	}
	for (int i = l; i <= r; i++) query[i] = tmp[i];
	solve(l, m);
	solve(m + 1, r);
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	cin >> n >> q;
	for (int i = 1; i <= n; i++) {
		query[++qTop] = Query(i, i, 1, ++timer, 0);
		pos[i] = i;
	}
	for (int i = 1, a, b; i <= q; i++) {
		cin >> a >> b;
		query[++qTop] = Query(a, pos[b], 1, ++timer, i);
		query[++qTop] = Query(b, pos[a], 1, ++timer, i);
		query[++qTop] = Query(a, pos[a], -1, ++timer, i);
		query[++qTop] = Query(b, pos[b], -1, ++timer, i);
		swap(pos[a], pos[b]);
	}
	sort(query + 1, query + qTop + 1);
	solve(1, timer);
	for (int i = 1; i <= q; i++) cout << (ans[i] += ans[i - 1]) << '\n';
}