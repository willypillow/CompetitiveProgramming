#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int c1, c2, c3, c4; cin >> c1 >> c2 >> c3 >> c4;
	int n, m; cin >> n >> m;
	int ansA = 0, ansB = 0;
	for (int i = 0; i < n; i++) {
		int x; cin >> x;
		ansA += min(c2, c1 * x);
	}
	ansA = min(ansA, c3);
	for (int i = 0; i < m; i++) {
		int x; cin >> x;
		ansB += min(c2, c1 * x);
	}
	ansB = min(ansB, c3);
	cout << min(c4, ansA + ansB);
}