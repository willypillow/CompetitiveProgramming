#include <bits/stdc++.h>
using namespace std;

typedef uint_fast32_t Int;
typedef uint_fast64_t LL;
const Int MAXN = 1E6 + 1;

Int p[MAXN], edges[MAXN] = { 0 };
bool toSelf[MAXN] = { false };

static inline Int find(Int x) {
	return (p[x] == x) ? x : (p[x] = find(p[x]));
}

static inline void join(int x, int y) {
	p[x] = y;
}

static inline Int readInt() {
	Int res = 0;
	char ch = getchar();
	while ((unsigned)(ch - '0') >= 10U) ch = getchar();
	while ((unsigned)(ch - '0') < 10U) {
		res = (res << 3) + (res << 1) + (ch & 15);
		ch = getchar();
	}
	return res;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	Int n, m; n = readInt(); m = readInt();
	for (Int i = 1; i <= n; i++) p[i] = i;
	for (Int i = 0; i < m; i++) {
		Int u, v; u = readInt(); v = readInt();
		if (u == v) {
			++edges[u];
			toSelf[u] = true;
		} else {
			Int pu = find(u), pv = find(v);
			if (pu != pv) join(pu, pv);
			++edges[u]; ++edges[v];
		}
	}
	for (Int i = 1, p1 = 0; i <= n; i++) {
		if (!p1 && edges[i]) {
			p1 = find(i);
		} else if (edges[i] && find(i) != p1) {
			cout << "0\n";
			return 0;
		}
	}
	LL ans = 0, selfCnt = 0;
	for (Int i = 1; i <= n; i++) {
		LL t = edges[i];
		if (t >= 2) ans += t * (t - 1) >> 1;
		if (toSelf[i]) {
			ans += m - t;
			++selfCnt;
		}
	}
	ans -= selfCnt * (selfCnt - 1) >> 1;
	cout << ans << '\n';
}