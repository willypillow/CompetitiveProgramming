#include <bits/stdc++.h>
using namespace std;

int a[200001];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	cin >> a[0];
	int dd = -1;
	for (int i = 1; i < n; i++) {
		cin >> a[i];
		int d = abs(a[i] - a[i - 1]);
		if (d == 1) continue;
		if (!d || (dd != -1 && dd != d)) {
			cout << "NO";
			return 0;
		}
		dd = d;
	}
	if (dd == -1) {
		cout << "YES\n1000000000 1";
	} else {
		for (int i = 1; i < n; i++) {
			if (abs(a[i] - a[i - 1]) != 1) continue;
			int x = (a[i - 1] - 1) / dd, y = (a[i] - 1) / dd;
			if (x != y) {
				cout << "NO";
				return 0;
			}
		}
		cout << "YES\n1000000000 " << dd;
	}
}