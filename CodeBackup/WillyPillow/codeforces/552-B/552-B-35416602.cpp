#include <bits/stdc++.h>
using namespace std;

int64_t poww(int64_t x) {
	int64_t ret = 1;
	while (x--) ret *= 10;
	return ret;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int64_t n; cin >> n;
	int64_t ans = 0, i;
	for (i = 0; ; i++) {
		int64_t x = 9 * poww(i);
		if (n - x <= 0) break;
		n -= x;
		ans += (i + 1) * x;
	}
	ans += (i + 1) * n;
	cout << ans;
}