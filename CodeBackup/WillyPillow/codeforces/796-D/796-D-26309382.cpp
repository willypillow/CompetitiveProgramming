#include <bits/stdc++.h>
using namespace std;

set<int> ans;
vector<pair<int, int> > g[300001];
bool vis[300001], pol[300001];
int n, k, d;

int main() {
	cin >> n >> k >> d;
	queue<pair<int, int> > q;
	for (int i = 0; i < k; i++) {
		int x; cin >> x;
		pol[x] = true;
		q.push(make_pair(x, 0));
	}
	for (int i = 1; i < n; i++) {
		int u, v; cin >> u >> v;
		g[u].push_back(make_pair(v, i)); g[v].push_back(make_pair(u, i));
	}
	while (!q.empty()) {
		auto p = q.front(); q.pop();
		if (vis[p.first]) continue;
		vis[p.first] = true;
		for (auto y: g[p.first]) {
			if (y.first == p.second) continue;
			if (vis[y.first] || pol[y.first]) {
				ans.insert(y.second);
			} else {
				q.push(make_pair(y.first, p.first));
			}
		}
	}
	cout << ans.size() << '\n';
	for (int x: ans) cout << x << ' ';
}