#include <bits/stdc++.h>
using namespace std;

int dp[1000001], notPrime[1000001];
vector<int> primes;

int solve(int x) {
	if (dp[x]) return x;
	if (notPrime[x] == x) return x;
	int xx = x;
	vector<int> ps;
	while (xx != 1) {
		if (!ps.size() || ps.back() != notPrime[xx]) ps.push_back(notPrime[xx]);
		xx /= notPrime[xx];
	}
	dp[x] = 1E9;
	for (int p: ps) {
		dp[x] = min(dp[x], x - p + 1);
	}
	return max(3, dp[x]);
}

int solve2(int x) {
	int xx = x;
	vector<int> ps;
	while (xx != 1) {
		if (!ps.size() || ps.back() != notPrime[xx]) ps.push_back(notPrime[xx]);
		xx /= notPrime[xx];
	}
	dp[x] = 1E9;
	for (int p: ps) {
		for (int i = x - p + 1; i <= x; i++) {
			dp[x] = min(dp[x], solve(i));
		}
	}
	return dp[x];
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	for (int i = 2; i <= 1000000; i++) {
		if (!notPrime[i]) primes.push_back(i), notPrime[i] = i;
		for (int p: primes) {
			if (i * p > 1000000) break;
			notPrime[i * p] = p;
			if (i % p == 0) break;
		}
	}
	int x2; cin >> x2;
	cout << solve2(x2);
}