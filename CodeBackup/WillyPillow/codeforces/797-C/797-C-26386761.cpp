#include <bits/stdc++.h>
using namespace std;

char s[100010];
stack<char> t;
multiset<char> stS;

int main() {
	cin >> s;
	int n = strlen(s), k = 0;
	char mn = CHAR_MAX;
	for (int i = 0; i < n; i++) mn = min(mn, s[i]);
	for (int i = 0; i < n; i++) {
		if (s[i] == mn) k++;
		stS.insert(s[i]);
	}
	int i, j = 0;
	for (i = 0; i < n && j < k; i++) {
		if (s[i] == mn) {
			cout << mn;
			j++;
		} else {
			t.push(s[i]);
		}
		auto it = stS.find(s[i]);
		stS.erase(it);
	}
	while (i < n) {
		/*while (*stS.begin() < s[i]) {
			t.push(s[i]);
			stS.erase(stS.find(s[i++]));
		}
		cout << s[i];
		stS.erase(s[i++]); */
		if (t.empty() || *stS.begin() < t.top()) {
			while (s[i] != *stS.begin()) {
				t.push(s[i]);
				auto it = stS.find(s[i++]);
				stS.erase(it);
			}
			cout << *stS.begin();
			stS.erase(stS.find(s[i++]));
		} else {
			cout << t.top();
			t.pop();
		}
	}
	while (!t.empty()) {
		cout << t.top();
		t.pop();
	}
}