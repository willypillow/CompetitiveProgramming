#include <bits/stdc++.h>
using namespace std;

string str;

bool isVowel(char c) {
	return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	cin >> str;
	int cnt = 0;
	for (int i = 0, j = 0; i < (int)str.size(); i++) {
		if (isVowel(str[i])) {
			cnt = 0;
			cout << str[i];
		} else {
			if (++cnt > 2) {
				if (str[i] == str[i - 1] && str[i] == str[i - 2]) {
					cout << str[i];
				} else {
					cout << ' ' << str[i];
					cnt = 1;
				}
			} else {
				cout << str[i];
			}

		}
	}
}