#include <bits/stdc++.h>
using namespace std;

const long long DX[] = { 1, 0, 0, -1 }, DY[] = { 0, 1, -1, 0 };
long long n, xs[40], ys[40];
int pD[40];
vector<int> g[40];

bool dfs(int cur, int p, int pDir, long long x, long long y, int d) {
	xs[cur] = x; ys[cur] = y;
	pD[cur] = pDir;
	if (g[cur].size() > 4) return false;
	int i = 0;
	for (int nxt: g[cur]) {
		if (nxt == p) continue;
		do { i = (i + 1) % 4; } while (i == pDir);
		long long nx = x + (DX[i] * (1 << d)), ny = y + (DY[i] * (1 << d));
		if (!dfs(nxt, cur, 3 - i, nx, ny, d - 1)) return false;
	}
	return true;
}

int main() {
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif
	cin >> n;
	for (int i = 1; i < n; i++) {
		int x, y; cin >> x >> y;
		g[x].push_back(y); g[y].push_back(x);
	}
	if (!dfs(1, 1, -1, 0, 0, 60)) {
		cout << "NO\n";
		return 0;
	}
	cout << "YES\n";
	for (int i = 1; i <= n; i++) {
		cout << xs[i] << ' ' << ys[i] << '\n';
	}
}