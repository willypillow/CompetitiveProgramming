#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int64_t n, m; cin >> n >> m;
	while (n && m) {
		if (n >= m * 2) {
			n %= m * 2;
		} else if (m >= n * 2) {
			m %= n * 2;
		} else {
			break;
		}
	}
	cout << n << ' ' << m;
}