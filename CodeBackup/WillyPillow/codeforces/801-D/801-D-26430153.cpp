#include <bits/stdc++.h>
using namespace std;

struct P { double x, y; } p[1010];
int n;

double cross(P o, P a, P b) {
	return (a.x - o.x) * (b.y - o.y) - (a.y - o.y) * (b.x - o.x);
}

double dist2(P a, P b) {
	return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y);
}

/*double height(P vect, P p1, P p2) {
	P p3 = p1;
	p3.x += vect.x; p3.y += vect.y;
	double t = fabs(cross(p1, p2, p3));
	t /= sqrt(dist2(p1, p3));
	return t;
}*/

double height(P p0, P p1, P p2) {
	double t = fabs(cross(p0, p1, p2));
	t /= sqrt(dist2(p0, p2));
	return t;
}

double solve() {
	double ans = 1E15;
	for (int i = 0; i < n; i++) {
		int prv = (i + n - 1) % n, nxt = (i + 1) % n;
		//P vect = p[i];
		//vect.x -= p[prv].x; vect.y -= p[prv].y;
		ans = min(ans, height(p[prv], p[i], p[nxt]));
		ans = min(ans, height(p[i], p[prv], p[nxt]));
		ans = min(ans, height(p[prv], p[nxt], p[i]));
	}
	return ans / 2;
}

int main() {
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> p[i].x >> p[i].y;
	}
	cout << fixed << setprecision(8) << solve() << '\n';
}