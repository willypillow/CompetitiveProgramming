#include <bits/stdc++.h>
using namespace std;

vector<pair<int, int> > tmp, edges;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int k; cin >> k;
	bool ok = true;
	int i;
	for (i = k; i <= 2 * k; i++) {
		edges.clear();
		ok = true;
		priority_queue<pair<int, int> > q;
		for (int j = 1; j < i; j++) q.push({k, j});
		q.push({k - 1, i});
		while (q.size()) {
			auto t = q.top(); q.pop();
			tmp.clear();
			while (t.first--) {
				if (q.empty()) {
					ok = false;
					goto Break;
				}
				tmp.push_back(q.top());
				edges.push_back({ t.second, q.top().second });
				q.pop();
			}
			for (int j = 0; j < (int)tmp.size(); j++) {
				if (--tmp[j].first) q.push(tmp[j]);
			}
		}
Break:;
		if (ok) break;
	}
	cout << (ok ? "YES\n" : "NO\n");
	if (ok) {
		cout << i * 2 << ' ' << edges.size() * 2 + 1 << '\n';
		for (int j = 0; j < 2; j++) {
			for (auto &p: edges) {
				cout << p.first + j * i << ' ' << p.second + j * i << '\n';
			}
		}
		cout << i << ' ' << i * 2;
	}
}