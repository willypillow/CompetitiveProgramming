#include <bits/stdc++.h>
using namespace std;

int a[100], b[100];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 0; i < n * 2; i++) cin >> a[i];
	sort(a, a + n * 2);
	int ans = 1E9;
	for (int i = 0; i < n * 2; i++) {
		for (int j = i + 1; j < n * 2; j++) {
			int t = 0, cnt = 0, last = 0;
			for (int k = 0; k < n * 2; k++) {
				if (k == i || k == j) continue;
				if (cnt & 1) t += a[k] - a[last];
				else last = k;
				cnt++;
			}
			ans = min(ans, t);
		}
	}
	cout << ans;
}