#include <bits/stdc++.h>
using namespace std;

#define int int64_t

int64_t solve(int64_t m, int x) {
	int64_t l = 1, r = m;
	while (l < r) {
		int64_t mid = (l + r) >> 1;
		if (m * m - (m / mid) * (m / mid) < x) l = mid + 1;
		else r = mid;
	}
	if (m * m - (m / l) * (m / l) == x) return l;
	else return -1;
}

int32_t main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		int x; cin >> x;
		int lim = 50000, ans = -1;
		for (int m = 1; m <= lim; m++) {
			ans = solve(m, x);
			if (ans != -1) {
				cout << m << ' ' << ans << '\n';
				break;
			}
		}
		if (ans == -1) cout << "-1\n";
	}
}