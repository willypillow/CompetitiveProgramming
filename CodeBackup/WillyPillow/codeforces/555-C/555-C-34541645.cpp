#include <bits/stdc++.h>
using namespace std;

const int MAXN = 2E5 + 10;

int x[MAXN], y[MAXN];
char dir[MAXN];
bool done[MAXN];

struct SegTree {
	int st[MAXN * 4];
	void modify(int id, int l, int r, int qL, int qR, int val) {
		if (qL <= l && r <= qR) {
			st[id] = max(st[id], val);
		} else {
			int m = (l + r) >> 1;
			if (qL <= m) modify(id << 1, l, m, qL, qR, val);
			if (m < qR) modify(id << 1 | 1, m + 1, r, qL, qR, val);
		}
	}
	int query(int id, int l, int r, int p) {
		if (l == r) {
			return st[id];
		} else {
			int m = (l + r) >> 1;
			if (p <= m) return max(st[id], query(id << 1, l, m, p));
			else return max(st[id], query(id << 1 | 1, m + 1, r, p));
		}
	}
} up, lft;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, q; cin >> n >> q;
	vector<int> xVal = { 0 }, yVal = { 0 };
	for (int i = 0; i < q; i++) {
		cin >> x[i] >> y[i] >> dir[i];
		xVal.push_back(x[i]), yVal.push_back(y[i]);
	}
	sort(xVal.begin(), xVal.end()), sort(yVal.begin(), yVal.end());
	xVal.resize(unique(xVal.begin(), xVal.end()) - xVal.begin());
	yVal.resize(unique(yVal.begin(), yVal.end()) - yVal.begin());
	for (int i = 0; i < q; i++) {
		x[i] = (int)(lower_bound(xVal.begin(), xVal.end(), x[i]) - xVal.begin());
		y[i] = (int)(lower_bound(yVal.begin(), yVal.end(), y[i]) - yVal.begin());
	}
	for (int i = 0; i < q; i++) {
		if (done[x[i]]) {
			cout << "0\n";
		} else {
			done[x[i]] = true;
			if (dir[i] == 'U') {
				int till = up.query(1, 1, (int)xVal.size() - 1, x[i]);
				cout << yVal[y[i]] - yVal[till] << '\n';
				lft.modify(1, 1, (int)yVal.size() - 1, till, y[i], x[i]);
			} else {
				int till = lft.query(1, 1, (int)yVal.size() - 1, y[i]);
				cout << xVal[x[i]] - xVal[till] << '\n';
				up.modify(1, 1, (int)xVal.size() - 1, till, x[i], y[i]);
			}
		}
	}
}