#include <bits/stdc++.h>
using namespace std;

struct Str {
	string s;
	int cnt[26], first[26];
	Str(): s("") { for (int i = 0; i < 26; i++) cnt[i] = 0, first[i] = -1; }
	Str(string _s): s(_s) {
		for (int i = 0; i < 26; i++) cnt[i] = 0, first[i] = -1;
		int i = 0;
		for (char c: _s) {
			cnt[c - 'a']++;
			if (first[c - 'a'] == -1) first[c - 'a'] = i;
			i++;
		}
	}
};

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	set<Str*> st;
	bool fail = false;
	for (int i = 0; i < n; i++) {
		string str; cin >> str;
		st.insert(new Str(str));
	}
	for (int i = 0; i < 26; i++) {
		vector<Str*> tmp;
		for (auto it = st.begin(); it != st.end(); ) {
			if ((*it)->cnt[i]) {
				tmp.push_back(*it);
				st.erase(it++);
			} else {
				it++;
			}
		}
		deque<char> strFact;
		string newStr;
		if (tmp.empty()) continue;
		for (int d = 0; ; d++) {
			char c = '\0';
			for (auto t: tmp) {
				if (t->first[i] + d >= (int)t->s.size()) continue;
				if (!c) c = t->s[t->first[i] + d];
				if (t->s[t->first[i] + d] != c) {
					fail = true;
					goto End;
				}
			}
			if (c) strFact.push_back(c);
			else break;
		}
		for (int d = 1; ; d++) {
			char c = '\0';
			for (auto t: tmp) {
				if (t->first[i] - d < 0) continue;
				if (!c) c = t->s[t->first[i] - d];
				if (t->s[t->first[i] - d] != c) {
					fail = true;
					goto End;
				}
			}
			if (c) strFact.push_front(c);
			else break;
		}
		for (Str* t: tmp) delete t;
		for (char c: strFact) newStr.push_back(c);
		st.insert(new Str(newStr));
	}
End:
	if (!fail) {
		vector<string> vect;
		map<char, int> mp;
		for (auto s: st) {
			vect.push_back(s->s);
			for (char c: s->s) mp[c]++;
		}
		for (auto &p: mp) {
			if (p.second > 1) {
				cout << "NO";
				return 0;
			}
		}
		sort(vect.begin(), vect.end());
		for (auto s: vect) cout << s;
	} else {
		cout << "NO";
	}
}