#include <bits/stdc++.h>
using namespace std;

vector<int> g[150001], scc[150001];
bool vis[150001];

void dfs(int cur, int c) {
	scc[c].push_back(cur);
	vis[cur] = true;
	for (int y: g[cur]) {
		if (!vis[y]) dfs(y, c);
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	for (int i = 0; i < m; i++) {
		int x, y; cin >> x >> y;
		g[x].push_back(y); g[y].push_back(x);
	}
	for (int x = 1, top = 0; x <= n; x++) {
		if (!vis[x]) {
			dfs(x, ++top);
			for (int y: scc[top]) {
				if (g[y].size() != scc[top].size() - 1) {
					cout << "NO\n";
					return 0;
				}
			}
		}
	}
	cout << "YES\n";
}