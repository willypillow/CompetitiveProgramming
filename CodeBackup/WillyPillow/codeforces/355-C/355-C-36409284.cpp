#include <bits/stdc++.h>
using namespace std;

int64_t w[100001];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int64_t n, l, r, ql, qr; cin >> n >> l >> r >> ql >> qr;
	for (int i = 1; i <= n; i++) cin >> w[i];
	int64_t lSum = 0, rSum = 0;
	for (int i = 1; i <= n; i++) rSum += w[i];
	int64_t ans = 1E18;
	for (int i = 1; i <= n; i++) {
		int64_t ll = i - 1, rr = n - ll, zero = 0;
		int64_t tmp = (ll > rr ? ql * max(zero, ll - rr - 1) : qr * max(zero, rr - ll - 1));
		ans = min(lSum * l + rSum * r + tmp, ans);
		lSum += w[i], rSum -= w[i];
	}
	ans = min(lSum * l + ql * (n - 1), ans);
	cout << ans;
}