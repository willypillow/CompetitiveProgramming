#include <bits/stdc++.h>
using namespace std;

int good[100001], ans[100001][101], vis[100001];
vector<int> g[100001];
queue<pair<int, int> > goodQ[100001];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n, m, s, k; cin >> n >> m >> k >> s;
	for (int i = 1; i <= n; i++) {
		cin >> good[i];
		goodQ[good[i]].push({i, 0});
	}
	while (m--) {
		int u, v; cin >> u >> v;
		g[u].push_back(v), g[v].push_back(u);
	}
	memset(ans, 0x3f, sizeof(ans));
	for (int i = 1; i <= k; i++) {
		while (goodQ[i].size()) {
			auto cur = goodQ[i].front(); goodQ[i].pop();
			vis[cur.first] = i;
			ans[cur.first][i] = min(ans[cur.first][i], cur.second);
			for (int y: g[cur.first]) {
				if (vis[y] == i) continue;
				vis[y] = i;
				goodQ[i].push({y, cur.second + 1});
			}
		}
	}
	for (int i = 1; i <= n; i++) {
		sort(ans[i] + 1, ans[i] + 1 + k);
		int outp = 0;
		for (int j = 1; j <= s; j++) outp += ans[i][j];
		cout << outp << ' ';
	}
}