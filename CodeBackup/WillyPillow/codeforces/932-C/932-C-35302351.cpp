#include <bits/stdc++.h>
using namespace std;

int arr[1000010];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, a, b; cin >> n >> a >> b;
	int x = -1, y = -1;
	for (int i = 0; i * a <= n; i++) {
		if ((n - i * a) % b == 0) {
			x = i, y = (n - i * a) / b;
			break;
		}
	}
	if (x == -1) {
		cout << -1;
	} else {
		for (int j = 1; j <= n; j++) arr[j] = j;
		int i = 1;
		for (int j = 0; j < x; j++) {
			rotate(arr + i, arr + i + 1, arr + i + a);
			i += a;
		}
		for (int j = 0; j < y; j++) {
			rotate(arr + i, arr + i + 1, arr + i + b);
			i += b;
		}
		for (int j = 1; j <= n; j++) cout << arr[j] << ' ';
	}
}