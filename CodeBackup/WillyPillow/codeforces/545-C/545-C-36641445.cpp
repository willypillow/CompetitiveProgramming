#include <bits/stdc++.h>
using namespace std;

pair<int64_t, int64_t> p[100010];

int main() {
	int n; cin >> n;
	for (int i = 1; i <= n; i++) cin >> p[i].first >> p[i].second;
	int64_t last = -2E9, ans = 0;
	p[0].first = -3E9, p[n + 1].first = 3E9;
	for (int i = 1; i <= n; i++) {
		if (p[i].first - p[i].second > max(last, p[i - 1].first)) {
			ans++;
			last = p[i].first;
		} else if (p[i].first + p[i].second < p[i + 1].first) {
			ans++;
			last = p[i].first + p[i].second;
		}
	}
	cout << ans;
}