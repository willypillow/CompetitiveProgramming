#include <bits/stdc++.h>
using namespace std;

int x[100010], h[100010], d[100010][2];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 1; i <= n; i++) cin >> x[i] >> h[i];
	x[0] = -2E9;
	for (int i = 1; i <= n; i++) {
		d[i][0] = d[i - 1][0] + (x[i] - h[i] > x[i - 1]);
		if (x[i] > x[i - 1] + h[i - 1]) {
			d[i][0] = max(d[i][0], d[i - 1][1]);
		}
		if (x[i] - h[i] > x[i - 1] + h[i - 1]) {
			d[i][0] = max(d[i][0], d[i - 1][1] + 1);
		}
		d[i][1] = d[i - 1][0] + 1;
		if (x[i] > x[i - 1] + h[i - 1]) d[i][1] = max(d[i][1], d[i - 1][1] + 1);
	}
	cout << max({ d[n][0], d[n][1] });
}