#include <bits/stdc++.h>
using namespace std;

vector<int> g[100001];
int par[100001][2], vis[100001];

bool loop(int x) {
	vis[x] = 1;
	for (int y: g[x]) {
		if (vis[y] == 1 || (!vis[y] && loop(y))) return true;
	}
	vis[x] = 2;
	return false;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	for (int i = 1; i <= n; i++) {
		int c; cin >> c;
		while (c--) {
			int j; cin >> j;
			g[i].push_back(j);
		}
	}
	int s; cin >> s;
	queue<pair<int, bool> > q;
	q.push({s, 0});
	pair<int, bool> ans = {-1, false};
	while (q.size()) {
		auto p = q.front(); q.pop();
		if (g[p.first].empty() && p.second) {
			ans = p;
			break;
		}
		for (auto nxt: g[p.first]) {
			if (!par[nxt][!p.second]) {
				par[nxt][!p.second] = p.first;
				q.push({nxt, !p.second});
			}
		}
	}
	if (ans.first != -1) {
		cout << "Win\n";
		vector<int> prnt;
		while (ans != make_pair(s, false)) {
			prnt.push_back(ans.first);
			ans = {par[ans.first][ans.second], !ans.second};
		}
		cout << s;
		for (int i = (int)prnt.size() - 1; i >= 0; i--) cout << ' ' << prnt[i];
	} else {
		cout << (loop(s) ? "Draw" : "Lose");
	}
}