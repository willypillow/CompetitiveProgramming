#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E5 + 10;

bool vis[MAXN], vis3[MAXN], visAns[MAXN];
int vis2[MAXN], dp[MAXN], a[MAXN];
vector<int> scc[MAXN], g[MAXN], gRev[MAXN], gScc[MAXN], fin, ansVect;

void dfs(int x) {
	if (vis[x]) return;
	vis[x] = true;
	for (int y: g[x]) dfs(y);
	fin.push_back(x);
}

void dfs2(int x, int colo) {
	vis2[x] = colo;
	scc[colo].push_back(x);
	for (int y: gRev[x]) {
		if (!vis2[y]) dfs2(y, colo);
	}
}

void dfs3(int colo) {
	vis3[colo] = true;
	for (int x: scc[colo]) {
		for (int y: g[x]) {
			if (vis2[y] != colo) gScc[colo].push_back(vis2[y]);
		}
	}
	sort(gScc[colo].begin(), gScc[colo].end());
	gScc[colo].resize(
			unique(gScc[colo].begin(), gScc[colo].end()) - gScc[colo].begin());
	for (int c: gScc[colo]) {
		if (!vis3[c]) dfs3(c);
	}
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, m, h; cin >> n >> m >> h;
	for (int i = 1; i <= n; i++) cin >> a[i];
	while (m--) {
		int u, v; cin >> u >> v;
		if ((a[v] + 1) % h == a[u]) g[v].push_back(u), gRev[u].push_back(v);
		if ((a[u] + 1) % h == a[v]) g[u].push_back(v), gRev[v].push_back(u);
	}
	for (int i = 1; i <= n; i++) dfs(i);
	int colo = 0;
	for (int i = (int)fin.size() - 1; i >= 0; i--) {
		if (!vis2[fin[i]]) dfs2(fin[i], ++colo);
	}
	for (int i = 1; i <= colo; i++) {
		if (!vis3[i]) dfs3(i);
	}
	pair<int, int> ans = {1E9, 0};
	for (int i = 1; i <= colo; i++) {
		if (gScc[i].empty()) {
			ans = min(ans, {(int)scc[i].size(), i});
		}
	}
	cout << ans.first << '\n';
	for (int x: scc[ans.second]) cout << x << ' ';
}