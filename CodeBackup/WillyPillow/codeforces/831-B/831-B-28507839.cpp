#include <bits/stdc++.h>
using namespace std;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	string str1, str2, str3;
	map<char, char> mp;
	cin >> str1 >> str2 >> str3;
	for (int i = 0; i < 26; i++) {
		mp[str1[i]] = str2[i];
		mp[toupper(str1[i])] = toupper(str2[i]);
	}
	for (int i = 0; i < (int)str3.size(); i++) {
		cout << (isalpha(str3[i]) ? mp[str3[i]] : str3[i]);
	}
	cout << '\n';
}