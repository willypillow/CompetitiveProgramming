#include <bits/stdc++.h>
using namespace std;

typedef long long LL;
typedef pair<LL, int> PLI;
const int MAXN = 1E5 + 5;
const LL INF = 1LL << 60;

int treeId[2][MAXN * 4], nodeId, n;
LL dist[MAXN * 6];
vector<pair<int, int> > g[MAXN * 6];

void build(int type, int id = 1, int l = 1, int r = n) {
	treeId[type][id] = nodeId++;
	if (l == r) {
		if (type == 0) g[l].push_back(make_pair(treeId[type][id], 0));
		else g[treeId[type][id]].push_back(make_pair(l, 0));
	} else {
		int m = (l + r) >> 1;
		build(type, id << 1, l, m); build(type, id << 1 | 1, m + 1, r);
		if (type == 0) {
			g[treeId[type][id << 1]].push_back(make_pair(treeId[type][id], 0));
			g[treeId[type][id << 1 | 1]].push_back(make_pair(treeId[type][id], 0));
		} else {
			g[treeId[type][id]].push_back(make_pair(treeId[type][id << 1], 0));
			g[treeId[type][id]].push_back(make_pair(treeId[type][id << 1 | 1], 0));
		}
	}
}

void add(int type, int dst, int weight, int qL, int qR,
		int id = 1, int l = 1, int r = n) {
	if (qR < l || r < qL) return;
	if (qL <= l && r <= qR) {
		if (type == 0) g[treeId[type][id]].push_back(make_pair(dst, weight));
		else g[dst].push_back(make_pair(treeId[type][id], weight));
	} else {
		int m = (l + r) >> 1;
		add(type, dst, weight, qL, qR, id << 1, l, m);
		add(type, dst, weight, qL, qR, id << 1 | 1, m + 1, r);
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif
	int q, s; cin >> n >> q >> s;
	nodeId = n + 1;
	build(0); build(1);
	while (q--) {
		int t; cin >> t;
		if (t == 1) {
			int v, u, w; cin >> v >> u >> w;
			g[v].push_back(make_pair(u, w));
		} else {
			int v, l, r, w; cin >> v >> l >> r >> w;
			add(t == 2, v, w, l, r);
		}
	}
	fill(dist, dist + nodeId, INF);
	priority_queue<PLI, vector<PLI>, greater<PLI> > qu;
	qu.push(make_pair(dist[s] = 0, s));
	while (!qu.empty()) {
		auto p = qu.top(); qu.pop();
		if (dist[p.second] < p.first) continue;
		for (auto &nxt: g[p.second]) {
			LL newD = p.first + nxt.second;
			if (newD < dist[nxt.first]) {
				qu.push(make_pair(dist[nxt.first] = newD, nxt.first));
			}
		}
	}
	for (int i = 1; i <= n; i++) cout << (dist[i] < INF ? dist[i] : -1) << ' ';
}