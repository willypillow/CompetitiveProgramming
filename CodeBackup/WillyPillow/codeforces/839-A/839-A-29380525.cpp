#include <bits/stdc++.h>
using namespace std;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, k, i; cin >> n >> k;
	int left = 0;
	for (i = 1;i <= n; i++) {
		int x; cin >> x;
		left += x;
		int t = min(8, left);
		k -= t;
		left -= t;
		if (k <= 0) break;
	}
	cout << (i > n ? -1 : i);
}