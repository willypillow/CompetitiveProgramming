#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E5 + 5;

int num[MAXN], beginn[MAXN], maxR[MAXN];
bool ans[MAXN];

struct Query {
	int l, r, id;
	bool operator <(const Query &rhs) const {
		return l < rhs.l;
	}
} query[MAXN];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			int x; cin >> x;
			if (x < num[j]) {
				maxR[beginn[j]] = i - 1;
				beginn[j] = i;
			}
			num[j] = x;
		}
	}
	for (int j = 0; j < m; j++) maxR[beginn[j]] = n - 1;
	int k; cin >> k;
	for (int i = 0; i < k; i++) {
		cin >> query[i].l >> query[i].r;
		query[i].l--; query[i].r--;
		query[i].id = i;
	}
	sort(query, query + k);
	set<int> s;
	for (int i = 0, top = 0; i < k; i++) {
		while (top <= query[i].l) s.insert(maxR[top++]);
		auto it = s.lower_bound(query[i].r);
		ans[query[i].id] = it != s.end();
	}
	for (int i = 0; i < k; i++) cout << (ans[i] ? "Yes\n" : "No\n");
}