#include <bits/stdc++.h>
using namespace std;

int a[200010];
bool ok[200010];
map<int, int> mp;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	int ans = 1;
	a[0] = -1;
	ok[0] = true;
	for (int i = 1; i <= n; i++) {
		cin >> a[i];
		mp[a[i]]++;
	}
	for (auto &p: mp) {
		if (p.second > 1) ans += p.second - 1;
	}
	cout << max(1, ans);
}