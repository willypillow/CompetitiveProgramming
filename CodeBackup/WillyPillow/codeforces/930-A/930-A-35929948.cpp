#include <bits/stdc++.h>
using namespace std;

//vector<int> dfs(int x, int p) {
//	if (g[x].size() == 1) {
//		return 
//	}
//	for (int y: g[x]) {
//		if (y != p) dfs(y, x);
//	}
//	pair<int, int> mx = {0, 0};
//	for (int y: g[x]) {
//		if (y != p) mx = max(mx, {vect[y].size(), y});
//	}
//	if (!mx.second)
//}

int parity[100001];
vector<int> g[100001];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 2; i <= n; i++) {
		int p; cin >> p;
		g[p].push_back(i);
	}
	queue<pair<int, int> > q;
	q.push({1, 0});
	while (q.size()) {
		auto p = q.front(); q.pop();
		parity[p.second] ^= 1;
		for (int y: g[p.first]) {
			q.push({y, p.second + 1});
		}
	}
	int ans = 0;
	for (int i = 0; i < n; i++) ans += parity[i];
	cout << ans;
}