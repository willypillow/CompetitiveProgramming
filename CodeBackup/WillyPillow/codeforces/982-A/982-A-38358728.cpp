#include <bits/stdc++.h>
using namespace std;

char s[2000];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	n += 2;
	cin >> (s + 1);
	s[0] = s[n - 1] = '0';
	bool ok = true;
	for (int i = 1; i < n; i++) ok &= s[i - 1] == '0' || s[i] == '0';
	for (int i = 1; i < n - 1; i++) ok &= !(s[i - 1] == '0' && s[i] == '0' && s[i + 1] == '0');
	cout << (ok ? "Yes" : "No");
}