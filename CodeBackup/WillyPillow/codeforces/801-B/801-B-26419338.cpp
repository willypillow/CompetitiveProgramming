#include <bits/stdc++.h>
using namespace std;

string ans, x, y;

int main() {
	cin >> x >> y;
	for (int i = 0; i < x.size(); i++) {
		if (x[i] == y[i]) ans.push_back('z');
		else if (x[i] > y[i]) ans.push_back(y[i]);
		else {
			cout << "-1\n";
			return 0;
		}
	}
	cout << ans << '\n';
}