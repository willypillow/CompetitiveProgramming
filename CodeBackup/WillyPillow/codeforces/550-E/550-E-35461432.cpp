#include <bits/stdc++.h>
using namespace std;

bool a[100010];
int lB[100010], rB[100010];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 0; i < n; i++) cin >> a[i];
	int cnt = 0, i;
	for (i = n - 1; i >= 0 && !a[i]; i--) cnt++;
	if (cnt >= 3) cnt = 1, i = n - 4;
	if (cnt & 1) {
		cout << "YES\n";
		for (int j = 0; j < n; j++) {
			cout << a[j];
			if (i != n - 2) {
				if (i >= 0 && j == n - 2) cout << ")";
			}
			if (j != n - 1) cout << "->";
			if (i != n - 2) {
				if (j == i) cout << "(";
			}
		}
	} else if (cnt) {
		int suf = 0;
		for (i = n - 1; i >= 1; i--) {
			if (!a[i]) suf++;
			if (suf >= 3) break;
			if (a[i - 1] && suf % 2) break;
			if (a[i - 1]) {
				int j;
				for (j = i - 1; j >= 0 && a[j]; j--);
				lB[j + 1]++, rB[i]++;
				i = j + 1;
			}
		}
		if (!i && !a[i]) suf++;
		if (suf >= 3) {
			lB[i]++;
			rB[n - 2]++;
		} else if (suf % 2) {
			lB[i]++;
			rB[n - 1]++;
		}
		if (suf >= 3 || suf % 2) {
			cout << "YES\n";
			for (int j = 0; j < n; j++) {
				for (int k = 0; k < lB[j]; k++) cout << '(';
				cout << a[j];
				for (int k = 0; k < rB[j]; k++) cout << ')';
				if (j != n - 1) cout << "->";
			}
		} else {
			cout << "NO\n";
		}
	} else if (!cnt) {
		cout << "NO\n";
	}
}