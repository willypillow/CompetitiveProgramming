#include <bits/stdc++.h>
using namespace std;

bool a[100010];
int lB[100010], rB[100010];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	int suf = 0, i;
	for (i = 0; i < n; i++) cin >> a[i];
	if (a[n - 1]) goto Fail;
	for (i = n - 1; i >= 1; i--) {
		if (!a[i]) suf++;
		if (suf >= 3 || (a[i - 1] && suf % 2)) break;
		if (a[i - 1]) {
			int j;
			for (j = i - 1; j >= 0 && a[j]; j--);
			lB[j + 1]++, rB[i]++;
			i = j + 1;
		}
	}
	if (!i && !a[i]) suf++;
	if (i >= 0) lB[i]++, rB[suf >= 3 ? n - 2 : n - 1]++;
	if (suf >= 3 || suf % 2) {
		cout << "YES\n";
		for (int j = 0; j < n; j++) {
			cout << string(lB[j], '(') << a[j] << string(rB[j], ')');
			if (j != n - 1) cout << "->";
		}
	} else {
Fail:	cout << "NO\n";
	}
}