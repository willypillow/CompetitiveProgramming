#include <bits/stdc++.h>
using namespace std;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	int s = round(sqrt(n));
	if (s * s == n) {
		cout << s * 4;
	} else if ((s + 1) * (s - 1) >= n) {
		cout << s * 4;
	} else {
		cout << (s * 2 + 1) * 2;
	}
}