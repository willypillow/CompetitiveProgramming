#include <bits/stdc++.h>
using namespace std;

int64_t a[100010], prefMx[100010], prefMn[100010], suffMx[100010], suffMn[100010];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int64_t n, p, q, r; cin >> n >> p >> q >> r;
	for (int i = 1; i <= n; i++) cin >> a[i];
	prefMn[0] = prefMx[0] = a[1];
	suffMx[n + 1] = suffMn[n + 1] = a[n];
	for (int i = 1; i <= n; i++) {
		prefMx[i] = max(a[i], prefMx[i - 1]);
		prefMn[i] = min(a[i], prefMn[i - 1]);
	}
	for (int i = n; i >= 0; i--) {
		suffMx[i] = max(a[i], suffMx[i + 1]);
		suffMn[i] = min(a[i], suffMn[i + 1]);
	}
	int64_t ans = INT64_MIN;
	for (int i = 1; i <= n; i++) {
		int64_t ml = max(prefMx[i] * p, prefMn[i] * p), mr = max(suffMx[i] * r, suffMn[i] * r);
		ans = max(ans, ml + a[i] * q + mr);
	}
	cout << ans;
}