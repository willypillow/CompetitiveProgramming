#include <bits/stdc++.h>
using namespace std;

int a[1000];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 0; i < n; i++) cin >> a[i];
	sort(a, a + n);
	n = unique(a, a + n) - a;
	if (!a[0]) n--;
	cout << n;
}