#include <bits/stdc++.h>
using namespace std;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int64_t n, k; cin >> n >> k;
	int64_t poww = pow(10, k);
	cout << n * poww / __gcd(n, poww);
}