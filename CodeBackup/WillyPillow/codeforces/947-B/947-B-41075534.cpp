#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;
using namespace std;

tree<pair<int64_t, int>, null_type, less<pair<int64_t, int> >, rb_tree_tag,
     tree_order_statistics_node_update> st;
int v[100000], t[100000];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 0; i < n; i++) cin >> v[i];
	for (int i = 0; i < n; i++) cin >> t[i];
	int64_t acc = 0;
	for (int i = 0; i < n; i++) {
		st.insert({v[i] + acc, i});
		int64_t ans = 0;
		while (st.size() && st.begin()->first - acc <= t[i]) {
			ans += st.begin()->first - acc;
			st.erase(st.begin());
		}
		acc += t[i], ans += t[i] * st.size();
		cout << ans << '\n';
	}
}