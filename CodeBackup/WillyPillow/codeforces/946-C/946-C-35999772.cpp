#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	string s; cin >> s;
	char it = 'a';
	for (char &c: s) {
		if (c <= it) c = it++;
		if (it > 'z') break;
	}
	if (it <= 'z') cout << -1;
	else cout << s;
}