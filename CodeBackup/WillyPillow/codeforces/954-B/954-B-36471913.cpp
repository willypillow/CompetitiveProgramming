#include <bits/stdc++.h>
using namespace std;

bool check(int i, string s) {
	if (i + i - 1 >= (int)s.size()) return false;
	for (int j = 0; j < i; j++) {
		if (s[j] != s[j + i]) return false;
	}
	return true;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	string s; cin >> s;
	int ans = n;
	for (int i = 1; i <= n; i++) {
		if (check(i, s)) ans = min(ans, i + 1 + (n - i * 2));
	}
	cout << ans;
}