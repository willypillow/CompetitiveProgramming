#include <bits/stdc++.h>
using namespace std;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int s,  v1, v2, t1, t2; cin >> s >> v1 >> v2 >> t1 >> t2;
	int ta = t1 * 2 + s * v1, tb = t2 * 2 + s * v2;
	if (ta < tb) cout << "First";
	else if (ta > tb) cout << "Second";
	else cout << "Friendship";
}