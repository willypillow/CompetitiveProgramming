#include <bits/stdc++.h>
using namespace std;

const double EPS = 1E-7;

struct P { double x, y; } circle[100];
struct Line { double dx, dy, x, y; };
enum TriangleType { ACUTE, OBTUSE, RIGHT };
set<pair<int, int> > points;

double dist2(P &a, P &b) {
	return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y);
}

Line bisector(const P &a, const P &b) {
	return Line { a.y - b.y, b.x - a.x, (a.x + b.x) / 2.0, (a.y + b.y) / 2.0 };
}

P intersect(const Line &a, const Line &b) {
	double ta = (b.x - a.x) * b.dy -  (b.y - a.y) * b.dx;
	ta /= a.dx * b.dy - a.dy * b.dx;
	return P { a.x + a.dx * ta, a.y + a.dy * ta };
}

P circumcenter(P &a, P &b, P &c) {
	return intersect(bisector(a, b), bisector(b, c));
}

TriangleType triangleType(P &a, P &b, P &c) {
	double ds[] =  { dist2(a, b), dist2(b, c), dist2(c, a) };
	sort(ds, ds + 3);
	return ds[0] + ds[1] > ds[2] ? ACUTE : ds[0] + ds[1] < ds[2] ? OBTUSE : RIGHT;
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int n; cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> circle[i].x >> circle[i].y;
		points.insert({ (int)circle[i].x, (int)circle[i].y });
	}
	double ans = -1;
	for (int i = 0; i < n; i++) {
		for (int j = i + 1; j < n; j++) {
			for (int k = j + 1; k < n; k++) {
				auto type = triangleType(circle[i], circle[j], circle[k]);
				if (type == RIGHT) {
					P p = circle[i];
					p.x += (circle[j].x - circle[i].x) + (circle[k].x - circle[i].x);
					p.y += (circle[j].y - circle[i].y) + (circle[k].y - circle[i].y);
					if (points.count({ (int)p.x, (int)p.y })) {
						P cir = P { (circle[i].x + p.x) / 2, (circle[i].y + p.y) / 2 };
						double res = dist2(cir, circle[i]);
						bool fail = false;
						for (int m = 0; m < n; m++) {
							if (dist2(cir, circle[m]) + EPS < res) {
								fail = true;
								break;
							}
						}
						if (!fail) ans = max(ans, res);
					}
				}
				if (type == ACUTE) {
					P cir = circumcenter(circle[i], circle[j], circle[k]);
					if (isinf(cir.x) || isinf(cir.y)) continue;
					double res = dist2(cir, circle[i]);
					bool fail = false;
					for (int m = 0; m < n; m++) {
						if (dist2(cir, circle[m]) + EPS < res) {
							fail = true;
							break;
						}
					}
					if (!fail) ans = max(ans, res);
				}
			}
		}
	}
	cout << fixed << setprecision(6) << (ans < 0 ? -1 : sqrt(ans)) << '\n';
}