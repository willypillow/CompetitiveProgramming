#include <bits/stdc++.h>
using namespace std;

char field[2010][2010];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, m, k; cin >> n >> m >> k;
	for (int i = 0; i < n; i++) cin >> field[i];
	int ans = 0;
	if (k == 1) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) ans += field[i][j] == '.';
		}
		cout << ans;
	} else {
		for (int i = 0; i < n; i++) {
			if (k > m) break;
			int tmp = 0;
			for (int j = 0; j < k; j++) tmp += field[i][j] == '*';
			if (!tmp) ans++;
			for (int j = 0; j + k < m; j++) {
				tmp -= field[i][j] == '*';
				tmp += field[i][j + k] == '*';
				if (!tmp) ans++;
			}
		}
		for (int j = 0; j < m; j++) {
			if (k > n) break;
			int tmp = 0;
			for (int i = 0; i < k; i++) tmp += field[i][j] == '*';
			if (!tmp) ans++;
			for (int i = 0; i + k < n; i++) {
				tmp -= field[i][j] == '*';
				tmp += field[i + k][j] == '*';
				if (!tmp) ans++;
			}
		}
		cout << ans;
	}
}