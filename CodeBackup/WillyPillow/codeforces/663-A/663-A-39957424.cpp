#include <bits/stdc++.h>
using namespace std;

vector<pair<bool, int> > sign;
bool signOf[110];
int ans[110];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int cnt = 0;
	char c; cin >> c;
	sign.push_back({c != '-', 0});
	signOf[0] = c != '-';
	while (cin >> c && c != '=') {
		if (c == '+') {
			sign.push_back({1, ++cnt});
			signOf[cnt] = 1;
		} else if (c == '-') {
			sign.push_back({0, ++cnt});
			signOf[cnt] = 0;
		}
	}
	int n; cin >> n;
	sort(sign.begin(), sign.end());
	int l, nn = n;
	for (l = 0; l <= cnt && !sign[l].first; l++) ans[sign[l].second] = 1, nn++;
	while (l && nn < (cnt - l + 1)) {
		for (int i = 0; i < l && nn < (cnt - l + 1); i++) ans[sign[i].second]++, nn++; 
	}
	while (nn) {
		for (int i = l; i <= cnt && nn; i++, nn--)  ans[sign[i].second]++;
	}
	for (int i = 0; i <= cnt; i++) {
		if (ans[i] < 1 || ans[i] > n) {
			cout << "Impossible";
			return 0;
		}
	}
	cout << "Possible\n";
	if (!signOf[0]) cout << "- ";
	cout << ans[0];
	for (int i = 1; i <= cnt; i++) {
		cout << (signOf[i] ? " + " : " - ") << ans[i];
	}
	cout << " = " << n;
}