#include <bits/stdc++.h>
using namespace std;

const int BLK = 1050, MAXN = 5E4 + 5;

int _ans[MAXN], _ptr[MAXN], a[MAXN];

bool vis[MAXN];

int qcnt = 1999;
void query(int p) {
	if (qcnt-- == 0) for(;;);
	cout << "? " << p << endl;
	int val, nxt; cin >> val >> nxt;
	assert(!(val == -1 && nxt == -1));
	_ans[p] = val;
	_ptr[p] = nxt;
}

int ans(int p) {
	if (_ans[p] != -1) return _ans[p];
	query(p);
	return _ans[p];
}

int ptr(int p) {
	if (_ptr[p]) return _ptr[p];
	query(p);
	return _ptr[p];
}

int main() {
	srand(71227122);
	memset(_ans, -1, sizeof(_ans));
	memset(_ptr, 0, sizeof(_ptr));
	int n, start, x; cin >> n >> start >> x;
	for (int i = 1; i <= n; i++) a[i] = i;
	random_shuffle(a + 1, a + n + 1);
	set<pair<int, int> > s;
	s.insert({ -1E9, start });
	s.insert({ ans(start), ptr(start) });
	s.insert({ 1E9 + 10, -1 });
	vis[start] = true;
	for (int i = 1, cnt = 0; cnt < BLK && i <= n; i++) {
		if (vis[a[i]]) continue;
		int nxt = ptr(a[i]);
		s.insert({ ans(a[i]), nxt });
		vis[a[i]] = true;
		if (nxt != -1) vis[nxt] = true;
		cnt++;
	}
	auto it = s.lower_bound({ x, -1 });
	int mx = it->second;
	it--;
	//cout << "MM" << it->second << ' ' << mx << endl;
	for (int i = it->second; i != mx; i = ptr(i)) {
		if (ans(i) >= x) {
			cout << "! " << ans(i) << endl;
			return 0;
		}
	}
	cout << "! -1" << endl;
}