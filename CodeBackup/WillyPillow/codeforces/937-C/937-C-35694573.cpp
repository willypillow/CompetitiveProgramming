#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int64_t k, d, t; cin >> k >> d >> t;
	t *= 2;
	auto x = (k + d - 1) / d;
	auto per = k + x * d; //k * 2 + (x * d - k);
	__float128 ans = t / per * x * d;
	t %= per;
	if (t <= k * 2) ans += (__float128)t / 2;
	else {
		t -= k * 2;
		ans += k + t;
	}
	cout << fixed << setprecision(10) << (double)ans;
}