#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n; cin >> n;
	while (n--) {
		int64_t p, q, b; cin >> p >> q >> b;
		q /= __gcd(p, q);
		int64_t g;
		while ((g = __gcd(q, b)) != 1) {
			while (q % g == 0) q /= g;
		}
		cout << (q == 1 ? "Finite\n" : "Infinite\n");
	}
}