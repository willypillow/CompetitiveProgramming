#include <bits/stdc++.h>
using namespace std;

const int MOD = 1E9 + 7;

int64_t dp[100010][3][11], n, m, k;
vector<int> g[100010];
bool vis[100010][3];

void dfs(int x, int p, int stat) {
	if (vis[x][stat]) return;
	vis[x][stat] = true;
	int64_t ans[2][11] = { 0 };
	if (stat == 0) {
		bool t = 0;
		ans[!t][1] = 1;
		for (int y: g[x]) {
			if (y == p) continue;
			dfs(y, x, 1);
			for (int i = 1; i <= 10; i++) {
				for (int j = 0; i + j <= 10; j++) {
					ans[t][i + j] += (ans[!t][i] * dp[y][1][j]) % MOD;
					ans[t][i + j] %= MOD;
				}
			}
			t = !t;
			for (int i = 0; i <= 10; i++) ans[t][i] = 0;
		}
		for (int i = 0; i <= 10; i++) dp[x][stat][i] += ans[!t][i];
	}
	if (stat != 1) {
		memset(ans, 0, sizeof(ans));
		bool t = 0;
		ans[!t][0] = m - k;
		for (int y: g[x]) {
			if (y == p) continue;
			dfs(y, x, 2);
			for (int i = 0; i <= 10; i++) {
				for (int j = 0; i + j <= 10; j++) {
					ans[t][i + j] += (ans[!t][i] * dp[y][2][j]) % MOD;
					ans[t][i + j] %= MOD;
				}
			}
			t = !t;
			for (int i = 0; i <= 10; i++) ans[t][i] = 0;
		}
		for (int i = 0; i <= 10; i++) dp[x][stat][i] += ans[!t][i];
	}
	memset(ans, 0, sizeof(ans));
	bool t = 0;
	ans[!t][0] = k - 1;
	for (int y: g[x]) {
		if (y == p) continue;
		dfs(y, x, 0);
		for (int i = 0; i <= 10; i++) {
			for (int j = 0; i + j <= 10; j++) {
				ans[t][i + j] += (ans[!t][i] * dp[y][0][j]) % MOD;
				ans[t][i + j] %= MOD;
			}
		}
		t = !t;
		for (int i = 0; i <= 10; i++) ans[t][i] = 0;
	}
	for (int i = 0; i <= 10; i++) {
		dp[x][stat][i] = (dp[x][stat][i] + ans[!t][i]) % MOD;
	}
}


int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	cin >> n >> m;
	for (int i = 1; i < n; i++) {
		int u, v; cin >> u >> v;
		g[u].push_back(v); g[v].push_back(u);
	}
	int x; cin >> k >> x;
	dfs(1, 1, 0);
	int64_t ans = 0;
	for (int i = 0; i <= x; i++) ans += dp[1][0][i];
	cout << ans % MOD << '\n';
	//for (int i = 1; i <= n; i++) {
	//	for (int j = 0; j < 3; j++) {
	//			for (int k = 0; k <= 10; k++) {
	//				cout << i << ',' << j << ',' << k << ' ' << dp[i][j][k] << '\n';
	//			}
	//	}
	//}
}