#include <bits/stdc++.h>
using namespace std;

int cntA[26], cntB[26], cntC[26];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	string a, b, c; cin >> a >> b >> c;
	for (char ch: a) cntA[ch - 'a']++;
	for (char ch: b) cntB[ch - 'a']++;
	for (char ch: c) cntC[ch - 'a']++;
	int mn = 1E9;
	for (int i = 0; i < 26; i++) {
		if (cntB[i]) mn = min(mn, cntA[i] / cntB[i]);
	}
	tuple<int, int, int> ans;
	for (int i = 0; i <= mn; i++) {
		int mn2 = 1E9;
		for (int j = 0; j < 26; j++) {
			if (cntC[j]) mn2 = min(mn2, (cntA[j] - cntB[j] * i) / cntC[j]);
		}
		ans = max(ans, {i + mn2, i, mn2});
	}
	for (int i = 0; i < get<1>(ans); i++) {
		cout << b;
		for (char ch: b) cntA[ch - 'a']--;
	}
	for (int i = 0; i < get<2>(ans); i++) {
		cout << c;
		for (char ch: c) cntA[ch - 'a']--;
	}
	for (int i = 0; i < 26; i++) {
		for (int j = 0; j < cntA[i]; j++) cout << char('a' + i);
	}
}