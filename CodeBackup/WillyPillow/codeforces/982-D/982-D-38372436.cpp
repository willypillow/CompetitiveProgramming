#include <bits/stdc++.h>
using namespace std;

int a[100010];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	set<tuple<int, int, int> > st;
	priority_queue<pair<int, int>, vector<pair<int, int> >, greater<pair<int, int> > > pq;
	for (int i = 1; i <= n; i++) {
		cin >> a[i];
		st.insert(make_tuple(i, i, 0));
		pq.push({a[i], i});
	}
	vector<int> todo;
	pair<int, int> ans = {0, 0};
	multiset<int> cnts;
	while (pq.size()) {
		todo.clear();
		int x = pq.top().first;
		todo.push_back(pq.top().second);
		pq.pop();
		while (pq.size() && pq.top().first == x) {
			todo.push_back(pq.top().second);
			pq.pop();
		}
		for (int p: todo) {
			auto it = st.lower_bound(make_tuple(p, 0, 0));
			auto tup = *it;
			get<2>(tup) = 1;
			if (it != st.begin()) {
				auto itp = prev(it);
				if (get<2>(*itp)) {
					get<0>(tup) = get<0>(*itp);
					auto rem = cnts.find(get<1>(*itp) - get<0>(*itp) + 1);
					cnts.erase(rem);
					st.erase(itp);
				}
			}
			if (next(it) != st.end()) {
				auto itn = next(it);
				if (get<2>(*itn)) {
					get<1>(tup) = get<1>(*itn);
					auto rem = cnts.find(get<1>(*itn) - get<0>(*itn) + 1);
					cnts.erase(rem);
					st.erase(itn);
				}
			}
			st.erase(it), st.insert(tup);
			cnts.insert(get<1>(tup) - get<0>(tup) + 1);
		}
		if (cnts.size() && *cnts.begin() == *cnts.rbegin()) {
			ans = max(ans, {cnts.size(), -x});
			// cout << cnts.size() << ' ' << x << '\n';
		}
	}
	cout << -ans.second + 1;
}