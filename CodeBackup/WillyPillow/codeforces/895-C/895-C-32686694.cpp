#include <bits/stdc++.h>
using namespace std;

const int MOD = 1E9 + 7;

vector<int> primes;
bool notP[71];
int dp[2][1 << 19], cnt[71];
int64_t poww[100001];

void genPrime() {
	for (int i = 2; i <= 70; i++) {
		if (!notP[i]) {
			primes.push_back(i);
			for (int j = i + i; j <= 70; j += i) notP[j] = true;
		}
	}
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	genPrime();
	int n; cin >> n;
	poww[0] = 1;
	for (int i = 1; i <= n; i++) poww[i] = (poww[i - 1] << 1) % MOD;
	for (int i = 0; i < n; i++) {
		int x; cin >> x;
		cnt[x]++;
	}
	dp[0][0] = 1;
	dp[1][0] = poww[cnt[1]];
	for (int i = 2; i <= 70; i++) {
		int x = i, mask = 0;
		for (int j = 0; j < (int)primes.size(); j++) {
			if (x % primes[j] == 0) {
				int t = 0;
				while (x % primes[j] == 0) {
					x /= primes[j];
					t++;
				}
				if (t & 1) mask |= 1 << j;
			}
		}
		for (int k = 0; k < (1 << primes.size()); k++) {
			if (cnt[i]) {
				int old1 = dp[~i & 1][k ^ mask] * poww[cnt[i] - 1] % MOD,
					old2 = dp[~i & 1][k] * poww[cnt[i] - 1] % MOD;
				dp[i & 1][k] = (old1 + old2) % MOD;
			} else {
				dp[i & 1][k] = dp[~i & 1][k];
			}
		}
	}
	cout << dp[70 & 1][0] - 1;
}