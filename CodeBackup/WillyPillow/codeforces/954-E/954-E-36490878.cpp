#include <bits/stdc++.h>
using namespace std;

int n, t;
pair<int64_t, int64_t> tap[200010];
long double totWater = 0, totTemp = 0;

bool solve(long double m) {
	long double sum = 0, left = m;
	for (int i = 0; i < n; i++) {
		auto quota = min(left, (long double)tap[i].second);
		left -= quota, sum += tap[i].first * quota;
	}
	return (totTemp > t ? sum <= t * m : sum >= t * m);
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	cin >> n >> t;
	for (int i = 0; i < n; i++) cin >> tap[i].second;
	for (int i = 0; i < n; i++) cin >> tap[i].first;
	for (int i = 0; i < n; i++) {
		totWater += tap[i].second;
		totTemp += tap[i].first * tap[i].second;
	}
	totTemp /= totWater;
	if (abs(totTemp - t) < 5E-8) {
		cout << totWater;
		return 0;
	}
	if (totTemp > t) sort(tap, tap + n);
	else sort(tap, tap + n, greater<pair<int, int> >());
	long double l = 0, r = totWater;
	while (r - l > 5E-8) {
		auto m = (l + r) / 2;
		if (solve(m)) l = m;
		else r = m;
	}
	cout << fixed << setprecision(10) << l;
}