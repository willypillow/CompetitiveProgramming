#include <bits/stdc++.h>
using namespace std;

int64_t calc(string &str, int l, int r) {
	int64_t ret = 0, mulTmp = str[l] - '0';
	for (int i = l + 1; i < r; i += 2) {
		if (str[i] == '+') {
			ret += mulTmp;
			mulTmp = str[i + 1] - '0';
		} else {
			mulTmp *= str[i + 1] - '0';
		}
	}
	return ret += mulTmp;
}

int64_t solve(string &str) {
	vector<int> mul;
	for (int i = 0; i < (int)str.size(); i++) {
		if (str[i] == '*') mul.push_back(i);
	}
	int ptr = 1;
	int64_t ans = 0, lPref = 0, mulTmp = str[0] - '0';
	for (int i = 0; i < (int)mul.size(); i++) {
		while (ptr < mul[i]) {
			if (str[ptr] == '+') {
				lPref += mulTmp;
				mulTmp = str[ptr + 1] - '0';
			} else {
				mulTmp *= str[ptr + 1] - '0';
			}
			ptr += 2;
		}
		int64_t rPref = 0, rMulTmp = str[str.size() - 1] - '0';
		ans = max(ans, lPref + mulTmp * calc(str, mul[i] + 1, (int)str.size() - 1));
		for (int j = (int)str.size() - 2; j > mul[i]; j -= 2) {
			if (str[j] == '+') {
				ans = max(ans, lPref + rPref + mulTmp * calc(str, mul[i] + 1, j - 1) + rMulTmp);
				rPref += rMulTmp;
				rMulTmp = str[j - 1] - '0';
			} else {
				ans = max(ans, lPref + rPref + mulTmp * calc(str, mul[i] + 1, j - 1) * rMulTmp);
				rMulTmp *= str[j - 1] - '0';
			}
		}
	}
	return ans;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	string str; cin >> str;
	int64_t ans = max(calc(str, 0, (int)str.size() - 1), solve(str));
	reverse(str.begin(), str.end());
	ans = max(ans, solve(str));
	cout << ans;
}