#include <bits/stdc++.h>
using namespace std;

struct ST {
	int n;
	int64_t st[200001 * 2];
	void pull(int p) {
		if (p <= 1) return;
		auto tmp = min(st[p], st[p ^ 1]);
		st[p] -= tmp, st[p ^ 1] -= tmp, st[p >> 1] += tmp;
	}
	void add(int l, int r, int64_t v) {
		l += n, r += n;
		if (l == r) {
			st[l] += v;
		} else {
			st[l] += v, st[r] += v;
			for (; l ^ r ^ 1; l >>= 1, r >>= 1) {
				if (~l & 1) st[l ^ 1] += v;
				if ( r & 1) st[r ^ 1] += v;
				pull(l), pull(r);
			}
		}
		for (; l > 1; l >>= 1) pull(l);
	}
	int64_t query(int l, int r, int64_t lMx = 0, int64_t rMx = 0) {
		l += n, r += n;
		if (l != r) {
			for (; l ^ r ^ 1; l >>= 1, r >>= 1) {
				lMx += st[l], rMx += st[r];
				if (~l & 1) lMx = min(lMx, st[l ^ 1]);
				if ( r & 1) rMx = min(rMx, st[r ^ 1]);
			}
		}
		int64_t ret = min(lMx + st[l], rMx + st[r]);
		for (l >>= 1; l; l >>= 1) ret += st[l];
		return ret;
	}
	void build(int _n) {
		n = _n;
		for (int i = n; i; i--) st[i] = min(st[i << 1], st[i << 1 | 1]);
		for (int i = n + n; i; i--) st[i] -= st[i >> 1];
	}
} st;

int main() {
	ios_base::sync_with_stdio(0), cin.tie(0);
	int n; cin >> n;
	for (int i = 1; i <= n; i++) cin >> st.st[n + i];
	st.build(n);
	int m; cin >> m >> ws;
	while (m--) {
		string str; getline(cin, str);
		stringstream ss(str);
		int i, a[3];
		for (i = 0; ss >> a[i]; i++);
		a[0]++, a[1]++;
		if (i == 2) {
			if (a[0] > a[1]) cout << min(st.query(a[0], n), st.query(1, a[1])) << '\n';
			else cout << st.query(a[0], a[1]) << '\n';
		} else if (i == 3) {
			if (a[0] > a[1]) st.add(a[0], n, a[2]), st.add(1, a[1], a[2]);
			else st.add(a[0], a[1], a[2]);
		} else {
			m++;
		}
	}
}