#include <bits/stdc++.h>
using namespace std;

const int MAXX = 1E6, MAXN = 3E5 + 1;

vector<int> primes;
int d[MAXX + 1], minDivCnt[MAXX + 1], cycle[MAXN * 4], a[MAXN];
int64_t sum[MAXN * 4];

void buildD() {
	primes.reserve(100000);
	d[1] = 1;
	for (int i = 2; i <= MAXX; i++) {
		if (!d[i]) {
			primes.push_back(i);
			minDivCnt[i] = 1, d[i] = 2;
		}
		for (int j: primes) {
			if (i * j > MAXX) break;
			minDivCnt[i * j] = (i % j ? 1 : minDivCnt[i] + 1);
			if (i % j == 0) {
				d[i * j] = d[i] / (minDivCnt[i] + 1) * (minDivCnt[i * j] + 1);
				break;
			}
			d[i * j] = d[i] * d[j];
		}
	}
}

void build(int id, int l, int r) {
	if (l == r) {
		cycle[id] = (d[a[l]] == a[l]), sum[id] = a[l];
	} else {
		int m = (l + r) >> 1;
		build(id << 1, l, m), build(id << 1 | 1, m + 1, r);
		cycle[id] = cycle[id << 1] + cycle[id << 1 | 1];
		sum[id] = sum[id << 1] + sum[id << 1 | 1];
	}
}

void modify(int id, int l, int r, int qL, int qR) {
	if (cycle[id] == r - l + 1 || qR < l || r < qL) return;
	if (l == r) {
		sum[id] = a[l] = d[a[l]];
		if (a[l] == d[a[l]]) cycle[id] = 1;
	} else {
		int m = (l + r) >> 1;
		modify(id << 1, l, m, qL, qR), modify(id << 1 | 1, m + 1, r, qL, qR);
		cycle[id] = cycle[id << 1] + cycle[id << 1 | 1];
		sum[id] = sum[id << 1] + sum[id << 1 | 1];
	}
}

int64_t query(int id, int l, int r, int qL, int qR) {
	if (qR < l || r < qL) return 0;
	if (qL <= l && r <= qR) return sum[id];
	int m = (l + r) >> 1;
	return query(id << 1, l, m, qL, qR) + query(id << 1 | 1, m + 1, r, qL, qR);
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	buildD();
	int n, m; cin >> n >> m;
	for (int i = 1; i <= n; i++) cin >> a[i];
	build(1, 1, n);
	while (m--) {
		int t, l, r; cin >> t >> l >> r;
		if (t == 1) modify(1, 1, n, l, r);
		else cout << query(1, 1, n, l, r) << '\n';
	}
}