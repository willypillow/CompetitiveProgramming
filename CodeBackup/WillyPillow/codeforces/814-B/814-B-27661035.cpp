#include <bits/stdc++.h>
using namespace std;

int a[1001], b[1001], aDup[2], bDup[2], ans[1001], vis[1001];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	int aMissing = 0, bMissing = 0;
	for (int i = 1; i <= n; i++) cin >> a[i];
	for (int i = 1; i <= n; i++) cin >> b[i];
	for (int i = 1; i <= n; i++) {
		if (vis[a[i]]) aDup[0] = vis[a[i]], aDup[1] = i;
		vis[a[i]] = i;
	}
	for (int i = 1; i <= n; i++) {
		if (!vis[i]) {
			aMissing = i;
			break;
		}
	}
	memset(vis, 0, sizeof(vis));
	for (int i = 1; i <= n; i++) {
		if (vis[b[i]]) bDup[0] = vis[b[i]], bDup[1] = i;
		vis[b[i]] = i;
	}
	for (int i = 1; i <= n; i++) {
		if (!vis[i]) {
			bMissing = i;
			break;
		}
	}
	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < 2; j++) {
			memset(vis, 0, sizeof(vis));
			bool ok = true;
			int difA = 0, difB = 0;
			for (int k = 1; k <= n; k++) {
				if (k == aDup[i]) ans[k] = aMissing;
				else if (k == bDup[j]) ans[k] = bMissing;
				else ans[k] = a[k];
				if (vis[ans[k]]) {
					ok = false;
					break;
				}
				vis[ans[k]] = k;
				if (ans[k] != a[k]) difA++;
				if (ans[k] != b[k]) difB++;
			}
			if (ok && difA == 1 && difB == 1) goto End;
		}
	}
End:
	for (int i = 1; i <= n; i++) cout << ans[i] << ' ';
}