#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int a[4]; cin >> a[0] >> a[1] >> a[2] >> a[3];
	int ans = 0;
	while (a[0] && a[2] && a[3]) {
		a[0]--, a[2]--, a[3]--;
		ans += 256;
	}
	while (a[0] && a[1]) {
		a[0]--, a[1]--;
		ans += 32;
	}
	cout << ans;
}