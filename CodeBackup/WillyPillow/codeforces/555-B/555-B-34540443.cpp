#include <bits/stdc++.h>
using namespace std;

tuple<int64_t, int64_t, int> d[200010];
pair<int64_t, int> b[200010];
int ans[200010];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	int64_t lastL = 0, lastR = 0;
	for (int i = 0; i < n; i++) {
		int64_t l, r; cin >> l >> r;
		if (i) d[i - 1] = { l - lastR, r - lastL, i - 1 };
		lastL = l, lastR = r;
	}
	for (int i = 0; i < m; i++) {
		cin >> b[i].first;
		b[i].second = i + 1;
	}
	sort(d, d + n - 1); sort(b, b + m);
	set<pair<int64_t, int> > st;
	for (int i = 0, j = 0; i < m; i++) {
		while (j < n - 1 && b[i].first >= get<0>(d[j])) {
			st.insert({ get<1>(d[j]), get<2>(d[j]) });
			j++;
		}
		if (st.empty()) continue;
		auto &tp = *st.begin();
		if (tp.first >= b[i].first) {
			ans[tp.second] = b[i].second;
			st.erase(st.begin());
		}
	}
	bool fail = false;
	for (int i = 0; i < n - 1; i++) fail |= !ans[i];
	if (fail) {
		cout << "No";
	} else {
		cout << "Yes\n";
		for (int i = 0; i < n - 1; i++) cout << ans[i] << ' ';
	}
}