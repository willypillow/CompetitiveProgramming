#include <bits/stdc++.h>
using namespace std;

int a[200], dp[200][4];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 1; i <= n; i++) cin >> a[i];
	for (int i = 1; i <= n; i++) {
		dp[i][0] = min({dp[i - 1][0], dp[i - 1][1], dp[i - 1][2]}) + 1;
		if (a[i] & 1) {
			dp[i][1] = min({dp[i - 1][0], dp[i - 1][2]});
		} else {
			dp[i][1] = 1E9;
		}
		if (a[i] & 2) {
			dp[i][2] = min({dp[i - 1][0], dp[i - 1][1]});
		} else {
			dp[i][2] = 1E9;
		}
	}
	cout << min({dp[n][0], dp[n][1], dp[n][2]});
}