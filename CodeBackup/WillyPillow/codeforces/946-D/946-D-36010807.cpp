#include <bits/stdc++.h>
using namespace std;

int dp[510][510], pos[510][510], mx1[510];
char arr[510][510];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, m, k; cin >> n >> m >> k;
	for (int i = 1; i <= n; i++) cin >> (char*)(arr[i] + 1);
	memset(pos, 0x3f, sizeof(pos));
	for (int i = 1; i <= n; i++) {
		int lCnt = 0;
		for (int l = 1; l <= m; l++) {
			int cnt = lCnt;
			for (int r = m; r >= l; r--) {
				if (arr[i][r] == '1') {
					pos[i][cnt] = min(pos[i][cnt], r - l + 1);
					cnt++;
				}
			}
			mx1[i] = max(mx1[i], cnt);
			if (arr[i][l] == '1') lCnt++;
		}
		pos[i][mx1[i]++] = 0;
	}
	for (int i = 1; i <= n; i++) {
		for (int j = 0; j <= k; j++) {
			dp[i][j] = 1E9;
			for (int kk = 0; kk <= min(j, mx1[i] - 1); kk++) {
				dp[i][j] = min(dp[i][j], dp[i - 1][j - kk] + pos[i][kk]);
			}
		}
	}
	cout << *min_element(dp[n], dp[n] + k + 1);
}