#include <bits/stdc++.h>
using namespace std;

const int MAX = 1E6;
vector<int> nums;
int dp[MAX * 3], par[MAX * 3];

void pre(int x) {
    nums.push_back(x);
    nums.push_back(x + 1);
    if (x * 10 <= MAX) {
        pre(x * 10);
        pre((x + 1) * 10);
    }
}

int main() {
    int n; cin >> n;
    nums.push_back(1);
    pre(10);
    memset(dp, 0x3f, sizeof(dp));
    dp[0] = 0;
    for (int i = 0; i < MAX; i++) {
        for (int x: nums) {
            if (dp[i + x] > dp[i] + 1) {
                dp[i + x] = dp[i] + 1;
                par[i + x] = x;
            }
        }
    }
    cout << dp[n] << '\n';
    while (n) {
        cout << par[n] << ' ';
        n -= par[n];
    }
}