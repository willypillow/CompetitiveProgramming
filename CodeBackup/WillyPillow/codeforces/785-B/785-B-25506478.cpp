#include <iostream>
#include <algorithm>
using namespace std;

struct P { int l, r; } a[200010], b[200010];

bool cmpR(P &l, P &r) {
	return l.r < r.r;
}

bool cmpL(P &l, P &r) {
	return l.l > r.l;
}

int main() {
	int n; cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> a[i].l >> a[i].r;
	}
	sort(a, a + n, cmpR);
	int m; cin >> m;
	for (int i = 0; i < m; i++) {
		cin >> b[i].l >> b[i].r;
	}
	sort(b, b + m, cmpL);
	int ans = max(0, b[0].l - a[0].r);
	sort(a, a + n, cmpL);
	sort(b, b + m, cmpR);
	ans = max(ans, a[0].l - b[0].r);
	cout << ans << '\n';
}