#include <bits/stdc++.h>
using namespace std;

vector<pair<int64_t, int> > g[200001];
int64_t dist[200001];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	while (m--) {
		int64_t u, v, w; cin >> u >> v >> w;
		g[u].push_back({w << 1, (int)v}), g[v].push_back({w << 1, (int)u});
	}
	typedef pair<int64_t, int> Pii;
	memset(dist, 0x3f, sizeof(dist));
	priority_queue<Pii, vector<Pii>, greater<Pii> > q;
	for (int i = 1; i <= n; i++) {
		int64_t a; cin >> a;
		q.push({dist[i] = a, i});
	}
	while (q.size()) {
		auto p = q.top(); q.pop();
		if (dist[p.second] < p.first) continue;
		for (auto &e: g[p.second]) {
			if (p.first + e.first < dist[e.second]) {
				q.push({dist[e.second] = p.first + e.first, e.second});
			}
		}
	}
	for (int i = 1; i <= n; i++) cout << dist[i] << ' ';
}