#include <bits/stdc++.h>
using namespace std;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	int x = 1, y = 2;
	if (n & 1) swap(x, y);
	vector<int> ans;
	for (int i = x; i <= n; i += 2) ans.push_back(i);
	for (int i = y; i <= n; i += 2) ans.push_back(i);
	for (int i = x; i <= n; i += 2) ans.push_back(i);
	cout << ans.size() << '\n';
	for (int i: ans) cout << i << ' ';
}