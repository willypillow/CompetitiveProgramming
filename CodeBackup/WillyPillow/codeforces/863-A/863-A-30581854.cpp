#include <bits/stdc++.h>
using namespace std;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	string str; cin >> str;
	reverse(str.begin(), str.end());
	for (int i = 0; i <= 10; i++) {
		bool fail = false;
		for (int j = 0; j < (int)str.size(); j++) {
			if (str[j] != str[(int)str.size() - j - 1]) {
				fail = true;
				break;
			}
		}
		if (!fail) {
			cout << "YES";
			return 0;
		}
		str.push_back('0');
	}
	cout << "NO";
}