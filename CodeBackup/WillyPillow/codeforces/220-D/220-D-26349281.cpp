#include <bits/stdc++.h>
using namespace std;

typedef long long LL;
const int MOD = 1E9 + 7;

inline LL c2(LL x) {
	return x * (x - 1) / 2;
}

inline LL calcCnt(int x, int i) {
	return (x + 1) / 2 + ((x + 1) % 2 && i);
}

inline int cross(int x1, int y1, int x2, int y2) {
	return x1 * y2 - x2 * y1;
}

int main() {
	int w, h; cin >> w >> h;
	LL ans = 0;
	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < 2; j++) {
			LL cnt = calcCnt(w, i) * calcCnt(h, j) % MOD, evenCnt = 0, c[4];
			for (int p = 0; p < 2; p++) {
				for (int q = 0; q < 2; q++) {
					c[p << 1 | q] = calcCnt(w, i ^ p) * calcCnt(h, j ^ q);
				}
			}
			c[0]--;
			for (int p = 0; p < 4; p++) {
				for (int q = p; q < 4; q++) {
					if (cross(p >> 1, p & 1, q >> 1, q & 1) % 2) continue;
					if (p == q) evenCnt += c2(c[p]);
					else evenCnt += c[p] * c[q];
					evenCnt %= MOD;
				}
			}
			ans = (ans + cnt * evenCnt) % MOD;
		}
	}
	ans *= 2;
	for (int i = 0; i <= w; i++) {
		for (int j = 0; j <= h; j++) {
			if (!i && !j) continue;
			LL cnt = (LL)(w - i + 1) * (h - j + 1) * (__gcd(i, j) - 1) * 6;
			if (i && j) cnt *= 2;
			ans = ((ans - cnt) % MOD + MOD) % MOD;
		}
	}
	cout << ans << '\n';
}