#include <bits/stdc++.h>
using namespace std;

int pos[1000];
set<int> can;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	for (int i = 0; i < m; i++) {
		int x, y; cin >> x >> y;
		pos[x] = y;
	}
	for (int i = 1; i <= 100; i++) {
		bool fail = false;
		for (int j = 1; j <= 100; j++) {
			if (!pos[j]) continue;
			int floor = ((j - 1) / i) + 1;
			if (floor != pos[j]) {
				fail = true;
				break;
			}
		}
		if (!fail) can.insert((n - 1) / i + 1);
	}
	if (can.size() != 1) {
		cout << "-1";
	} else {
		cout << *can.begin();
	}
}