#include <bits/stdc++.h>
using namespace std;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int k; cin >> k;
	int mx = 0;
	for (int i = 0; i < k; i++) {
		int x; cin >> x;
		mx = max(mx, x);
	}
	cout << max(mx - 25, 0);
}