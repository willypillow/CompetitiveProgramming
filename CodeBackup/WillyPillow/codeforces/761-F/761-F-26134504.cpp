#include <bits/stdc++.h>
using namespace std;

typedef long long LL;
const int MAXN = 1E3 + 10, MAXK = 3E5 + 10;

char field[MAXN][MAXN];
int cnt[MAXN][MAXN][26];
LL g[MAXN][MAXN], q[MAXN][MAXN][26];
struct P {
	int a, b, c, d;
	char e;
} p[MAXK];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
#ifndef ONLINE_JUDGE
	freopen("in.txt", "r", stdin);
#endif
	int n, m, k; cin >> n >> m >> k;
	for (int i = 1; i <= n; i++) cin >> (field[i] + 1);
	for (int i = 0; i < k; i++) {
		cin >> p[i].a >> p[i].b >> p[i].c >> p[i].d >> p[i].e;
		int e = (p[i].e -= 'a');
		cnt[p[i].a][p[i].b][e]++; cnt[p[i].a][p[i].d + 1][e]--;
		cnt[p[i].c + 1][p[i].b][e]--; cnt[p[i].c + 1][p[i].d + 1][e]++;
	}
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++) {
			for (int c = 0; c < 26; c++) {
				cnt[i][j][c] += cnt[i][j - 1][c] + cnt[i - 1][j][c] -
					cnt[i - 1][j - 1][c];
			}
		}
	}
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++) {
			int f = 0;
			for (int c = 0; c < 26; c++) {
				f += abs(field[i][j] - 'a' - c) * cnt[i][j][c];
			}
			g[i][j] = f + g[i - 1][j] + g[i][j - 1] - g[i - 1][j - 1];
		}
	}
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++) {
			int cntSum = 0;
			for (int c = 0; c < 26; c++) cntSum += cnt[i][j][c];
			for (int c = 0; c < 26; c++) {
				int c2 = (field[i][j] - 'a' == c) ?
					(k - cntSum + cnt[i][j][c]) : cnt[i][j][c];
				q[i][j][c] = c2 + q[i - 1][j][c] + q[i][j - 1][c] - q[i - 1][j - 1][c];
			}
		}
	}
	LL ans = 1LL << 60;
	for (int x = 0; x < k; x++) {
		int a = p[x].a, b = p[x].b, c = p[x].c, d = p[x].d;
		LL t = g[n][m] - g[c][d] + g[c][b - 1] + g[a - 1][d] - g[a - 1][b - 1];
		for (int ch = 0; ch < 26; ch++) {
			LL cnt = q[c][d][ch] - q[c][b - 1][ch] - q[a - 1][d][ch] +
				q[a - 1][b - 1][ch];
			t += cnt * abs(ch - p[x].e);
		}
		ans = min(ans, t);
	}
	cout << ans << '\n';
}