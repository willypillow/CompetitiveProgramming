#include <bits/stdc++.h>
using namespace std;

multiset<int> st;
int a[300000];

bool hasPrefix(int x, int d) {
	int beg = x, fin = x + (1 << d);
	auto it = st.lower_bound(beg), it2 = st.lower_bound(fin);
	return it != it2;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 0; i < n; i++) cin >> a[i];
	for (int i = 0; i < n; i++) {
		int p; cin >> p;
		st.insert(p);
	}
	for (int i = 0; i < n; i++) {
		int mask = 0;
		for (int j = 29; j >= 0; j--) {
			int bit = a[i] & (1 << j);
			if (hasPrefix(mask ^ bit, j)) mask ^= bit;
			else mask ^= ~bit & (1 << j);
		}
		st.erase(st.find(mask));
		cout << (a[i] ^ mask) << ' ';
	}
}