#include <bits/stdc++.h>
using namespace std;

int poss[100][100];
double possD[100][100], solveArr[100][100];

double calPoss(int x, int d) {
	if (possD[x][d]) return possD[x][d];
	if (!d) return 1.0;
	int start = x >> d << d, end = start | ((1 << d) - 1);
	int m = (start + end) >> 1;
	if (x <= m) start = m + 1;
	else end = m;
	for (int i = start; i <= end; i++) {
		possD[x][d] += poss[x][i] / 100.0 * calPoss(i, d - 1);
	}
	return possD[x][d] *= calPoss(x, d - 1);
}

double solve(int x, int d) {
	if (solveArr[x][d]) return solveArr[x][d];
	if (!d) return 0;
	int start = x >> d << d, end = start | ((1 << d) - 1);
	int m = (start + end) >> 1;
	if (x <= m) start = m + 1;
	else end = m;
	double t = 0;
	for (int i = start; i <= end; i++) {
		t = max(t, solve(i, d - 1));
	}
	return solveArr[x][d] = solve(x, d - 1) + (1 << (d - 1)) * calPoss(x, d) + t;
}


int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	int n2 = 1 << n;
	for (int i = 0; i < n2; i++) {
		for (int j = 0; j < n2; j++) cin >> poss[i][j];
	}
	double ans = 0;
	for (int i = 0; i < n2; i++) {
		ans = max(ans, solve(i, n));
	}
	cout << fixed << setprecision(20) << ans;
}