#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int k, d; cin >> k >> d;
	if (!d) {
		if (k == 1) cout << 0;
		else cout << "No solution";
	} else {
		cout << d;
		k--;
		while (k--) cout << 0;
	}
}