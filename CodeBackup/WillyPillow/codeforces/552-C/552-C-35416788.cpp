#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int64_t w, m; cin >> w >> m;
	vector<int64_t> v;
	while (m) {
		v.push_back(m % w);
		m /= w;
	}
	bool fail = false;
	for (int i = 0; i < (int)v.size(); i++) {
		if (v[i] <= 1) continue;
		if (v[i] == w - 1) {
			int j;
			for (j = i; j < (int)v.size() && v[j] == w - 1; j++) {
				v[j] = 0;
			}
			if (j < (int)v.size()) v[j]++;
		} else {
			fail = true;
			break;
		}
	}
	if (!fail) cout << "YES";
	else cout << "NO";
}