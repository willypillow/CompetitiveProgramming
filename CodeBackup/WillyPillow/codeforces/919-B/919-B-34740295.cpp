#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	vector<int> ans;
	for (int i = 1; i <= 20000000; i++) {
		int x = i, tmp = 0;
		while (x) {
			tmp += x % 10;
			x /= 10;
		}
		if (tmp == 10) ans.push_back(i);
	}
	int k; cin >> k;
	cout << ans[k - 1];
}