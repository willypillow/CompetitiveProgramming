#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E6 + 1;

bool hole[MAXN], has[MAXN];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, m, k; cin >> n >> m >> k;
	int ans = -1;
	for (int i = 0; i < m; i++) {
		int x; cin >> x;
		hole[x] = true;
	}
	has[1] = true;
	if (hole[1]) {
		cout << "1\n";
		return 0;
	}
	for (int i = 0; i < k; i++) {
		int u, v; cin >> u >> v;
		if (has[u] || has[v]) {
			swap(has[u], has[v]);
			if (has[u] && hole[u]) {
				ans = u;
				break;
			} else if (has[v] && hole[v]) {
				ans = v;
				break;
			}
		}
	}
	if (ans == -1) {
		for (int i = 1; i <= n; i++) {
			if (has[i]) {
				ans = i;
				break;
			}
		}
	}
	cout << ans << '\n';
}