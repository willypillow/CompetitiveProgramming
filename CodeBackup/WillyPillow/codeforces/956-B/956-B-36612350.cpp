#include <bits/stdc++.h>
using namespace std;
#define double long double
int es[100010];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	int n, u; cin >> n >> u;
	for (int i = 1; i <= n; i++) cin >> es[i];
	double ans = -1;
	for (int i = 1; i <= n; i++) {
		int j = upper_bound(es + i + 1, es + n + 1, es[i] + u) - es - 1;
		if (i + 1 < j) {
			ans = max(ans, (double)(es[j] - es[i + 1]) / (es[j] - es[i]));
		}
	}
	//for (int k = n, i = n - 2; k >= 3; k--) {
	//	while (i && es[k] - es[i] <= u) i--;
	//	i++;
	//	if (k > i + 1 && es[k] - es[i] <= u) {
	//		ans = max(ans, (double)(es[k] - es[i + 1]) / (es[k] - es[i]));
	//	}
	//}
	//for (int k = n; k >= 1; k--) {
	//	for (int i = k - 2; i >= 1; i--) {
	//		if (es[k] - es[i] > u) break;
	//		ans = max(ans, (double)(es[k] - es[i + 1]) / (es[k] - es[i]));
	//	}
	//}
	cout << fixed << setprecision(12) << ans;
}