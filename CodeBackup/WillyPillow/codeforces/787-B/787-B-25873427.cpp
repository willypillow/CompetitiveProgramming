#include <bits/stdc++.h>
using namespace std;

int vis[10005] = { 0 };

int main() {
	int n, m; cin >> n >> m;
	for (int i = 0, k; i < m; i++) {
		memset(vis, false, sizeof(vis));
		bool ok = false;
		cin >> k;
		while (k--) {
			int v; cin >> v;
			if (vis[abs(v)] == -v) ok = true;
			vis[abs(v)] = v;
		}
		if (!ok) {
			cout << "YES\n";
			return 0;
		}
	}
	cout << "NO\n";
}