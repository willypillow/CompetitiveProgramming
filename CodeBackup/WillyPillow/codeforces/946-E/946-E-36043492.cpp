#include <bits/stdc++.h>
using namespace std;

int cnt[10], oddCnt;

bool check10Pref(string s) {
	for (int i = 0; i < (int)s.size() - 1; i++) {
		if (s[i] != (i ? '0' : '1')) return false;
	}
	return true;
}

void rem(int c) { oddCnt += ((--cnt[c] & 1) ? 1 : -1); }
void add(int c) { oddCnt += ((++cnt[c] & 1) ? 1 : -1); }

string gen(string s, int suf) {
	vector<int> appen;
	for (int i = 0; i < 10; i++) {
		if (cnt[i] & 1) appen.push_back(i);
	}
	while ((int)appen.size() < suf) appen.push_back(9);
	sort(appen.begin(), appen.end());
	for (int i = (int)s.size() - suf, j = suf; i < (int)s.size(); i++) {
		s[i] = char(appen[--j] + '0');
	}
	return s;
}

string solve(string s) {
	if (check10Pref(s) && s.back() <= '1') return string(s.size() - 2, '9');
	memset(cnt, 0, sizeof(cnt));
	oddCnt = 0;
	for (int i = 0; i < (int)s.size(); i++) cnt[s[i] - '0']++;
	for (int i = 0; i < 10; i++) oddCnt += cnt[i] & 1;
	for (int suf = 0, pos = (int)s.size() - 1; pos >= 0; suf++, pos--) {
		for (int j = s[pos] - '0'; j; j--) {
			rem(j), add(j - 1);
			s[pos] = char('0' + (j - 1));
			if (oddCnt <= suf) return gen(s, suf);
		}
		rem(0);
	}
	return "";
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		string s; cin >> s;
		cout << solve(s) << '\n';
	}
}