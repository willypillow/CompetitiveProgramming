#include <bits/stdc++.h>
using namespace std;

int dp[100010][400], n, a[100010], bk;

int dfs(int p, int k) {
	if (p > n) return 0;
	if (k <= bk && dp[p][k]) return dp[p][k];
	if (k <= bk) return dp[p][k] = dfs(p + a[p] + k, k) + 1;
	return dfs(p + a[p] + k, k) + 1;
}

int main() {
	cin >> n;
	for (int i = 1; i <= n; i++) cin >> a[i];
	bk = round(sqrt(n));
	int q; cin >> q;
	while (q--) {
		int p, k; cin >> p >> k;
		cout << dfs(p, k) << '\n';
	}
}