#include <bits/stdc++.h>
using namespace std;

bool notPrime[101];
vector<int> primes;
string res;

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	for (int i = 2; i <= 50; i++) {
		if (!notPrime[i]) primes.push_back(i);
		for (int p: primes) {
			if (i * p > 50) break;
			notPrime[i * p] = true;
			if (i % p == 0) break;
		}
	}
	int ans = 0;
	for (int p: primes) {
		cout << p << endl;
		cin >> res;
		ans += res[0] == 'y';
		if (p <= 10 && res[0] == 'y') {
			cout << p * p << endl;
			cin >> res;
			ans += res[0] == 'y';
		}
		if (ans > 1) break;
	}
	if (ans > 1) cout << "composite";
	else cout << "prime";
}