#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	vector<int> ans;
	for (int i = 1; i <= n; i++) {
		bool fail = false;
		for (int j = 1; j <= n; j++) {
			int x; cin >> x;
			if (x == 1 || x == 3) fail = true;
		}
		if (!fail) ans.push_back(i);
	}
	cout << ans.size() << '\n';
	for (int x: ans) cout << x << ' ';
}