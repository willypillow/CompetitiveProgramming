#include <bits/stdc++.h>
using namespace std;

int d[2][60], delta[2][60];

int main() {
	int n, l; cin >> n >> l;
	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < n; j++) cin >> d[i][j];
		for (int j = 1; j < n; j++) delta[i][j] = d[i][j] - d[i][j - 1];
		delta[i][0] = d[i][0] - d[i][n - 1] + l;
	}
	for (int i = 0; i < n; i++) {
		bool ok = true;
		for (int j = 0; j < n; j++) {
			if (delta[0][j] != delta[1][(i + j) % n]) {
				ok = false;
				break;
			}
		}
		if (ok) {
			cout << "YES\n";
			return 0;
		}
	}
	cout << "NO\n";
}