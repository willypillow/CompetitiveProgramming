#include <bits/stdc++.h>
using namespace std;

const int64_t INF = 0x3f3f3f3f3f3f3f3f;

int64_t dp[1010][2010], a[1010], b[2010];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n, k, p; cin >> n >> k >> p;
	for (int i = 1; i <= n; i++) cin >> a[i];
	sort(a + 1, a + n + 1);
	for (int i = 1; i <= k; i++) cin >> b[i];
	sort(b + 1, b + k + 1);
	memset(dp, INF, sizeof(dp));
	for (int i = 0; i <= k; i++) dp[n][i] = 0;
	for (int i = n; i > 0; i--) {
		int64_t mn = INF;
		for (int j = k; j > 0; j--) {
			mn = min(mn, max(dp[i][j], abs(a[i] - b[j]) + abs(b[j] - p)));
			dp[i - 1][j - 1] = mn;
		}
	}
	cout << dp[0][0] << '\n';
}