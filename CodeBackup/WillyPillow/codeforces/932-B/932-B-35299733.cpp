#include <bits/stdc++.h>
using namespace std;

int g[1000001];
unordered_map<int, vector<int> > ans;

int f(int x) {
	int ret = 1;
	while (x) {
		if (x % 10) ret *= x % 10;
		x /= 10;
	}
	return ret;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	for (int i = 1; i <= 1000000; i++) {
		g[i] = (i < 10) ? i : g[f(i)];
	}
	for (int i = 1; i <= 1000000; i++) ans[g[i]].push_back(i);
	int q; cin >> q;
	while (q--) {
		int l, r, k; cin >> l >> r >> k;
		auto &v = ans[k];
		cout << upper_bound(v.begin(), v.end(), r) - lower_bound(v.begin(), v.end(), l) << '\n';
	}
}