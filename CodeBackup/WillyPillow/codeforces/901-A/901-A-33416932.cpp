#include <bits/stdc++.h>
using namespace std;

int a[100010];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int h; cin >> h;
	int cnt = 0;
	for (int i = 0; i <= h; i++) {
		cin >> a[i];
		if (i && a[i] > 1 && a[i - 1] > 1) cnt++;
	}
	if (cnt == 0) {
		cout << "perfect\n";
	} else {
		cout << "ambiguous\n";
		int x = 1, lastMn = 0;
		for (int i = 0; i <= h; i++) {
			int curMn = 1E9;
			for (int j = 0; j < a[i]; j++) {
				curMn = min(curMn, x++);
				cout << lastMn << ' ';
			}
			lastMn = curMn;
		}
		cout << '\n';
		x = 2, lastMn = 1;
		cout << "0 ";
		for (int i = 1; i <= h; i++) {
			int curMn = 1E9;
			for (int j = 0; j < a[i]; j++) {
				curMn = min(curMn, x++);
				cout << lastMn + (j % a[i - 1]) << ' ';
			}
			lastMn = curMn;
		}
		cout << '\n';
	}
}