#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int p, y; cin >> p >> y;
	for (int i = y; i > max(p, y - 300); i--) {
		int x = i;
		bool ok = true;
		for (int j = 2; j <= p && j * j <= x; j++) {
			if (x % j == 0) {
				ok = false;
				break;
			}
		}
		if (ok) {
			cout << i;
			return 0;
		}
	}
	cout << -1;
}