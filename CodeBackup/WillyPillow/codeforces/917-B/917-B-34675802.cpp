#include <bits/stdc++.h>
using namespace std;

char dp[101][101][30];
vector<pair<int, int> > g[101];

int dfs(int x, int y, int l) {
	char &ans = dp[x][y][l];
	if (ans) return ans;
	ans = 'L';
	for (auto &p: g[x]) {
		if (p.second < l) continue;
		if (dfs(y, p.first, p.second) == 'L') ans = 'W';
	}
	return ans;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	for (int i = 0; i < m; i++) {
		int u, v; cin >> u >> v;
		char c; cin >> c;
		g[u].push_back({ v, c - 'a' + 1 });
	}
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= n; j++) cout << (dfs(i, j, 0) == 'W' ? 'A' : 'B');
		cout << '\n';
	}
}