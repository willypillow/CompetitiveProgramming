#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	priority_queue<pair<int, int>, vector<pair<int, int> >, greater<pair<int, int> > > pq;
	priority_queue<pair<int, int> > pq2;
	for (int i = 1; i <= n; i++) {
		int x; cin >> x;
		pq.push({x, i});
	}
	string s; cin >> s;
	for (int i = 0; i < n * 2; i++) {
		if (s[i] == '0') {
			auto p = pq.top(); pq.pop();
			cout << p.second << ' ';
			pq2.push(p);
		} else {
			auto p = pq2.top(); pq2.pop();
			cout << p.second << ' ';
		}
	}
}