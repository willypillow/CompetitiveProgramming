#include <bits/stdc++.h>
using namespace std;

int a[2010], aSum[2010], b[2010];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int k, n; cin >> k >> n;
	for (int i = 0; i < k; i++) cin >> a[i];
	aSum[0] = a[0];
	for (int i = 1; i < k; i++) aSum[i] = aSum[i - 1] + a[i];
	sort(aSum, aSum + k);
	k = unique(aSum, aSum + k) - aSum;
	for (int i = 0; i < n; i++) cin >> b[i];
	sort(b, b + n);
	int ans = 0;
	for (int i = 0; i <= k - n; i++) {
		int d = aSum[i] - b[0], j, kk;
		for (j = i, kk = 0; j < k && kk < n; j++, kk++) {
			while (j < k && aSum[j] - b[kk] != d) j++;
			if (j >= k) break;
		}
		if (kk == n) ans++;
	}
	cout << ans << '\n';
}