#include <bits/stdc++.h>
using namespace std;

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	int ans = 1E9;
	for (int i = 0; i < m; i++) {
		int l, r; cin >> l >> r;
		ans = min(ans, r - l + 1);
	}
	cout << ans << '\n';
	for (int i = 0; i < n; i++) {
		cout << i % ans << ' ';
	}
}