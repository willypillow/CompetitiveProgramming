#include <bits/stdc++.h>
using namespace std;

const long double EPS = 1E-7;

struct Device { int a, b; } device[100000];
int n, p;

bool solve(long double t) {
	long double need = 0;
	for (int i = 0; i < n; i++) {
		if (device[i].b >= device[i].a * t + EPS) continue;
		need += device[i].a * t - device[i].b;
	}
	return need / p + EPS <= t;
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	cin >> n >> p;
	for (int i = 0; i < n; i++) {
		cin >> device[i].a >> device[i].b;
	}
	long double l = 0, r = 1E12;
	if (solve(r)) {
		cout << "-1\n";
		return 0;
	}
	int i = 0;
	while (l + EPS < r && i < 300) {
		long double m = (l + r) / 2;
		if (solve(m)) l = m;
		else r = m;
		i++;
	}
	if (r + EPS >= 1E12) {
		cout << "-1\n";
	} else {
		cout << (l + r) / 2 << '\n';
	}
}