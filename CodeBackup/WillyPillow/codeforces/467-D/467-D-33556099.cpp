#include <bits/stdc++.h>
using namespace std;

unordered_map<string, int> mp;
pair<int, int> cnt[300010], minR[300010], dp[300010];
vector<int> g[300010], g2[300010], fin, colorV[300010];
bool vis[300010], vis2[300010];
int color[300010], arr[300010];

int toId(string s) {
	int tmp = 0;
	for (int j = 0; j < (int)s.size(); j++) {
		if (isupper(s[j])) s[j] = (char)tolower(s[j]);
		if (s[j] == 'r') tmp++;
	}
	if (mp.count(s)) return mp[s];
	int i = (int)mp.size();
	mp[s] = i;
	cnt[i] = { tmp, (int)s.size() };
	return i;
}

void dfsScc(int x) {
	vis[x] = true;
	for (int y: g[x]) {
		if (!vis[y]) dfsScc(y);
	}
	fin.push_back(x);
}

void dfsScc2(int x, int colo) {
	color[x] = colo;
	colorV[colo].push_back(x);
	minR[colo] = min(minR[colo], cnt[x]);
	for (int y: g2[x]) {
		if (!color[y]) dfsScc2(y, colo);
	}
}

pair<int, int> dfs(int x) {
	if (vis2[x]) return dp[x];
	vis2[x] = true;
	dp[x] = minR[x];
	for (int vx: colorV[x]) {
		for (int y: g[vx]) dp[x] = min(dp[x], dfs(color[y]));
	}
	return dp[x];
}

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	cin.tie(0); ios_base::sync_with_stdio(0);
	memset(minR, 0x3f, sizeof(minR));
	int m; cin >> m;
	for (int i = 0; i < m; i++) {
		string str; cin >> str;
		arr[i] = toId(str);
	}
	int n; cin >> n;
	for (int i = 0; i < n; i++) {
		string a, b; cin >> a >> b;
		int na = toId(a), nb = toId(b);
		g[na].push_back(nb);
		g2[nb].push_back(na);
	}
	for (int i = 0; i < (int)mp.size(); i++) {
		if (!vis[i]) dfsScc(i);
	}
	int colo = 0;
	for (int i = (int)fin.size() - 1; i >= 0; i--) {
		if (!color[fin[i]]) dfsScc2(fin[i], ++colo);
	}
	pair<int, int64_t> ans = { 0, 0 };
	for (int i = 0; i < m; i++) {
		auto p = dfs(color[arr[i]]);
		ans.first += p.first, ans.second += p.second;
	}
	cout << ans.first << ' ' << ans.second;
}