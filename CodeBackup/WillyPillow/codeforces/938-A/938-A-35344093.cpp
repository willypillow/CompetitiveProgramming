#include <bits/stdc++.h>
using namespace std;

bool del[10000];

bool isV(char c) {
	return (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y');
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	string s; cin >> s;
	for (int i = 1; i < (int)s.size(); i++) {
		int j;
		for (j = i - 1; j >= 0; j--) {
			if (!del[j]) break;
		}
		if (j >= 0 && isV(s[j]) && isV(s[i])) del[i] = true;
	}
	for (int i = 0; i < (int)s.size(); i++) {
		if (!del[i]) cout << s[i];
	}
}