#include <bits/stdc++.h>
using namespace std;

struct Node {
	static Node pool[2000010], *ptr;
	uint32_t val, pri, tag, size;
	Node *lc, *rc;
	Node() {}
	Node(int v): val(v), pri(rand()), tag(0), size(1), lc(NULL), rc(NULL) {}
	void pull() {
		size = 1 + (lc ? lc->size : 0) + (rc ? rc->size : 0);
	}
	void push() {
		if (!tag) return;
		val += tag;
		if (lc) lc->tag += tag;
		if (rc) rc->tag += tag;
		tag = 0;
	}
} *root = NULL, Node::pool[2000010], *Node::ptr = Node::pool;

int getSize(Node *n) {
	return n ? n->size : 0;
}

Node *merge(Node *a, Node *b) {
	if (!a || !b) return a ? a : b;
	if (a->pri < b->pri) {
		a->push();
		a->rc = merge(a->rc, b);
		a->pull();
		return a;
	} else {
		b->push();
		b->lc = merge(a, b->lc);
		b->pull();
		return b;
	}
}

void split(Node *n, int k, Node *&a, Node *&b) {
	if (!n) {
		a = b = NULL;
	} else if (n->val + n->tag <= k) {
		a = n;
		a->push();
		split(n->rc, k, a->rc, b);
		a->pull();
	} else {
		b = n;
		b->push();
		split(n->lc, k, a, b->lc);
		b->pull();
	}
}

void splitBySize(Node *n, int k, Node *&a, Node *&b) {
	if (!n) {
		a = b = NULL;
	} else if (getSize(n->lc) + 1 <= k) {
		a = n;
		a->push();
		splitBySize(n->rc, k - getSize(n->lc) - 1, a->rc, b);
		a->pull();
	} else {
		b = n;
		b->push();
		splitBySize(n->lc, k, a, b->lc);
		b->pull();
	}
}

int vect[2000010];
struct P {
	int l, r;
	bool operator <(const P &rhs) const {
		return r < rhs.r;
	}
} p[100010], q[100010];

int main() {
	srand(time(0));
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	int n, maxC = 0, maxR = 0; cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> p[i].l >> p[i].r;
		vect[p[i].l << 1]++; vect[p[i].r << 1 | 1]--;
		maxR = max(maxR, p[i].r << 1 | 1);
	}
	for (int i = 1; i <= maxR; i++) {
		vect[i] += vect[i - 1];
		maxC = max(maxC, vect[i]);
	}
	int m; cin >> m;
	for (int i = 0; i < m; i++) {
		cin >> q[i].l >> q[i].r;
		maxR = max(maxR, q[i].r * 2);
	}
	sort(q, q + m);
	for (int i = 2, j = 0; i <= maxR; i += 2) {
		Node *a, *b;
		split(root, vect[i], a, b);
		root = merge(merge(a, new(Node::ptr++) Node(vect[i])), b);
		while (q[j].r * 2 == i) {
			Node *a, *b, *c, *t1, *t2;
			splitBySize(root, q[j].l, a, b);
			splitBySize(b, 1, t1, t2);
			int thres = (t1 ? t1->val + t1->tag : INT_MAX);
			b = merge(t1, t2);
			split(b, thres, t1, t2);
			a->tag += 1;
			split(a, thres, a, c);
			root = merge(merge(a, t1), merge(c, t2));
			j++;
		}
	}
	if (root) {
		Node *a, *b;
		splitBySize(root, root->size - 1, a, b);
		cout << b->val + b->tag << '\n';
	}
}
