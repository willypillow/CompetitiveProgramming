#include <bits/stdc++.h>
using namespace std;

typedef bitset<81> BS;
int ans = 0, n, p[81];
bool g[81][81], ins[81], vis[81];
vector<int> edges[81];
BS neighbor[81];

bool match(int x) {
	if (ins[x]) return false;
	ins[x] = true;
	for (int y: edges[x]) {
		if (!vis[y] || (p[y] != x && match(p[y]))) {
			p[y] = x;
			vis[y] = true;
			ins[x] = false;
			return true;
		}
	}
	ins[x] = false;
	return false;
}

int hung() {
	int ans = 0;
	for (int i = 0; i < n / 2; i++) {
		if (match(i)) ans++;
	}
	return ans;
}

void dfs(BS r, BS p, BS x) {
	if ((double)clock() / CLOCKS_PER_SEC >= 1) {
		cout << ans << '\n';
		exit(0);
	}
	if (p.none() && x.none()) {
		ans = max(ans, (int)r.count());
	} else {
		if ((int)p.count() + r.count() < ans) return;
		int pivot = 0;
		for (; !p[pivot] && !x[pivot] && pivot < n; pivot++);
		for (int i = 0; i < n; i++) {
			if (!neighbor[pivot][i] && p[i]) {
				r[i] = true;
				dfs(r, p & neighbor[i], x & neighbor[i]);
				r[i] = false;
				p[i] = false;
				x[i] = true;
			}
		}
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	clock();
	int m; cin >> n >> m;
	bool notBi = false;
	for (int i = 0; i < m; i++) {
		int u, v; cin >> u >> v;
		g[u][v] = g[v][u] = true;
		edges[u].push_back(v); edges[v].push_back(u);
		if (min(u, v) >= n / 2 || max(u, v) < n / 2) notBi = true;
	}
	if (notBi) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i != j && !g[i][j]) neighbor[i][j] = true;
			}
		}
		BS r, p, x;
		for (int i = 0; i < n; i++) p[i] = true;
		dfs(r, p, x);
		cout << ans << '\n';
	} else {
		cout << n - hung() << '\n';
	}
}
