#include <bits/stdc++.h>
using namespace std;

struct P { int l, r, w; } p[100000];
int scanAdd[1000000], scanSub[1000000];
vector<int> pos;

int main() {
	int n; cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> p[i].l >> p[i].r >> p[i].w;
		pos.push_back(p[i].l); pos.push_back(p[i].r);
	}
	sort(pos.begin(), pos.end());
	pos.resize(unique(pos.begin(), pos.end()) - pos.begin());
	for (int i = 1, top = pos.size(); i < top; i++) {
		if (pos[i - 1] + 1 != pos[i]) pos.push_back(pos[i - 1] + 1);
	}
	sort(pos.begin(), pos.end());
	pos.resize(unique(pos.begin(), pos.end()) - pos.begin());
	for (int i = 0; i < n; i++) {
		p[i].l = lower_bound(pos.begin(), pos.end(), p[i].l) - pos.begin();
		p[i].r = lower_bound(pos.begin(), pos.end(), p[i].r) - pos.begin();
		scanAdd[p[i].l] += p[i].w;
		scanSub[p[i].r] += p[i].w;
	}
	int cur = 0, ans = 0, mx = 0;
	for (int i = 0; i < (int)pos.size(); i++) {
		cur += scanAdd[i], mx += scanAdd[i];
		mx = max(mx, cur);
		ans = max(ans, mx);
		cur -= scanSub[i];
	}
	cout << ans << '\n';
}
