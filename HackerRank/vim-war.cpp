#include <bits/stdc++.h>
using namespace std;

const int MOD = 1E9 + 7;

int dp[1 << 20], pow2[1 << 20 | 1], n, m;
char buf[24];

int read(int ret = 0) {
	cin.getline(buf, sizeof(buf));
	for (int i = 0; i < m; i++) ret = ret << 1 | (buf[i] - '0');
	return ret;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	cin >> n >> m >> std::ws;
	pow2[0] = 1;
	for (int i = 1; i <= (1 << m); i++) pow2[i] = (pow2[i - 1] << 1) % MOD;
	for (int i = 0; i <= (1 << m); i++) pow2[i] = (pow2[i] - 1 + MOD) % MOD;
	for (int i = 0; i < n; i++) dp[read()]++;
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < (1 << m); j++) {
			if (j & (1 << i)) dp[j] += dp[j ^ (1 << i)];
		}
	}
	int ans = 0, need = read();
	for (int i = need; ; i = (i - 1) & need) {
		if (__builtin_popcount(need ^ i) & 1) {
			ans = (ans - pow2[dp[i]] + MOD) % MOD;
		} else {
			ans = (ans + pow2[dp[i]]) % MOD;
		}
		if (!i) break;
	}
	cout << ans;
}
