#include <bits/stdc++.h>
using namespace std;

const int POOLSZ = 2E5, MAXN = 1005;

pair<int, int> toEdge[POOLSZ];

struct SplayNode {
	static SplayNode HOLE, pool[POOLSZ], *ptr;
	SplayNode *ch[2], *par;
	bool rev;
	pair<int, SplayNode*> mx;
	int val;
	SplayNode(int _v = 0): par(&HOLE), rev(false), mx({ 0, &HOLE }), val(_v) {
		ch[0] = ch[1] = &HOLE;
	}
	bool isRoot() {
		return (par->ch[0] != this && par->ch[1] != this);
	}
	void push() {
		if (rev) {
			ch[0]->rev ^= 1, ch[1]->rev ^= 1;
			swap(ch[0], ch[1]);
			rev ^= 1;
		}
	}
	void pushFromRoot() {
		if (!isRoot()) par->pushFromRoot();
		push();
	}
	void pull() {
		mx = { 0, NULL };
		mx = max({ mx, ch[0]->mx, { ch[0]->val, ch[0] } });
		mx = max({ mx, ch[1]->mx, { ch[1]->val, ch[1] } });
	}
	void rotate() {
		SplayNode *p = par, *gp = p->par;
		bool dir = (p->ch[1] == this);
		par = gp;
		if (!p->isRoot()) gp->ch[gp->ch[1] == p] = this;
		p->ch[dir] = ch[dir ^ 1];
		p->ch[dir]->par = p;
		p->par = this;
		ch[dir ^ 1] = p;
		p->pull(), pull();
	}
	void splay() {
		pushFromRoot();
		while (!isRoot()) {
			if (!par->isRoot()) {
				SplayNode *gp = par->par;
				if ((gp->ch[0] == par) == (par->ch[0] == this)) rotate();
				else par->rotate();
			}
			rotate();
		}
	}
} SplayNode::HOLE(0), SplayNode::pool[POOLSZ], *SplayNode::ptr, *root[MAXN];

namespace LCT {
	SplayNode *access(SplayNode *x) {
		SplayNode *last = &SplayNode::HOLE;
		while (x != &SplayNode::HOLE) {
			x->splay();
			x->ch[1] = last;
			x->pull();
			last = x;
			x = x->par;
		}
		return last;
	}
	void makeRoot(SplayNode *x) {
		access(x);
		x->splay();
		x->rev ^= 1;
	}
	void link(SplayNode *x, SplayNode *y) {
		makeRoot(x);
		x->par = y;
	}
	void cut(SplayNode *x, SplayNode *y) {
		makeRoot(x);
		access(y);
		y->splay();
		y->ch[0] = &SplayNode::HOLE;
		x->par = &SplayNode::HOLE;
	}
	SplayNode *findRoot(SplayNode *x) {
		x = access(x);
		while (x->ch[0] != &SplayNode::HOLE) x = x->ch[0];
		x->splay();
		return x;
	}
	SplayNode *query(SplayNode *x, SplayNode *y) {
		makeRoot(x);
		return access(y);
	}
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		SplayNode::ptr = SplayNode::pool;
		int n, m; cin >> n >> m;
		int cost = 0, cc = n;
		for (int i = 1; i <= n; i++) root[i] = new(SplayNode::ptr++) SplayNode(0);
		while (m--) {
			int x, y, d; cin >> x >> y >> d;
			auto px = LCT::findRoot(root[x]), py = LCT::findRoot(root[y]);
			if (px != py) {
				auto p = new(SplayNode::ptr++) SplayNode(d);
				toEdge[p - SplayNode::pool] = { x, y };
				LCT::link(root[x], p), LCT::link(p, root[y]);
				cost += d, cc--;
			} else {
				auto p = LCT::query(root[x], root[y]);
				if (p->mx.first > d) {
					cost -= p->mx.first - d;
					auto rt = p->mx.second;
					auto pr = toEdge[rt - SplayNode::pool];
					LCT::cut(rt, root[pr.first]), LCT::cut(rt, root[pr.second]);
					auto pNew = new(SplayNode::ptr++) SplayNode(d);
					toEdge[pNew - SplayNode::pool] = { x, y };
					LCT::link(root[x], pNew), LCT::link(pNew, root[y]);
				}
			}
			if (cc > 1) cout << "\\\\0w0//\n";
			else cout << cost << '\n';
		}
	}
}
