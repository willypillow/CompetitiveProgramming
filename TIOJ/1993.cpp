#include <bits/stdc++.h>
using namespace std;

int x[1000], y[1000], z[1000];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		int n, h; cin >> n >> h;
		for (int i = 0; i < n; i++) cin >> x[i] >> y[i] >> z[i];
		bitset<100001> d;
		d[0] = 1;
		for (int i = 0; i < n; i++) {
			d = (d << x[i]) | (d << y[i]) | (d << z[i]);
		}
		int i;
		for (i = h; i >= 0; i--) {
			if (d[i]) {
				cout << i << '\n';
				break;
			}
		}
		if (i < 0) cout << "no solution\n";
	}
}
