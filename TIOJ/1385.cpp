#include <bits/stdc++.h>
using namespace std;

int dp[1001][1001];
char s1[1002], s2[1002];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	cin.getline(s1 + 1, 1001); cin.getline(s2 + 1, 1001);
	int n1 = strlen(s1 + 1), n2 = strlen(s2 + 1);
	for (int i = 1; i <= n1; i++) dp[i][0] = i;
	for (int j = 1; j <= n2; j++) dp[0][j] = j;
	for (int i = 1; i <= n1; i++) {
		for (int j = 1; j <= n2; j++) {
			if (s1[i] == s2[j]) {
				dp[i][j] = dp[i - 1][j - 1];
			} else {
				dp[i][j] = 1 + min({
					dp[i - 1][j - 1], // Modify / Same
					dp[i][j - 1], // Insert
					dp[i - 1][j] // Remove
					});
			}
		}
	}
	cout << dp[n1][n2] << '\n';
}
