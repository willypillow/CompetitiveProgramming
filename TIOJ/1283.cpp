#include <bits/stdc++.h>
using namespace std;

typedef long long LL;
const int MAXN = 1E5 + 10;

struct P { long long x, y; } up[MAXN], down[MAXN];
struct Node { int l, r, h; };

LL area(P &a, P &b) {
	if (a.x >= b.x || a.y <= b.y) return 0;
	return (LL)(b.x - a.x) * (a.y - b.y);
}

int main() {
	int n; cin >> n;
	n /= 2;
	P last = { 0, 0 };
	for (int i = 0; i < n; i++) {
		int l1, l2; cin >> l1 >> l2;
		up[i] = P { last.x, last.y + l1 };
		last = P { last.x + l2, last.y + l1 };
	}
	int m; cin >> m;
	m /= 2;
	last = { 0, 0 };
	for (int i = 0; i < m; i++) {
		int l1, l2; cin >> l1 >> l2;
		down[i] = P { last.x + l1, last.y };
		last = P { last.x + l1, last.y + l2 };
	}
	deque<Node> q;
	q.push_back({ 0, m - 1, 0 });
	LL ans = 0;
	int j = 1;
	for (int i = 0; i < m; i++) {
		if (q.front().r < i) q.pop_front();
		q.front().l = max(q.front().l, i);
		while (j < n && area(up[j], down[i]) > 0) {
			while (!q.empty() && area(up[q.back().h], down[q.back().l]) < area(up[j], down[q.back().l])) {
				q.pop_back();
			}
			if (q.empty()) {
				q.push_back({ i, m - 1, j });
			} else {
				int l = q.back().l, r = q.back().r + 1;
				while (l + 1 < r) {
					int mid = (l + r) >> 1;
					if (area(up[q.back().h], down[mid]) > area(up[j], down[mid])) l = mid;
					else r = mid;
				}
				q.back().r = l;
				if (r < m) q.push_back({ r, m - 1, j });
			}
			j++;
		}
		ans = max(ans, area(up[q.front().h], down[i]));
	}
	cout << ans << '\n';
}
