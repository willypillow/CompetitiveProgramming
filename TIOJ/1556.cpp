#include <bits/stdc++.h>
using namespace std;

int main() {
	long long n, ans = 0;
	cin >> n;
	long long last = n + 1, lastI = 0;
	for (long long i = 1; i < n; i = n / (n / (i + 1))) {
		long long x = n / i;
		ans += lastI * (last - x);
		last = x, lastI = i;
	}
	ans += lastI * (last - 1);
	cout << ans << '\n';
}
