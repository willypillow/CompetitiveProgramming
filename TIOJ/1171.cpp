#include <bits/stdc++.h>
using namespace std;

typedef pair<int, int> PII;
typedef long long LL;
const int MAXN = 1E5 + 5, INF = 1E9;

vector<PII> g[MAXN];
LL decmpPar[MAXN], decmpDep[MAXN], cnt[MAXN], size[MAXN], n;
LL toSonDist[MAXN], fromSonDist[MAXN], decmpDist[30][MAXN];
bool vis[MAXN];
PII st[MAXN];
LL max(LL x, LL y) { return (x > y) ? x : y; }

PII centroid(int x) {
	int top = 0;
	st[top++] = { x, -1 };
	for (int i = 0; i < top; i++) {
		auto p = st[i];
		for (auto it: g[p.first]) {
			if (it.first != p.second && decmpDep[it.first] == -1) {
				st[top++] = { it.first, p.first };
			}
		}
	}
	PII cent = { INF, 0 };
	for (int i = top - 1; i >= 0; i--) {
		auto p = st[i];
		int mxSize = 0;
		size[p.first] = 1;
		for (auto it: g[p.first]) {
			if (it.first != p.second && decmpDep[it.first] == -1) {
				mxSize = max(mxSize, size[it.first]);
				size[p.first] += size[it.first];
			}
		}
		mxSize = max(mxSize, top - size[p.first]);
		cent = min(cent, { mxSize, p.first });
	}
	return cent;
}


void dfsFill(int x, int p, LL d, int centD) {
	decmpDist[centD][x] = d;
	for (auto it: g[x]) {
		if (it.first != p && decmpDep[it.first] == -1) {
			dfsFill(it.first, x, d + it.second, centD);
		}
	}
}

void centroidDecomp(int x, int p, int d) {
	auto cent = centroid(x);
	x = cent.second;
	decmpPar[x] = p; decmpDep[x] = d;
	dfsFill(x, p, 0, d);
	for (auto it: g[x]) {
		if (it.first != p && decmpDep[it.first] == -1) {
			centroidDecomp(it.first, x, d + 1);
		}
	}
}

void modify(int x, int cent, int child, int d) {
	if (cent == -1) return;
	cnt[cent]++;
	if (child != -1) {
		toSonDist[cent] += decmpDist[d][x];
		fromSonDist[child] += decmpDist[d][x];
	}
	modify(x, decmpPar[cent], cent, d - 1);
}

LL query(int x, int cent, int child, int d) {
	if (cent == -1) return 0;
	LL t = (child != -1) ? ((cnt[cent] - cnt[child]) * decmpDist[d][x] +
		toSonDist[cent] - fromSonDist[child]) :
		toSonDist[cent];
	return t + query(x, decmpPar[cent], cent, d - 1);
}

int32_t main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int q; cin >> n >> q;
	for (int i = 1; i < n; i++) {
		int a, b, l; cin >> a >> b >> l;
		g[a].push_back({ b, l }); g[b].push_back({ a, l });
	}
	memset(decmpDep, -1, sizeof(decmpDep));
	centroidDecomp(0, -1, 0);
	while (q--) {
		int t, x; cin >> t >> x;
		if (t == 1) {
			if (!vis[x]) modify(x, x, -1, decmpDep[x]);
			vis[x] = true;
		} else {
			cout << query(x, x, -1, decmpDep[x]) << '\n';
		}
	}
}
