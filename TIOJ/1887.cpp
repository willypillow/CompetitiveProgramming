#include <iostream>
using std::cin; using std::cout;

const long long MOD = 1000000007, MAXN = 500010;
long long x[MAXN], y[MAXN];

inline long long modFunc(long long x, bool *tag) {
	if (x >= MOD) *tag = true;
	return x % MOD;
}

struct Seg {
	static Seg pool[MAXN * 2], *ptr;
	int pos;
	long long lProd, rProd;
	bool lMod, rMod;
	Seg *lNode = NULL, *rNode = NULL;
	void pull() {
		lMod = rMod = false;
		bool mMod = lNode->rMod | rNode->lMod;
		long long mProd = modFunc(lNode->rProd * rNode->lProd, &mMod);
		if (mMod || mProd * y[rNode->pos] > y[lNode->pos]) {
			pos = rNode->pos;
			lProd = modFunc(lNode->lProd * mProd, &lMod);
			lMod |= lNode->lMod | mMod;
			rProd = rNode->rProd;
			rMod = rNode->rMod;
		} else {
			pos = lNode->pos;
			lProd = lNode->lProd;
			lMod = lNode->lMod;
			rProd = modFunc(mProd * rNode->rProd, &rMod);
			rMod |= mMod | rNode->rMod;
		}
	}
	Seg () {}
	Seg (int l, int r) {
		if (l == r) {
			lProd = x[l];
			rProd = 1;
			lMod = rMod = false;
			pos = l;
		} else {
			int m = (l + r) >> 1;
			lNode = new(ptr++) Seg(l, m);
			rNode = new(ptr++) Seg(m + 1, r);
			pull();
		}
	}
	void update(int l, int r, int p) {
		if (p < l || r < p) return;
		if (l == r) {
			lProd = x[p];
		} else {
			int m = (l + r) >> 1;
			lNode->update(l, m, p);
			rNode->update(m + 1, r, p);
			pull();
		}
	}
	long long ans() {
		return lProd * y[pos] % MOD;
	}
} Seg::pool[MAXN * 2], *Seg::ptr;


int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		Seg::ptr = Seg::pool;
		int n; cin >> n;
		for (int i = 0; i < n; i++) cin >> x[i];
		for (int i = 0; i < n; i++) cin >> y[i];
		Seg root = Seg(0, n - 1);
		cout << root.ans() << '\n';
		int m; cin >> m;
		while (m--) {
			int k, p, v; cin >> k >> p >> v;
			if (k == 1) x[p] = v;
			else y[p] = v;
			root.update(0, n - 1, p);
			cout << root.ans() << '\n';
		}
	}
}
