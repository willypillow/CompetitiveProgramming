#include <bits/stdc++.h>
using namespace std;

pair<double, double> p[100000];

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	for (int n; cin >> n; ) {
		for (int i = 0; i < n; i++) cin >> get<0>(p[i]) >> get<1>(p[i]);
		sort(p, p + n);
		double mn = INFINITY;
		for (int i = 0; i < n; i++) {
			for (int j = max(0, i - 500); j < min(n, i + 500); j++) {
				if (i == j) continue;
				double d = norm(complex<double>(get<0>(p[i]) - get<0>(p[j]),
						get<1>(p[i]) - get<1>(p[j])));
				mn = min(mn, d);
			}
		}
		cout << fixed << setprecision(6) << sqrt(mn) << '\n';
	}
}
