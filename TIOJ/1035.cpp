#include <iostream>
#include <cstring>
using std::cin; using std::cout;

char str1[50010], str2[50010];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	cin.getline(str1, 50010); cin.getline(str2, 50010);
	int l1 = strlen(str1), l2 = strlen(str2), ans = 0;
	for (int i = 0; i < l2; i++) {
		int cnt = 0;
		for (int j = 0; j + i < l2; j++) {
			if (str1[j] == str2[j + i]) cnt++;
		}
		ans = std::max(ans, cnt);
		cnt = 0;
		for (int j = 0; j + i < l1; j++) {
			if (str1[j + i] == str2[j]) cnt++;
		}
		ans = std::max(ans, cnt);
	}
	cout << ans << '\n';
}
