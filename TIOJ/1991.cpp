#include <bits/stdc++.h>
using namespace std;
#pragma GCC optimize("Ofast","no-stack-protector","no-math-errno")
#pragma GCC target("avx2","tune=native", "arch=core-avx2")

struct Query { int r1, c1, r2, c2, ans; } query[1000010];

char _field[2000010];
int _pref[2000010], _prefCol[2000010], _prefRow[2000010], r, c, rr, cc;

static inline char &field(int i, int j) { return _field[i * cc + j]; }
static inline int &pref(int i, int j) { return _pref[i * cc + j]; }
static inline int &prefCol(int j, int i) { return _prefCol[j * rr + i]; }
static inline int &prefRow(int i, int j) { return _prefRow[i * cc + j]; }

#pragma GCC diagnostic ignored "-Wimplicit-fallthrough"
static inline int_fast32_t fastAtoi(const char *p, uint_fast32_t len) {
	uint_fast32_t res = 0;
	uint_fast8_t neg = *p == '-';
	if (neg) p++, len--;
	switch (len) {
		case 10: res += (*p++ & 15) * 1000000000;
		case 9: res += (*p++ & 15) * 100000000;
		case 8: res += (*p++ & 15) * 10000000;
		case 7: res += (*p++ & 15) * 1000000;
		case 6: res += (*p++ & 15) * 100000;
		case 5: res += (*p++ & 15) * 10000;
		case 4: res += (*p++ & 15) * 1000;
		case 3: res += (*p++ & 15) * 100;
		case 2: res += (*p++ & 15) * 10;
		case 1: res += (*p & 15);
	}
	return res * (neg ? -1 : 1);
}

static inline bool getRawChar(char *c) {
	static char buf[1 << 20], *p = buf, *end = buf;
	if (p == end) {
		if ((end = buf + fread(buf, 1, 1 << 20, stdin)) == buf) return false;
		p = buf;
	}
	*c = *p++;
	return true;
}

static inline bool getChar(char *c) {
	while (getRawChar(c) && (*c == ' ' || *c == '\n' || *c == '\r'));
	return !(*c == ' ' || *c == '\n' || *c == '\r');
}

static inline bool getInt(int32_t *x) {
	static char buf[12];
	uint_fast32_t i = 0;
	while (getRawChar(buf + i)) {
		if ((unsigned)(buf[i] - '0') > 10U && buf[i] != '-') {
			if (i) break;
			else continue;
		}
		i++;
	}
	if (!i) return false;
	*x = fastAtoi(buf, i);
	return true;
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	getInt(&r); getInt(&c);
	rr = r + 1, cc = c + 1;
	for (int i = 1; i <= r; i++) {
		for (int j = 1; j <= c; j++) getChar(&field(i, j));
	}
	for (int i = 1; i <= r; i++) {
		for (int j = 1; j <= c; j++) {
			pref(i, j) = pref(i, j - 1) + pref(i - 1, j) - pref(i - 1, j - 1)
				+ (field(i, j) == '1' && field(i, j - 1) == '1')
				+ (field(i, j) == '1' && field(i - 1, j) == '1');
		}
	}
	for (int i = 1; i <= r; i++) {
		for (int j = 1; j <= c; j++) {
			bool t = field(i, j) == '1';
			prefCol(j, i) = prefCol(j, i - 1) + (t && field(i, j - 1) == '1');
			prefRow(i, j) = prefRow(i, j - 1) + (t && field(i - 1, j) == '1');
		}
	}
	int q; getInt(&q);
	for (int i = 0; i < q; i++) {
		getInt(&query[i].r1), getInt(&query[i].c1),
			getInt(&query[i].r2), getInt(&query[i].c2);
	}
#pragma GCC ivdep
	for (int i = 0; i < q; i++) {
		const int r1 = query[i].r1, c1 = query[i].c1, r2 = query[i].r2, c2 = query[i].c2;
		query[i].ans = pref(r2, c2) - pref(r1 - 1, c2) - pref(r2, c1 - 1)
			+ pref(r1 - 1, c1 - 1)
			- (prefRow(r1, c2) - prefRow(r1, c1 - 1))
			- (prefCol(c1, r2) - prefCol(c1, r1 - 1));
	}
	for (int i = 0; i < q; i++) cout << query[i].ans << 'x';
}
