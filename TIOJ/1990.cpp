#include <bits/stdc++.h>
using namespace std;
#define _FORTIFY_SOURCE 0
#pragma GCC optimize("Ofast","no-stack-protector","no-math-errno")
#pragma GCC target("avx2","tune=native")

const int MAXN = 1E6 + 1;

int row[MAXN], col[MAXN];
char field[MAXN * 8], *ptr[MAXN];

int main() {
#ifdef OJ_DEBUG
	freopen("in.txt", "r", stdin);
#endif
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	int m, n; cin >> m >> n >> ws;
	for (int i = 0; i < m; i++) {
		int ofst = i * n * 8;
		cin.getline(&field[ofst], MAXN * 8);
		for (int j = 0; j < n; j++) ptr[i * n + j] = &field[ofst + j * 8];
	}
	for (int i = 0; i < m; i++) row[i] = i;
	for (int i = 0; i < n; i++) col[i] = i;
	int p; cin >> p;
	while (p--) {
		char ch; cin >> ch;
		if (ch == 'R') {
			int a, b; cin >> a >> b;
			swap(row[a - 1], row[b - 1]);
		} else if (ch == 'C') {
			int a, b; cin >> a >> b;
			swap(col[a - 1], col[b - 1]);
		} else {
			int x1, y1, x2, y2; cin >> x1 >> y1 >> x2 >> y2;
			swap(ptr[row[x1 - 1] * n + col[y1 - 1]],
					ptr[row[x2 - 1] * n + col[y2 - 1]]);
		}
	}
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			for (int k = 0; k < 7; k++) putchar(*(ptr[row[i] * n + col[j]] + k));
			putchar(' ');
		}
		putchar('\n');
	}
}
