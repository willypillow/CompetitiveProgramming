#include <bits/stdc++.h>
using namespace std;

const int64_t INF = 1E18;

int a[2000001], n;

pair<int64_t, int> solve(int m) {
	pair<int64_t, int> ret = {0, 0}, candid = {-INF, 0};
	for (int i = 1; i <= n; i++) {
		auto tmp = max(candid, {ret.first - m - a[i], ret.second - 1});
		ret = max(ret, {candid.first + a[i], candid.second});
		candid = tmp;
	}
	ret.second *= -1;
	return ret;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int k; cin >> n >> k;
	for (int i = 1; i <= n; i++) cin >> a[i];
	int l = 0, r = 1E7;
	while (l != r) {
		int m = (l + r) >> 1;
		if (solve(m).second > k) l = m + 1;
		else r = m;
	}
	cout << solve(l).first + (int64_t)l * k;
}
