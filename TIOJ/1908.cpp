#include <iostream>
#include <cstring>
#include <algorithm>
#include <vector>
using std::cin; using std::cout; using std::max_element; using std::vector; using std::max;

int dp[2][1 << 22], n, field[25][25];
vector<int> state;

void dfs(int row, int state, int nextState, int depth, int value) {
	if (depth == n) {
		dp[~row & 1][nextState] = max(dp[~row & 1][nextState], dp[row & 1][state] + value);
		return;
	}
	if (depth == 0) {
		if ((state & ((1 << 2) - 1)) == 0) {
			dfs(row, state, nextState | 1, depth + 1, value + field[row][depth]);
		}
	} else if ((nextState & (1 << (depth - 1))) == 0) {
		if ((state & (((1 << 3) - 1) << (depth - 1))) == 0) {
			dfs(row, state, nextState | (1 << depth), depth + 1, value + field[row][depth]);
		}
	}
	dfs(row, state, nextState, depth + 1, value);
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	cin >> n;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) cin >> field[i][j];
	}
	for (int i = 0; i < (1 << n); i++) {
		bool fail = false;
		for (int j = 1; j < n; j++) {
			fail |= (i & (1 << j)) && (i & (1 << (j - 1)));
		}
		if (!fail) {
			state.push_back(i);
		}
	}
	memset(dp[0], 0, sizeof(dp[0]));
	for (int i = 0; i < n; i++) {
		memset(dp[~i & 1], 0, sizeof(dp[~i & 1]));
		for (int j: state) {
			dfs(i, j, 0, 0, 0);
		}
	}
	cout << *max_element(dp[n & 1], dp[n & 1] + (1 << n)) << '\n';
}
