#include <bits/stdc++.h>
using namespace std;

unordered_map<int, bool> field;

void dfs(int x, int y) {
	const int D[][2] = { { -1, 0 }, { 0, -1 }, { 1, 0 }, { 0, 1 } };
	field[x << 15 | y] = 0;
	for (int i = 0; i < 4; i++) {
		int nx = x + D[i][0], ny = y + D[i][1];
		if (field[nx << 15 | ny] == 1) dfs(nx, ny);
	}
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int a, b, c; cin >> a >> b >> c;
	vector<pair<int, int> > mine;
	field.reserve(c * 8); mine.reserve(c);
	for (int i = 0; i < c; i++) {
		int x, y; cin >> x >> y;
		for (int j = max(1, (x - 1) << 1); j <= min(a << 1, (x + 1) << 1); j++) {
			for (int k = max(1, (y - 1) << 1); k <= min(b << 1, (y + 1) << 1); k++) {
				field[j << 15 | k] = 1;
			}
		}
		mine.push_back({ x << 1, y << 1 });
	}
	int ans = 0;
	for (auto &p: mine) {
		if (field[p.first << 15 | p.second]) {
			dfs(p.first, p.second);
			ans++;
		}
	}
	cout << ans << '\n';
}
