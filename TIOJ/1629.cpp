#include <iostream>
#include <vector>
using namespace std;

bool g[1001][1001] = { false };
int dp[1001][1001][2] = { 0 }, n, m;
enum Type { LEFT, RIGHT };

inline int minus1(int x) {
	return (x - 2 + n) % n + 1;
}

inline int plus1(int x) {
	return x % n + 1;
}

int link(int start, int end, Type type) {
	if (start == end) return true;
	if (dp[start][end][type]) return dp[start][end][type];
	int a1, a2;
	if (type == LEFT) {
		a1 = g[minus1(end)][end] ? link(start, minus1(end), LEFT) : -1;
		a2 = g[start][end] ? link(minus1(end), start, RIGHT) : -1;
	} else {
		a1 = g[start][end] ? link(plus1(end), start, LEFT) : -1;
		a2 = g[end][plus1(end)] ? link(start, plus1(end), RIGHT) : -1;
	}
	if (a1 != -1) {
		if (a2 != -1 &&
				((type == LEFT && start < minus1(end)) ||
				(type == RIGHT && plus1(end) < start))) {
			dp[start][end][type] = 2;
		} else dp[start][end][type] = 1;
	} else {
		dp[start][end][type] = (a2 != -1) ? 2 : -1;
	}
	return dp[start][end][type];
}

void print(int start, int end, Type type) {
	cout << end << '\n';
	if (start == end) {
		return;
	}
	if (dp[start][end][type] == 1) {
		if (type == LEFT) print(start, minus1(end), LEFT);
		else print(plus1(end), start, LEFT);
	} else {
		if (type == LEFT) print(minus1(end), start, RIGHT);
		else print(start, plus1(end), RIGHT);
	}
}

int main() {
	cin >> n >> m;
	for (int i = 0, a, b; i < m; i++) {
		cin >> a >> b;
		g[a][b] = g[b][a] = true;
	}
	int i = 1;
	for (i = 1; i <= n; i++) {
		if (link(minus1(i), i, RIGHT) != -1) break;
	}
	if (i <= n) {
		print(minus1(i), i, RIGHT);
	} else {
		cout << "-1\n";
	}
}
