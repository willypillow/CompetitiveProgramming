#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E6 + 2;

int arr[MAXN], inv[MAXN], par[MAXN], ans[MAXN];
stack<int> s;

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 1; i <= n; i++) {
		cin >> arr[i];
		inv[arr[i]] = i;
	}
	par[inv[1]] = 0;
	s.push(inv[1]);
	for (int i = 2; i <= n; i++) {
		int last = 0;
		while (!s.empty() && s.top() > inv[i]) {
			last = s.top();
			s.pop();
		}
		par[inv[i]] = s.empty() ? 0 : s.top();
		par[last] = inv[i];
		s.push(inv[i]);
	}
	for (int i = 1; i <= n; i++) ans[arr[i]] = arr[par[i]];
	for (int i = 1; i <= n; i++) cout << ans[i] << '\n';
}
