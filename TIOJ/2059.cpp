#include <bits/stdc++.h>
#pragma GCC target("avx2", "tune=native")
using namespace std;

uint64_t gcd(uint64_t u, uint64_t v) {
	if (u == 0) return v;
	if (v == 0) return u;
	int shift = __builtin_ctzll(u | v);
	u >>= shift; v >>= shift;
	while ((u & 1) == 0) u >>= 1;
	do {
		v >>= __builtin_ctzll(v);
		if (u > v) swap(u, v);
		v = v - u;
	} while (v != 0);
	return u << shift;
}

uint64_t prng(uint64_t a, uint64_t b) {
	static const uint64_t table[3] = {0x9e3779b185ebca87, 0xc2b2ae3d27d4eb4f, 0x165667b19e3779f9};
	auto Mix = [](uint64_t a, uint64_t b) {
		a += b * table[1];
		a = (a << 31) | (a >> 33);
		return a * table[0];
	};
	uint64_t v1 = Mix(-table[0], a);
	uint64_t v2 = Mix(table[1], b);
	uint64_t ret = ((v1 << 18) | (v1 >> 46)) + ((v2 << 7) | (v2 >> 57));
	ret ^= ret >> 33;
	ret *= table[1];
	ret ^= ret >> 29;
	ret *= table[2];
	ret ^= ret >> 32;
	return ret;
}

int main() {
	cin.tie(0), ios_base::sync_with_stdio(0);
	uint64_t n, a, b; cin >> n >> a >> b;
	uint64_t c = 0;
	for (uint64_t i = 1; i <= n; i++) {
		uint64_t g = gcd(a, b);
		a = prng(b, g), b = prng(a, g), c = prng(c, g);
	}
	cout << c;
}
