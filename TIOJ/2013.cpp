#include "lib2013.h"

struct SplayNode {
	SplayNode *par, *ch[2];
	bool isRoot() { return !par || (par->ch[0] != this && par->ch[1] != this); }
	void rotate() {
		auto p = par, gp = (p ? p->par : nullptr);
		if (!p->isRoot()) gp->ch[gp->ch[1] == p] = this;
		bool d = (p->ch[1] == this);
		p->ch[d] = ch[!d], ch[!d] = p, par = gp, p->par = this;
		if (p->ch[d]) p->ch[d]->par = p;
	}
	void splay() {
		while (!isRoot()) {
			if (!par->isRoot()) {
				if ((par->ch[1] == this) == (par->par->ch[1] == par)) rotate();
				else par->rotate();
			}
			rotate();
		}
	}
} root[1000001];

SplayNode *access(SplayNode *sn) {
	SplayNode *last = nullptr;
	while (sn) {
		sn->splay();
		sn->ch[1] = last, last = sn, sn = sn->par;
	}
	return last;
}

void init(int n) { }
void explore(int a, int b) { root[b].splay(), root[b].par = &root[a]; }
int run(int a, int b) { return access(&root[a]), access(&root[b]) == &root[a]; }
