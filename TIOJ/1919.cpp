#include <bits/stdc++.h>
using namespace std;

// I/O optimization start
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wimplicit-fallthrough"
static inline int_fast32_t fastAtoi(const char *p, uint_fast32_t len) {
	uint_fast32_t res = 0;
	uint_fast8_t neg = *p == '-';
	if (neg) p++, len--;
	switch (len) {
		case 10: res += (*p++ & 15) * 1000000000;
		case 9: res += (*p++ & 15) * 100000000;
		case 8: res += (*p++ & 15) * 10000000;
		case 7: res += (*p++ & 15) * 1000000;
		case 6: res += (*p++ & 15) * 100000;
		case 5: res += (*p++ & 15) * 10000;
		case 4: res += (*p++ & 15) * 1000;
		case 3: res += (*p++ & 15) * 100;
		case 2: res += (*p++ & 15) * 10;
		case 1: res += (*p & 15);
	}
	return res * (neg ? -1 : 1);
}

static inline bool getRawChar(char *c) {
	static char buf[1 << 20], *p = buf, *end = buf;
	if (p == end) {
		if ((end = buf + fread_unlocked(buf, 1, 1 << 20, stdin)) == buf) return false;
		p = buf;
	}
	*c = *p++;
	return true;
}

static inline bool getInt(int32_t *x) {
	static char buf[12];
	uint_fast32_t i = 0;
	while (getRawChar(buf + i)) {
		if ((unsigned)(buf[i] - '0') > 10U && buf[i] != '-') {
			if (i) break;
			else continue;
		}
		i++;
	}
	if (!i) return false;
	*x = fastAtoi(buf, i);
	return true;
}
#pragma GCC diagnostic pop
// I/O optimization end

const int MAXN = 1E5 + 1;

struct Oper {
	int pos, lim, val, time;
	bool operator <(const Oper &rhs) const { return pos < rhs.pos; }
} oper[MAXN * 2];
int ans[MAXN], land[MAXN], person[MAXN], tmpLast[MAXN],
		owner[MAXN], lastEqOwner[MAXN], goal[MAXN], n, m, q;
int64_t bit[MAXN], tmpAns[MAXN];
int _buf[MAXN * 10];

template<class It, class Pred>
int part(It first, It last, Pred p) {
	typedef typename iterator_traits<It>::value_type Val;
	Val *buf = (Val*)_buf;
	int len = (int)distance(first, last), lTop = 0, rTop = len - 1;
	for (It it = first; it != last; it++) buf[p(*it) ? lTop++ : rTop--] = *it;
	It it = first;
	for (int i = 0; i < lTop; i++) *it++ = buf[i];
	for (int i = len - 1; i >= lTop; i--) *it++ = buf[i];
	return lTop;
}

void bitAdd(int p, int v) {
	for (p = m - p; p <= m; p += p & -p) bit[p] += v;
}

int64_t bitQuery(int p) {
	int64_t ret = 0;
	for (p = m - p; p; p -= p & -p) ret += bit[p];
	return ret;
}

void cdq(int pL, int pR, int lL, int lR, int oL, int oR, int tL, int tR) {
	if (tL == tR) {
		for (int i = pL; i <= pR; i++) ans[person[i]] = tL;
	} else {
		int tM = (tL + tR) >> 1,
				oM = oL + part(oper + oL, oper + oR + 1, [&](Oper &o) {
						return o.time <= tM;
					});
		for (int i = pL; i <= pR; i++) tmpAns[person[i]] = 0;
		int j = oL;
		for (int i = lL; i <= lR; i++) {
			while (j < oM && oper[j].pos <= land[i]) {
				bitAdd(oper[j].lim, oper[j].val);
				j++;
			}
			tmpAns[owner[land[i]]] += bitQuery(lastEqOwner[land[i]]);
		}
		for (int i = oL; i < j; i++) bitAdd(oper[i].lim, -oper[i].val);
		int lM = lL + part(land + lL, land + lR + 1, [&](int x) {
						return tmpAns[owner[x]] >= goal[owner[x]];
					}),
				pM = pL + part(person + pL, person + pR + 1, [&](int x) {
						return tmpAns[x] >= goal[x];
					});
		for (int i = pM; i <= pR; i++) goal[person[i]] -= (int)tmpAns[person[i]];
		cdq(pL, pM - 1, lL, lM - 1, oL, oM - 1, tL, tM);
		cdq(pM, pR, lM, lR, oM, oR, tM + 1, tR);
	}
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	getInt(&n), getInt(&m), getInt(&q);
	for (int i = 1; i <= m; i++) {
		getInt(owner + i);
		lastEqOwner[i] = tmpLast[owner[i]];
		tmpLast[owner[i]] = i;
	}
	for (int i = 1; i <= n; i++) getInt(goal + i);
	for (int i = 0; i < q; i++) {
		int l, r, c; getInt(&l), getInt(&r), getInt(&c);
		oper[i << 1] = { l, l - 1, c, i };
		oper[i << 1 | 1] = { r + 1, l - 1, -c, i };
	}
	sort(oper, oper + (q << 1));
	for (int i = 1; i <= n; i++) person[i] = i;
	for (int i = 1; i <= m; i++) land[i] = i;
	cdq(1, n, 1, m, 0, (q << 1) - 1, 0, q);
	for (int i = 1; i <= n; i++) cout << (ans[i] == q ? -1 : ans[i] + 1) << '\n';
}
