#include <iostream>
#include "lib1956.h"
#include <algorithm>
using std::cin; using std::cout; using std::sort;

long long sum[200010] = { 0 };
int p[200010], *pw;

bool cmp(const int l, const int r) {
	return pw[l] < pw[r];
}

int solve(int l, int u, int w[], int n, int result[]) {
	pw = w;
	for (int i = 0; i < n; i++) p[i] = i;
	sort(p, p + n, cmp);
	for (int i = 0; i < n; i++) {
		sum[i + 1] = sum[i] + w[p[i]];
	}
	int i, j;
	bool success = false;
	for (i = 0, j = 0; i <= n; i++) {
		while (i < j && sum[j] - sum[i] > u) i++;
		while (j < n && sum[j] - sum[i] < l) j++;
		if (sum[j] - sum[i] <= u && sum[j] - sum[i] >= l) {
			success = true;
			break;
		}
	}
	if (success) {
		for (int ii = i; ii < j; ii++) result[ii - i] = p[ii];
		return j - i;
	} else {
		return 0;
	}
}
