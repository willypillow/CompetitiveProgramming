#include "lib1341.h"
#include <iostream>
using namespace std;

int n, cnt = 0;

bool check(long long x, long long y) {
	bool res = 0 < x && x <= n && 0 < y && y <= n && Examine(x, y);
	return res;
}

int main() {
	int _x0, _y0, i, j;
	Init(&n, &_x0, &_y0);
	long long x0 = _x0, y0 = _y0, xlb, xrb, yub, xans, yans;
	for (i = 0; check(x0 + (1LL << i), y0); i++);
	if (i) {
		long long l = x0 + (1LL << (i - 1)), r = x0 + (1LL << i);
		while (l < r) {
			long long m = (l + r) >> 1;
			if (check(m, y0)) l = m + 1;
			else r = m;
		}
		xrb = l;
	} else {
		xrb = x0 + 1;
	}
	for (i = 0; check(x0 - (1LL << i), y0); i++);
	if (i) {
		long long l = x0 - (1LL << i), r = x0 - (1LL << (i - 1));
		while (l < r) {
			long long m = (l + r) >> 1;
			if (check(m, y0)) r = m;
			else l = m + 1;
		}
		xlb = l - 1;
	} else {
		xlb = x0 - 1;
	}
	long long xc = (xlb + xrb) / 2, m = xrb - xlb - 1;
	for (i = 0; check(xc, y0 + (1LL << i)); i++);
	if (i) {
		long long l = y0 + (1LL << (i - 1)), r = y0 + (1LL << i);
		while (l < r) {
			long long m = (l + r) >> 1;
			if (check(xc, m)) l = m + 1;
			else r = m;
		}
		yub = l;
	} else {
		yub = y0 + 1;
	}
	long long ydb = yub - m - 1, yc = (yub + ydb) / 2;
	for (i = 1; check(xc, yc + i * 2 * m); i++);
	for (j = 1; check(xc, yc - j * 2 * m); j++);
	if (i + j == 3) {
		if (i == 2) yans = yc + m;
		else yans = yc - m;
	} else {
		yans = yc + (i - 2) * 2 * m;
	}
	for (i = 1; check(xc + i * 2 * m, yc); i++);
	for (j = 1; check(xc - j * 2 * m, yc); j++);
	if (i + j == 3) {
		if (i == 2) xans = xc + m;
		else xans = xc - m;
	} else {
		xans = xc + (i - 2) * 2 * m;
	}
	Solution(xans, yans);
}
