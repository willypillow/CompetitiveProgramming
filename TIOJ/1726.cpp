#include <bits/stdc++.h>
using namespace std;

vector<uint16_t> pos[60001];
uint16_t a[60001], blk[60001], vis[60001], dist[250][60001];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n, m; cin >> n >> m;
	uint16_t thres = (uint16_t)round(sqrt(n * log2(n))), blkCnt = 0;
	for (int i = 1; i <= n; i++) {
		cin >> a[i];
		pos[a[i]].push_back((uint16_t)i);
	}
	for (int i = 0; i <= n; i++) {
		if ((int)pos[i].size() >= thres) {
			blk[i] = blkCnt++;
			memset(dist[blk[i]], 0xff, sizeof(dist[blk[i]]));
			queue<pair<int, int> > q;
			dist[blk[i]][i] = 0;
			for (int p: pos[i]) {
				q.push({ p, 0 });
				vis[p] = blkCnt;
			}
			while (q.size()) {
				auto x = q.front(); q.pop();
				for (int nx: { x.first - 1, x.first + 1 }) {
					if (nx < 1 || nx > n || vis[nx] == blkCnt) continue;
					vis[nx] = blkCnt;
					dist[blk[i]][a[nx]] = min(dist[blk[i]][a[nx]], (uint16_t)(x.second + 1));
					q.push({ nx, x.second + 1 });
				}
			}
		}
	}
	while (m--) {
		int s, t; cin >> s >> t;
		if (pos[s].size() < pos[t].size()) swap(s, t);
		if (s == t && pos[s].size()) {
			cout << "0\n";
		} else if ((int)pos[s].size() >= thres) {
			cout << (dist[blk[s]][t] == UINT16_MAX ? -1 : dist[blk[s]][t]) << '\n';
		} else {
			int ans = UINT16_MAX;
			for (int p: pos[t]) {
				auto idx = lower_bound(pos[s].begin(), pos[s].end(), p) - pos[s].begin();
				if (idx < (int)pos[s].size()) ans = min(ans, pos[s][idx] - p);
				if (idx - 1 >= 0) ans = min(ans, p - pos[s][idx - 1]);
			}
			cout << (ans == UINT16_MAX ? -1 : ans) << '\n';
		}
	}
}
