#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
#include "lib1960.h"
using namespace std;

int *ans, n;

void prnt(string str, int start, int end) {
	for (int i = start; i < end; i++) {
		str[i] = '1';
		add_element(str.c_str());
		str[i] = '0';
	}
}

void genWrite(int begin, int end) {
	if (begin + 1 >= end) return;
	string str = string(n, '1');
	int m = (begin + end) / 2;
	for (int i = begin; i < end; i++) str[i] = '0';
	for (int i = begin; i < m; i++) {
		str[i] = '1';
		add_element(str.c_str());
		str[i] = '0';
	}
	genWrite(begin, m);
	genWrite(m, end);
}

void solve(int begin, int end, vector<int> possible) {
	if (possible.size() == 0) {
		exit(123);
	}
	if (possible.size() == 1) {
		ans[possible[0]] = begin;
		return;
	}
	string str = string(n, '1');
	vector<int> newPos, newPos2;
	for (int x: possible) str[x] = '0';
	for (int x: possible) {
		str[x] = '1';
		if (check_element(str.c_str())) newPos.push_back(x);
		else newPos2.push_back(x);
		str[x] = '0';
	}
	solve(begin, begin + newPos.size(), newPos);
	solve(begin + newPos.size(), end, newPos2);
}


void restore_permutation(int _n, int w, int r, int *result) {
	ans = result;
	n = _n;
	string str(n, '0');
	vector<int> all;
	for (int i = 0; i < n; i++) all.push_back(i);
	genWrite(0, n);
	compile_set();
	solve(0, n, all);
}
