#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E5 + 5;

enum Type { OBSTACLE, LASER, QUERY };
struct Operation {
	Type type;
	int p, v, v2, w, timer;
	bool operator <(const Operation &rhs) const { return p < rhs.p; }
};

vector<Operation> oper, tmp;
int64_t bit[MAXN], ans[MAXN * 2];
int a[MAXN], last[MAXN], lastIden[MAXN], n;
pair<int, int> laser[MAXN];

inline void bitAdd(int p, int v) {
	for (; p <= n + 1; p += p & -p) bit[p] += v;
}

inline int64_t bitQuery(int p) {
	int64_t ret = 0;
	for (; p; p -= p & -p) ret += bit[p];
	return ret;
}

inline void cdq(int l, int r) {
	if (l == r) return;
	int m = (l + r) >> 1, tL = l, tR = m + 1;
	for (int i = l; i <= r; i++) {
		tmp[(oper[i].timer <= m) ? tL++ : tR++] = oper[i];
	}
	for (int i = l; i <= r; i++) oper[i] = tmp[i];
	int j = l;
	for (int i = m + 1; i <= r; i++) {
		if (oper[i].type != QUERY) continue;
		while (j <= m && oper[j].p <= oper[i].p) {
			if (oper[j].type == OBSTACLE) bitAdd(oper[j].v + 1, oper[j].w);
			j++;
		}
		ans[oper[i].w] += bitQuery(oper[i].v + 1);
	}
	for (int i = l; i < j; i++) {
		if (oper[i].type == OBSTACLE) bitAdd(oper[i].v + 1, -oper[i].w);
	}
	j = l;
	for (int i = m + 1; i <= r; i++) {
		if (oper[i].type != QUERY) continue;
		while (j <= m && oper[j].p <= oper[i].p) {
			if (oper[j].type == LASER) bitAdd(oper[j].v + 1, oper[j].w);
			j++;
		}
		ans[oper[i].w] += bitQuery(oper[i].v2 + 1);
	}
	for (int i = l; i < j; i++) {
		if (oper[i].type == LASER) bitAdd(oper[i].v + 1, -oper[i].w);
	}
	cdq(l, m), cdq(m + 1, r);
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int q; cin >> n >> q;
	set<pair<int, int> > st;
	for (int i = 1; i <= n; i++) {
		cin >> a[i];
		lastIden[i] = last[a[i]];
		oper.push_back({ OBSTACLE, i, lastIden[i], 0, a[i], (int)oper.size() });
		last[a[i]] = i;
		st.insert({ a[i], i });
	}
	int ansCnt = 0;
	for (int i = 0; i < q; i++) {
		int op; cin >> op;
		int l, r, v;
		set<pair<int, int> >::iterator itL, itR, itL2;
		switch (op) {
		case 1:
			cin >> l >> v;
			if (v == a[l]) break;
			itL = st.lower_bound({ v, l }), itR = st.upper_bound({ a[l], l });
			itL2 = itL;
			if (itR != st.end() && itR->first == a[l]) {
				oper.push_back({ OBSTACLE, itR->second,
						lastIden[itR->second], 0, -a[itR->second], (int)oper.size() });
				lastIden[itR->second] = lastIden[l];
				oper.push_back({ OBSTACLE, itR->second,
						lastIden[itR->second], 0, a[itR->second], (int)oper.size() });
			}
			oper.push_back({ OBSTACLE, l, lastIden[l], 0, -a[l], (int)oper.size() });
			lastIden[l] =
					(itL != st.begin() && (--itL)->first == v) ? itL->second : 0;
			oper.push_back({ OBSTACLE, l, lastIden[l], 0, v, (int)oper.size() });
			if (itL2 != st.end() && itL2->first == v) {
				oper.push_back({ OBSTACLE, itL2->second,
						lastIden[itL2->second], 0, -a[itL2->second], (int)oper.size() });
				lastIden[itL2->second] = l;
				oper.push_back({ OBSTACLE, itL2->second,
						lastIden[itL2->second], 0, a[itL2->second], (int)oper.size() });
			}
			st.erase({ a[l], l }), st.insert({ v, l });
			a[l] = v;
			break;
		case 2:
			cin >> l >> r >> v;
			laser[l] = { r, v };
			oper.push_back({ LASER, l, r, 0, v, (int)oper.size() });
			break;
		case 3:
			cin >> l;
			oper.push_back({ LASER, l,
					laser[l].first, 0, -laser[l].second, (int)oper.size() });
			break;
		case 4:
			cin >> l >> r;
			oper.push_back({ QUERY, l - 1, l - 1, r, ansCnt++, (int)oper.size() });
			oper.push_back({ QUERY, r, l - 1, r, ansCnt++, (int)oper.size() });
			break;
		}
	}
	sort(oper.begin(), oper.end());
	tmp.resize(oper.size());
	cdq(0, (int)oper.size() - 1);
	for (int i = 0; i < ansCnt; i += 2) cout << ans[i + 1] - ans[i] << '\n';
}
