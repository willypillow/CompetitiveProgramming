#include <bits/stdc++.h>
using namespace std;

const int MAXN = 50001, MAXQ = 10001;

vector<int> vals;
int arr[MAXN];

int rnd() {
	static uint_fast32_t x = 20170319;
	return x = x * 0xdefaced + 1;
}

struct Query { int type, l, r, k; } queries[MAXQ];
struct Node {
	static Node pool[MAXN * 70], *ptr;
	int val, pri, size;
	Node *lPtr, *rPtr;
	void pull() {
		size = 1 + (lPtr ? lPtr->size : 0) + (rPtr ? rPtr->size : 0);
	}
	Node() {}
	Node(int v): val(v), pri(rnd()), size(1), lPtr(NULL), rPtr(NULL) {}
} Node::pool[MAXN * 70], *Node::ptr, *node[MAXN * 8];

int getSize(Node *n) {
	return (n ? n->size : 0);
}

Node* merge(Node *a, Node *b) {
	if (!a || !b) return a ? a : b;
	if (a->pri < b->pri) {
		a->rPtr = merge(a->rPtr, b);
		a->pull();
		return a;
	} else {
		b->lPtr = merge(a, b->lPtr);
		b->pull();
		return b;
	}
}

void split(Node *n, int k, Node *&a, Node *&b) {
	if (!n) {
		a = b = NULL;
	} else if (n->val <= k) {
		a = n;
		split(n->rPtr, k, a->rPtr, b);
		a->pull();
	} else {
		b = n;
		split(n->lPtr, k, a, b->lPtr);
		b->pull();
	}
}

void splitBySize(Node *n, int k, Node *&a, Node *&b) {
	if (!n) {
		a = b = NULL;
	} else if (getSize(n->lPtr) + 1 <= k) {
		a = n;
		splitBySize(n->rPtr, k - getSize(n->lPtr) - 1, a->rPtr, b);
		a->pull();
	} else {
		b = n;
		splitBySize(n->lPtr, k, a, b->lPtr);
		b->pull();
	}
}

void build(int id, int l, int r) {
	if (l == r) {
		node[id] = new(Node::ptr++) Node(arr[l]);
	} else {
		int m = l + (r - l) / 2;
		build(id * 2, l, m); build(id * 2 + 1, m + 1, r);
		vector<int> tmp;
		for (int i = l; i <= r; i++) tmp.push_back(arr[i]);
		sort(tmp.begin(), tmp.end());
		node[id] = NULL;
		for (int x: tmp) node[id] = merge(node[id], new(Node::ptr++) Node(x));
	}
}

void pull(int id, int old, int now) {
	Node *t1 = NULL, *t2 = NULL, *t3 = NULL, *erase = NULL;
	split(node[id], old - 1, t1, t2);
	split(t2, old, t2, t3);
	splitBySize(t2, 1, erase, t2);
	node[id] = merge(t1, merge(t2, t3));
	split(node[id], now, t1, t2);
	node[id] = merge(t1, merge(new(Node::ptr++) Node(now), t2));
}

void modify(int id, int l, int r, int oldIdx, int now) {
	if (l == r) {
		node[id] = new(Node::ptr++) Node(now);
	} else {
		int m = l + (r - l) / 2;
		if (oldIdx <= m) modify(id * 2, l, m, oldIdx, now);
		else modify(id * 2 + 1, m + 1, r, oldIdx, now);
		pull(id, arr[oldIdx], now);
	}
}

int query(int id, int l, int r, int qL, int qR, int v) {
	if (qL <= l && r <= qR) {
		Node *t1 = NULL, *t2 = NULL;
		split(node[id], v, t1, t2);
		int s = getSize(t1);
		node[id] = merge(t1, t2);
		return s;
	} else {
		int m = l + (r - l) / 2, ans = 0;
		if (qL <= m) ans += query(id * 2, l, m, qL, qR, v);
		if (m < qR) ans += query(id * 2 + 1, m + 1, r, qL, qR, v);
		return ans;
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		vals.clear();
		Node::ptr = Node::pool;
		int n, q; cin >> n >> q;
		for (int i = 1; i <= n; i++) {
			cin >> arr[i];
			vals.push_back(arr[i]);
		}
		for (int i = 0; i < q; i++) {
			cin >> queries[i].type;
			if (queries[i].type == 1) {
				cin >> queries[i].l >> queries[i].r >> queries[i].k;
				vals.push_back(queries[i].k);
			} else if (queries[i].type == 2) {
				cin >> queries[i].l >> queries[i].k;
				vals.push_back(queries[i].k);
			} else {
				cin >> queries[i].l >> queries[i].k;
			}
		}
		sort(vals.begin(), vals.end());
		vals.resize(unique(vals.begin(), vals.end()) - vals.begin());
		build(1, 1, n);
		for (int i = 0; i < q; i++) {
			if (queries[i].type == 1) {
				int l = 0, r = vals.size() - 1;
				while (l != r) {
					int m = l + (r - l) / 2;
					int rank = query(1, 1, n, queries[i].l, queries[i].r, vals[m]);
					if (rank >= queries[i].k) r = m;
					else l = m + 1;
				}
				cout << vals[l] << '\n';
			} else if (queries[i].type == 2) {
				modify(1, 1, n, queries[i].l, queries[i].k);
				arr[queries[i].l] = queries[i].k;
			} else {
				cout << "7122\n";
			}
		}
	}
}
