#include <bits/stdc++.h>
using namespace std;

struct Scan {
	int x, y1, y2, t;
	bool operator <(const Scan &rhs) const {
		return x < rhs.x;
	}
} scan[200000];
int cnt[4000001], area[4000001];

void insert(int id, int l, int r, int qL, int qR, int v) {
	if (qL <= l && r <= qR) {
		cnt[id] += v;
	} else {
		int m = (l + r) >> 1;
		if (qL <= m) insert(id << 1, l, m, qL, min(m, qR), v);
		if (m < qR) insert(id << 1 | 1, m + 1, r, max(m, qL), qR, v);
	}
	area[id] = (cnt[id] ? (r - l + 1) : (area[id << 1] + area[id << 1 | 1]));
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	int maxX = 0, maxY = 0, sTop = 0;
	for (int i = 0; i < n; i++) {
		int x1, x2, y1, y2; cin >> x1 >> x2 >> y1 >> y2;
		y2--;
		scan[sTop++] = { x1, y1, y2, 1 }; scan[sTop++] = { x2, y1, y2, -1 };
		maxX = max(maxX, x2); maxY = max(maxY, y2);
	}
	sort(scan, scan + sTop);
	long long ans = 0;
	for (int i = 0, j = 0; i <= maxX; i++) {
		ans += area[1];
		for (; i == scan[j].x && j < sTop; j++) {
			insert(1, 0, maxY, scan[j].y1, scan[j].y2, scan[j].t);
		}
	}
	cout << ans << '\n';
}
