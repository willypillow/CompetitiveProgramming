#include <bits/stdc++.h>
using namespace std;

typedef long long LL;
const int MAXN = 5E5 + 10;

vector<pair<int, int> > operation[MAXN * 4];
unordered_map<LL, list<int> > id;
int par[MAXN], unionRand[MAXN], distinct, qTop;

struct Query { int a, b, t0, t1; } query[MAXN];

void modify(int id, int qL, int qR, int a, int b, int l, int r) {
	if (qL <= l && r <= qR) {
		operation[id].push_back({ a, b });
	} else {
		int m = (l + r) >> 1;
		if (qL <= m) modify(id << 1, qL, min(m, qR), a, b, l, m);
		if (m < qR) modify(id << 1 | 1, max(m, qL), qR, a, b, m + 1, r);
	}
}

int find(int x) {
	return par[x] == x ? x : find(par[x]);
}

void merge(int a, int b, vector<pair<int, int> > &undo) {
	int pa = find(a), pb = find(b);
	if (unionRand[pa] > unionRand[pb]) swap(pa, pb);
	if (pa != pb) {
		undo.push_back({ pa, par[pa] });
		par[pa] = pb;
		distinct--;
	}
}

void dfs(int id, int l, int r) {
	vector<pair<int, int> > undo;
	for (auto p: operation[id]) merge(p.first, p.second, undo);
	if (l == r) {
		cout << distinct << '\n';
	} else {
		int m = (l + r) >> 1;
		dfs(id << 1, l, m); dfs(id << 1 | 1, m + 1, r);
	}
	for (int i = undo.size() - 1; i >= 0; i--) {
		par[undo[i].first] = undo[i].second;
		distinct++;
	}
}

void init(int n) {
	distinct = n;
	qTop = 0;
	for (int i = 0; i < MAXN * 4; i++) operation[i].clear();
	for (int i = 0; i < n; i++) {
		par[i] = i;
		unionRand[i] = rand();
	}
	id.clear();
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	srand(time(0));
	int t; cin >> t;
	while (t--) {
		int n, m, q; cin >> n >> m >> q;
		init(n);
		for (int i = 0; i < m; i++, qTop++) {
			cin >> query[qTop].a >> query[qTop].b;
			if (query[qTop].a > query[qTop].b) swap(query[qTop].a, query[qTop].b);
			query[qTop].t0 = 1; query[qTop].t1 = q;
			id[query[qTop].a * (LL)MAXN + query[qTop].b].push_back(qTop);
		}
		for (int i = 1, a, b; i <= q; i++) {
			char c; cin >> c >> a >> b;
			if (a > b) swap(a, b);
			if (c == 'N') {
				query[qTop] = { a, b, i, q };
				id[a * (LL)MAXN + b].push_back(qTop++);
			} else {
				auto &lis = id[a * (LL)MAXN + b];
				query[*lis.begin()].t1 = i - 1;
				lis.erase(lis.begin());
			}
		}
		for (int i = 0; i < qTop; i++) {
			if (query[i].t0 <= query[i].t1) {
				modify(1, query[i].t0, query[i].t1, query[i].a, query[i].b, 1, q);
			}
		}
		dfs(1, 1, q);
	}
}
