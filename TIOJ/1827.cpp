#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E5 + 5;

int n;
struct Node {
	static Node pool[MAXN * 40], *ptr, *root[MAXN];
	int size;
	Node *lc, *rc;
	static void init() {
		ptr = pool;
		root[0] = new(ptr++) Node(1, n);
	}
	Node (int _s = 0): size(_s) {}
	Node (int l, int r): size(0) {
		if (l == r) return;
		int m = (l + r) >> 1;
		this->lc = new(Node::ptr++) Node(l, m);
		this->rc = new(Node::ptr++) Node(m + 1, r);
	}
	Node *modify(int pos, int l = 1, int r = n) {
		if (l == r) return new(Node::ptr++) Node(size + 1);
		Node *node = new(Node::ptr++) Node(*this);
		int m = (l + r) >> 1;
		if (pos <= m) node->lc = lc->modify(pos, l, m);
		else node->rc = rc->modify(pos, m + 1, r);
		node->size = node->lc->size + node->rc->size;
		return node;
	}
	int query(int qL, int qR, int l = 1, int r = n) {
		if (qR < l || r < qL) return 0;
		if (qL <= l && r <= qR) return size;
		int m = (l + r) >> 1;
		return lc->query(qL, qR, l, m) + rc->query(qL, qR, m + 1, r);
	}
} Node::pool[MAXN * 40], *Node::ptr, *Node::root[MAXN];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int m; cin >> n >> m;
	Node::init();
	for (int i = 0, x; i < n; i++) {
		cin >> x;
		Node::root[i + 1] = Node::root[i]->modify(x);
	}
	while (m--) {
		int p, k; cin >> p >> k;
		p++;
		int l = 0, r = n;
		while (l != r) {
			int m = (l + r) >> 1, lo = max(p - m, 1), hi = min(p + m, n);
			if (Node::root[hi]->query(1, m) - Node::root[lo - 1]->query(1, m) >= k) {
				r = m;
			} else {
				l = m + 1;
			}
		}
		cout << l << '\n';
	}
}
