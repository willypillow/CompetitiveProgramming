#include <bits/stdc++.h>
using namespace std;

static inline bool getRawChar(char *c) {
	static char buf[1 << 16], *p = buf, *end = buf;
	if (p == end) {
		if ((end = buf + fread_unlocked(buf, 1, 1 << 16, stdin)) == buf) return false;
		p = buf;
	}
	*c = *p++;
	return true;
}

static inline void getInt() { }
template<class... Args> static inline void getInt(int &i, Args&... args) {
	i = 0;
	char c;
	while (getRawChar(&c)) {
		if (unsigned(c - '0') > 10U) break;
		i = i * 10 + (c - '0');
	}
	getInt(args...);
}

const int64_t INF = 1E18;

int a[2000000];
vector<int64_t> v = {INF}, nxt;

inline pair<int64_t, int> merger(int64_t pivot, int left) {
	int64_t cost = 0, cnt = 0;
	nxt = {INF};
	for (int j = 1; j < (int)v.size() && cnt <= left; j++) {
		nxt.push_back(v[j]);
		while (cnt <= left && nxt.size() >= 3 &&
				nxt[nxt.size() - 2] <= nxt.back() && nxt[nxt.size() - 2] <= pivot) {
			nxt[nxt.size() - 3] += nxt.back() - nxt[nxt.size() - 2];
			cost += nxt[nxt.size() - 2];
			nxt.resize(nxt.size() - 2);
			cnt++;
		}
	}
	return {cost, (int)cnt};
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	v.reserve(1 << 21), nxt.reserve(1 << 21);
	int n, k; getInt(n, k);
	int64_t ans = 0;
	bool lastPos = 0;
	for (int i = 0; i < n; i++) getInt(a[i]);
	for (int i = 1; i < n; i++) {
		int d = a[i] - a[i - 1];
		if ((d >= 0) != lastPos) lastPos ^= 1, v.push_back(0);
		v.back() += abs(d);
		if (lastPos) ans += abs(d);
	}
	if (lastPos) v.push_back(INF);
	else v.back() = INF;
	int left = ((int)v.size() >> 1) - k;
	while (left > 0) {
		auto tmp = v;
		nth_element(tmp.begin(), tmp.begin() + left - 1, tmp.end());
		int64_t pivot = tmp[left - 1];
		auto res = merger(pivot, left);
		if (res.second > left) {
			res = merger(pivot - 1, left);
			ans -= res.first + (left - res.second) * pivot;
			break;
		}
		ans -= res.first, left -= res.second;
		swap(v, nxt);
	}
	cout << ans;
}
