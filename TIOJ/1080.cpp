#include <bits/stdc++.h>
using namespace std;

struct Trie {
	int ch[16], cnt;
	Trie(): cnt(0) { memset(ch, 0, sizeof(ch)); }
} pool[750000];
int ptr;

int newNode() {
	pool[ptr] = Trie();
	return ptr++;
}

void insert(int rt, int x) {
	for (int ofst = 28; ofst >= 0; ofst -= 4) {
		pool[rt].cnt++;
		int d = x >> ofst & 0xF;
		if (!pool[rt].ch[d]) pool[rt].ch[d] = newNode();
		rt = pool[rt].ch[d];
	}
	pool[rt].cnt++;
}

int query(int rt, int x, int ans = 0) {
	for (int ofst = 28; rt && ofst >= 0; ofst -= 4) {
		int d = x >> ofst & 0xF;
		for (int i = d + 1; i < 16; i++) ans += pool[pool[rt].ch[i]].cnt;
		rt = pool[rt].ch[d];
	}
	return ans;
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	for (int n, cases = 1, rt; cin >> n && n; cases++) {
		int64_t ans = 0;
		ptr = 1, rt = newNode();
		while (n--) {
			int x; cin >> x;
			x = (uint32_t)((int64_t)x - INT_MIN);
			ans += query(rt, x);
			insert(rt, x);
		}
		cout << "Case #" << cases << ": " << ans << '\n';
	}
}
