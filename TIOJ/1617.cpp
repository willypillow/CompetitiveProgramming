#include <iostream>
#include <algorithm>
#include "lib1617.h"
using namespace std;

int n, id[1500];

bool cmp(const int &i, const int &j) {
	return (i != j) ? (Med3(i, j, id[n]) == i) : false;
}

int main() {
	n = Get_Box();
	for (int i = 0; i < n; i++) id[i] = i + 1;
	for (int i = 0; i < n - 2; i++) { // Move max/min to id[n - 1]/id[n]
		int med = Med3(id[n - 3], id[n - 2], id[n - 1]);
		if (med == id[n - 3]) swap(id[n - 3], id[i]);
		else if (med == id[n - 2]) swap(id[n - 2], id[i]);
		else if (med == id[n - 1]) swap(id[n - 1], id[i]);
	}
	n -= 2; // Remove max/min
	random_shuffle(id, id + n);
	nth_element(id, id + n / 2, id + n, cmp);
	Report(id[n / 2]);
}
