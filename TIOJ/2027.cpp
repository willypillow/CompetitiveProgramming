#include <bits/stdc++.h>
using namespace std;

const int MAXN = 3E5 + 1;

struct Trie {
	static Trie pool[MAXN * 12], *ptr;
	Trie *ch[2];
	int cnt;
	void insert(int x, int ofst = 23) {
		cnt++;
		if (ofst == -1) return;
		bool d = x & (1 << ofst);
		if (!ch[d]) ch[d] = new(ptr++) Trie { { NULL, NULL }, 0 };
		ch[d]->insert(x, ofst - 1);
	}
	int query(int x, int xorred, int ofst = 23) {
		if (ofst == -1) return cnt;
		bool d = x & (1 << ofst), xd = xorred & (1 << ofst);
		return (ch[d ^ xd] ? ch[d ^ xd]->query(x, xorred, ofst - 1) : 0) +
				(!d && ch[!xd] ? ch[!xd]->cnt : 0);
	}
} Trie::pool[MAXN * 12], *Trie::ptr;
int a[MAXN];

int64_t cdq(int l, int r, int64_t ans = 0) {
	if (l == r) return 0;
	Trie::ptr = Trie::pool;
	Trie *root = new(Trie::ptr++) Trie { { NULL, NULL }, 0 };
	int m = (l + r) >> 1;
	for (int i = m + 1, j = m, lMx = 0, rMx = 0; i <= r; i++) {
		rMx = max(rMx, a[i]);
		while (j >= l && max(a[j], lMx) <= rMx) {
			lMx = max(a[j], lMx);
			root->insert(a[j--]);
		}
		ans += root->query(rMx, a[i]);
	}
	Trie::ptr = Trie::pool;
	root = new(Trie::ptr++) Trie { { NULL, NULL }, 0 };
	for (int i = m, j = m + 1, lMx = 0, rMx = 0; i >= l; i--) {
		lMx = max(a[i], lMx);
		while (j <= r && max(rMx, a[j]) < lMx) {
			rMx = max(rMx, a[j]);
			root->insert(a[j++]);
		}
		ans += root->query(lMx, a[i]);
	}
	return ans + cdq(l, m) + cdq(m + 1, r);
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 1; i <= n; i++) cin >> a[i];
	cout << cdq(1, n) << '\n';
}
