#include <iostream>
using std::cin; using std::cout; using std::min;

long long pos[10000010], lCost[10000010], rCost[10000010];

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int t; cin >> t;
	while (t--) {
		int n, k, l; cin >> n >> k >> l;
		for (int i = 1; i <= n; i++) {
			cin >> pos[i];
		}
		lCost[0] = rCost[0] = pos[n + 1] = 0;
		for (int i = 1; i <= n; i++) {
			lCost[i] = 2 * pos[i];
			if (i - k >= 0) lCost[i] += lCost[i - k];
		}
		for (int i = 1; i <= n; i++) {
			rCost[i] = 2 * (l - pos[n - i + 1]);
			if (i - k >= 0) rCost[i] += rCost[i - k];
		}
		long long ans = 1LL << 60;
		for (int i = 0; i <= n; i++) {
			ans = min(ans, lCost[i] + rCost[n - i]);
		}
		for (int i = 0; i + k <= n; i++) {
			ans = min(ans, lCost[i] + l + rCost[n - k - i]);
		}
		cout << ans << '\n';
	}
}
