#include <iostream>
#include <queue>
#include <unordered_set>
using std::cin; using std::cout; using std::queue; using std::unordered_set;

const int move[4][2] = { { -1, 0 }, { 0, -1 }, { 1, 0 }, { 0, 1 } };

struct State { uint64_t v; int dist; };

uint64_t update(uint64_t x, int pos, int val) {
	uint64_t mask = (1 << 4) - 1;
	mask = ~(mask << (pos * 4));
	return (x & mask) | ((uint64_t)val << (pos * 4));
}

int get(uint64_t x, int pos) {
	return (x >> (pos * 4)) & ((1 << 4) - 1);
}

unordered_set<uint64_t> rep;

int main() {
	uint64_t src = 0, dst = 0, x;
	for (int i = 0; i < 9; i++) {
		cin >> x;
		if (!x) src |= (uint64_t)i << (4 * 9);
		src |= x << (i * 4);
	}
	for (int i = 0; i < 9; i++) {
		cin >> x;
		if (!x) dst |= (uint64_t)i << (4 * 9);
		dst |= x << (i * 4);
	}
	State s = State { src, 0 };
	queue<State> q;
	q.push(s);
	int ans = 0;
	while (!q.empty()) {
		State cur = q.front(); q.pop();
		if (cur.v == dst) {
			ans = cur.dist;
			break;
		}
		for (int i = 0; i < 4; i++) {
			int o = get(cur.v, 9);
			int nx = o / 3 + move[i][0], ny = o % 3 + move[i][1];
			if (0 <= nx && nx < 3 && 0 <= ny && ny < 3) {
				State nxt = cur;
				nxt.dist++;
				int no = nx * 3 + ny;
				nxt.v = update(nxt.v, 9, no);
				int a = get(nxt.v, o), b = get(nxt.v, no);
				nxt.v = update(nxt.v, o, b); nxt.v = update(nxt.v, no, a);
				if (!rep.count(nxt.v)) {
					rep.insert(nxt.v);
					q.push(nxt);
				}
			}
		}
	}
	cout << ans << '\n';
}
