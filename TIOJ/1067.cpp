#include <bits/stdc++.h>
using namespace std;

int dp[2][10000] = { 0 };

int main() {
	for (int n, m; cin >> n >> m && n; ) {
		memset(dp, 0, sizeof(dp));
		for (int i = 0, cur; i < n; i++) {
			cin >> cur;
			for (int j = 0; j < m; j++) {
				if (j == 0 || dp[i & 1][j]) {
					int &ans = dp[~i & 1][(j * 10 + cur) % m];
					ans = max(ans, dp[i & 1][j] + 1);
				}
			}
			for (int j = 0; j < m; j++) dp[i & 1][j] = dp[~i & 1][j];
		}
		int ans = 0;
		for (int i = 0; i < m; i++) {
			if (__gcd(i, m) == 1) ans = max(ans, dp[~n & 1][i]);
		}
		cout << ans << '\n';
	}
}
