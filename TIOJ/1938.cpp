#include <bits/stdc++.h>
using namespace std;

int arr[210][210], pref[210][210];

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int m, n, a, b, c, d; cin >> m >> n >> a >> b >> c >> d;
	for (int i = 0; i < n; i++) {
		memset(pref, 0, sizeof(pref));
		for (int j = 1; j <= m; j++) {
			for (int k = 1; k <= m; k++) {
				pref[j][k] = max({ pref[j - 1][k], pref[j][k - 1], arr[j - 1][k - 1] });
			}
		}
		for (int j = 0; j < m; j++) {
			for (int k = 0; k < m; k++) {
				arr[j][k] = max(arr[j][k], pref[j][k] + (a * i + b * j + c * k) % d);
			}
		}
	}
	int ans = 0;
	for (int j = 0; j < m; j++) {
		for (int k = 0; k < m; k++) ans = max(ans, arr[j][k]);
	}
	cout << ans << '\n';
}
