#include <iostream>
#include <algorithm>
using std::cin; using std::cout; using std::sort; using std::swap;

const int MAXN = 100010;

enum Move { UP, RIGHT, DOWN, LEFT };

struct Wall { int to, id; } move[MAXN][4];
struct P {
	int x, y;
	bool operator ==(const P &rhs) const {
		return x == rhs.x && y == rhs.y;
	}
	bool operator <(const P &rhs) const {
		return (x != rhs.x) ? (x < rhs.x) : (y < rhs.y);
	}
} p[MAXN];
struct Edge {
	int from, to, id;
	bool operator <(const Edge &rhs) const {
		return (p[from] == p[rhs.from]) ?
			(p[to] < p[rhs.to]) : (p[from] < p[rhs.from]);
	}
} edge[MAXN * 2];
int collapse[MAXN * 2] = { 0 }, edgeCount[MAXN * 2] = { 0 }, nodeCount[MAXN],
	timer = 0, source;

void flood(int node, int dir) {
	nodeCount[node]++;
	if (nodeCount[source] == 2) return;
	for (int i = 0, nxt = (dir + 3) % 4; i < 4; i++, nxt = (nxt + 1) % 4) {
		int id = move[node][nxt].id;
		if (id && (!collapse[id] || collapse[id] == timer)) {
			flood(move[node][nxt].to, nxt);
			collapse[id] = timer;
			edgeCount[id]++;
			return;
		}
	}
}

int main() {
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(0);
	int n; cin >> n;
	for (int i = 1; i <= n; i++) cin >> p[i].x >> p[i].y;
	int w; cin >> w;
	for (int i = 1, a, b; i <= w; i++) {
		cin >> a >> b;
		if (p[a].x == p[b].x) {
			if (p[a].y > p[b].y) swap(a, b);
			move[a][UP] = Wall { b, i }; move[b][DOWN] = Wall { a, i };
		} else {
			if (p[a].x > p[b].x) swap(a, b);
			move[a][RIGHT] = Wall { b, i }; move[b][LEFT] = Wall { a, i };
		}
		edge[i] = Edge { a, b, i };
	}
	sort(edge + 1, edge + w + 1);
	int ans = 0;
	for (int i = 1; i <= w; i++) {
		if (!collapse[edge[i].id]) {
			source = edge[i].from;
			nodeCount[source] = 0;
			timer++;
			flood(source, UP);
		}
		if (edgeCount[edge[i].id] == 2) ans++;
	}
	cout << ans << '\n';
	for (int i = 1; i <= w; i++) {
		if (edgeCount[i] == 2) cout << i << '\n';
	}
}
