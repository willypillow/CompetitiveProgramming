#include <bits/stdc++.h>
using namespace std;

struct Splay {
	Splay *ch[2], *par;
	int color, acc, lazyColor, lCol, rCol;
	bool isRoot() { return !par || (par->ch[0] != this && par->ch[1] != this); }
	void updateCol(int col) {
		color = lazyColor = lCol = rCol = col;
		acc = 1;
	}
	void pull() {
		lCol = (ch[0] ? ch[0]->lCol : color), rCol = (ch[1] ? ch[1]->rCol : color);
		acc = (ch[0] ? ch[0]->acc : 0) + (ch[1] ? ch[1]->acc : 0) - 1 +
			(ch[0] ? ch[0]->rCol != color : 1) + (ch[1] ? ch[1]->lCol != color : 1);
	}
	void push() {
		if (lazyColor == -1) return;
		if (ch[0]) ch[0]->updateCol(lazyColor);
		if (ch[1]) ch[1]->updateCol(lazyColor);
		lazyColor = -1;
	}
	void pushFromRoot() {
		if (!isRoot()) par->pushFromRoot();
		push();
	}
	void rotate() {
		Splay *p = par, *gp = (p ? p->par : NULL);
		bool d = (p->ch[1] == this);
		par = gp;
		if (!p->isRoot()) gp->ch[gp->ch[1] == p] = this;
		p->ch[d] = ch[!d], ch[!d] = p, p->par = this;
		if (p->ch[d]) p->ch[d]->par = p;
		p->pull(), pull();
	}
	void splay() {
		pushFromRoot();
		while (!isRoot()) {
			if (!par->isRoot()) {
				if ((par->ch[1] == this) == (par->par->ch[1] == par)) rotate();
				else par->rotate();
			}
			rotate();
		}
	}
} node[100001];
vector<int> g[100001];

Splay *access(Splay *x) {
	Splay *last = NULL;
	while (x) {
		x->splay();
		x->ch[1] = last;
		x->pull();
		last = x;
		x = x->par;
	}
	return last;
}

void build(int x, int p) {
	for (vector<int>::iterator it = g[x].begin(); it != g[x].end(); it++) {
		if (*it == p) continue;
		node[*it].par = &node[x];
		node[*it].pull();
		node[*it].lazyColor = -1;
		build(*it, x);
	}
}

int query(Splay *a, Splay *b) {
	access(a);
	Splay *lca = access(b);
	a->splay();
	int ret =
			(lca->ch[1] ? (lca->ch[1]->acc + (lca->color != lca->ch[1]->lCol)) : 1);
	if (a == lca) return ret;
	else return ret + a->acc - (lca->color == a->lCol);
}

void modify(Splay *a, Splay *b, int color) {
	access(a);
	Splay *lca = access(b);
	a->splay();
	lca->color = color;
	if (lca->ch[1]) lca->ch[1]->updateCol(color);
	if (a != lca) a->updateCol(color);
}

int main() {
	int n, m; scanf("%d%d", &n, &m);
	for (int i = 1; i <= n; i++) scanf("%d", &node[i].color);
	for (int i = 1; i < n; i++) {
		int u, v; scanf("%d%d", &u, &v);
		g[u].push_back(v), g[v].push_back(u);
	}
	build(1, 1);
	while (m--) {
		char op; scanf(" %c", &op);
		int a, b; scanf("%d%d", &a, &b);
		if (op == 'C') {
			int c; scanf("%d", &c);
			modify(&node[a], &node[b], c);
		} else {
			printf("%d\n", query(&node[a], &node[b]));
		}
	}
}
