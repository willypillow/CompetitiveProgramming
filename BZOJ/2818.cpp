#include <bits/stdc++.h>
using namespace std;

const int MAXN = 1E7;

vector<int> primes;
int mu[MAXN + 1], n;
bool notPrime[MAXN + 1];

void mobius() {
	primes.reserve(100000);
	mu[1] = 1;
	for (int i = 2; i <= n; i++) {
		if (!notPrime[i]) {
			primes.push_back(i);
			mu[i] = -1;
		}
		for (int j = 0; j < (int)primes.size(); j++) {
			int p = primes[j], x = i * p;
			if (x > n) break;
			notPrime[x] = true;
			mu[x] = (i % p) ? -mu[i] : 0;
		}
	}
}

int main() {
	cin.tie(0); ios_base::sync_with_stdio(0);
	int64_t ans = 0;
	cin >> n;
	mobius();
	for (int i = 0; i < (int)primes.size() && primes[i] <= n; i++) {
		int x = n / primes[i];
		for (int j = 1; j <= x; j++) {
			ans += (int64_t)mu[j] * (x / j) * (x / j);
		}
	}
	cout << ans << '\n';
}
