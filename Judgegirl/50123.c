#include <stdio.h>

int grid[1000][1000], n;

void upperRight(int x, int y, int *newX, int *newY) {
	*newX = (x - 1 + n) % n, *newY = (y + 1) % n;
}

void down(int x, int y, int *newX, int *newY) {
	*newX = (x + 1) % n, *newY = y;
}

void lowerLeft(int x, int y, int *newX, int *newY) {
	*newX = (x + 1) % n, *newY = (y - 1 + n) % n;
}

int main() {
	int k, x, y; scanf("%d%d%d%d", &n, &k, &x, &y);
	for (int i = 1, newX, newY; i < k; i++) {
		lowerLeft(x, y, &newX, &newY);
		x = newX, y = newY;
	}
	grid[x][y] = 1;
	for (int cnt = 2, newX, newY; cnt <= n * n; cnt++) {
		upperRight(x, y, &newX, &newY);
		if (grid[newX][newY]) down(x, y, &newX, &newY);
		grid[x = newX][y = newY] = cnt;
	}
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n - 1; j++) printf("%d ", grid[i][j]);
		printf("%d\n", grid[i][n - 1]);
	}
}
